document.addEventListener("DOMContentLoaded", function () {
  function emit(action, value) {
    parent.postMessage({ event: "BLOKKLI", action, value }, "*");
  }

  if (window.parent === window) {
    return;
  }

  var candidates = [
    ".paragraphs-blokkli-paragraph-form .ck-blurred",
    ".paragraphs-blokkli-paragraph-form input",
  ];
  for (var i = 0; i < candidates.length; i++) {
    var el = document.querySelector(candidates[i]);
    if (el) {
      el.focus();
      break;
    }
  }

  const cancelButton = document.querySelector("#edit-actions-cancel");
  if (cancelButton) {
    cancelButton.addEventListener("click", function (e) {
      e.preventDefault();
      e.stopPropagation();
      emit("CANCEL");
    });
  }

  var _scrollHeight = 0;
  var _dialogWidth = 0;

  function loop() {
    const newHeight = document.body.offsetHeight;

    if (_scrollHeight !== newHeight) {
      emit("HEIGHT", newHeight);
      _scrollHeight = newHeight;
    }
    const hasDialog = !!document.querySelector(".ui-dialog");
    let newDialogWidth = _dialogWidth;

    if (hasDialog) {
      const hasLargeDialog = !!document.querySelector(
        ".paragraphs-library-item-form",
      );
      newDialogWidth = hasLargeDialog ? 1000 : 400;
    } else {
      newDialogWidth = 0;
    }

    if (_dialogWidth !== newDialogWidth) {
      emit("DIALOG_WIDTH", newDialogWidth);
    }

    _dialogWidth = newDialogWidth;
    window.requestAnimationFrame(loop);
  }

  loop();
});
