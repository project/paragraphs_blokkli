(function ($, Drupal) {
  Drupal.behaviors.paragraph_allowed_bundles = {
    attach: function (context, settings) {
      function getRowData(el, identifier) {
        var classes = [...el.classList];

        for (var i = 0; i < classes.length; i++) {
          var className = classes[i];
          var parts = className.split("___");
          if (parts[0] !== "allowed-bundles") {
            continue;
          }

          if (parts[1] === identifier) {
            return parts[2];
          }
        }
      }

      var urlParams = new URLSearchParams(window.location.search);

      var selectedEntityType = urlParams.get("entity_type") || "all";
      var selectedEntityBundle = urlParams.get("entity_bundle") || "all";
      var selectedFieldName = urlParams.get("field_name") || "all";

      function setState() {
        var rows = [
          ...document.querySelectorAll("#edit-paragraph-bundle-table tbody tr"),
        ];

        rows.forEach(function (row) {
          var shouldShow = true;
          var entityType = getRowData(row, "entity_type");
          var entityBundle = getRowData(row, "entity_bundle");
          var fieldName = getRowData(row, "field_name");

          if (
            selectedEntityType !== "all" &&
            selectedEntityType !== entityType
          ) {
            shouldShow = false;
          }

          if (
            selectedEntityBundle !== "all" &&
            selectedEntityBundle !== entityBundle
          ) {
            shouldShow = false;
          }

          if (selectedFieldName !== "all" && selectedFieldName !== fieldName) {
            shouldShow = false;
          }

          if (shouldShow) {
            $(row).show();
          } else {
            $(row).hide();
          }
        });

        var stateUrl = [
          "entity_type=" + selectedEntityType,
          "entity_bundle=" + selectedEntityBundle,
          "field_name=" + selectedFieldName,
        ].join("&");

        var newurl =
          window.location.protocol +
          "//" +
          window.location.host +
          window.location.pathname +
          "?" +
          stateUrl;
        window.history.pushState({ path: newurl }, "", newurl);
      }

      // Set initial state.
      setState();

      function setInitialSelectValue(id, value) {
        var select = document.querySelector("#select-filter___" + id);
        select.value = value;
      }

      setInitialSelectValue("entity_type", selectedEntityType);
      setInitialSelectValue("entity_bundle", selectedEntityBundle);
      setInitialSelectValue("field_name", selectedFieldName);

      $("#edit-paragraph-bundle-table").change(function () {
        var selects = [
          ...document.querySelectorAll("#edit-paragraph-bundle-table select"),
        ];

        selects.forEach(function (el) {
          var filterType = el.id.split("___")[1];
          if (filterType === "entity_type") {
            selectedEntityType = el.value;
          } else if (filterType === "entity_bundle") {
            selectedEntityBundle = el.value;
          } else if (filterType === "field_name") {
            selectedFieldName = el.value;
          }
        });

        setState();
      });
    },
  };
})(jQuery, Drupal);
