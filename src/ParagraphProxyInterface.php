<?php

namespace Drupal\paragraphs_blokkli;

use Drupal\Core\Entity\EntityInterface;
use Drupal\paragraphs\ParagraphInterface;

/**
 * A proxy for a paragraph.
 */
interface ParagraphProxyInterface {

  /**
   * Set a single paragraph behavior setting.
   *
   * @param string $pluginId
   *   The behavior plugin ID.
   * @param string $key
   *   The option key.
   * @param string $value
   *   The option value.
   *
   * @return static
   */
  public function setBehaviorSetting(string $pluginId, string $key, string $value): static;

  /**
   * Set the paragraph behavior settings.
   *
   * @param array $settings
   *   The behavior settings.
   *
   * @return static
   */
  public function setBehaviorSettings(array $settings): static;

  /**
   * Get the behavior settings.
   *
   * @return array
   *   The behavior settings.
   */
  public function getBehaviorSettings(): array;

  /**
   * Get the field key.
   *
   * @return string
   *   The field key.
   */
  public function getFieldListKey(): string;

  /**
   * Get the paragraph UUID.
   *
   * @return string
   *   The paragraph UUID.
   */
  public function uuid(): string;

  /**
   * Set the paragraph entity.
   *
   * @param ParagraphInterface $paragraph
   *   The paragraph entity.
   *
   * @return static
   */
  public function setParagraph(ParagraphInterface $paragraph): static;

  /**
   * Get the paragraph entity.
   *
   * @return \Drupal\paragraphs\Entity\Paragraph|null
   *   The paragraph entity.
   */
  public function getParagraph(): ParagraphInterface|null;

  /**
   * Set the host entity type.
   *
   * @param string $hostEntityType
   *   The host entity type.
   *
   * @return static
   */
  public function setHostEntityType(string $hostEntityType): static;

  /**
   * Set the host UUID.
   *
   * @param string $hostUuid
   *   The host UUID.
   *
   * @return static
   */
  public function setHostUuid(string $hostUuid): static;

  /**
   * Set the host field name.
   *
   * @param string $hostFieldName
   *   The host field name.
   *
   * @return static
   */
  public function setHostFieldName(string $hostFieldName): static;

  /**
   * Get the host entity type.
   *
   * @return string
   *   The host entity type.
   */
  public function getHostEntityType(): string;

  /**
   * Get the host UUID.
   *
   * @return string
   *   The host UUID.
   */
  public function getHostUuid(): string;

  /**
   * Get the host field name.
   *
   * @return string
   *   The host field name.
   */
  public function getHostFieldName(): string;

  /**
   * Get the host entity.
   *
   * @return EntityInterface|null
   *   The host entity.
   */
  public function getHostEntity(): ?EntityInterface;

}
