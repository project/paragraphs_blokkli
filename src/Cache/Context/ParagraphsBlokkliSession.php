<?php

namespace Drupal\paragraphs_blokkli\Cache\Context;

use Drupal\Core\Cache\Context\RequestStackCacheContextBase;
use Drupal\paragraphs_blokkli\BlokkliSession;

/**
 * Defines the cache context for the blokkli session.
 *
 * Varies by the grants for edit state IDs.
 *
 * Cache context ID: 'paragraphs_blokkli_session'.
 */
class ParagraphsBlokkliSession extends RequestStackCacheContextBase {

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('Paragraphs Blokkli Session');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    $session = $this->requestStack->getSession();
    $blokkliSession = BlokkliSession::fromSession($session);
    return $blokkliSession->getCacheContextValue();
  }

}
