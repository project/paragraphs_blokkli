<?php

namespace Drupal\paragraphs_blokkli;

trait ParagraphsBlokkliPluginTrait {

  /**
   * Returns a UUID for mutations that create a new paragraph.
   *
   * The UUID is only generated once and is stored in the plugin's
   * configuration.
   *
   * @param string $existingUuid
   *   The existing UUID for which to get a new UUID.
   *
   * @return string
   *   The UUID.
   */
  public function getUuidForNewEntity(string $existingUuid = 'default'): string {
    $key = 'new_paragraph_uuid_' . $existingUuid;
    if (empty($this->configuration[$key])) {
      $this->configuration[$key] = $this->uuidHelper->generate();
    }

    return $this->configuration[$key];
  }

}
