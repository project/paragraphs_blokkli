<?php

namespace Drupal\paragraphs_blokkli\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines paragraph_mutation annotation object.
 *
 * @Annotation
 */
class ParagraphMutation extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
