<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\paragraphs_blokkli\PbEntityMappingInterface;

/**
 * Defines the pb_entity_mapping entity type.
 *
 * @ConfigEntityType(
 *   id = "pb_entity_mapping",
 *   label = @Translation("blökkli Entity Mapping"),
 *   label_collection = @Translation("pb_entity_mappings"),
 *   label_singular = @Translation("pb_entity_mapping"),
 *   label_plural = @Translation("pb_entity_mappings"),
 *   label_count = @PluralTranslation(
 *     singular = "@count blökkli Entity Mapping",
 *     plural = "@count blökkli Entity Mappings",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\paragraphs_blokkli\PbEntityMappingListBuilder",
 *     "storage" = "Drupal\paragraphs_blokkli\PbEntityMappingStorage",
 *     "form" = {
 *       "add" = "Drupal\paragraphs_blokkli\Form\PbEntityMappingForm",
 *       "edit" = "Drupal\paragraphs_blokkli\Form\PbEntityMappingForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   config_prefix = "pb_entity_mapping",
 *   admin_permission = "administer pb_entity_mapping",
 *   links = {
 *     "collection" = "/admin/config/content/blokkli/entity-mapping",
 *     "add-form" = "/admin/config/content/blokkli/entity-mapping/add",
 *     "edit-form" = "/admin/config/content/blokkli/entity-mapping/{pb_entity_mapping}",
 *     "delete-form" = "/admin/config/content/blokkli/entity-mapping/{pb_entity_mapping}/delete",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "target_entity_type",
 *     "target_entity_bundle",
 *     "paragraph_bundle",
 *     "paragraph_field",
 *   },
 * )
 */
final class PbEntityMapping extends ConfigEntityBase implements PbEntityMappingInterface {

  /**
   * The ID.
   */
  protected string $id;

  /**
   * The label.
   */
  protected string $label;

  /**
   * The target entity type.
   */
  protected string|null $target_entity_type = NULL;

  /**
   * The target entity bundle.
   */
  protected string|null $target_entity_bundle = NULL;

  /**
   * The paragraph bundle.
   */
  protected string|null $paragraph_bundle = NULL;

  /**
   * The paragraph field.
   */
  protected string|null $paragraph_field = NULL;

  /**
   * Set the automatic ID.
   */
  private function setId() {
    $this->id = implode('__', [
      $this->target_entity_type,
      $this->target_entity_bundle,
    ]);
  }

  /**
   * Set the automatic label.
   */
  private function setLabel() {
    $this->label = implode(' - ', [
      $this->target_entity_type,
      $this->target_entity_bundle,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    // Set the automatic ID and label.
    $this->setId();
    $this->setLabel();
    return parent::save();
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetEntityType(): string|null {
    return $this->target_entity_type;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetEntityBundle(): string|null {
    return $this->target_entity_bundle;
  }

  /**
   * {@inheritdoc}
   */
  public function getParagraphBundle(): string|null {
    return $this->paragraph_bundle;
  }

  /**
   * {@inheritdoc}
   */
  public function getParagraphField(): string|null {
    return $this->paragraph_field;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    parent::calculateDependencies();

    // Helper method to add a entity bundle dependency.
    $addBundleDependency = function (string $entityType, string $bundle) {
      $bundleConfigDependency = $this->entityTypeManager()
        ->getDefinition($entityType)
        ->getBundleConfigDependency($bundle);
      $this->addDependency($bundleConfigDependency['type'], $bundleConfigDependency['name']);
    };

    $targetEntityType = $this->getTargetEntityType();

    if ($targetEntityType) {
      $entityType = $this->entityTypeManager()->getStorage($targetEntityType)->getEntityType();
      $this->addDependency('module', $entityType->getProvider());
    }

    $targetEntityBundle = $this->getTargetEntityBundle();

    if ($targetEntityBundle) {
      $addBundleDependency($targetEntityType, $targetEntityBundle);
    }

    $this->addDependency('module', 'paragraphs');

    $paragraphBundle = $this->getParagraphBundle();

    if ($paragraphBundle) {
      $addBundleDependency('paragraph', $paragraphBundle);
    }

    $paragraphField = $this->getParagraphField();

    if ($paragraphField) {
      /** @var \Drupal\Core\Entity\EntityFieldManager $entityFieldManager */
      $entityFieldManager = \Drupal::service('entity_field.manager');

      $definitions = $entityFieldManager->getFieldDefinitions('paragraph', $paragraphBundle);
      foreach ($definitions as $fieldName => $definition) {
        if ($fieldName === $paragraphField && $definition instanceof ConfigEntityInterface) {
          $this->addDependency('config', $definition->getConfigDependencyName());
        }
      }
    }

    return $this;
  }

}
