<?php

namespace Drupal\paragraphs_blokkli\Entity;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\RevisionableStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList;
use Drupal\paragraphs_blokkli\Exception\MutationException;
use Drupal\paragraphs_blokkli\ParagraphMutationContext;
use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationError;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginBase;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliConfig;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliEditStateInterface;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliHelper;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliMutatedField;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliMutatedState;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the paragraphs blokkli edit state entity class.
 *
 * @ContentEntityType(
 *   id = "paragraphs_blokkli_edit_state",
 *   label = @Translation("Paragraphs Blokkli Edit State"),
 *   label_collection = @Translation("Paragraphs Blokkli Edit States"),
 *   label_singular = @Translation("paragraphs blokkli edit state"),
 *   label_plural = @Translation("paragraphs blokkli edit states"),
 *   label_count = @PluralTranslation(
 *     singular = "@count paragraphs blokkli edit states",
 *     plural = "@count paragraphs blokkli edit states",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\paragraphs_blokkli\ParagraphsBlokkliEditStateListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "storage" = "Drupal\paragraphs_blokkli\ParagraphsBlokkliEditStateStorage",
 *     "access" = "Drupal\paragraphs_blokkli\ParagraphsBlokkliEditStateAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\paragraphs_blokkli\Form\ParagraphsBlokkliEditStateForm",
 *       "edit" = "Drupal\paragraphs_blokkli\Form\ParagraphsBlokkliEditStateForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\paragraphs_blokkli\Routing\ParagraphsBlokkliEditStateHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "paragraphs_blokkli_edit_state",
 *   admin_permission = "administer paragraphs blokkli edit state",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/paragraphs-blokkli-edit-state",
 *     "add-form" = "/paragraphs-blokkli-edit-state/add",
 *     "canonical" = "/paragraphs-blokkli-edit-state/{paragraphs_blokkli_edit_state}",
 *     "edit-form" = "/paragraphs-blokkli-edit-state/{paragraphs_blokkli_edit_state}",
 *     "delete-form" = "/paragraphs-blokkli-edit-state/{paragraphs_blokkli_edit_state}/delete",
 *   },
 *   field_ui_base_route = "entity.paragraphs_blokkli_edit_state.settings",
 * )
 */
class ParagraphsBlokkliEditState extends ContentEntityBase implements ParagraphsBlokkliEditStateInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * The mutated state.
   */
  protected ParagraphsBlokkliMutatedState|null $mutatedState = NULL;

  /**
   * The loaded host entity.
   */
  protected FieldableEntityInterface|null $hostEntity = NULL;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(static::class . '::getDefaultEntityOwner')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the paragraphs blokkli edit state was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the paragraphs blokkli edit state was last edited.'));

    $fields['current_index'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Current Index'))
      ->setDescription(t('The current position in the mutation history.'))
      ->setDefaultValue(-1);

    $fields['mutations'] = BaseFieldDefinition::create('paragraphs_blokkli_mutation')
      ->setLabel(t('Mutations'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDescription(t('The mutations applied.'));

    $fields['host_entity_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Host Entity Type'))
      ->setRequired(TRUE)
      ->setDescription(t('The host entity type.'));

    $fields['host_entity_uuid'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Host Entity UUID'))
      ->setRequired(TRUE)
      ->setDescription(t('The host entity UUID.'));

    $fields['preview_hash'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Preview Hash'))
      ->setRequired(TRUE)
      ->setTranslatable(FALSE)
      ->setDefaultValueCallback(static::class . '::generatePreviewHash')
      ->setDescription(t('The hash for generating preview links.'));

    return $fields;
  }

  /**
   * Generate a preview hash.
   *
   * @return string
   *   The preview hash
   */
  public static function generatePreviewHash(): string {
    return Crypt::randomBytesBase64(48);
  }

  /**
   * Gets the paragraphs blokkli helper.
   *
   * @return \Drupal\paragraphs_blokkli\ParagraphsBlokkliHelper
   *   The helper.
   */
  protected function getHelper(): ParagraphsBlokkliHelper {
    return \Drupal::service('paragraphs_blokkli.helper');
  }

  /**
   * Gets the blokkli config helper.
   *
   * @return \Drupal\paragraphs_blokkli\ParagraphsBlokkliConfig
   *   The config.
   */
  protected function getBlokkliConfig(): ParagraphsBlokkliConfig {
    return \Drupal::service('paragraphs_blokkli.config');
  }

  /**
   * {@inheritdoc}
   */
  public function getHostEntityType(): string {
    return $this->get('host_entity_type')->first()?->getString();
  }

  /**
   * {@inheritdoc}
   */
  public function getHostEntityUuid(): string {
    return $this->get('host_entity_uuid')->first()?->getString();
  }

  /**
   * {@inheritdoc}
   */
  public function isPreviewHashValid(string $hash): bool {
    return hash_equals($this->get('preview_hash')->getString(), $hash);
  }

  /**
   * {@inheritdoc}
   */
  public function getHostEntity($force_reload = FALSE): FieldableEntityInterface|null {
    if (!$this->hostEntity) {
      $storage = \Drupal::entityTypeManager()->getStorage($this->getHostEntityType());
      /** @var \Drupal\Core\Entity\FieldableEntityInterface|null $entity */
      $entity = array_values($storage->loadByProperties([
        'uuid' => $this->getHostEntityUuid(),
      ]))[0] ?? NULL;

      if (!$entity) {
        return NULL;
      }

      // Load the latest revision of the entity if its revisionable.
      if ($entity instanceof RevisionableInterface) {
        if ($storage instanceof RevisionableStorageInterface) {
          $latestRevision = $storage->getLatestRevisionId($entity->id());
          if ($latestRevision) {
            $entity = $storage->loadRevision($latestRevision);
          }
        }
      }
      $this->hostEntity = $entity;
    }

    // In order to simulate the correct behavior of publishing in tests, we have to ignore the cached entity.
    if ($force_reload && $this->hostEntity) {
      $this->hostEntity = \Drupal::entityTypeManager()->getStorage($this->getHostEntityType())->loadUnchanged($this->hostEntity->id());
    }

    return $this->hostEntity;
  }

  /**
   * Get the key for a field item list.
   *
   * @param \Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList $field
   *   The field.
   *
   * @return string
   *   The key.
   */
  private function getFieldKey(EntityReferenceRevisionsFieldItemList $field): string {
    return implode(':', [
      $field->getEntity()->getEntityTypeId(),
      $field->getEntity()->uuid(),
      $field->getName(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getMutatedState(bool $forSaving = FALSE, int|null $historyIndex = NULL): ParagraphsBlokkliMutatedState {
    // We cache the mutated state here in case it's being called multiple times.
    if ($this->mutatedState && !$forSaving) {
      return $this->mutatedState;
    }

    $entity = $this->getHostEntity();
    if (!$entity) {
      throw new \Exception('Host entity not found.');
    }

    /** @var \Drupal\paragraphs_blokkli\ParagraphProxy[] $proxies */
    $proxies = $this->getHelper()->buildProxies($entity);
    $mutationContext = $this->executeMutations($proxies, $historyIndex);

    // Collect all behavior settings, keyed by paragraph UUID.
    $behaviorSettingsMap = [];

    // Create a map of all proxies by their field key.
    $proxiesByFieldKey = [];

    // The mutated fields.
    /** @var \Drupal\paragraphs_blokkli\ParagraphsBlokkliMutatedField[] $mutatedFields */
    $mutatedFields = [];

    foreach ($mutationContext->proxies as $proxy) {
      $uuid = $proxy->uuid();
      $isDeleted = $mutationContext->isDeleted($uuid);
      $paragraph = $proxy->getParagraph();

      // Update the behavior settings.
      $currentSettings = $paragraph->getAllBehaviorSettings();
      $overridenSettings = $proxy->getBehaviorSettings();
      $mergedSettings = array_merge($currentSettings, $overridenSettings);
      $behaviorSettingsMap[$uuid] = $mergedSettings ?: new \stdClass();

      // When actually saving, change the parent entity on the paragraphs.
      if ($forSaving && !$isDeleted) {
        $paragraph->setAllBehaviorSettings($mergedSettings);
        $hostEntity = $proxy->getHostEntity();

        // The host entity might not exist at this point because it is still new.
        // This is the case for a nested paragraphs which has another
        // paragraph as the parent.
        if (!$hostEntity && $proxy->getHostEntityType() === 'paragraph') {
          // Get the parent paragraph via the proxy. It is ok if getProxy() returns NULL.
          $hostEntity = $mutationContext->getProxy($proxy->getHostUuid())?->getParagraph();
        }

        // We should have a host entity at this point.
        if ($hostEntity) {
          $paragraph->setParentEntity($hostEntity, $proxy->getHostFieldName());
        }
      }

      $fieldListKey = $proxy->getFieldListKey();
      $proxiesByFieldKey[$fieldListKey][] = $proxy;
      if (!$isDeleted) {
        if (empty($mutatedFields[$fieldListKey])) {
          $mutatedFields[$fieldListKey] = new ParagraphsBlokkliMutatedField($proxy->getHostEntityType(), $proxy->getHostUuid(), $proxy->getHostFieldName());
        }
        $mutatedFields[$fieldListKey]->appendParagraph($paragraph);
      }
    }

    $violations = $this->validate();

    if ($forSaving) {
      $this->rebuildFields($entity, $mutationContext, $proxiesByFieldKey);
    }
    else {
      $fieldNames = $this->getHelper()->getAvailableHostFieldNames($entity);
      foreach ($fieldNames as $fieldName) {
        // We have to empty the paragraph fields on the host entity, since they
        // are not actually used during editing.
        $entity->get($fieldName)->setValue([]);
      }
    }

    $mutatedState = new ParagraphsBlokkliMutatedState(
      $mutatedFields,
      $behaviorSettingsMap,
      iterator_to_array($violations),
      $mutationContext->getErrors(),
    );
    $this->mutatedState = $mutatedState;
    return $mutatedState;
  }

  /**
   * Rebuild the field structure for saving.
   */
  protected function rebuildFields(FieldableEntityInterface $entity, ParagraphMutationContextInterface $mutationContext, array $proxiesByFieldKey, int $level = 0) {
    $fieldNames = $this->getHelper()->getAvailableHostFieldNames($entity);
    // Loop over the host field names and rebuild the field item list.
    foreach ($fieldNames as $fieldName) {
      /** @var \Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList $field */
      $field = $entity->get($fieldName);
      $fieldKey = $this->getFieldKey($field);

      // Get all proxies whose paragraph should be added to this field.
      /** @var \Drupal\paragraphs_blokkli\ParagraphProxy[] $fieldProxies */
      $fieldProxies = $proxiesByFieldKey[$fieldKey] ?? [];

      // First, empty the list.
      $field->setValue([], TRUE);

      foreach ($fieldProxies as $proxy) {
        // Skip paragraphs that will be deleted.
        if ($mutationContext->isDeleted($proxy->uuid())) {
          continue;
        }

        $paragraph = $proxy->getParagraph();
        $paragraph->setNeedsSave(TRUE);
        if ($level < 4) {
          $this->rebuildFields($paragraph, $mutationContext, $proxiesByFieldKey);
        }

        // Finally append the paragraph to the list.
        $field->appendItem([
          'entity' => $paragraph,
        ]);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function executeMutations(array $proxies, int|null $historyIndex = NULL): ParagraphMutationContext {
    $mutationContext = new ParagraphMutationContext($this->getHostEntity(), $proxies);
    $currentIndex = $historyIndex ?? $this->getCurrentIndex();
    $mutationItems = $this->getMutations();

    foreach ($mutationItems as $index => $item) {
      // Only execute mutations up to the current history index.
      if ($index > $currentIndex) {
        break;
      }

      $mutation = $item->getMutationPlugin();

      // Skip mutation items that are not enabled.
      if (!$item->isEnabled()) {
        continue;
      }

      try {
        $mutation->executeMutation($mutationContext);
      }
      catch (MutationException $e) {
        $error = new ParagraphMutationError($index, $mutation, $e);
        $mutationContext->addError($error);
      }
    }

    return $mutationContext;
  }

  /**
   * {@inheritdoc}
   */
  public function addMutation(ParagraphMutationPluginBase $mutation): static {
    $mutations = $this->get('mutations');
    // Reset previously created mutation state.
    $this->mutatedState = NULL;
    $currentIndex = $this->getCurrentIndex();

    // Check if the current index is at the latest position.
    if ($this->getCurrentIndex() !== $mutations->count() - 1) {
      // Undo was previously done, so remove all mutations up to the current index.
      for ($i = $mutations->count() - 1; $i > $currentIndex; $i--) {
        $mutations->removeItem($i);
      }
    }

    // Append the new mutation to the list.
    /** @var \Drupal\paragraphs_blokkli\Plugin\Field\FieldType\ParagraphsBlokkliMutationItem $item */
    $item = $mutations->appendItem([
      'plugin_id' => $mutation->getPluginId(),
      'timestamp' => time(),
      'enabled' => TRUE,
      'configuration' => $mutation->getConfiguration(),
    ]);

    $newCurrentIndex = $mutations->count() - 1;

    // Update the current index.
    $this->set('current_index', $newCurrentIndex);

    // Execute all mutations, including the new one.
    $mutatedState = $this->getMutatedState();

    // Check whether the mutation we just added has thrown an error.
    $newMutationError = $mutatedState->getErrorForIndex($newCurrentIndex);

    // If the new mutation failed, we rethrow the exception. That way we can
    // prevent saving mutations that have thrown an exception, because
    // it's very likely not resolvable.
    // We don't want to throw an exception here if a *previous* mutation has
    // failed, because this might be the case when the underlying state has
    // changed, for example when there is a copy mutation that references a
    // paragraph that has since been deleted. In this case it might be possible
    // to manually recover the state.
    if ($newMutationError) {
      throw $newMutationError->exception;
    }

    // Fetch the plugin instance from the field item. The plugin might have
    // updated its configuration, so we also have to update it on the field item.
    /** @var \Drupal\paragraphs_blokkli\ParagraphMutationPluginBase $alteredPlugin */
    $alteredPlugin = $item->getMutationPlugin();
    $item->set('configuration', $alteredPlugin->getConfiguration());

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentIndex(): int {
    return $this->get('current_index')->first()->getValue()['value'];
  }

  /**
   * {@inheritdoc}
   */
  public function getMutations(): array {
    return iterator_to_array($this->get('mutations'));
  }

  /**
   * {@inheritdoc}
   */
  public function undo(): static {
    $currentIndex = $this->getCurrentIndex();

    // Undo is only possible if there's at least one mutation present.
    if ($currentIndex >= 0) {
      $this->mutatedState = NULL;
      $this->set('current_index', $currentIndex - 1);
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function redo(): static {
    $currentIndex = $this->getCurrentIndex();

    // Redo is only possible if the index is lower.
    if ($currentIndex < $this->get('mutations')->count() - 1) {
      $this->mutatedState = NULL;
      $this->set('current_index', $currentIndex + 1);
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setHistoryIndex(int $index): static {
    $totalMutations = $this->get('mutations')->count();
    if ($index === -1 || ($index < $totalMutations && $index >= -1)) {
      $this->mutatedState = NULL;
      $this->set('current_index', $index);
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setMutationStatus(int $index, bool $status): static {
    $field = $this->get('mutations');
    /** @var \Drupal\paragraphs_blokkli\Plugin\Field\FieldType\ParagraphsBlokkliMutationItem $item */
    $item = $field->get($index);
    $item->set('enabled', $status);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    $config = $this->getBlokkliConfig();
    $violations = parent::validate();

    // No validation should be performed.
    if (!$config->shouldValidate()) {
      return $violations;
    }

    $entity = $this->getHostEntity();

    // Validate host entity.
    if ($config->shouldValidateType('host')) {
      $violations->addAll($entity->validate());
    }

    $shouldValidateFields = $config->shouldValidateType('field');
    $shouldValidateParagraphs = $config->shouldValidateType('paragraph');

    if ($shouldValidateFields || $shouldValidateParagraphs) {
      $fieldNames = $this->getHelper()->getAvailableHostFieldNames($entity);

      foreach ($fieldNames as $fieldName) {
        /** @var \Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList $field */
        $field = $entity->get($fieldName);

        // Validate the field itself.
        if ($shouldValidateFields) {
          $fieldValidations = $field->validate();
          $violations->addAll($fieldValidations);
        }

        // Validate the referenced paragraphs.
        if ($shouldValidateParagraphs) {
          /** @var \Drupal\paragraphs\Entity\Paragraph[] $paragraphs */
          $paragraphs = $field->referencedEntities();

          if ($config->shouldValidateType('paragraph')) {
            foreach ($paragraphs as $paragraph) {
              $paragraphValidations = $paragraph->validate();
              $violations->addAll($paragraphValidations);
            }
          }
        }
      }
    }

    return $violations;
  }

  /**
   * Get the host entity cache tags.
   *
   * @param string $uuid
   *   The host UUID.
   *
   * @return string[]
   *   The cache tags.
   */
  public static function getBlokkliHostCacheTags(string $uuid): array {
    return ['paragraphs_blokkli_host:' . $uuid];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return array_merge(
      parent::getCacheTags(),
      $this->getBlokkliHostCacheTags($this->getHostEntityUuid())
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTagsToInvalidate() {
    return array_merge(
      parent::getCacheTagsToInvalidate(),
      $this->getBlokkliHostCacheTags($this->getHostEntityUuid())
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __sleep(): array {
    $this->mutatedState = NULL;
    return parent::__sleep();
  }

}
