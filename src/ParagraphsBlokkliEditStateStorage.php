<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Paragraphs Blokkli Edit State storage.
 */
class ParagraphsBlokkliEditStateStorage extends SqlContentEntityStorage implements ParagraphsBlokkliEditStateStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadForEntity(FieldableEntityInterface $entity): ParagraphsBlokkliEditStateInterface|null {
    return $this->loadForEntityTypeAndUuid(
      $entity->getEntityTypeId(),
      $entity->uuid()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function loadForEntityTypeAndUuid(string $entityType, string $uuid): ?ParagraphsBlokkliEditStateInterface {
    $result = $this->getQuery()
      ->condition('host_entity_type', $entityType)
      ->condition('host_entity_uuid', $uuid)
      ->accessCheck(FALSE)
      ->execute();
    $id = array_values($result)[0] ?? NULL;

    if ($id) {
      return $this->load($id);
    }
    return NULL;
  }

}
