<?php

namespace Drupal\paragraphs_blokkli;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityInterface;

/**
 * Config helper.
 */
class ParagraphsBlokkliConfig implements CacheableDependencyInterface {

  /**
   * Config key for the schema file path.
   */
  public const SCHEMA_FILE = 'schema_file';

  /**
   * Config key for the "remote video" media bundle.
   */
  public const MEDIA_BUNDLE_REMOTE_VIDEO = 'media_bundle_remote_video';

  /**
   * Config key for the "image" media bundle.
   */
  public const MEDIA_BUNDLE_IMAGE = 'media_bundle_image';

  /**
   * Config key for the "file" media bundle.
   */
  public const MEDIA_BUNDLE_FILE = 'media_bundle_file';

  /**
   * Config key for the text clipboard paragraph settings.
   */
  public const CLIPBOARD_TEXT_PARAGRAPH = 'clipboard_text_paragraph';

  /**
   * Config key for the enabled entity types and bundles.
   */
  public const ENABLED_TYPES = 'enabled_types';

  /**
   * Config key for the image style.
   */
  public const IMAGE_STYLE = 'image_style';

  /**
   * Config key for enabled validation types.
   */
  public const VALIDATION_TYPES = 'validation_types';

  /**
   * The config.
   */
  protected ImmutableConfig $config;

  /**
   * Constructs a new ParagraphsBlokkliConfig.
   *
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
  ) {
    $this->config = $configFactory->get('paragraphs_blokkli.settings');
  }

  /**
   * Get the media bundle to use for creating a remote video.
   *
   * @return string|null
   *   The media bundle ID.
   */
  public function getMediaBundleRemoteVideo(): string|null {
    return $this->config->get(self::MEDIA_BUNDLE_REMOTE_VIDEO);
  }

  /**
   * Get the media bundle to use for creating an image.
   *
   * @return string|null
   *   The media bundle ID.
   */
  public function getMediaBundleImage(): string|null {
    return $this->config->get(self::MEDIA_BUNDLE_IMAGE);
  }

  /**
   * Get the media bundle to use for creating a file.
   *
   * @return string|null
   *   The media bundle ID.
   */
  public function getMediaBundleFile(): string|null {
    return $this->config->get(self::MEDIA_BUNDLE_FILE);
  }

  /**
   * Get the enabled entity types and bundles.
   *
   * @return array<string, string[]>
   *   The enabled entity types and bundles.
   */
  public function getEnabledEntityTypes(): array {
    return $this->config->get(self::ENABLED_TYPES) ?? [];
  }

  /**
   * Check if blökkli is enabled for the given entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return bool
   *   Whether blökkli is enabled for the entity.
   */
  public function isEnabledForEntity(EntityInterface $entity): bool {
    $enabled = $this->getEnabledEntityTypes();
    $entityTypeId = $entity->getEntityTypeId();
    $enabledBundles = $enabled[$entityTypeId] ?? NULL;

    if ($entityTypeId === 'paragraphs_library_item') {
      return TRUE;
    }

    return is_array($enabledBundles) && in_array($entity->bundle(), $enabledBundles);
  }

  /**
   * Get the clipboard text paragraph settings.
   *
   * @return array{ 'bundle': string, 'field': string }|null
   *   The clipboard text paragraph settings.
   */
  public function getClipboardTextParagraph(): array|null {
    $setting = $this->config->get(self::CLIPBOARD_TEXT_PARAGRAPH);

    if (!empty($setting['bundle']) && !empty($setting['field'])) {
      return [
        'bundle' => $setting['bundle'],
        'field' => $setting['field'],
      ];
    }

    return NULL;
  }

  /**
   * Get the image style for the media library and content search.
   *
   * @return string|null
   *   The image style.
   */
  public function getImageStyle(): string|null {
    return $this->config->get(self::IMAGE_STYLE);
  }

  /**
   * Check whether the given validation should be performed.
   *
   * @param string $type
   *   The validation type.
   *
   * @return bool
   *   TRUE if the validation should be performed.
   */
  public function shouldValidateType(string $type): bool {
    $validations = $this->config->get(self::VALIDATION_TYPES) ?? [];
    return is_array($validations) && in_array($type, $validations);
  }

  /**
   * Check whether validations are enabled generally.
   *
   * @return bool
   *   TRUE if any validation type is enabled.
   */
  public function shouldValidate(): bool {
    $validations = $this->config->get(self::VALIDATION_TYPES) ?? [];
    return !empty($validations);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return $this->config->getCacheContexts();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return $this->config->getCacheTags();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return $this->config->getCacheMaxAge();
  }

}
