<?php

namespace Drupal\paragraphs_blokkli;

class ParagraphsBlokkliMutatedState {

  /**
   * Constructs a new ParagraphsBlokkliMutatedState.
   *
   * @param ParagraphsBlokkliMutatedField[] $fields
   *   The mutated fields.
   * @param array $behaviorSettings
   *   The mutated behavior settings, keyed by paragraph UUID.
   * @param \Symfony\Component\Validator\ConstraintViolationInterface[] $violations
   *   The violations.
   * @param \Drupal\paragraphs_blokkli\ParagraphMutationError[] $errors
   *   The mutation errors.
   */
  public function __construct(
    protected array $fields,
    protected array $behaviorSettings,
    protected array $violations,
    protected array $errors,
  ) {
  }

  /**
   * Get the mutated field item lists.
   *
   * @return ParagraphsBlokkliMutatedField[]
   *   The mutated field item lists.
   */
  public function getFields(): array {
    return $this->fields;
  }

  /**
   * Get the behavior settings.
   *
   * @return array
   *   The behavior settings keyed by paragraph UUID.
   */
  public function getBehaviorSettings(): array {
    return $this->behaviorSettings;
  }

  /**
   * Get the violations.
   *
   * @return \Symfony\Component\Validator\ConstraintViolationInterface[]
   *   The violations.
   */
  public function getViolations(): array {
    return $this->violations;
  }

  /**
   * Get the errors.
   *
   * @return \Drupal\paragraphs_blokkli\ParagraphMutationError[]
   *   The errors.
   */
  public function getErrors(): array {
    return $this->errors;
  }

  /**
   * Whether there are any errors.
   *
   * @return bool
   *   Whether there are any errors.
   */
  public function hasErrors(): bool {
    return !empty($this->errors);
  }

  /**
   * Find an error for a specific mutation index.
   *
   * @param int $index
   *   The mutation index.
   *
   * @return \Drupal\paragraphs_blokkli\ParagraphMutationError|null
   *   The mutation error.
   */
  public function getErrorForIndex(int $index): ParagraphMutationError|null {
    foreach ($this->errors as $error) {
      if ($error->index === $index) {
        return $error;
      }
    }

    return NULL;
  }

}
