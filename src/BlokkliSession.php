<?php

namespace Drupal\paragraphs_blokkli;

use Drupal\Core\Entity\EntityInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Wrapper class for the blokkli session.
 */
final class BlokkliSession {

  /**
   * The key of the session.
   */
  const SESSION_KEY = 'paragraphs_blokkli';

  /**
   * The array key for the preview grants.
   */
  const PREVIEW_GRANTS = 'preview_grants';

  /**
   * The array key for the preview entities.
   */
  const PREVIEW_ENTITIES = 'preview_entities';

  /**
   * Create a new BlokkliSession instance.
   *
   * @param array $previewGrants
   *   The preview grants.
   * @param array $previewEntities
   *   The additional entities for which preview is possible.
   */
  public function __construct(
    protected array $previewGrants,
    protected array $previewEntities,
  ) {

  }

  /**
   * Create a new instance from a session.
   *
   * @param \Symfony\Component\HttpFoundation\Session\SessionInterface $session
   *   The session.
   *
   * @return static
   */
  public static function fromSession(SessionInterface $session) {
    $pbSession = $session->get(self::SESSION_KEY, []);
    $previewGrants = $pbSession[self::PREVIEW_GRANTS] ?? [];
    $previewEntities = $pbSession[self::PREVIEW_ENTITIES] ?? [];
    return new static($previewGrants, $previewEntities);
  }

  /**
   * Add a preview grant for the given edit state.
   *
   * @param \Drupal\paragraphs_blokkli\Entity\ParagraphsBlokkliEditState $editState
   *   The edit state.
   *
   * @return static
   */
  public function addPreviewGrant(EditStateInterface $editState): static {
    $id = $editState->id();
    $hostUuid = $editState->getHostEntityUuid();
    $this->previewGrants[$id] = $hostUuid;
    return $this;
  }

  /**
   * Whether any preview grants exist.
   *
   * @return bool
   *   Any preview grants exists.
   */
  public function hasAnyPreviewGrant(): bool {
    return !empty($this->previewGrants);
  }

  /**
   * Check whether a preview grant exists for the given edit state.
   *
   * @param \Drupal\paragraphs_blokkli\Entity\ParagraphsBlokkliEditState $editState
   *   The edit state.
   *
   * @return bool
   *   A preview grant exists.
   */
  public function hasPreviewGrantForState(EditStateInterface $editState): bool {
    $id = $editState->id();
    if (!$id) {
      return FALSE;
    }

    $grantedHostUuid = $this->previewGrants[$id] ?? NULL;

    if (!$grantedHostUuid) {
      return FALSE;
    }

    $editStateHostUuid = $editState->getHostEntityUuid();

    return $grantedHostUuid === $editStateHostUuid;
  }

  /**
   * Add an entity that can be previewed.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return static
   */
  public function addPreviewEntity(EntityInterface $entity): static {
    $entityType = $entity->getEntityTypeId();
    $uuid = $entity->uuid();
    if (!isset($this->previewEntities[$entityType])) {
      $this->previewEntities[$entityType] = [];
    }
    $this->previewEntities[$entityType][] = $uuid;
    return $this;
  }

  /**
   * Apply to session.
   *
   * @param \Symfony\Component\HttpFoundation\Session\SessionInterface $session
   *   The session.
   *
   * @return static
   */
  public function applyToSession(SessionInterface $session): static {
    $session->set(self::SESSION_KEY, [
      self::PREVIEW_GRANTS => $this->previewGrants,
      self::PREVIEW_ENTITIES => $this->previewEntities,
    ]);

    return $this;
  }

  /**
   * Get the cache context value.
   *
   * @return string
   *   The cache context value.
   */
  public function getCacheContextValue(): string {
    $ids = array_keys($this->previewGrants);
    return implode(':', $ids);
  }

}
