<?php

namespace Drupal\paragraphs_blokkli;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Helper for blokkli options schema.
 */
class BlokkliOptionsSchemaHelper {

  /**
   * The cached schema.
   */
  protected array $schema;

  /**
   * The constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
  ) {
  }

  /**
   * Get the schema.
   *
   * @return array
   *   The options schema.
   */
  protected function loadSchema(): array {
    if (!isset($this->schema)) {
      $filePath = $this->configFactory
        ->get('paragraphs_blokkli.settings')
        ->get('schema_file');

      if (file_exists($filePath)) {
        $json = file_get_contents($filePath);
        $this->schema = json_decode($json, TRUE);
      }
      else {
        $this->schema = [];
      }
    }

    return $this->schema;
  }

  /**
   * Get the options for a bundle.
   *
   * @param string $bundle
   *   The bundle.
   *
   * @return array
   *   The options.
   */
  public function getOptionsForBundle(string $bundle): array {
    return $this->loadSchema()[$bundle] ?? [];
  }

  /**
   * Get the value for a checkboxes item.
   *
   * @param mixed $value
   *   The value.
   *
   * @return string[]
   *   The value as an array of strings.
   */
  public function getCheckboxesValue($value) {
    if (is_array($value)) {
      return $value;
    }
    elseif (is_string($value)) {
      return explode(',', $value);
    }

    return [];
  }

  /**
   * Get the persistable value.
   *
   * Internally, all option values are stored as strings, including arrays.
   *
   * @param string $type
   *   The option type.
   * @param mixed $value
   *   The value.
   *
   * @return string
   *   The value to store.
   */
  public function getPersistableValue(string $type, $value): string {
    if ($type === 'checkboxes') {
      // Checkboxes value is stored as a comma separated string.
      $asArray = $this->getCheckboxesValue($value);
      return implode(',', $asArray);
    }
    elseif ($type === 'checkbox') {
      // Checkbox values are stored as 1 or 0.
      return $type ? '1' : '0';
    }

    if ($value === NULL || is_array($value)) {
      return '';
    }

    return (string) $value;
  }

}
