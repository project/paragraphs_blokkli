<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli\Controller;

use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\paragraphs_blokkli\Entity\ParagraphsBlokkliEditState;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliEditStateStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Returns the last changed date of a paragraphs blokkli edit state entity.
 *
 * This is used by preview views to check if they need to reload the edit state.
 */
class LastChangedController extends ControllerBase {

  /**
   * The paragraphs_blokkli_edit_state storage.
   */
  protected ParagraphsBlokkliEditStateStorageInterface $editStorage;

  /**
   * LastChangedController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->editStorage = $entityTypeManager->getStorage('paragraphs_blokkli_edit_state');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Renders and returns the JSON response.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The response.
   */
  public function content(string $entity_type, string $entity_uuid): JsonResponse {
    $response = new CacheableJsonResponse([]);

    $editState = $this->editStorage->loadForEntityTypeAndUuid($entity_type, $entity_uuid);

    if ($editState) {
      $response->addCacheableDependency($editState);
      $entity = $editState->getHostEntity();
      if ($entity) {
        $response->addCacheableDependency($entity);
        $access = $editState->access('view', NULL, TRUE);
        $response->addCacheableDependency($access);
        if ($access->isAllowed()) {
          $response->setData([
            'changed' => (int) $editState->getChangedTime(),
          ]);
          return $response;
        }
      }
    }

    $response
      ->getCacheableMetadata()
      ->addCacheTags(ParagraphsBlokkliEditState::getBlokkliHostCacheTags($entity_uuid));
    return $response;
  }

}
