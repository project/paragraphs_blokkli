<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Url;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliEditStateStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Controller to redirect to the entity form.
 */
class EntityFormController extends ControllerBase {

  /**
   * The paragraphs_blokkli_edit_state storage.
   */
  protected ParagraphsBlokkliEditStateStorageInterface $editStorage;

  /**
   * LastChangedController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->editStorage = $entityTypeManager->getStorage('paragraphs_blokkli_edit_state');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Renders and returns the redirect page.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The response.
   */
  public function content(string $entity_type, string $entity_uuid, string $langcode): RedirectResponse {
    $editState = $this->editStorage->loadForEntityTypeAndUuid($entity_type, $entity_uuid);
    if (!$editState) {
      throw new \Exception('Edit state not found.');
    }

    $entity = $editState->getHostEntity();
    $destination = Url::fromRoute('paragraphs_blokkli.redirect');
    $url = $this->getEntityFormUrl($entity, $langcode);
    $url->setAbsolute(TRUE);
    $url->setOption('query', [
      'paragraphsBlokkli' => 'true',
      'destination' => $destination->toString(),
    ]);
    return new RedirectResponse($url->toString());
  }

  /**
   * Get the matching entity form URL.
   *
   * @param EntityInterface $entity
   *   The entity.
   * @param string $langcode
   *   The desired langcode to use.
   *
   * @return Url
   *   The entity form URL.
   */
  private function getEntityFormUrl(EntityInterface $entity, string $langcode): Url {
    if ($entity instanceof TranslatableInterface && $entity->isTranslatable()) {
      if (!$entity->hasTranslation($langcode)) {
        $url = $entity->toUrl('drupal:content-translation-add');
        $url->setRouteParameter('target', $langcode);
        $url->setRouteParameter(
          'source',
          $entity->getTranslation(LanguageInterface::LANGCODE_DEFAULT)->language()->getId()
        );
        return $url;
      }
    }

    return $entity
      ->toUrl('edit-form')
      ->setOption('language', $this->languageManager()->getLanguage($langcode));
  }

}
