<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\paragraphs_blokkli\BlokkliSession;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliEditStateStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Grants permission to view the edit state.
 *
 * The controller stores the UUID of the host entity in the current session
 * and redirects to the preview URL of the edit state.
 */
class PreviewGrantController extends ControllerBase {

  /**
   * The paragraphs_blokkli_edit_state storage.
   */
  protected ParagraphsBlokkliEditStateStorageInterface $editStorage;

  /**
   * LastChangedController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    protected RequestStack $requestStack,
  ) {
    $this->editStorage = $entityTypeManager->getStorage('paragraphs_blokkli_edit_state');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('request_stack'),
    );
  }

  /**
   * Renders and returns the redirect page.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   */
  public function content(string $id, string $hash) {
    /** @var \Drupal\paragraphs_blokkli\Entity\ParagraphsBlokkliEditState|null $editState */
    $editState = $this->editStorage->load($id);
    if ($editState) {
      if (!$editState->isPreviewHashValid($hash)) {
        throw new AccessDeniedHttpException();
      }

      $request = $this->requestStack->getCurrentRequest();
      $session = $request->getSession();
      $blokkliSession = BlokkliSession::fromSession($session);
      $blokkliSession->addPreviewGrant($editState);
      $hostEntity = $editState->getHostEntity();
      if (!$hostEntity) {
        throw new NotFoundHttpException();
      }

      $blokkliSession->addPreviewEntity($hostEntity);

      // Let modules alter the session.
      \Drupal::moduleHandler()->invokeAll(
        'paragraphs_blokkli_preview_session',
        [$editState, $blokkliSession]
      );

      $blokkliSession->applyToSession($session);

      $entity = $editState->getHostEntity();
      $url = $entity->toUrl();
      $url->setOption('query', [
        'blokkliPreview' => $editState->getHostEntityUuid(),
      ]);
      return new RedirectResponse($url->toString());
    }

    throw new NotFoundHttpException();
  }

}
