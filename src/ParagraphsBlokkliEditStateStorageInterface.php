<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * Paragraphs Blokkli Edit State storage.
 */
interface ParagraphsBlokkliEditStateStorageInterface extends ContentEntityStorageInterface {

  /**
   * Load an edit state for the given entity.
   *
   * @param FieldableEntityInterface $entity
   *   The entity.
   *
   * @return ParagraphsBlokkliEditStateInterface|null
   *   The edit state.
   */
  public function loadForEntity(FieldableEntityInterface $entity): ParagraphsBlokkliEditStateInterface|null;

  /**
   * Load an edit state for the given type and UUID.
   *
   * @param string $entityType
   *   The entity type.
   * @param string $uuid
   *   The entity uui.
   *
   * @return ParagraphsBlokkliEditStateInterface|null
   *   The edit state.
   */
  public function loadForEntityTypeAndUuid(string $entityType, string $uuid): ParagraphsBlokkliEditStateInterface|null;

}
