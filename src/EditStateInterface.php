<?php

namespace Drupal\paragraphs_blokkli;

use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * The edit state.
 */
interface EditStateInterface {

  /**
   * Get the host entity type.
   *
   * @return string
   *   The host entity type.
   */
  public function getHostEntityType(): string;

  /**
   * Get the host entity UUID.
   *
   * @return string
   *   The host entity UUID.
   */
  public function getHostEntityUuid(): string;

  /**
   * Get the entity.
   *
   * @param bool $forceReload
   *   Whether to force reload the host entity.
   *
   * @return \Drupal\Core\Entity\FieldableEntityInterface|null
   *   The entity.
   */
  public function getHostEntity(bool $forceReload = FALSE): FieldableEntityInterface|null;

  /**
   * Check if the given preview hash is valid.
   *
   * @param string $hash
   *   The hash to check.
   *
   * @return bool
   *   TRUE if the hash is valid.
   */
  public function isPreviewHashValid(string $hash): bool;

  /**
   * Get the mutated paragraph fields.
   *
   * @param bool $forSaving
   *   Whether to return the state for saving.
   * @param int|null $historyIndex
   *   The history index to use.
   *
   * @return ParagraphsBlokkliMutatedState
   *   The mutated state.
   */
  public function getMutatedState(bool $forSaving = FALSE, int|null $historyIndex = NULL): ParagraphsBlokkliMutatedState;

  /**
   * Execute the mutations on the given proxies.
   *
   * @param ParagraphProxy[] $proxies
   *   The proxies.
   * @param int|null $historyIndex
   *   The history index.
   *
   * @return ParagraphMutationContext
   *   The mutation context.
   */
  public function executeMutations(array $proxies, int|null $historyIndex = NULL): ParagraphMutationContext;

  /**
   * Add a mutation.
   *
   * @param ParagraphMutationPluginBase $mutation
   *   The mutation.
   *
   * @return static
   */
  public function addMutation(ParagraphMutationPluginBase $mutation): static;

  /**
   * Get the current index.
   *
   * @return int
   *   The current index.
   */
  public function getCurrentIndex(): int;

  /**
   * Get mutations.
   *
   * @return \Drupal\paragraphs_blokkli\ParagraphMutationItemInterface[]
   *   The mutation items.
   */
  public function getMutations(): array;

  /**
   * Undo the last mutation.
   *
   * Decrements the index.
   */
  public function undo(): static;

  /**
   * Redo.
   *
   * Increments the index.
   */
  public function redo(): static;

  /**
   * Set the history index.
   *
   * @param int $index
   *   The new index.
   */
  public function setHistoryIndex(int $index): static;

  /**
   * Set the status of a mutation item.
   *
   * @param int $index
   *   The index.
   * @param bool $status
   *   The status of the mutation.
   */
  public function setMutationStatus(int $index, bool $status): static;

  /**
   * Validate.
   *
   * @return \Drupal\Core\Entity\EntityConstraintViolationListInterface
   *   The violations.
   */
  public function validate();

  /**
   * Save.
   */
  public function save();

  /**
   * Delete.
   */
  public function delete();

  /**
   * Set owner ID.
   *
   * @param string $uid
   *   The user ID.
   */
  public function setOwnerId($uid);

  /**
   * Get the owner ID.
   *
   * @return string|int|null
   *   The owner ID.
   */
  public function getOwnerId();

}
