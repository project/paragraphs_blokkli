<?php

namespace Drupal\paragraphs_blokkli;

/**
 * Interface for paragraph_mutation plugins.
 */
interface ParagraphMutationInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

  /**
   * Returns the UUID of the affected paragraph.
   *
   * @return string|null
   *   The UUID of the affected paragraph.
   */
  public function getAffectedParagraphUuid(): string|null;

  /**
   * Execute the mutation.
   *
   * @param \Drupal\paragraphs_blokkli\ParagraphMutationContextInterface $context
   *   The mutation context.
   */
  public function executeMutation(ParagraphMutationContextInterface $context);

  /**
   * Get the UUID for the new entity created by the mutation.
   *
   * @param string $uuid
   *   The UUID of the original entity.
   *
   * @return string
   *   The UUID of the new entity.
   */
  public function getUuidForNewEntity(string $uuid): string;

}
