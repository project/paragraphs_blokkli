<?php

namespace Drupal\paragraphs_blokkli;

/**
 * Form helpers.
 */
final class BlokkliFormHelper {

  /**
   * Get the diff of old and new form values.
   *
   * @param string[] $persistKeys
   *   The keys that should be persisted.
   * @param array $oldValues
   *   The before values.
   * @param array $newValues
   *   The after values.
   *
   * @return array
   *   The diff.
   */
  public static function extractDiff(array $persistKeys, array $oldValues, array $newValues): array {
    $valuesToPersist = [];

    foreach (array_keys($newValues) as $key) {
      // Key should not be persisted.
      if (!in_array($key, $persistKeys)) {
        continue;
      }

      $oldValue = array_filter($oldValues[$key] ?? []);
      $newValue = array_filter($newValues[$key] ?? []);

      // Both the old and the new value is empty.
      if (empty($oldValue) && empty($newValue)) {
        continue;
      }

      // Values are the same.
      if ($oldValue == $newValue) {
        continue;
      }

      $valuesToPersist[$key] = $newValues[$key];
    }

    return $valuesToPersist;
  }

}
