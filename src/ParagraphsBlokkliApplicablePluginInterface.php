<?php

namespace Drupal\paragraphs_blokkli;

use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * An interface for plugins that apply only for certain contexts.
 */
interface ParagraphsBlokkliApplicablePluginInterface {

  /**
   * Returns whether the plugin can be used for the given context.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $hostEntity
   *   The host entity.
   * @param string $langcode
   *   The langcode of the blökkli session. Note that the host entity might
   *   not yet be translated in this language, since the plugin itself might
   *   do that (e.g. transform plugins that do the translation).
   *
   * @return bool
   *   Whether the plugin can be used for the given context.
   */
  public function applies(FieldableEntityInterface $hostEntity, string $langcode): bool;

}
