<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a listing of pb_entity_mappings.
 */
final class PbEntityMappingListBuilder extends ConfigEntityListBuilder {

  /**
   * Constructs a new PbEntityMappingListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entityType
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo $bundleInfo
   *   The entity bundle info service.
   * @param \Drupal\Core\Entity\EntityFieldManager $entityFieldManager
   *   The entity field manager.
   */
  public function __construct(
    EntityTypeInterface $entityType,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected EntityTypeBundleInfoInterface $bundleInfo,
    protected EntityFieldManagerInterface $entityFieldManager,
  ) {
    parent::__construct($entityType, $entityTypeManager->getStorage($entityType->id()));
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entityType): self {
    return new static(
      $entityType,
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_field.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['target_entity_type'] = $this->t('Target entity type');
    $header['target_entity_bundle'] = $this->t('Target entity bundle');
    $header['paragraph_bundle'] = $this->t('Paragraph type');
    $header['paragraph_field'] = $this->t('Field');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    $row['target_entity_type'] = $this->getTargetEntityType($entity);
    $row['target_entity_bundle'] = $this->getTargetEntityBundle($entity);
    $row['paragraph_bundle'] = $this->getParagraphBundle($entity);
    $row['paragraph_field'] = $this->getParagrapField($entity);
    return $row + parent::buildRow($entity);
  }

  /**
   * Get the target entity type.
   *
   * @param \Drupal\paragraphs_blokkli\Entity\PbEntityMapping $entity
   */
  private function getTargetEntityType(PbEntityMappingInterface $entity) {
    $targetEntityType = $entity->getTargetEntityType();

    if (!$targetEntityType) {
      return NULL;
    }

    /** @var \Drupal\Core\Entity\EntityType $entityType */
    $entityType = $this->entityTypeManager->getDefinition($targetEntityType);

    if (!$entityType) {
      return NULL;
    }

    $label = $entityType->getLabel();

    return "$label ($targetEntityType)";
  }

  /**
   * Get the target bundle type.
   *
   * @param \Drupal\paragraphs_blokkli\Entity\PbEntityMapping $entity
   */
  private function getTargetEntityBundle(PbEntityMappingInterface $entity) {
    $targetEntityType = $entity->getTargetEntityType();

    if (!$targetEntityType) {
      return NULL;
    }

    $targetEntityBundle = $entity->getTargetEntityBundle();

    if (!$targetEntityBundle) {
      return NULL;
    }

    $entityBundleInfo = $this->bundleInfo->getBundleInfo($targetEntityType);

    if (!$entityBundleInfo) {
      return NULL;
    }

    $label = $entityBundleInfo[$targetEntityBundle]['label'] ?? NULL;

    if (!$label) {
      return NULL;
    }

    return "$label ($targetEntityType)";
  }

  /**
   * Get the paragraph bundle.
   *
   * @param \Drupal\paragraphs_blokkli\Entity\PbEntityMapping $entity
   */
  private function getParagraphBundle(PbEntityMappingInterface $entity) {
    $paragraphBundle = $entity->getParagraphBundle();
    if (!$paragraphBundle) {
      return NULL;
    }

    $entityBundleInfo = $this->bundleInfo->getBundleInfo('paragraph');

    if (!$entityBundleInfo) {
      return NULL;
    }

    $label = $entityBundleInfo[$paragraphBundle]['label'] ?? NULL;

    if (!$label) {
      return NULL;
    }

    return "$label ($paragraphBundle)";
  }

  /**
   * Get the paragraph field.
   *
   * @param \Drupal\paragraphs_blokkli\Entity\PbEntityMapping $entity
   */
  private function getParagrapField(PbEntityMappingInterface $entity) {
    $paragraphBundle = $entity->getParagraphBundle();
    if (!$paragraphBundle) {
      return NULL;
    }

    $fieldName = $entity->getParagraphField();

    if (!$fieldName) {
      return NULL;
    }

    $fields = $this->entityFieldManager->getFieldDefinitions('paragraph', $paragraphBundle);

    $field = $fields[$fieldName] ?? NULL;

    if (!$field) {
      return NULL;
    }

    $label = $field->getLabel();

    if (!$label) {
      return NULL;
    }

    return "$label ($fieldName)";
  }

}
