<?php

namespace Drupal\paragraphs_blokkli\Exception;

use Drupal\Core\Entity\EntityInterface;

/**
 * Exception when blökkli is not enabled for an entity.
 */
class BlokkliNotEnabledException extends \Exception {

  /**
   * Constructs a BlokkliNotEnabledException object.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param int $code
   *   The Exception code.
   * @param \Exception|null $previous
   *   The previous exception used for the exception chaining.
   */
  public function __construct(
    EntityInterface $entity,
    $code = 0,
    ?\Exception $previous = NULL,
  ) {
    $entityType = $entity->getEntityTypeId();
    $bundle = $entity->bundle();
    parent::__construct("blokkli is not enabled for entity type '$entityType' and bundle '$bundle'.", $code, $previous);
  }

}
