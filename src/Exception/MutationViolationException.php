<?php

namespace Drupal\paragraphs_blokkli\Exception;

use Drupal\Core\Entity\EntityConstraintViolationListInterface;

/**
 * Entity violation exception during paragraph mutations.
 */
class MutationViolationException extends MutationException {

  /**
   * Constructs a MutationException object.
   *
   * @param \Drupal\Core\Entity\EntityConstraintViolationList $violations
   *   The violations.
   */
  public function __construct(
    protected EntityConstraintViolationListInterface $violations,
  ) {
    parent::__construct('Entity Violations');
  }

}
