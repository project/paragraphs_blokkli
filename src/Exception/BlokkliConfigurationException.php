<?php
// phpcs:ignoreFile

namespace Drupal\paragraphs_blokkli\Exception;

/**
 * Exception due to wrong blökkli configuration.
 */
class BlokkliConfigurationException extends \Exception {

  /**
   * Constructs a BlokkliConfigurationException object.
   *
   * @param string $message
   *   The message.
   * @param int $code
   *   The Exception code.
   * @param \Exception|null $previous
   *   The previous exception used for the exception chaining.
   */
  public function __construct(
    string $message,
    $code = 0,
    ?\Exception $previous = NULL,
  ) {
    parent::__construct($message, $code, $previous);
  }

}
