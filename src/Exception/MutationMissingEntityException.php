<?php

namespace Drupal\paragraphs_blokkli\Exception;

/**
 * Exception during paragraph mutations.
 */
class MutationMissingEntityException extends MutationException {

  /**
   * Constructs a new MutationMissingEntityException object.
   *
   * @param string $entity
   *   The type of the missing entity.
   * @param string|null $id
   *   The ID of the missing entity.
   * @param string|null $uuid
   *   The UUID of the missing entity.
   */
  public function __construct(
    protected string $entity,
    protected string|null $id = NULL,
    protected string|null $uuid = NULL,
  ) {
    parent::__construct('Missing Entity');
  }

}
