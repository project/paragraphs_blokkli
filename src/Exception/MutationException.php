<?php
// phpcs:ignoreFile

namespace Drupal\paragraphs_blokkli\Exception;

/**
 * Exception during paragraph mutations.
 */
class MutationException extends \Exception {

  /**
   * Constructs a MutationException object.
   *
   * @param string $message
   *   The message for the exception.
   * @param int $code
   *   The Exception code.
   * @param \Exception|null $previous
   *   The previous exception used for the exception chaining.
   */
  public function __construct(
    $message = '',
    $code = 0,
    ?\Exception $previous = NULL,
  ) {
    parent::__construct($message, $code, $previous);
  }

}
