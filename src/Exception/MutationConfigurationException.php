<?php

namespace Drupal\paragraphs_blokkli\Exception;

/**
 * Exception during paragraph mutations.
 */
class MutationConfigurationException extends MutationException {

}
