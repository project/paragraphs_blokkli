<?php

namespace Drupal\paragraphs_blokkli;

use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * The edit state.
 */
interface ParagraphMutationContextInterface {

  /**
   * Get the index of the proxy for the given paragraph UUID.
   *
   * @param string $uuid
   *   The UUID of the paragraph to find.
   *
   * @return int|null
   *   The index of the proxy.
   */
  public function getIndex(string $uuid): int|null;

  /**
   * Get the paragraph proxy for the given UUID.
   *
   * @param string $uuid
   *   The UUID of the paragraph to find.
   *
   * @return ParagraphProxyInterface|null
   *   The paragraph mutation proxy.
   */
  public function getProxy(string $uuid): ParagraphProxyInterface|null;

  /**
   * Get proxies for the given host entity.
   *
   * @param string $hostEntityType
   *   The entity type of the host entity.
   * @param string $hostUuid
   *   The entity UUID of the host entity.
   *
   * @return ParagraphProxyInterface[]
   *   The procxies belonging to the host.
   */
  public function getProxiesForHost(string $hostEntityType, string $hostUuid): array;

  /**
   * Append a paragraph after the given UUID.
   *
   * @param ParagraphProxyInterface $proxy
   *   The paragraph proxy to add.
   * @param string|null $precedingUuid
   *   The UUID of the preceding paragraph.
   */
  public function addProxy(ParagraphProxyInterface $proxy, string|null $precedingUuid = NULL): void;

  /**
   * Add a proxy to the end of the list.
   *
   * @param ParagraphProxyInterface $proxy
   *   The paragraph proxy to add.
   */
  public function appendProxy(ParagraphProxyInterface $proxy): static;

  /**
   * Remove the paragraph proxy with the given UUID.
   *
   * @param string $uuid
   *   The paragraph to remove.
   *
   * @return ParagraphProxyInterface|null
   *   The removed paragraph proxy.
   */
  public function removeProxy(string $uuid): ParagraphProxyInterface|null;

  /**
   * Move the proxy after another proxy.
   *
   * @param string $uuid
   *   The UUID of the proxy to remove.
   * @param string $precedingUuid
   *   The UUID of the preceding proxy.
   */
  public function moveProxyAfter(string $uuid, string $precedingUuid): void;

  /**
   * Mark the given paragraph for deletion.
   *
   * @param string $uuid
   *   The UUID of the paragraph to be deleted.
   */
  public function markAsDeleted(string $uuid): static;

  /**
   * Check if the given paragraph is deleted.
   *
   * @param string $uuid
   *   The UUID of the paragraph to check.
   */
  public function isDeleted(string $uuid): bool;

  /**
   * Get the host entity.
   *
   * @return FieldableEntityInterface
   *   The host entity of the edit state.
   */
  public function getHostEntity(): FieldableEntityInterface;

}
