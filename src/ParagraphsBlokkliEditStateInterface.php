<?php

namespace Drupal\paragraphs_blokkli;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a paragraphs blokkli edit state entity type.
 */
interface ParagraphsBlokkliEditStateInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface, EditStateInterface {

}
