<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli\Traits;

use Drupal\paragraphs_blokkli\EditStateInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * A trait for forms dealing with edit state.
*/
trait EditStateFormTrait {

  /**
   * Load the edit state and perform access checks.
   *
   * @param string $entityType
   *   The entity type.
   * @param string $entityUuid
   *   The UUID of the entity.
   *
   * @return \Drupal\paragraphs_blokkli\EditStateInterface
   *   The edit state.
   */
  protected function loadEditState(string $entityType, string $entityUuid): EditStateInterface {
    /** @var \Drupal\paragraphs_blokkli\ParagraphsBlokkliEditStateStorage $storage */
    $storage = $this->entityTypeManager->getStorage('paragraphs_blokkli_edit_state');
    $state = $storage->loadForEntityTypeAndUuid($entityType, $entityUuid);

    if (!$state) {
      throw new \Exception('Edit state not found.');
    }

    if (!$state->access('update')) {
      throw new AccessDeniedHttpException();
    }

    $entity = $state->getHostEntity();

    if (!$entity) {
      throw new \Exception('Failed to load host entity for edit state.');
    }

    if (!$entity->access('update')) {
      throw new AccessDeniedHttpException();
    }

    return $state;
  }

}
