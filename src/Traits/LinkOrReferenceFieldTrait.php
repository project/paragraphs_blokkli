<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli\Traits;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\link\LinkItemInterface;
use Drupal\paragraphs_blokkli\Exception\MutationConfigurationException;

/**
 * A trait for handling setting "reference" field values.
*/
trait LinkOrReferenceFieldTrait {

  /**
   * Set an entity reference or link field value.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity on which to set the field value.
   * @param string $fieldName
   *   The name of the field to set.
   * @param \Drupal\Core\Entity\EntityInterface $targetEntity
   *   The target entity.
   */
  protected function setLinkOrEntityReferenceFieldValue(FieldableEntityInterface $entity, string $fieldName, EntityInterface $targetEntity) {
    $field = $entity->get($fieldName);

    if ($field instanceof EntityReferenceFieldItemListInterface) {
      $field->setValue([
        'target_id' => $targetEntity->id(),
      ]);
      return;
    }

    $fieldDefinition = $field->getFieldDefinition();
    $storageDefinition = $fieldDefinition->getFieldStorageDefinition();
    $propertyDefinitions = $storageDefinition->getPropertyDefinitions();
    $uriDefinition = $propertyDefinitions['uri'] ?? NULL;

    if ($uriDefinition) {
      $type = $uriDefinition->getDataType();
      if ($type === 'uri') {
        $linkTypeSetting = $fieldDefinition->getSetting('link_type');

        if ($linkTypeSetting === LinkItemInterface::LINK_EXTERNAL) {
          throw new MutationConfigurationException("The link field '$fieldName' only supports external links. To set a link for entities via entity mapping the link field must support internal links.");
        }

        $needsTitle = $fieldDefinition->getSetting('title');
        $entityType = $targetEntity->getEntityTypeId();
        $id = $targetEntity->id();
        $field->setValue([
          'uri' => "entity:$entityType/$id",
          'title' => $needsTitle ? $entity->label() : NULL,
        ]);

        return;
      }
    }

    throw new MutationConfigurationException("Failed to set reference or link on field '$fieldName'. The target field must either be an entity reference or link field.");
  }

  /**
   * Determine whether the given field can be used to reference an entity.
   *
   * @param \Drupal\Core\Field\FieldDefinition $fieldDefinition
   *   The field definition.
   * @param string $entityType
   *   The target entity type.
   * @param string $entityBundle
   *   The target entity bundle.
   *
   * @return bool
   *   TRUE if the field can be used.
   */
  protected function isLinkOrEntityReferenceField(FieldDefinitionInterface $fieldDefinition, string $entityType, string $entityBundle): bool {
    $propertyDefinitions = $fieldDefinition->getFieldStorageDefinition()->getPropertyDefinitions();

    // Check link fields.
    $uriDefinition = $propertyDefinitions['uri'] ?? NULL;

    if ($uriDefinition) {
      $type = $uriDefinition->getDataType();
      if ($type === 'uri') {
        $linkTypeSetting = $fieldDefinition->getSetting('link_type');
        // The link field must allow internal URIs.
        return $linkTypeSetting === LinkItemInterface::LINK_GENERIC || LinkItemInterface::LINK_INTERNAL;
      }
    }

    // Check entity reference fields.
    $targetIdDefinition = $propertyDefinitions['target_id'] ?? NULL;

    if ($targetIdDefinition) {
      $targetType = $fieldDefinition->getSetting('target_type');
      if ($targetType === $entityType) {
        $handlerSettings = $fieldDefinition->getSetting('handler_settings');
        $targetBundles = $handlerSettings['target_bundles'] ?? [];
        return in_array($entityBundle, $targetBundles);
      }
    }

    return FALSE;
  }

}
