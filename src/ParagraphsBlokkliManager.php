<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList;
use Drupal\field\Entity\FieldConfig;
use Drupal\paragraphs_blokkli\Exception\BlokkliNotEnabledException;

/**
 * Edit state manager.
 */
class ParagraphsBlokkliManager {

  /**
   * The paragraphs blokkli edit state storage.
   */
  protected ParagraphsBlokkliEditStateStorageInterface $editStateStorage;

  /**
   * Constructs a new ParagraphsBlokkliManager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   * @param \Drupal\Component\Datetime\Time $time
   *   The time service.
   * @param \Drupal\paragraphs_blokkli\ParagraphsBlokkliConfig $blokkliConfig
   *   The blokkli config.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected AccountInterface $currentUser,
    protected TimeInterface $time,
    protected ParagraphsBlokkliConfig $blokkliConfig,
  ) {
    $this->editStateStorage = $entityTypeManager->getStorage('paragraphs_blokkli_edit_state');
  }

  /**
   * Get the edit state.
   */
  public function getParagraphsEditState(FieldableEntityInterface $entity, bool $noCreate = FALSE): ParagraphsBlokkliEditStateInterface|null {
    if (!$this->blokkliConfig->isEnabledForEntity($entity)) {
      throw new BlokkliNotEnabledException($entity);
    }

    $state = $this->editStateStorage->loadForEntity($entity);
    if ($state) {
      return $state;
    }

    if ($noCreate) {
      return NULL;
    }

    /** @var \Drupal\paragraphs_blokkli\EditStateInterface $state */
    $state = $this->editStateStorage->create([
      'host_entity_type' => $entity->getEntityTypeId(),
      'host_entity_uuid' => $entity->uuid(),
    ]);
    $state->save();
    return $state;
  }

  /**
   * Get allowed paragraph types for a field.
   *
   * @param \Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList $field
   *   The entity reference revisions field item list.
   *
   * @return \Drupal\paragraphs\ParagraphsTypeInterface[]
   *   The array of allowed paragraph types.
   */
  public function getAllowedParagraphTypes(EntityReferenceRevisionsFieldItemList $field): array {
    // Get the field definition.
    $definition = $field->getFieldDefinition();
    $handlerSettings = $definition->getSetting('handler_settings');

    // Contains an array of bundle IDs.
    $targetBundles = array_keys($handlerSettings['target_bundles'] ?? []);
    if ($definition instanceof FieldConfig) {
      $storage = $definition->getFieldStorageDefinition();
      $targetType = $storage->getSetting('target_type');
      $storage = $this->entityTypeManager->getStorage($targetType);
      $bundleEntityType = $storage->getEntityType()->getBundleEntityType();
      $typeStorage = $this->entityTypeManager->getStorage($bundleEntityType);
      return $typeStorage->loadMultiple($targetBundles);
    }

    return [];
  }

  /**
   * Save the paragraphs blokkli context.
   */
  public function saveState(EditStateInterface $state) {
    $state->save();
  }

  /**
   * Take ownership of an edit state.
   */
  public function takeOwnership(EditStateInterface $state) {
    if ($this->currentUser->hasPermission('take ownership of paragraphs blokkli edit state')) {
      $state->setOwnerId($this->currentUser->id());
    }
  }

  /**
   * Save the paragraphs blokkli context.
   */
  public function revertAllChanges(EditStateInterface $state) {
    $state->delete();
  }

  /**
   * Publish all changes.
   *
   * @param \Drupal\paragraphs_blokkli\EditStateInterface $state
   *   The edit state.
   * @param bool $forceReload
   *   Whether to force reload the host entity.
   * @param bool $validate
   *   Whether to perform validations.
   *
   * @return bool
   *   Whether publishing was successful.
   */
  public function publish(EditStateInterface $state, bool $forceReload = FALSE, bool $validate = FALSE): bool {
    $entity = $state->getHostEntity($forceReload);
    $mutatedState = $state->getMutatedState(TRUE);
    $fields = $mutatedState->getFields();

    foreach ($fields as $field) {
      // Update the paragraphs field on the host entity with the mutated paragraphs.
      if (
        $field->getEntityType() === $state->getHostEntityType() &&
        $field->getEntityUuid() === $state->getHostEntityUuid()
      ) {
        $entity->set($field->getFieldName(), $field->getParagraphs());
      }
    }

    if ($entity->getEntityType()->hasKey('revision')) {
      // Revision handling for revisionable entities.
      if ($entity instanceof RevisionableInterface) {
        $entity->setNewRevision(TRUE);
      }
      if ($entity instanceof RevisionLogInterface) {
        $entity->setRevisionUserId($state->getOwnerId());
        $entity->setRevisionCreationTime($this->time->getRequestTime());
      }
    }

    // Users with this permission may bypass validation checks during
    // publishing.
    $skipValidation = $this->currentUser->hasPermission('publish paragraphs blokkli edit state with validation errors');

    if (!$skipValidation && $validate && $this->blokkliConfig->shouldValidate()) {
      $validations = $state->validate();
      if ($validations->count()) {
        return FALSE;
      }
    }

    // Save entity.
    $entity->save();

    $state->delete();
    return TRUE;
  }

}
