<?php

namespace Drupal\paragraphs_blokkli;

use Drupal\paragraphs\ParagraphInterface;

/**
 * Wrapper class for a mutated field.
 */
class ParagraphsBlokkliMutatedField {

  /**
   * Construct a new ParagraphsBlokkliMutatedField object.
   *
   * @param string $entityType
   * @param string $entityUuid
   * @param string $fieldName
   * @param \Drupal\paragraphs\ParagraphInterface[] $paragraphs
   */
  public function __construct(
    protected string $entityType,
    protected string $entityUuid,
    protected string $fieldName,
    protected array $paragraphs = [],
  ) {
  }

  /**
   * Get the entity type.
   *
   * @return string
   *   The entity type.
   */
  public function getEntityType(): string {
    return $this->entityType;
  }

  /**
   * Get the entity UUID.
   *
   * @return string
   *   The entity UUID.
   */
  public function getEntityUuid(): string {
    return $this->entityUuid;
  }

  /**
   * Get the field name.
   */
  public function getFieldName(): string {
    return $this->fieldName;
  }

  /**
   * Get the paragraphs.
   *
   * @return ParagraphInterface[]
   *   The paragraphs.
   */
  public function getParagraphs(): array {
    return $this->paragraphs;
  }

  /**
   * Append a paragraph.
   *
   * @param ParagraphInterface $paragraph
   *   The paragraph to append.
   */
  public function appendParagraph(ParagraphInterface $paragraph) {
    $this->paragraphs[] = $paragraph;
  }

}
