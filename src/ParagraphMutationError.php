<?php

namespace Drupal\paragraphs_blokkli;

/**
 * Defines an error that happened executing mutations.
 */
final class ParagraphMutationError {

  /**
   * Constructor.
   *
   * @param int $index
   *   The delta of the mutation.
   * @param \Drupal\paragraphs_blokkli\ParagraphMutationPluginBase $mutation
   *   The mutation that failed.
   * @param \Exception $exception
   *   The exception.
   */
  public function __construct(
    public readonly int $index,
    public readonly ParagraphMutationInterface $mutation,
    public readonly \Exception $exception,
  ) {
  }

}
