<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a pb_entity_mapping entity type.
 */
interface PbEntityMappingInterface extends ConfigEntityInterface {

  /**
   * Get the target entity type.
   *
   * @return string|null
   *   The target entity type.
   */
  public function getTargetEntityType(): string|null;

  /**
   * Get the target entity bundle.
   *
   * @return string|null
   *   The target entity bundle.
   */
  public function getTargetEntityBundle(): string|null;

  /**
   * Get the paragraph bundle.
   *
   * @return string|null
   *   The paragraph bundle.
   */
  public function getParagraphBundle(): string|null;

  /**
   * Get the paragraph field.
   *
   * @return string|null
   *   The paragraph field.
   */
  public function getParagraphField(): string|null;

}
