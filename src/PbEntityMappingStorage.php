<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli;

use Drupal\Core\Config\Entity\ConfigEntityStorage;

/**
 * Storage class for pb_entity_mapping entities.
 */
class PbEntityMappingStorage extends ConfigEntityStorage {

  /**
   * Find mappings for the given target type and bundle.
   *
   * @param string $entityType
   *   The entity type.
   * @param string $entityBundle
   *   The entity bundle.
   *
   * @return \Drupal\paragraphs_blokkli\Entity\PbEntityMapping[]
   *   The mappings.
   */
  public function findMappings(string $entityType, string $entityBundle): array {
    $ids = $this
      ->getQuery()
      ->condition('target_entity_type', $entityType)
      ->condition('target_entity_bundle', $entityBundle)
      ->execute();

    return array_filter(array_values($this->loadMultiple($ids)), function (PbEntityMappingInterface $mapping) {
      // Make sure all required fields are set on the mapping.
      return $mapping->getParagraphBundle() &&
        $mapping->getParagraphField() &&
        $mapping->getTargetEntityType() &&
        $mapping->getTargetEntityBundle();
    });
  }

  /**
   * Get mapped bundles for an entity type.
   *
   * @param string $entityType
   *   The entity type.
   *
   * @return string[]
   *   The mapped bundles.
   */
  public function getMappedBundlesForEntityType(string $entityType): array {
    $ids = $this
      ->getQuery()
      ->condition('target_entity_type', $entityType)
      ->execute();

    $bundles = [];

    foreach ($ids as $id) {
      /** @var \Drupal\paragraphs_blokkli\Entity\PbEntityMapping $mapping */
      $mapping = $this->load($id);

      $bundle = $mapping->getTargetEntityBundle();

      if ($bundle) {
        $bundles[] = $bundle;
      }
    }

    return $bundles;
  }

}
