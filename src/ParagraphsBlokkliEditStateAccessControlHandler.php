<?php

namespace Drupal\paragraphs_blokkli;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the paragraphs blokkli edit state entity type.
 */
class ParagraphsBlokkliEditStateAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\paragraphs_blokkli\Entity\ParagraphsBlokkliEditState $entity */

    switch ($operation) {
      case 'view':
        $session = \Drupal::request()->getSession();
        $blokkliSession = BlokkliSession::fromSession($session);

        if ($blokkliSession->hasPreviewGrantForState($entity)) {
          return AccessResult::allowed()->addCacheContexts(['user', 'paragraphs_blokkli_session']);
        }

        return AccessResult::allowedIfHasPermission(
          $account,
          'view paragraphs blokkli edit state'
        )->addCacheContexts(['paragraphs_blokkli_session']);

      case 'update':
        return AccessResult::allowedIfHasPermissions(
          $account,
          [
            'edit paragraphs blokkli edit state',
            'administer paragraphs blokkli edit state',
          ],
          'OR',
        );

      case 'delete':
        return AccessResult::allowedIfHasPermissions(
          $account,
          [
            'delete paragraphs blokkli edit state',
            'administer paragraphs blokkli edit state',
          ],
          'OR',
        );

      default:
        // No opinion.
        return AccessResult::neutral();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions(
      $account,
      [
        'create paragraphs blokkli edit state',
        'administer paragraphs blokkli edit state',
      ],
      'OR',
    );
  }

}
