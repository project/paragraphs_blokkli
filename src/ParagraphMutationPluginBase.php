<?php

namespace Drupal\paragraphs_blokkli;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\Exception\ContextException;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\ContextAwarePluginTrait;
use Drupal\paragraphs\ParagraphInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for paragraph_mutation plugins.
 */
abstract class ParagraphMutationPluginBase extends PluginBase implements ParagraphMutationInterface, ConfigurableInterface, ContainerFactoryPluginInterface {

  use ContextAwarePluginTrait;
  use DependencySerializationTrait;
  use ParagraphsBlokkliPluginTrait;

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The UUID service.
   */
  protected UuidInterface $uuidHelper;

  /**
   * The paragraphs blokkli helper.
   */
  protected ParagraphsBlokkliHelper $paragraphsBlokkliHelper;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('uuid'),
      $container->get('paragraphs_blokkli.helper')
    );
  }

  /**
   * Convert constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param UuidInterface $uuidHelper
   *   The UUID helper.
   * @param ParagraphsBlokkliHelper $paragraphsBlokkliHelper
   *   The paragraphs blokkli helper.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    EntityTypeManagerInterface $entityTypeManager,
    UuidInterface $uuidHelper,
    ParagraphsBlokkliHelper $paragraphsBlokkliHelper,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityTypeManager = $entityTypeManager;
    $this->uuidHelper = $uuidHelper;
    $this->paragraphsBlokkliHelper = $paragraphsBlokkliHelper;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getAffectedParagraphUuid(): string|null {
    return $this->configuration['uuid'] ?? $this->configuration['new_paragraph_uuid_default'] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getContextDefinitions() {
    $definition = $this->getPluginDefinition();
    return !empty($definition['arguments']) ? $definition['arguments'] : [];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\ContextException
   */
  public function getContextDefinition($name) {
    $definitions = $this->getContextDefinitions();
    if (!empty($definitions[$name])) {
      return $definitions[$name];
    }

    throw new ContextException(sprintf("The %s context is not a valid context.", $name));
  }

  /**
   * Execute the mutation.
   */
  public function executeMutation(ParagraphMutationContextInterface $context) {
    $definitions = $this->getContextDefinitions();
    foreach ($definitions as $name => $definition) {
      $this->setContextValue($name, $this->configuration[$name] ?? NULL);
    }
    $params = $this->getContextValues();
    return call_user_func_array(
      [$this, 'execute'],
      array_merge(
        ['context' => $context],
        $params
      )
    );
  }

  /**
   * Create a new paragraph.
   *
   * @param array $values
   *   The values for the paragraph.
   *
   * @return ParagraphInterface
   *   The paragraph.
   */
  protected function createNewParagraph(array $values): ParagraphInterface {
    $storage = $this->entityTypeManager->getStorage('paragraph');
    return $storage->create($values);
  }

}
