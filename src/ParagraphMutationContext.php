<?php

namespace Drupal\paragraphs_blokkli;

use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * Context passed to a paragraph mutation.
 *
 * The class contains the current stack of paragraphs. Mutations are allowed
 * to freely mutate the stack.
 */
final class ParagraphMutationContext implements ParagraphMutationContextInterface {

  /**
   * Map of all deleted paragraph UUIDs.
   *
   * @var string[]
   */
  public array $deletedUuids = [];

  /**
   * The errors.
   *
   * @var \Drupal\paragraphs_blokkli\ParagraphMutationError[]
   */
  protected array $errors = [];

  /**
   * Construct a new ParagraphsMutationContext.
   *
   * @param FieldableEntityInterface $hostEntity
   *   The host entity.
   * @param ParagraphProxy[] $proxies
   *   Array of paragraph proxies.
   */
  public function __construct(
    protected FieldableEntityInterface $hostEntity,
    public array $proxies = [],
  ) {
    $this->deletedUuids = [];
  }

  /**
   * Add error.
   */
  public function addError(ParagraphMutationError $error) {
    $this->errors[] = $error;
  }

  /**
   * Get errors.
   */
  public function getErrors(): array {
    return $this->errors;
  }

  /**
   * {@inheritdoc}
   */
  public function getIndex(string $uuid): int|null {
    foreach ($this->proxies as $index => $proxy) {
      if ($proxy->uuid() === $uuid) {
        return $index;
      }
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getProxy(string $uuid): ParagraphProxyInterface|null {
    $index = $this->getIndex($uuid);
    if ($index !== NULL) {
      return $this->proxies[$index];
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getProxiesForHost(string $hostEntityType, string $hostUuid): array {
    $matches = [];

    foreach ($this->proxies as $proxy) {
      if ($proxy->getHostEntityType() === $hostEntityType && $proxy->getHostUuid() === $hostUuid) {
        $matches[] = $proxy;
      }
    }

    return $matches;
  }

  /**
   * {@inheritdoc}
   */
  public function removeProxy(string $uuid): ParagraphProxyInterface|null {
    $index = $this->getIndex($uuid);
    if ($index !== NULL) {
      $proxy = $this->proxies[$index];
      unset($this->proxies[$index]);
      return $proxy;
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function addProxy(ParagraphProxyInterface $proxy, string|null $precedingUuid = NULL): void {
    $index = $precedingUuid ? $this->getIndex($precedingUuid) : NULL;
    if ($index === NULL) {
      if ($precedingUuid) {
        $this->proxies[] = $proxy;
        return;
      }
      // No preceding paragraph: Put paragraph at the beginning of the array.
      array_unshift($this->proxies, $proxy);
    }
    else {
      // Put the paragraph after the preceding paragraph.
      array_splice($this->proxies, $index + 1, 0, [$proxy]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function moveProxyAfter(string $uuid, string $precedingUuid): void {
    // Get the current index of the proxy.
    $index = $this->getIndex($uuid);

    // Get the current index of the preceding proxy.
    $precedingIndex = $this->getIndex($precedingUuid);

    // Calculate the target index.
    $targetIndex = $index > $precedingIndex ? $precedingIndex + 1 : $precedingIndex;

    // Remove the proxy from the list.
    $proxy = $this->removeProxy($uuid);

    if ($targetIndex === NULL) {
      $targetIndex = 0;
    }

    // Put the paragraph after the preceding paragraph.
    array_splice($this->proxies, $targetIndex, 0, [$proxy]);
  }

  /**
   * {@inheritdoc}
   */
  public function appendProxy(ParagraphProxyInterface $proxy): static {
    $this->proxies[] = $proxy;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function markAsDeleted(string $uuid): static {
    $this->deletedUuids[] = $uuid;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isDeleted(string $uuid): bool {
    return in_array($uuid, $this->deletedUuids);
  }

  /**
   * {@inheritdoc}
   */
  public function getHostEntity(): FieldableEntityInterface {
    return $this->hostEntity;
  }

}
