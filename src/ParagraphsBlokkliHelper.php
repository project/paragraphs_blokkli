<?php

namespace Drupal\paragraphs_blokkli;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList;
use Drupal\paragraphs\ParagraphInterface;

/**
 * Helpers.
 */
class ParagraphsBlokkliHelper {

  /**
   * The paragraph fields on host entities.
   *
   * @var array<string, array<string, string[]>>
   */
  protected array $hostFieldNames = [];

  /**
   * The names of nested paragraph field names, keyed by paragraph type.
   *
   * @var string[][]
   */
  protected array $nestedFieldNames = [];

  /**
   * The constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {
  }

  /**
   * Get the names of the fields that can be used.
   *
   * @param FieldableEntityInterface $entity
   *   The entity to get the field names for.
   *
   * @return string[]
   *   Array of field names.
   */
  public function getAvailableHostFieldNames(FieldableEntityInterface $entity): array {
    $key = $entity->getEntityTypeId() . ':' . $entity->bundle();
    if (isset($this->hostFieldNames[$key])) {
      return $this->hostFieldNames[$key];
    }
    $fieldNames = $this->getParagraphReferenceFieldNames($entity);
    $this->hostFieldNames[$key] = $fieldNames;
    return $fieldNames;
  }

  /**
   * Get the field names of a paragraph containing other paragraphs.
   *
   * @param ParagraphInterface $paragraph
   *   The paragraph.
   *
   * @return string[]
   *   The array of field names.
   */
  public function getNestedParagraphFieldNames(ParagraphInterface $paragraph): array {
    $bundle = $paragraph->bundle();
    if (!isset($this->nestedFieldNames[$bundle])) {
      $this->nestedFieldNames[$bundle] = $this->getParagraphReferenceFieldNames($paragraph);
    }

    return $this->nestedFieldNames[$bundle];
  }

  /**
   * Get the names of the fields that reference paragraphs.
   *
   * @param FieldableEntityInterface $entity
   *   The entity.
   *
   * @return string[]
   *   Array of field names.
   */
  public function getParagraphReferenceFieldNames(FieldableEntityInterface $entity): array {
    return array_filter(array_map(function (FieldItemListInterface $field) {
      if ($field instanceof EntityReferenceRevisionsFieldItemList) {
        $fieldDefiniton = $field->getFieldDefinition();
        $fieldStorageDefinition = $fieldDefiniton->getFieldStorageDefinition();
        $targetType = $fieldStorageDefinition->getSetting('target_type');
        if ($targetType === 'paragraph') {
          return $field->getName();
        }
      }
    }, $entity->getFields()));
  }

  /**
   * Build proxies for all paragraphs beloning to this entity.
   *
   * @param FieldableEntityInterface $entity
   *   The entity to get the proxies for.
   * @param ParagraphProxy[] $proxies
   *   The proxies.
   * @param int $level
   *   The level of recursion.
   *
   * @return ParagraphProxy[]
   *   The proxies.
   */
  public function buildProxies(FieldableEntityInterface $entity, array|null &$proxies = [], int $level = 0): array {
    $fieldNames = $this->getAvailableHostFieldNames($entity);
    foreach ($fieldNames as $fieldName) {
      /** @var \Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList $field */
      $field = $entity->get($fieldName);

      /** @var \Drupal\paragraphs\Entity\Paragraph[] $paragraphs */
      $paragraphs = $field->referencedEntities();

      foreach ($paragraphs as $paragraph) {
        $uuid = $paragraph->uuid();
        if (empty($proxies[$uuid])) {
          $proxies[$uuid] = new ParagraphProxy($paragraph, $entity->getEntityTypeId(), $entity->uuid(), $fieldName);
        }
        if ($level < 4) {
          $this->buildProxies($paragraph, $proxies, $level + 1);
        }
      }
    }

    return array_values($proxies);
  }

}
