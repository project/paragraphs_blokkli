<?php

namespace Drupal\paragraphs_blokkli;

use Drupal\Core\Entity\EntityInterface;
use Drupal\paragraphs\ParagraphInterface;

/**
 * A proxy class for a mutated paragraph.
 */
final class ParagraphProxy implements ParagraphProxyInterface {

  /**
   * Paragraph behavior settings.
   */
  protected array $behaviorSettings;

  /**
   * Constructs a new ParagraphProxy.
   *
   * @param ParagraphInterface $paragraph
   *   The paragraph entity.
   * @param string $hostEntityType
   *   The entity type of the host.
   * @param string $hostUuid
   *   The UUID of the host.
   * @param string $hostFieldName
   *   The name of the field in which the paragraph is contained.
   */
  public function __construct(
    protected ParagraphInterface $paragraph,
    protected string $hostEntityType,
    protected string $hostUuid,
    protected string $hostFieldName,
  ) {
    $this->behaviorSettings = $paragraph->getAllBehaviorSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function setBehaviorSetting(string $pluginId, string $key, string $value): static {
    $this->behaviorSettings[$pluginId][$key] = $value;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setBehaviorSettings(array $settings): static {
    $this->behaviorSettings = $settings;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getBehaviorSettings(): array {
    return $this->behaviorSettings;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldListKey(): string {
    return implode(':', [
      $this->getHostEntityType(),
      $this->getHostUuid(),
      $this->getHostFieldName(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function uuid(): string {
    return $this->paragraph->uuid();
  }

  /**
   * {@inheritdoc}
   */
  public function setParagraph(ParagraphInterface $paragraph): static {
    $this->paragraph = $paragraph;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getParagraph(): ParagraphInterface|null {
    return $this->paragraph;
  }

  /**
   * {@inheritdoc}
   */
  public function setHostEntityType(string $hostEntityType): static {
    $this->hostEntityType = $hostEntityType;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setHostUuid(string $hostUuid): static {
    $this->hostUuid = $hostUuid;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setHostFieldName(string $hostFieldName): static {
    $this->hostFieldName = $hostFieldName;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getHostEntityType(): string {
    return $this->hostEntityType;
  }

  /**
   * {@inheritdoc}
   */
  public function getHostUuid(): string {
    return $this->hostUuid;
  }

  /**
   * {@inheritdoc}
   */
  public function getHostFieldName(): string {
    return $this->hostFieldName;
  }

  /**
   * {@inheritdoc}
   */
  public function getHostEntity(): ?EntityInterface {
    $storage = \Drupal::entityTypeManager()->getStorage($this->getHostEntityType());
    $entities = $storage->loadByProperties(['uuid' => $this->getHostUuid()]);
    return array_values($entities)[0] ?? NULL;
  }

}
