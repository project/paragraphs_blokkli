<?php

namespace Drupal\paragraphs_blokkli\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Url;

/**
 * Builds the edit form for an existing paragraph entity.
 */
class EditParagraphForm extends ParagraphsBlokkliFormBase {

  /**
   * If editing a translation.
   *
   * @var bool
   */
  protected $isTranslation = FALSE;

  /**
   * Original paragraph values.
   *
   * @var array
   */
  protected array $originalValues = [];

  /**
   * {@inheritDoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    ?string $entity_type = NULL,
    ?string $entity_uuid = NULL,
    ?string $uuid = NULL,
  ) {
    $this->editState = $this->loadEditState($entity_type, $entity_uuid);

    $entity = $this->editState->getHostEntity();
    $proxies = $this->paragraphsBlokkliHelper->buildProxies($entity);
    $mutationContext = $this->editState->executeMutations($proxies);
    $proxy = $mutationContext->getProxy($uuid);
    if ($proxy === NULL) {
      throw new \Exception('Invalid UUID.');
    }

    $paragraph = $proxy->getParagraph();
    if (!$paragraph) {
      throw new \Exception('Failed to load paragraph from proxy.');
    }
    $langcode = $this->languageManager->getCurrentLanguage()->getId();

    if ($paragraph->isTranslatable()) {
      $defaultLanguage = $paragraph->getTranslation(LanguageInterface::LANGCODE_DEFAULT);
      if ($defaultLanguage->id() !== $langcode) {
        $this->isTranslation = TRUE;

        if (!$paragraph->hasTranslation($langcode)) {
          $paragraph->addTranslation($langcode, $paragraph->toArray());
        }

        $paragraph = $paragraph->getTranslation($langcode);
      }
    }
    $this->paragraph = $paragraph;
    $this->originalValues = $paragraph->toArray();

    $form = $this->buildComponentForm($form, $form_state);

    $form['#attached']['library'][] = 'paragraphs_blokkli/form';

    return $form;
  }

  /**
   * Create the form title.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The form title.
   */
  protected function formTitle() {
    return $this->t('Edit @type', ['@type' => $this->paragraph->getParagraphType()->label()]);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Submit the form. This will update the values in the paragraph.
    parent::submitForm($form, $form_state);

    // Get the updated values.
    $newValues = $this->paragraph->toArray();

    // All the keys of the new values array whose value has changed.
    $diffKeys = [];

    // The actual value keys from the form.
    $persistKeys = array_keys($form_state->getValues());

    // Loop over the keys of the new values array and compare original value.
    foreach (array_keys($newValues) as $key) {
      if (
        !isset($this->originalValues[$key]) ||
        $this->originalValues[$key] != $newValues[$key] &&
        in_array($key, $persistKeys)
      ) {
        $diffKeys[] = $key;
      }
    }

    // Only add a mutation if the value actually changed.
    if (!empty($diffKeys)) {
      // Build an array of the updated values.
      $diffValues = array_intersect_key($newValues, array_flip($diffKeys));

      $mutation = $this->mutationPluginManager->createInstance('edit', [
        'uuid' => $this->paragraph->uuid(),
        'langcode' => $this->paragraph->language()->getId(),
        'values' => $diffValues,
      ]);

      $this->editState->addMutation($mutation);
      $this->paragraphsBlokkliManager->saveState($this->editState);
    }

    $redirect = Url::fromRoute('paragraphs_blokkli.redirect');
    $form_state->setRedirectUrl($redirect);
  }

}
