<?php

namespace Drupal\paragraphs_blokkli\Form;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;
use Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs_blokkli\BlokkliFormHelper;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginManager;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliHelper;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliManager;
use Drupal\paragraphs_blokkli\Traits\EditStateFormTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to translate multiple paragraphs at once.
 */
class TranslateParagraphsForm extends FormBase {

  use EditStateFormTrait;

  /**
   * The paragraph edit state.
   *
   * @var \Drupal\paragraphs_blokkli\EditStateInterface
   */
  protected $editState;

  /**
   * {@inheritDoc}
   */
  public function __construct(
    protected ParagraphsBlokkliManager $paragraphsBlokkliManager,
    protected ParagraphsBlokkliHelper $paragraphsBlokkliHelper,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected ParagraphMutationPluginManager $mutationPluginManager,
    protected LanguageManagerInterface $languageManager,
  ) {
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('paragraphs_blokkli.manager'),
      $container->get('paragraphs_blokkli.helper'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.paragraph_mutation'),
      $container->get('language_manager'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'paragraphs_blokkli_translate_paragraphs_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    ?string $entity_type = NULL,
    ?string $entity_uuid = NULL,
  ) {
    $this->editState = $this->loadEditState($entity_type, $entity_uuid);
    $currentLangcode = $this->languageManager->getCurrentLanguage()->getId();

    $entity = $this->editState->getHostEntity();

    if (!($entity instanceof TranslatableInterface)) {
      throw new \Exception('Host entity is not translatable.');
    }
    $defaultLangcode = $entity->getTranslation(LanguageInterface::LANGCODE_DEFAULT)->language()->getId();

    if ($currentLangcode === $defaultLangcode) {
      throw new \Exception("Can't open translation form for default language.");
    }

    $proxies = $this->paragraphsBlokkliHelper->buildProxies($entity);
    $mutationContext = $this->editState->executeMutations($proxies);

    foreach ($mutationContext->proxies as $proxy) {
      if ($mutationContext->isDeleted($proxy->uuid())) {
        continue;
      }
      $paragraph = $proxy->getParagraph();

      if (!$paragraph) {
        continue;
      }

      if (!$paragraph->isTranslatable()) {
        continue;
      }

      if (!$paragraph->hasTranslation($currentLangcode)) {
        $paragraph->addTranslation($currentLangcode, $paragraph->toArray());
      }

      $paragraph = $paragraph->getTranslation($currentLangcode);
      $form_key = 'paragraph_' . $proxy->uuid();
      $componentForm = $this->buildComponentForm($paragraph, $form_state, $form_key);
      $form[$form_key] = [
        '#type' => 'fieldset',
        '#title' => $paragraph->bundle() . ' - ' . $paragraph->uuid(),
        'component' => $componentForm,
      ];
    }

    $form['#attached']['library'][] = 'paragraphs_blokkli/form';
    $form['actions'] = [
      '#weight' => 100,
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#weight' => 100,
        '#value' => $this->t('Save'),
        '#button_type' => 'primary',
      ],
    ];

    return $form;
  }

  /**
   * Builds a component (paragraph) edit form.
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraph
   *   The paragraph to build the form for.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   * @param string $form_key
   *   The form key.
   */
  protected function buildComponentForm(
    ParagraphInterface $paragraph,
    FormStateInterface $form_state,
    string $form_key,
  ) {
    $form = [
      '#tree' => TRUE,
      '#parents' => [$form_key],
    ];

    $display = EntityFormDisplay::collectRenderDisplay($paragraph, 'default');

    $paragraphFields = $paragraph->getFields();
    foreach ($paragraphFields as $field) {
      $definition = $field->getFieldDefinition();
      if ($field instanceof EntityReferenceRevisionsFieldItemList || !$definition->isTranslatable()) {
        $display->removeComponent($field->getName());
      }
    }

    $display->buildForm($paragraph, $form, $form_state);
    $form += [
      '#paragraph' => $paragraph,
      '#display' => $display,
      '#tree' => TRUE,
      '#after_build' => [
        [$this, 'afterBuild'],
      ],
    ];

    $form['#attributes']['class'][] = 'paragraphs-blokkli-paragraph-form--' . $paragraph->bundle();

    return $form;
  }

  /**
   * After build callback fixes issues with data-drupal-selector.
   *
   * See https://www.drupal.org/project/drupal/issues/2897377
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form element.
   */
  public function afterBuild(array $element, FormStateInterface $form_state) {
    return $element;
  }

  /**
   * Saves the paragraph component.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $langcode = $this->languageManager->getCurrentLanguage()->getId();
    $keys = Element::getVisibleChildren($form);

    /** @var array<string, array<string, mixed>> $updatedValues */
    $updatedValues = [];

    foreach ($keys as $key) {
      if (!str_starts_with($key, 'paragraph_')) {
        continue;
      }

      $element = $form[$key]['component'];
      /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
      $paragraph = $element['#paragraph'];
      /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface $display */
      $display = $element['#display'];

      $originalValues = $paragraph->toArray();

      /** @var \Drupal\paragraphs\Entity\Paragraph $clone */
      $clone = clone $paragraph;
      $clone->getAllBehaviorSettings();
      $clone->setNeedsSave(TRUE);
      $display->extractFormValues($clone, $element, $form_state);
      $newValues = $clone->toArray();

      $persistKeys = array_keys($form_state->getValue($key));

      $diff = BlokkliFormHelper::extractDiff($persistKeys, $originalValues, $newValues);

      if (!empty($diff)) {
        $updatedValues[$paragraph->uuid()][$langcode] = $diff;
      }
    }

    if (!empty($updatedValues)) {
      $mutation = $this->mutationPluginManager->createInstance('translate_multiple', [
        'values' => $updatedValues,
      ]);
      $this->editState->addMutation($mutation);
      $this->paragraphsBlokkliManager->saveState($this->editState);
    }

    // Redirect to redirect page.
    $redirect = Url::fromRoute('paragraphs_blokkli.redirect');
    $form_state->setRedirectUrl($redirect);
  }

}
