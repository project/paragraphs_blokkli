<?php

namespace Drupal\paragraphs_blokkli\Form;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginManager;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliHelper;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliManager;
use Drupal\paragraphs_blokkli\Traits\EditStateFormTrait;
use Drupal\text\Plugin\Field\FieldType\TextItemBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds the form to render a single rich text field of the host entity.
 */
class EditEntityRichTextFieldForm extends FormBase {

  use EditStateFormTrait;

  /**
   * The name of the field being edited.
   */
  protected string|null $fieldName = NULL;

  /**
   * The entity that is being edited.
   */
  protected FieldableEntityInterface|null $entity = NULL;

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'paragraphs_blokkli_entity_rich_text_field_form';
  }

  /**
   * {@inheritDoc}
   */
  public function __construct(
    protected ParagraphsBlokkliManager $paragraphsBlokkliManager,
    protected ParagraphsBlokkliHelper $paragraphsBlokkliHelper,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected ParagraphMutationPluginManager $mutationPluginManager,
    protected LanguageManagerInterface $languageManager,
  ) {
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('paragraphs_blokkli.manager'),
      $container->get('paragraphs_blokkli.helper'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.paragraph_mutation'),
      $container->get('language_manager'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    ?string $entity_type = NULL,
    ?string $entity_uuid = NULL,
    ?string $field_name = NULL,
  ) {
    $state = $this->loadEditState($entity_type, $entity_uuid);

    $entity = $state->getHostEntity();
    $proxies = $this->paragraphsBlokkliHelper->buildProxies($entity);
    $mutationContext = $state->executeMutations($proxies);
    $entity = $mutationContext->getHostEntity();

    if (!$entity->hasField($field_name)) {
      throw new \Exception('Invalid field name.');
    }

    $field = $entity->get($field_name);
    $item = $field->first();

    if (!$item instanceof TextItemBase) {
      throw new \Exception('Field type not supported.');
    }

    if ($entity instanceof TranslatableInterface && $entity->isTranslatable()) {
      $langcode = $this->languageManager->getCurrentLanguage()->getId();
      $defaultLanguage = $entity->getTranslation(LanguageInterface::LANGCODE_DEFAULT);
      if ($defaultLanguage->id() !== $langcode) {
        if (!$entity->hasTranslation($langcode)) {
          $entity->addTranslation($langcode, $entity->toArray());
        }

        $entity = $entity->getTranslation($langcode);
      }
    }

    $field = $entity->get($field_name);
    $this->entity = $entity;

    $display = EntityFormDisplay::collectRenderDisplay($this->entity, 'default');
    $fields = array_keys($this->entity->getFields());
    foreach ($fields as $name) {
      if ($name !== $field_name) {
        $display->removeComponent($name);
      }
    }
    $display->buildForm($this->entity, $form, $form_state);

    $form['#attached']['library'][] = 'paragraphs_blokkli/form';
    $form['#attached']['library'][] = 'paragraphs_blokkli/rich-text';
    $form['#attributes']['class'][] = 'paragraphs-blokkli-rich-text';

    return $form;
  }

  /**
   * Create the form title.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The form title.
   */
  protected function formTitle() {
    return $this->t('Edit field @name', ['@name' => $this->fieldName]);
  }

  /**
   * Saves the paragraph component.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // The form is not actually submitted through Drupal.
  }

}
