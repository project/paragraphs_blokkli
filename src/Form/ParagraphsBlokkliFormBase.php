<?php

namespace Drupal\paragraphs_blokkli\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginManager;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliHelper;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliManager;
use Drupal\paragraphs_blokkli\Traits\EditStateFormTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for paragraph forms rendered in the editor.
 */
abstract class ParagraphsBlokkliFormBase extends FormBase {

  use EditStateFormTrait;

  /**
   * The paragraph type.
   *
   * @var \Drupal\paragraphs\Entity\ParagraphsType
   */
  protected $paragraphType;

  /**
   * The paragraph.
   *
   * @var \Drupal\paragraphs\Entity\Paragraph
   */
  protected $paragraph;

  /**
   * The paragraph edit state.
   *
   * @var \Drupal\paragraphs_blokkli\EditStateInterface
   */
  protected $editState;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    protected ParagraphsBlokkliManager $paragraphsBlokkliManager,
    protected ParagraphsBlokkliHelper $paragraphsBlokkliHelper,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected ParagraphMutationPluginManager $mutationPluginManager,
    protected LanguageManagerInterface $languageManager,
    protected AccountInterface $currentUser,
  ) {
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('paragraphs_blokkli.manager'),
      $container->get('paragraphs_blokkli.helper'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.paragraph_mutation'),
      $container->get('language_manager'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'paragraphs_blokkli_paragraph_form';
  }

  /**
   * {@inheritDoc}
   */
  public function getParagraph() {
    return $this->paragraph;
  }

  /**
   * {@inheritDoc}
   */
  public function setParagraph(ParagraphInterface $paragraph) {
    $this->paragraph = $paragraph;
  }

  /**
   * Get the.
   *
   * /**
   * Builds a component (paragraph) edit form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  protected function buildComponentForm(
    array $form,
    FormStateInterface $form_state,
  ) {
    $display = EntityFormDisplay::collectRenderDisplay($this->paragraph, 'default');

    $paragraphFields = $this->paragraph->getFields();
    foreach ($paragraphFields as $field) {
      if ($field instanceof EntityReferenceRevisionsFieldItemList) {
        $display->removeComponent($field->getName());
      }
    }

    $display->buildForm($this->paragraph, $form, $form_state);
    $this->paragraphType = $this->paragraph->getParagraphType();

    $form += [
      '#title' => $this->formTitle(),
      '#paragraph' => $this->paragraph,
      '#display' => $display,
      '#tree' => TRUE,
      '#after_build' => [
        [$this, 'afterBuild'],
      ],
      '#attached' => [
        'library' => [],
      ],
      'actions' => [
        '#weight' => 100,
        '#type' => 'actions',
        'submit' => [
          '#type' => 'submit',
          '#weight' => 100,
          '#value' => $this->t('Save'),
          '#button_type' => 'primary',
        ],
      ],
    ];

    $form['#attributes']['class'][] = 'paragraphs-blokkli-paragraph-form--' . $this->paragraphType->id();

    return $form;
  }

  /**
   * Validate the component form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // Validate the paragraph with submitted form values.
    $paragraph = $this->buildParagraphComponent($form, $form_state);
    $violations = $paragraph->validate();
    // Remove violations of inaccessible fields.
    $violations->filterByFieldAccess($this->currentUser());
    // The paragraph component was validated.
    $paragraph->setValidationRequired(FALSE);
    // Flag entity level violations.
    foreach ($violations->getEntityViolations() as $violation) {

      $violationMessage = $violation->getMessage();

      $fieldName = '';
      if ($violationMessage instanceof TranslatableMarkup) {

        $arguments = $violationMessage->getArguments();

        if (isset($arguments['%field'])) {
          $fieldName = $arguments['%field'];
        }
      }

      /** @var \Symfony\Component\Validator\ConstraintViolationInterface $violation */
      $form_state->setErrorByName($fieldName, $violationMessage);
    }
    $form['#display']->flagWidgetsErrorsFromViolations($violations, $form, $form_state);
  }

  /**
   * Saves the paragraph component.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->setParagraph($this->buildParagraphComponent($form, $form_state));
  }

  /**
   * Builds the paragraph component using submitted form values.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return \Drupal\paragraphs\Entity\Paragraph
   *   The paragraph entity.
   */
  public function buildParagraphComponent(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface $display */
    $display = $form['#display'];

    $paragraph = clone $this->paragraph;
    $paragraph->getAllBehaviorSettings();
    $paragraph->setNeedsSave(TRUE);
    $display->extractFormValues($paragraph, $form, $form_state);
    return $paragraph;
  }

  /**
   * Create the form title.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The form title.
   */
  protected function formTitle() {
    return $this->t('Component form');
  }

  /**
   * After build callback fixes issues with data-drupal-selector.
   *
   * See https://www.drupal.org/project/drupal/issues/2897377
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form element.
   */
  public function afterBuild(array $element, FormStateInterface $form_state) {
    return $element;
  }

  /**
   * Access check.
   *
   * @todo Actually check something.
   *
   * @return bool
   *   True if access.
   */
  public function access() {
    return AccessResult::allowed();
  }

}
