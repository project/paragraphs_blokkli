<?php

namespace Drupal\paragraphs_blokkli\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Builds the edit form for an existing paragraph entity.
 */
class AddParagraphForm extends ParagraphsBlokkliFormBase {

  /**
   * The host entity type.
   */
  protected string $hostType;

  /**
   * The host entity UUID.
   */
  protected string $hostUuid;

  /**
   * The host entity field name.
   */
  protected string $hostFieldName;

  /**
   * UUID of the preceding paragraph.
   */
  protected string|null $precedingUuid;

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    ?string $entity_type = NULL,
    ?string $entity_uuid = NULL,
    ?string $paragraph_type = NULL,
    ?string $host_type = NULL,
    ?string $host_uuid = NULL,
    ?string $host_field_name = NULL,
    ?string $preceding_uuid = NULL,
  ) {
    $this->editState = $this->loadEditState($entity_type, $entity_uuid);

    $paragraphStorage = $this->entityTypeManager->getStorage('paragraph');
    /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
    $paragraph = $paragraphStorage->create([
      'type' => $paragraph_type,
    ]);
    $paragraph->setParentEntity($this->editState->getHostEntity(), $host_field_name);

    $this->paragraph = $paragraph;
    $this->hostType = $host_type;
    $this->hostUuid = $host_uuid;
    $this->hostFieldName = $host_field_name;
    $this->precedingUuid = $preceding_uuid;

    $form = $this->buildComponentForm($form, $form_state);
    $form['#attached']['library'][] = 'paragraphs_blokkli/form';

    return $form;
  }

  /**
   * Create the form title.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The form title.
   */
  protected function formTitle() {
    return $this->t('Add @type', ['@type' => $this->paragraph->getParagraphType()->label()]);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $redirect = Url::fromRoute('paragraphs_blokkli.redirect');
    $form_state->setRedirectUrl($redirect);

    parent::submitForm($form, $form_state);
    $mutation = $this->mutationPluginManager->createInstance('add', [
      'type' => $this->paragraph->bundle(),
      'hostType' => $this->hostType,
      'hostUuid' => $this->hostUuid,
      'hostFieldName' => $this->hostFieldName,
      'afterUuid' => $this->precedingUuid,
      'values' => $this->paragraph->toArray(),
    ]);
    $this->editState->addMutation($mutation);
    $this->paragraphsBlokkliManager->saveState($this->editState);
  }

}
