<?php

namespace Drupal\paragraphs_blokkli\Form;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\text\Plugin\Field\FieldType\TextItemBase;

/**
 * Builds the form to render a single rich text field of an existing paragraph.
 */
class EditParagraphRichTextFieldForm extends ParagraphsBlokkliFormBase {

  /**
   * Whether the paragraph is a translation.
   *
   * @var bool
   */
  protected $isTranslation = FALSE;

  /**
   * The original value of the field.
   *
   * @var string
   */
  protected string $originalValue = '';

  /**
   * {@inheritDoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    ?string $entity_type = NULL,
    ?string $entity_uuid = NULL,
    ?string $field_name = NULL,
    ?string $uuid = NULL,
  ) {
    $this->editState = $this->loadEditState($entity_type, $entity_uuid);
    $entity = $this->editState->getHostEntity();
    $proxies = $this->paragraphsBlokkliHelper->buildProxies($entity);
    $mutationContext = $this->editState->executeMutations($proxies);
    $proxy = $mutationContext->getProxy($uuid);
    if ($proxy === NULL) {
      throw new \Exception('Invalid UUID.');
    }

    $paragraph = $proxy->getParagraph();
    if (!$paragraph) {
      throw new \Exception('Failed to load paragraph from proxy.');
    }

    if (!$paragraph->hasField($field_name)) {
      throw new \Exception('Invalid field name.');
    }
    $field = $paragraph->get($field_name);
    $item = $field->first();

    if (!$item instanceof TextItemBase) {
      throw new \Exception('Field type not supported.');
    }

    $langcode = $this->languageManager->getCurrentLanguage()->getId();

    if ($paragraph->isTranslatable()) {
      $defaultLanguage = $paragraph->getTranslation(LanguageInterface::LANGCODE_DEFAULT);
      if ($defaultLanguage->id() !== $langcode) {
        $this->isTranslation = TRUE;

        if (!$paragraph->hasTranslation($langcode)) {
          $paragraph->addTranslation($langcode, $paragraph->toArray());
        }

        $paragraph = $paragraph->getTranslation($langcode);
      }
    }
    $field = $paragraph->get($field_name);
    $this->originalValue = $field->value;
    $this->paragraph = $paragraph;

    $display = EntityFormDisplay::collectRenderDisplay($this->paragraph, 'default');
    $paragraphFields = array_keys($this->paragraph->getFields());
    foreach ($paragraphFields as $name) {
      if ($name !== $field_name) {
        $display->removeComponent($name);
      }
    }
    $display->buildForm($this->paragraph, $form, $form_state);

    $form['#attached']['library'][] = 'paragraphs_blokkli/form';
    $form['#attached']['library'][] = 'paragraphs_blokkli/rich-text';
    $form['#attributes']['class'][] = 'paragraphs-blokkli-rich-text';

    return $form;
  }

  /**
   * Create the form title.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The form title.
   */
  protected function formTitle() {
    return $this->t('Edit @type', ['@type' => $this->paragraph->getParagraphType()->label()]);
  }

}
