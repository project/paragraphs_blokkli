<?php

namespace Drupal\paragraphs_blokkli\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the paragraphs blokkli edit state entity edit forms.
 */
class ParagraphsBlokkliEditStateForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $entity = $this->getEntity();

    $message_arguments = ['%label' => $entity->toLink()->toString()];
    $logger_arguments = [
      '%label' => $entity->label(),
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New paragraphs blokkli edit state %label has been created.', $message_arguments));
        $this->logger('paragraphs_blokkli')->notice('Created new paragraphs blokkli edit state %label', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The paragraphs blokkli edit state %label has been updated.', $message_arguments));
        $this->logger('paragraphs_blokkli')->notice('Updated paragraphs blokkli edit state %label.', $logger_arguments);
        break;
    }

    $form_state->setRedirect('entity.paragraphs_blokkli_edit_state.collection');

    return $result;
  }

}
