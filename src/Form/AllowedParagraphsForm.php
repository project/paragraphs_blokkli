<?php

namespace Drupal\paragraphs_blokkli\Form;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\field\Entity\FieldConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Renders a 2D matrix table of all paragraph fields and their allowed bundles.
 */
class AllowedParagraphsForm extends FormBase {

  /**
   * Constructs a new ParagraphBundleAdminForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager service.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected EntityFieldManagerInterface $entityFieldManager,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'paragraphs_blokkli_allowed_bundles_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['description'] = [
      '#markup' => '<p>' . $this->t('Select which paragraphs can be referenced in supported entity_reference_revisions fields. These are the same settings as on the configuration page for the field.') . '</p>',
    ];
    $paragraph_bundles = $this->entityTypeManager->getStorage('paragraphs_type')->loadMultiple();
    $bundle_options = array_map(function ($bundle) {
      return $bundle->label();
    }, $paragraph_bundles);

    $entity_definitions = $this->entityTypeManager->getDefinitions();
    $entity_reference_revision_fields = [];

    // Collect entity reference revision fields across all entity types and bundles.
    foreach ($entity_definitions as $entity_type => $entity_definition) {
      if (!$entity_definition->get('field_ui_base_route')) {
        continue;
      }

      $bundle_entity_type = $entity_definition->getBundleEntityType();
      if (!$bundle_entity_type) {
        continue;
      }

      $bundles = $this->entityTypeManager->getStorage($bundle_entity_type)->loadMultiple();
      foreach ($bundles as $bundle_name => $bundle) {
        $fields = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle_name);
        foreach ($fields as $field_name => $field_definition) {
          if ($field_definition->getType() === 'entity_reference_revisions' && $field_definition->getSetting('target_type') === 'paragraph') {
            $entity_reference_revision_fields["{$entity_type}.{$bundle_name}.{$field_name}"] = $field_definition;
          }
        }
      }
    }

    $filterFields = [];
    $filterBundles = [];
    $filterEntityTypes = [];

    $paragraphBundleHeaders = [];

    foreach ($bundle_options as $bundle_id => $label) {
      $paragraphBundleHeaders[] = Markup::create("<span>$bundle_id</span>");
    }

    // Build the table rows.
    $rows = [];
    foreach ($entity_reference_revision_fields as $field_key => $field_definition) {
      [$entity_type, $bundle_name, $field_name] = explode('.', $field_key);
      $allowed_bundles = array_values($field_definition->getSetting('handler_settings')['target_bundles'] ?? []);
      $row = [
        $entity_type,
        $bundle_name,
        $field_name,
      ];

      if (!in_array($field_name, $filterFields)) {
        $filterFields[] = $field_name;
      }

      if (!in_array($bundle_name, $filterBundles)) {
        $filterBundles[] = $bundle_name;
      }

      if (!in_array($entity_type, $filterEntityTypes)) {
        $filterEntityTypes[] = $entity_type;
      }

      foreach (array_keys($bundle_options) as $bundle_id) {
        $checked = in_array($bundle_id, $allowed_bundles);
        $name = "allowed_____{$entity_type}_____{$bundle_name}_____{$field_name}_____{$bundle_id}";
        $row[$name] = [
          'data' => [
            '#type' => 'checkbox',
            '#name' => $name,
            '#title' => $bundle_id,
            '#id' => $name,
            '#checked' => $checked,
          ],
        ];
      }
      $rows[$field_key] = [
        'data' => $row,
        'class' => [
          'allowed-bundles___entity_type___' . $entity_type,
          'allowed-bundles___entity_bundle___' . $bundle_name,
          'allowed-bundles___field_name___' . $field_name,
        ],
      ];
    }

    $header = array_merge([
      $this->buildHeaderFilterElement('entity_type', $this->t('Entity type'), $filterEntityTypes),
      $this->buildHeaderFilterElement('entity_bundle', $this->t('Bundle'), $filterBundles),
      $this->buildHeaderFilterElement('field_name', $this->t('Field name'), $filterFields),
    ], $paragraphBundleHeaders);

    $form['paragraph_bundle_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#tree' => TRUE,
      '#rows' => $rows,
    ];
    $form['#tree'] = TRUE;

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Save'),
      ],
    ];

    $form['#attached']['library'][] = 'paragraphs_blokkli/allowed_bundles';

    return $form;
  }

  /**
   * Build the form element for the header filter.
   *
   * @param string $id
   *   The ID.
   * @param string $label
   *   The label.
   * @param string[] $values
   *   The values.
   *
   * @return array
   *   The form element.
   */
  private function buildHeaderFilterElement(string $id, string $label, array $values): array {
    $options = array_merge([
      'all' => '-',
    ], array_combine($values, $values));
    return [
      'data' => [
        '#type' => 'select',
        '#title' => $label,
        '#id' => 'select-filter___' . $id,
        '#name' => $id,
        '#options' => $options,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getUserInput();

    $allowed = [];

    foreach ($values as $key => $value) {
      if (!str_starts_with($key, 'allowed_____')) {
        continue;
      }

      [$_, $entityType, $entityBundle, $fieldName, $paragraphBundle] = explode('_____', $key);

      $allowed[$entityType][$entityBundle][$fieldName][] = $paragraphBundle;
    }

    foreach ($allowed as $entityType => $bundles) {
      foreach ($bundles as $bundle => $fieldNames) {
        foreach ($fieldNames as $fieldName => $newAllowed) {
          /** @var \Drupal\field\Entity\FieldConfig $field */
          $field = FieldConfig::loadByName($entityType, $bundle, $fieldName);
          $currentlyAllowed = array_values($field->getSetting('handler_settings')['target_bundles'] ?? []);
          $diff1 = array_diff($newAllowed, $currentlyAllowed);
          $diff2 = array_diff($currentlyAllowed, $newAllowed);
          $symmetricDiff = array_merge($diff1, $diff2);

          // There was a change.
          if (!empty($symmetricDiff)) {
            $newTargetBundles = array_combine($newAllowed, $newAllowed);
            $handlerSettings = $field->getSetting('handler_settings');
            $handlerSettings['target_bundles'] = $newTargetBundles;
            $field->setSetting('handler_settings', $handlerSettings);
            $field->save();
          }
        }
      }
    }
  }

}
