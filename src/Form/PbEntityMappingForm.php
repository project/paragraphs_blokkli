<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli\Form;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs_blokkli\Traits\LinkOrReferenceFieldTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Pb_entity_mapping form.
 */
final class PbEntityMappingForm extends EntityForm {

  use LinkOrReferenceFieldTrait;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo $bundleInfo
   * @param \Drupal\Core\Entity\EntityFieldManager $entityFieldManager
   */
  public function __construct(
    protected EntityTypeBundleInfoInterface $bundleInfo,
    protected EntityFieldManagerInterface $entityFieldManager,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.bundle.info'),
      $container->get('entity_field.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $formState): array {
    $form = parent::form($form, $formState);

    /** @var \Drupal\paragraphs_blokkli\Entity\PbEntityMapping $entity */
    $entity = $this->entity;

    $targetEntityType = $entity->get('target_entity_type') ?? $formState->getValue('target_entity_type');
    $targetEntityBundle = $entity->get('target_entity_bundle') ?? $formState->getValue('target_entity_bundle');
    $paragraphBundle = $entity->get('paragraph_bundle') ?? $formState->getValue('target_entity_bundle');
    $paragraphField = $entity->get('paragraph_field') ?? $formState->getValue('target_entity_bundle');

    $form['mapping'] = [
      '#type' => 'container',
      '#id' => 'mapping',
      'title' => [
        '#markup' => '<p>' . $this->t('The entity mapping defines how a target entity can be added as a paragraph in blökkli. For example, when drag and dropping an image from the media library, the mapping decides which paragraph will be created to add the image and which field to use to reference the media entity.') . '</p>',
      ],
    ];

    $form['mapping']['target_entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Target entity type'),
      '#description' => $this->t('Select the entity type.'),
      '#options' => $this->getContentEntityTypeOptions(),
      '#default_value' => $targetEntityType,
      '#required' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'formCallback'],
        'disable-refocus' => FALSE,
        'event' => 'change',
        'wrapper' => 'mapping',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Loading bundles...'),
        ],
      ],
    ];

    $form['mapping']['target_entity_bundle'] = [
      '#type' => 'select',
      '#title' => $this->t('Target entity bundle'),
      '#description' => $this->t('Select the entity bundle.'),
      '#options' => $this->getBundleOptions($targetEntityType),
      '#default_value' => $targetEntityBundle,
      '#required' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'formCallback'],
        'disable-refocus' => FALSE,
        'event' => 'change',
        'wrapper' => 'mapping',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Loading fields...'),
        ],
      ],
      '#states' => [
        'visible' => [
          ':input[name="target_entity_type"]' => ['!value' => ''],
        ],
      ],
    ];

    $form['mapping']['paragraph_bundle'] = [
      '#type' => 'select',
      '#title' => $this->t('Paragraph type'),
      '#description' => $this->t('Select which paragraph bundle should be created for this target bundle.'),
      '#options' => $this->getBundleOptions('paragraph'),
      '#default_value' => $paragraphBundle,
      '#required' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'formCallback'],
        'disable-refocus' => FALSE,
        'event' => 'change',
        'wrapper' => 'mapping',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Loading fields...'),
        ],
      ],
      '#states' => [
        'visible' => [
          ':input[name="target_entity_bundle"]' => ['!value' => ''],
        ],
      ],
    ];

    $form['mapping']['paragraph_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Field'),
      '#description' => $this->t('Select which field should be used to reference the target entity. For entity reference fields the selected entity type and bundle must be able to be referenced by the field. For link fields the field must allow internal URLs.'),
      '#options' => $this->getFieldOptions($targetEntityType, $targetEntityBundle, $paragraphBundle),
      '#default_value' => $paragraphField,
      '#required' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'formCallback'],
        'disable-refocus' => FALSE,
        'event' => 'change',
        'wrapper' => 'mapping',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Loading fields...'),
        ],
      ],
      '#states' => [
        'visible' => [
          ':input[name="paragraph_bundle"]' => ['!value' => ''],
        ],
      ],
    ];

    return $form;
  }

  /**
   * The AJAX callback when a mapping field is updated.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormState $formState
   *
   * @return array
   *   The updated mapping form element.
   */
  public function formCallback(&$form, FormStateInterface $formState) {
    $formState->setRebuild();
    return $form['mapping'];
  }

  /**
   * Get all content entity types.
   *
   * @return array<string, string>
   *   The result.
   */
  private function getContentEntityTypeOptions(): array {
    $definitions = $this->entityTypeManager->getDefinitions();

    $options = [
      '' => $this->t('Select an entity type'),
    ];

    foreach ($definitions as $definition) {
      if ($definition instanceof ContentEntityTypeInterface) {
        $id = $definition->id();
        $label = $definition->getLabel();
        $options[$id] = "$label ($id)";
      }
    }

    return $options;
  }

  /**
   * Get the bundles for the given content entity type.
   *
   * @param string|null $entityType
   *   The entity type.
   *
   * @return array<string, string>
   *   The result.
   */
  private function getBundleOptions(string|null $entityType): array {
    $options = [
      '' => $this->t('Select a bundle'),
    ];

    if ($entityType) {
      $bundleInfo = $this->bundleInfo->getBundleInfo($entityType);

      foreach ($bundleInfo as $bundle => $info) {
        $label = $info['label'];
        $options[$bundle] = "$label ($bundle)";
      }
    }

    return $options;
  }

  /**
   * Get the fields for the given entity type, bundle and paragraph bundle.
   *
   * @param string|null $entityType
   *   The entity type.
   * @param string|null $entityBundle
   *   The entity bundle.
   * @param string|null $paragraphBundle
   *   The paragraph bundle.
   *
   * @return array<string, string>
   *   The options.
   */
  private function getFieldOptions(string|null $entityType, string|null $entityBundle, string|null $paragraphBundle): array {
    $options = [
      '' => $this->t('Select a field'),
    ];

    if ($entityType && $entityBundle && $paragraphBundle) {
      $fields = $this->entityFieldManager->getFieldDefinitions('paragraph', $paragraphBundle);
      foreach ($fields as $fieldName => $field) {
        if ($this->isLinkOrEntityReferenceField($field, $entityType, $entityBundle)) {
          $label = $field->getLabel();
          $fieldType = $field->getType();
          $options[$fieldName] = "$label ($fieldName) [$fieldType]";
        }
      }
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $formState): int {
    /** @var \Drupal\paragraphs_blokkli\Entity\PbEntityMapping $entity */
    $entity = $this->entity;

    $keys = [
      'target_entity_type',
      'target_entity_bundle',
      'paragraph_bundle',
      'paragraph_field',
    ];

    foreach ($keys as $key) {
      $value = $formState->getValue($key);
      $entity->set($key, $value);
    }
    $result = $entity->save();
    $message_args = ['%label' => $this->entity->label()];
    $this->messenger()->addStatus(
      match($result) {
        \SAVED_NEW => $this->t('Created new example %label.', $message_args),
        \SAVED_UPDATED => $this->t('Updated example %label.', $message_args),
      }
    );
    $formState->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

}
