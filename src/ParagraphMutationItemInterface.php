<?php

namespace Drupal\paragraphs_blokkli;

/**
 * Interface for a mutation item.
 */
interface ParagraphMutationItemInterface {

  /**
   * Get the instance of the mutation plugin.
   *
   * @return ParagraphMutationInterface
   *   The mutation plugin.
   */
  public function getMutationPlugin(): ParagraphMutationInterface;

  /**
   * Whether the mutation is enabled.
   *
   * @return bool
   *   TRUE if the mutation is enabled.
   */
  public function isEnabled(): bool;

}
