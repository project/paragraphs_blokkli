<?php

namespace Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldConfigBase;
use Drupal\Core\Field\TypedData\FieldItemDataDefinition;
use Drupal\paragraphs_blokkli\Exception\MutationBadDataException;

/**
 * Update a field value of an entity.
 */
trait UpdateFieldValueTrait {

  /**
   * Update the value of a text field on the given entity.
   *
   * @param FieldableEntityInterface $entity
   *   The entity where to replace the field value.
   * @param string $langcode
   *   The langcode.
   * @param string $fieldName
   *   The field name.
   * @param string $value
   *   The updated field value.
   */
  protected function updateTextFieldValue(FieldableEntityInterface $entity, string $langcode, string $fieldName, string $value) {
    if (!$entity->hasField($fieldName)) {
      $uuid = $entity->uuid();
      $label = $entity->label();
      throw new MutationBadDataException("Entity '$label' ($uuid) does not have a field '$fieldName'");
    }

    if ($entity instanceof TranslatableInterface && $entity->isTranslatable()) {
      if (!$entity->hasTranslation($langcode)) {
        $entity->addTranslation($langcode, $entity->toArray());
      }

      $entity = $entity->getTranslation($langcode);
    }

    $field = $entity->get($fieldName);
    $fieldDefinition = $field->getFieldDefinition();
    if (!$fieldDefinition instanceof FieldConfigBase && !$fieldDefinition instanceof BaseFieldDefinition) {
      return;
    }

    $itemDefinition = $fieldDefinition->getItemDefinition();
    if (!$itemDefinition instanceof FieldItemDataDefinition) {
      return;
    }

    // Only update the field value and do not touch the format.
    $values = $field->getValue()[0];
    $values['value'] = $value;
    $entity->set($fieldName, $values);
  }

  /**
   * Update the referenced entity on a entity reference field.
   *
   * @param FieldableEntityInterface $entity
   *   The entity where the referenced entity should be replaced.
   * @param string $langcode
   *   The langcode.
   * @param string $fieldName
   *   The field name.
   * @param string $targetEntityType
   *   The target entity type.
   * @param string $targetId
   *   The target entity ID.
   */
  protected function updateEntityReferenceField(FieldableEntityInterface $entity, string $langcode, string $fieldName, string $targetEntityType, string $targetId) {
    if (!$entity->hasField($fieldName)) {
      $uuid = $entity->uuid();
      $label = $entity->label();
      throw new MutationBadDataException("Entity '$label' ($uuid) does not have a field '$fieldName'");
    }

    if ($entity instanceof TranslatableInterface && $entity->isTranslatable()) {
      if (!$entity->hasTranslation($langcode)) {
        $entity->addTranslation($langcode, $entity->toArray());
      }

      $entity = $entity->getTranslation($langcode);
    }

    $field = $entity->get($fieldName);

    if (!$field instanceof EntityReferenceFieldItemListInterface) {
      throw new MutationBadDataException("The requested field '$fieldName' is not an entity reference field.");
    }

    $fieldSettings = $field->getFieldDefinition()->getFieldStorageDefinition()->getSettings();
    // @todo Check if target bundle is allowed.
    $field->setValue(['target_id' => $targetId]);
  }

}
