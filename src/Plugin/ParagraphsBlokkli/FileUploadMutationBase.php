<?php

namespace Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli;

use Drupal\Component\Utility\Bytes;
use Drupal\Component\Utility\Environment;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\file\FileRepositoryInterface;
use Drupal\file\FileUsage\FileUsageInterface;
use Drupal\media\MediaInterface;
use Drupal\paragraphs_blokkli\Exception\MutationBadDataException;
use Drupal\paragraphs_blokkli\Exception\MutationConfigurationException;
use Drupal\paragraphs_blokkli\Exception\MutationMissingEntityException;
use Drupal\paragraphs_blokkli\Exception\MutationStateChangedException;
use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginBase;
use Drupal\paragraphs_blokkli\ParagraphProxy;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliConfig;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliHelper;
use Drupal\paragraphs_blokkli\Traits\LinkOrReferenceFieldTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Mime\MimeTypeGuesserInterface;

/**
 * Base class for file upload mutations.
 */
abstract class FileUploadMutationBase extends ParagraphMutationPluginBase {

  use LinkOrReferenceFieldTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('uuid'),
      $container->get('paragraphs_blokkli.helper'),
      $container->get('paragraphs_blokkli.config'),
      $container->get('file.repository'),
      $container->get('renderer'),
      $container->get('file.usage'),
      $container->get('file.mime_type.guesser'),
      $container->get('file_system'),
      $container->get('rokka.service', ContainerInterface::NULL_ON_INVALID_REFERENCE),
    );
  }

  /**
   * Convert constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Component\Uuid\UuidInterface $uuidHelper
   *   The UUID helper.
   * @param \Drupal\paragraphs_blokkli\ParagraphsBlokkliHelper $paragraphsBlokkliHelper
   *   The paragraphs blokkli helper.
   * @param \Drupal\paragraphs_blokkli\ParagraphsBlokkliConfig $blokkliConfig
   *   The blokkli config.
   * @param \Drupal\file\FileRepository $fileRepository
   *   The file repository.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer.
   * @param \Drupal\file\FileUsage\FileUsageBase $fileUsage
   *   The file usage.
   * @param \Symfony\Component\Mime\MimeTypeGuesserInterface $mimeTypeGuesser
   *   The mime type guesser.
   * @param \Drupal\Core\File\FileSystemInterface $filesystem
   *   The file system.
   * @param \Drupal\rokka\RokkaService|null $rokkaService
   *   The rokka service.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    EntityTypeManagerInterface $entityTypeManager,
    UuidInterface $uuidHelper,
    ParagraphsBlokkliHelper $paragraphsBlokkliHelper,
    protected ParagraphsBlokkliConfig $blokkliConfig,
    protected FileRepositoryInterface $fileRepository,
    protected RendererInterface $renderer,
    protected FileUsageInterface $fileUsage,
    protected MimeTypeGuesserInterface $mimeTypeGuesser,
    protected FileSystemInterface $filesystem,
    protected $rokkaService,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition, $entityTypeManager, $uuidHelper, $paragraphsBlokkliHelper);
  }

  /**
   * Return the target media bundle.
   *
   * @return string
   *   The target media bundle.
   */
  abstract protected function getTargetMediaBundle(): string;

  /**
   * Return the required media type source plugin ID.
   *
   * @return string
   *   The media source plugin ID.
   */
  abstract protected function getMediaSourcePluginId(): string;

  /**
   * Executes the mutation for uploading a file and adding a paragraph.
   *
   * @param ParagraphMutationContextInterface $context
   *   The mutation context.
   * @param string $data
   *   The data representing the file.
   * @param string $fileName
   *   The name of the file.
   * @param string $hostType
   *   The type of the host entity.
   * @param string $hostUuid
   *   The UUID of the host entity.
   * @param string $hostFieldName
   *   The name of the field in the host entity.
   * @param string|null $afterUuid
   *   The UUID of the paragraph after which the new paragraph should be added.
   */
  public function execute(
    ParagraphMutationContextInterface $context,
    string $data,
    string $fileName,
    string $hostType,
    string $hostUuid,
    string $hostFieldName,
    ?string $afterUuid,
  ): void {
    $mediaBundle = $this->getTargetMediaBundle();

    /** @var \Drupal\paragraphs_blokkli\PbEntityMappingStorage $mappingStorage */
    $mappingStorage = $this->entityTypeManager->getStorage('pb_entity_mapping');

    $mapping = $mappingStorage->findMappings('media', $mediaBundle)[0] ?? NULL;

    if (!$mapping) {
      throw new MutationConfigurationException("Missing entity mapping for entity type 'media' and bundle '$mediaBundle'. To support pasting files, please add the missing entity mapping.");
    }

    // Must be wrapped in a render context or else the GraphQL mutation might fail.
    $renderContext = new RenderContext();
    /** @var \Drupal\media\Entity\Media $media */
    $media = $this->renderer->executeInRenderContext($renderContext, function () use ($data, $fileName, $mediaBundle) {
      return $this->getOrCreateMediaEntity($data, $fileName, $mediaBundle);
    });

    // Store the media entity in the configuration, so that subsequent
    // executions of the mutation don't recreate the media entity.
    $this->configuration['media_id'] = $media->id();

    // Remove the base64 data from the configuration and context so that it
    // doesn't get persisted in the database.
    $this->configuration['data'] = '';
    $this->setContextValue('data', '');

    $paragraphBundle = $mapping->getParagraphBundle();
    $paragraphFieldName = $mapping->getParagraphField();

    // Create the paragraph.
    $paragraph = $this->createNewParagraph([
      'type' => $paragraphBundle,
      'uuid' => $this->getUuidForNewEntity('paragraph'),
    ]);

    $this->setLinkOrEntityReferenceFieldValue($paragraph, $paragraphFieldName, $media);

    $proxy = new ParagraphProxy($paragraph, $hostType, $hostUuid, $hostFieldName);
    $context->addProxy($proxy, $afterUuid);
  }

  /**
   * Get or create the media entity.
   *
   * @param string $data
   *   The binary file data.
   * @param string $fileName
   *   The file name.
   * @param string $mediaBundle
   *   The media bundle to create.
   */
  private function getOrCreateMediaEntity(string $data, string $fileName, string $mediaBundle): MediaInterface {
    $mid = $this->configuration['media_id'] ?? NULL;
    $mediaStorage = $this->entityTypeManager->getStorage('media');

    // If a media entity was previously created, try to load it.
    if ($mid) {
      $media = $mediaStorage->load($mid);
      if ($media) {
        // If the configuration has changed, the media entity may not be used anymore.
        if ($media->bundle() !== $mediaBundle) {
          throw new MutationStateChangedException("The previously created media bundle is not valid.");
        }

        return $media;
      }

      throw new MutationMissingEntityException('media', $mid);
    }

    /** @var \Drupal\media\Entity\MediaType $mediaType */
    $mediaType = $this->entityTypeManager->getStorage('media_type')->load($mediaBundle);
    $mediaSource = $mediaType->getSource();

    $requiredSourceId = $this->getMediaSourcePluginId();
    if ($mediaSource->getPluginId() !== $requiredSourceId) {
      throw new MutationConfigurationException("The configured media bundle '$mediaBundle' must use the '$requiredSourceId' source to support creating files from clipboard.");
    }

    $sourceField = $mediaSource->getSourceFieldDefinition($mediaType);

    if (!$sourceField) {
      throw new MutationConfigurationException("Missing source field for media bundle ''");
    }

    $sourceFieldName = $sourceField->getName();

    // Get field storage settings for source field.
    $settings = $sourceField->getSettings();
    $scheme = $settings['uri_scheme'];
    $destination = $scheme . '://' . trim($settings['file_directory'], '/');

    $matches = [];

    // Extract the parts of the Data URI.
    if (!preg_match('/data:(?P<mime>[^;]+);(?P<encoding>\w+),(?P<data>.+)$/', $data, $matches)) {
      throw new MutationBadDataException('Failed to create media entity from given data. The data for creating files must be a valid base64 encoded file.');
    }

    $data = $matches['data'];
    $encoding = $matches['encoding'];

    // The data is base64 encoded so that we don't need to handle file uploads
    // via GraphQL, which can be tricky.
    if ($encoding !== 'base64') {
      throw new MutationBadDataException('The provided file data should be base64 encoded.');
    }

    $decoded = base64_decode($data);

    if ($decoded === FALSE) {
      throw new MutationBadDataException("Failed to base64 decode file from clipboard data.");
    }

    $isRokka = $scheme === 'rokka';

    // If possible, try to find an existing media entity using the binary hash from rokka.
    if ($isRokka) {
      $media = $this->getMediaFromRokka($decoded, $mediaBundle);

      if ($media) {
        return $media;
      }
    }

    $allowedFileExtension = explode(' ', $settings['file_extensions'] ?: '');

    $maxFilesize = Bytes::toNumber(Environment::getUploadMaxSize());
    if (!empty($settings['max_filesize'])) {
      $maxFilesize = min($maxFilesize, Bytes::toNumber($settings['max_filesize']));
    }

    // Check file size.
    if ($maxFilesize && strlen($decoded) > $maxFilesize) {
      throw new MutationBadDataException('The uploaded file is larger than the allowed size.');
    }

    // Check extension.
    $finfo = new \finfo(FILEINFO_MIME_TYPE);
    $actualMimeType = $finfo->buffer($decoded);
    $allowedMimeTypesExtension = [];
    foreach ($allowedFileExtension as $extension) {
      $allowedMimeTypesExtension[] = $this->mimeTypeGuesser->guessMimeType('filename.' . $extension);
    }

    if (in_array($actualMimeType, $allowedMimeTypesExtension) === FALSE) {
      throw new MutationBadDataException('The extension is not allowed.');
    }

    if (!$this->filesystem->prepareDirectory($destination, FileSystemInterface::CREATE_DIRECTORY)) {
      throw new MutationBadDataException('The directory is not writable.');
    }
    $file = $this->fileRepository->writeData($decoded, $destination . '/' . $fileName);

    $data = [
      'bundle' => $mediaBundle,
      'uuid' => $this->getUuidForNewEntity('media'),
    ];

    // Create media entity for the remote video.
    /** @var \Drupal\media\Entity\Media $media */
    $media = $mediaStorage->create($data);

    $fieldData = [
      'target_id' => $file->id(),
        // @todo This is not ideal. Should be provided together with the mutation.
        // When using rokka, this is later overriden with the automatically
        // generated alt text.
      'alt' => $fileName,
    ];

    if ($isRokka && $this->rokkaService) {
      $metadata = $this->rokkaService->loadRokkaMetadataByUri($file->getFileUri());
      if (!empty($metadata)) {
        /** @var \Drupal\rokka\Entity\RokkaMetadata|false $metadata */
        $metadata = reset($metadata);
        if ($metadata && $staticMetadata = $metadata->getStaticMetadata()) {
          $staticMetadata = reset($staticMetadata);
          // Use the default langcode of the entity.
          $langcode = $media->language()->getId();
          $autoDescription = $staticMetadata['auto_description']['descriptions'][$langcode] ?? NULL;
          if ($autoDescription) {
            $fieldData['alt'] = $autoDescription;
          }
        }
      }
    }

    // Set the file field.
    $media->set($sourceFieldName, $fieldData);

    // The media entity must be saved at this point, because the media
    // library widget throws an error when it tries to generate a URL.
    // But this is pretty much the same behavior when using the media
    // library on a node form: Creating a new media there will also save it
    // instantly.
    $media->save();

    return $media;
  }

  /**
   * Try to get an existing media entity from rokka if possible.
   *
   * @param string $decodedData
   *   The decoded data.
   * @param string $mediaBundle
   *   The desired media bundle.
   *
   * @return \Drupal\media\Entity\Media|null
   *   The media entity if found.
   */
  protected function getMediaFromRokka(string $decodedData, string $mediaBundle): MediaInterface|null {
    if (!$this->rokkaService) {
      return NULL;
    }
    $binaryHash = sha1($decodedData);

    // Based on the binary hash, we check if the file already exists in the system.
    // If we find it, we determine the file, then the media entity and then reuse it.
    $metadataList = $this->rokkaService->loadRokkaMetadataByBinaryHash($binaryHash);
    if (!$metadataList) {
      return NULL;
    }
    /** @var \Drupal\rokka\Entity\RokkaMetadata $metadata */
    $metadata = reset($metadataList);

    /** @var \Drupal\file\Entity\File|null $file */
    $file = array_values($this->entityTypeManager
      ->getStorage('file')
      ->loadByProperties(['uri' => $metadata->getUri()])
    )[0] ?? NULL;

    if (!$file) {
      return NULL;
    }

    $result = $this->fileUsage->listUsage($file);
    if (!empty($result)) {
      $media = $this->findMediaInUsages($result, $mediaBundle);
      if ($media) {
        return $media;
      }
    }

    return NULL;
  }

  /**
   * Find a matching media in the usages.
   *
   * @param array $result
   *   The usage result.
   * @param string $mediaBundle
   *   The expected media bundle.
   *
   * @return \Drupal\media\Entity\Media|null
   *   The media entity if found.
   */
  private function findMediaInUsages(array $result, string $mediaBundle): MediaInterface|null {
    foreach ($result as $usages) {
      foreach ($usages as $usage) {
        if (is_array($usage)) {
          $media_id = key($usage);
          $media = $this->entityTypeManager->getStorage('media')->load($media_id);
          if ($media && $media->bundle() === $mediaBundle) {
            return $media;
          }
        }
      }
    }

    return NULL;
  }

}
