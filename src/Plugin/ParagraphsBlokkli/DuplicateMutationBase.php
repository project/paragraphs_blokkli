<?php

namespace Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli;

use Drupal\paragraphs_blokkli\Exception\MutationMissingEntityException;
use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginBase;
use Drupal\paragraphs_blokkli\ParagraphProxy;

/**
 * Base class for mutations that duplicate paragraphs.
 */
class DuplicateMutationBase extends ParagraphMutationPluginBase {

  /**
   * Duplicate the paragraph.
   *
   * @param ParagraphMutationContextInterface $sourceContext
   *   The paragraph mutation context from which to duplicate the paragraph.
   * @param ParagraphMutationContextInterface $targetContext
   *   The paragraph mutation context to which the clone should be added.
   * @param string $uuid
   *   The UUID of the paragraph to duplicate.
   * @param string|null $preceedingUuid
   *   The UUID of the preceeding paragraph.
   *
   * @return ParagraphProxy
   *   The proxy of the clone.
   */
  protected function duplicateParagraph(
    ParagraphMutationContextInterface $sourceContext,
    ParagraphMutationContextInterface $targetContext,
    string $uuid,
    ?string $preceedingUuid = NULL,
  ): ParagraphProxy {
    $proxy = $sourceContext->getProxy($uuid);
    if ($proxy === NULL) {
      throw new MutationMissingEntityException('paragraph', NULL, $uuid);
    }

    $paragraph = $proxy->getParagraph();
    if (!$paragraph) {
      throw new MutationMissingEntityException('paragraph', NULL, $uuid);
    }
    $clone = $paragraph->createDuplicate();
    $cloneUuid = $this->getUuidForNewEntity($paragraph->uuid());
    $clone->set('uuid', $cloneUuid);

    // Array of all proxies that belong to the cloned paragraph.
    $children = $sourceContext->getProxiesForHost(
      'paragraph',
      $proxy->uuid()
    );

    foreach ($children as $childProxy) {
      // Create a new UUID for the cloned child proxy.
      $newUuid = $this->getUuidForNewEntity($childProxy->uuid());
      $clonedChildParagraph = $childProxy->getParagraph()->createDuplicate();
      $clonedChildParagraph->set('uuid', $newUuid);
      $clonedChildProxy = new ParagraphProxy(
        $clonedChildParagraph,
        $childProxy->getHostEntityType(),
        $cloneUuid,
        $childProxy->getHostFieldName(),
      );
      $clonedChildProxy->setBehaviorSettings($childProxy->getBehaviorSettings());
      $targetContext->appendProxy($clonedChildProxy);
    }

    $cloneProxy = new ParagraphProxy(
      $clone,
      $proxy->getHostEntityType(),
      $proxy->getHostUuid(),
      $proxy->getHostFieldName()
    );
    $cloneProxy->setBehaviorSettings($proxy->getBehaviorSettings());
    $targetContext->addProxy($cloneProxy, $preceedingUuid ?: $proxy->uuid());
    return $cloneProxy;
  }

}
