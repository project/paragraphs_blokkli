<?php

namespace Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\paragraphs_blokkli\Exception\MutationBadDataException;
use Drupal\paragraphs_blokkli\Exception\MutationMissingEntityException;
use Drupal\paragraphs_blokkli\ParagraphMutationContext;
use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\DuplicateMutationBase;

/**
 * Copy paragraphs from an existing entity.
 *
 * Only paragraphs from the same entity type and bundle can be copied.
 *
 * @ParagraphMutation(
 *   id = "copy_from_existing",
 *   label = @Translation("Copy paragraphs from an existing entity."),
 *   description = @Translation("Copies all paragraphs from an existing entity."),
 *   arguments = {
 *     "sourceUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the entity from which to copy the paragraphs."),
 *     ),
 *     "fields" = @ContextDefinition("string",
 *       label = @Translation("The names of the fields to copy."),
 *       multiple = TRUE,
 *     ),
 *   }
 * )
 */
class CopyFromExisting extends DuplicateMutationBase {

  /**
   * Execute the mutation.
   *
   * @param ParagraphMutationContextInterface $context
   *   The paragraph mutation context.
   * @param string $sourceUuid
   *   The UUID of the entity from which to copy the paragraphs.
   * @param string[] $fields
   *   The names of the paragraph fields (entity reference revisions) to copy.
   */
  public function execute(
    ParagraphMutationContextInterface $context,
    string $sourceUuid,
    array $fields,
  ): void {
    $hostEntity = $context->getHostEntity();

    if ($hostEntity->uuid() === $sourceUuid) {
      throw new MutationBadDataException('Can not copy from same host entity.');
    }

    $entityType = $hostEntity->getEntityTypeId();
    $storage = $this->entityTypeManager->getStorage($entityType);
    $sourceEntity = array_values($storage->loadByProperties(['uuid' => $sourceUuid]))[0] ?? NULL;
    if (!$sourceEntity) {
      throw new MutationMissingEntityException($entityType, NULL, $sourceUuid);
    }

    if (!($sourceEntity instanceof FieldableEntityInterface)) {
      throw new MutationBadDataException('The source entity to copy from must be fieldable.');
    }

    if ($sourceEntity->bundle() !== $hostEntity->bundle()) {
      throw new MutationBadDataException('The bundle of the entity to copy from must be the same as the target entity.');
    }

    $sourceProxies = $this->paragraphsBlokkliHelper->buildProxies($sourceEntity);
    $sourceContext = new ParagraphMutationContext($sourceEntity, $sourceProxies);

    $prevUuid = NULL;
    foreach ($sourceProxies as $sourceProxy) {
      // Skip paragraphs that don't have the source entity as their host.
      if ($sourceProxy->getHostUuid() !== $sourceUuid) {
        continue;
      }

      // Skip paragraphs not in a field that should be copied.
      if (!in_array($sourceProxy->getHostFieldName(), $fields)) {
        continue;
      }

      $cloneProxy = $this->duplicateParagraph($sourceContext, $context, $sourceProxy->uuid());
      if (!$cloneProxy) {
        continue;
      }

      $cloneProxy->setHostUuid($hostEntity->uuid());
      if ($prevUuid) {
        $context->moveProxyAfter($cloneProxy->uuid(), $prevUuid);
      }
      $prevUuid = $cloneProxy->uuid();
    }
  }

}
