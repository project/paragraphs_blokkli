<?php

namespace Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\paragraphs_blokkli\Exception\MutationMissingEntityException;
use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginBase;
use Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\UpdateFieldValueTrait;

/**
 * Replace the referenced media on a entity reference field.
 *
 * @ParagraphMutation(
 *   id = "replace_media",
 *   label = @Translation("Replace Media"),
 *   description = @Translation("Replace referenced media."),
 *   arguments = {
 *     "uuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the paragraph.")
 *     ),
 *     "langcode" = @ContextDefinition("string",
 *       label = @Translation("The langcode.")
 *     ),
 *     "fieldName" = @ContextDefinition("string",
 *       label = @Translation("The name of the field.")
 *     ),
 *     "mediaId" = @ContextDefinition("string",
 *       label = @Translation("The new media ID.")
 *     ),
 *   }
 * )
 */
class ReplaceMedia extends ParagraphMutationPluginBase {

  use UpdateFieldValueTrait;

  /**
   * Execute the mutation.
   *
   * @param ParagraphMutationContextInterface $context
   *   The paragraph mutation context.
   * @param string $uuid
   *   The UUID of the paragraph.
   * @param string $langcode
   *   The langcode.
   * @param string $fieldName
   *   The field name.
   * @param string $mediaId
   *   The new media ID.
   */
  public function execute(ParagraphMutationContextInterface $context, string $uuid, string $langcode, string $fieldName, string $mediaId): void {
    $proxy = $context->getProxy($uuid);
    if ($proxy === NULL) {
      throw new MutationMissingEntityException('paragraph', NULL, $uuid);
    }

    if ($context->isDeleted($uuid)) {
      return;
    }

    $paragraph = $proxy->getParagraph();

    if (!$paragraph) {
      return;
    }

    $this->updateEntityReferenceField($paragraph, $langcode, $fieldName, 'media', $mediaId);
    $paragraph->setNeedsSave(TRUE);
  }

}
