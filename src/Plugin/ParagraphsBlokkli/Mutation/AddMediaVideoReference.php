<?php

namespace Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\paragraphs_blokkli\Exception\MutationConfigurationException;
use Drupal\paragraphs_blokkli\Exception\MutationMissingEntityException;
use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginBase;
use Drupal\paragraphs_blokkli\ParagraphProxy;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliConfig;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Adds a new video paragraph from an ID.
 *
 * @ParagraphMutation(
 *   id = "add_media_video_reference",
 *   label = @Translation("Add Media Video Paragraph"),
 *   description = @Translation("Adds a new paragraph of type video for an ID"),
 *   arguments = {
 *     "id" = @ContextDefinition("string",
 *       label = @Translation("The ID of the media video."),
 *     ),
 *     "hostType" = @ContextDefinition("string",
 *       label = @Translation("The entity type of the target host.")
 *     ),
 *     "hostUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the target host.")
 *     ),
 *     "hostFieldName" = @ContextDefinition("string",
 *       label = @Translation("The field name of the target host.")
 *     ),
 *     "afterUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the paragraph after which to add this one."),
 *       required = FALSE,
 *     ),
 *   }
 * )
 */
class AddMediaVideoReference extends ParagraphMutationPluginBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('uuid'),
      $container->get('paragraphs_blokkli.helper'),
      $container->get('paragraphs_blokkli.config'),
    );
  }

  /**
   * Convert constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param UuidInterface $uuidHelper
   *   The UUID helper.
   * @param ParagraphsBlokkliHelper $paragraphsBlokkliHelper
   *   The paragraphs blokkli helper.
   * @param \Drupal\paragraphs_blokkli\ParagraphsBlokkliConfig $blokkliConfig
   *   The blokkli config.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    EntityTypeManagerInterface $entityTypeManager,
    UuidInterface $uuidHelper,
    ParagraphsBlokkliHelper $paragraphsBlokkliHelper,
    protected ParagraphsBlokkliConfig $blokkliConfig,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition, $entityTypeManager, $uuidHelper, $paragraphsBlokkliHelper);
  }

  /**
   * Executes the method.
   *
   * @param \Drupal\paragraphs_blokkli\ParagraphMutationContextInterface $context
   *   The context object for the mutation operation.
   * @param string $id
   *   The ID of the media.
   * @param string $hostType
   *   The type of the host entity.
   * @param string $hostUuid
   *   The UUID of the host entity.
   * @param string $hostFieldName
   *   The field name of the host entity.
   * @param string|null $afterUuid
   *   The UUID of the paragraph after which the new paragraph should be added.
   */
  public function execute(
    ParagraphMutationContextInterface $context,
    string $id,
    string $hostType,
    string $hostUuid,
    string $hostFieldName,
    ?string $afterUuid,
  ): void {
    $media = $this->entityTypeManager->getStorage('media')->load($id);
    if (!$media) {
      throw new MutationMissingEntityException('media', $id);
    }
    $videoRemoteMediaBundle = $this->blokkliConfig->getMediaBundleRemoteVideo();

    if (!$videoRemoteMediaBundle) {
      throw new MutationConfigurationException("Missing 'Media Bundle Remote Video' configuration. In order to add remote video media references, please configure which media bundle to use.");
    }
    /** @var \Drupal\paragraphs_blokkli\PbEntityMappingStorage $mappingStorage */
    $mappingStorage = $this->entityTypeManager->getStorage('pb_entity_mapping');
    $mapping = $mappingStorage->findMappings('media', $videoRemoteMediaBundle)[0] ?? NULL;

    if (!$mapping) {
      throw new MutationConfigurationException("Missing entity mapping for entity type 'media' and bundle '$videoRemoteMediaBundle'");
    }

    $paragraphBundle = $mapping->getParagraphBundle();
    $paragraphField = $mapping->getParagraphField();

    $paragraph = $this->createNewParagraph([
      'type' => $paragraphBundle,
      'uuid' => $this->getUuidForNewEntity(),
      $paragraphField => [
        'target_id' => $id,
      ],
    ]);
    $proxy = new ParagraphProxy($paragraph, $hostType, $hostUuid, $hostFieldName);
    $context->addProxy($proxy, $afterUuid);
  }

}
