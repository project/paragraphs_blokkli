<?php

namespace Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginBase;

/**
 * Move multiple paragraphs to another position.
 *
 * @ParagraphMutation(
 *   id = "move_multiple",
 *   label = @Translation("Move multiple"),
 *   description = @Translation("Move multiple paragraphs to another position."),
 *   arguments = {
 *     "uuids" = @ContextDefinition("string",
 *       label = @Translation("The UUIDs of the paragraphs being moved."),
 *       multiple = TRUE,
 *     ),
 *     "hostType" = @ContextDefinition("string",
 *       label = @Translation("The entity type of the target host.")
 *     ),
 *     "hostUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the target host.")
 *     ),
 *     "hostFieldName" = @ContextDefinition("string",
 *       label = @Translation("The field name of the target host.")
 *     ),
 *     "afterUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the preceeding paragraph."),
 *       required = FALSE,
 *     ),
 *   }
 * )
 */
class MoveMultiple extends ParagraphMutationPluginBase {

  /**
   * {@inheritdoc}
   */
  public function label() {
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * Execute the mutation.
   *
   * @param ParagraphMutationContextInterface $context
   *   The paragraph mutation context.
   * @param array $uuids
   *   The UUIDs of the paragraphs to move.
   * @param string $hostType
   *   The new host type.
   * @param string $hostUuid
   *   The new host UUID.
   * @param string $hostFieldName
   *   The new host field name.
   * @param string|null $afterUuid
   *   After which paragraph the paragraphs should be moved to.
   */
  public function execute(
    ParagraphMutationContextInterface $context,
    array $uuids,
    string $hostType,
    string $hostUuid,
    string $hostFieldName,
    ?string $afterUuid,
  ): void {
    $after = $afterUuid;
    foreach ($uuids as $uuid) {
      $proxy = $context->getProxy($uuid);
      if (!$proxy || $context->isDeleted($uuid)) {
        continue;
      }

      $proxy
        ->setHostUuid($hostUuid)
        ->setHostEntityType($hostType)
        ->setHostFieldName($hostFieldName);

      if ($after) {
        $context->moveProxyAfter($uuid, $after);
        $after = $uuid;
        continue;
      }

      $proxy = $context->removeProxy($uuid);
      $context->addProxy($proxy);
      $after = $uuid;
    }
  }

}
