<?php

namespace Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\paragraphs_blokkli\Exception\MutationBadDataException;
use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\DuplicateMutationBase;

/**
 * Duplicate an existing paragraph.
 *
 * @ParagraphMutation(
 *   id = "duplicate_multiple",
 *   label = @Translation("Duplicate Multiple"),
 *   description = @Translation("Duplicates multiple paragraphs."),
 *   arguments = {
 *     "uuids" = @ContextDefinition("string",
 *       label = @Translation("The UUIDs of the paragraphs being duplicated."),
 *       multiple = TRUE,
 *     ),
 *     "afterUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the paragraph after which to duplicate this one."),
 *       required = FALSE,
 *     ),
 *   }
 * )
 */
class DuplicateMultiple extends DuplicateMutationBase {

  /**
   * Execute the mutation.
   *
   * @param ParagraphMutationContextInterface $context
   *   The paragraph mutation context.
   * @param string[] $uuids
   *   The UUIDs of the paragraphs to duplicate.
   * @param string|null $afterUuid
   *   The UUID after which the duplicated paragraphs should be added.
   */
  public function execute(ParagraphMutationContextInterface $context, array $uuids, string|null $afterUuid): void {
    if (empty($uuids)) {
      throw new MutationBadDataException('At least one UUID should be provided when duplication paragraphs.');
    }

    // We have to sort the UUIDs by their index, so that they are duplicated in the right order.
    $sorted = $uuids;
    usort($sorted, function ($a, $b) use ($context) {
      $indexA = $context->getIndex($a);
      $indexB = $context->getIndex($b);

      return $indexA <=> $indexB;
    });
    $preceeding = $afterUuid ?? $sorted[count($sorted) - 1];
    foreach ($sorted as $uuid) {
      $proxy = $this->duplicateParagraph($context, $context, $uuid, $preceeding);
      if ($proxy) {
        $preceeding = $proxy->uuid();
      }
    }
  }

}
