<?php

namespace Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginBase;

/**
 * Move a paragraph to another position.
 *
 * @ParagraphMutation(
 *   id = "move",
 *   label = @Translation("Move Paragraph"),
 *   description = @Translation("PMove a paragraph to another position."),
 *   arguments = {
 *     "uuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the paragraph being moved.")
 *     ),
 *     "hostType" = @ContextDefinition("string",
 *       label = @Translation("The entity type of the target host.")
 *     ),
 *     "hostUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the target host.")
 *     ),
 *     "hostFieldName" = @ContextDefinition("string",
 *       label = @Translation("The field name of the target host.")
 *     ),
 *     "afterUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the preceeding paragraph."),
 *       required = FALSE,
 *     ),
 *   }
 * )
 */
class Move extends ParagraphMutationPluginBase {

  /**
   * {@inheritdoc}
   */
  public function label() {
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * Execute the mutation.
   *
   * @param ParagraphMutationContextInterface $context
   *   The paragraph mutation context.
   * @param string $uuid
   *   The UUID of the paragraph to move.
   * @param string $hostType
   *   The new host type.
   * @param string $hostUuid
   *   The new host UUID.
   * @param string $hostFieldName
   *   The new host field name.
   * @param string|null $afterUuid
   *   After which paragraph to move the paragraph.
   */
  public function execute(
    ParagraphMutationContextInterface $context,
    string $uuid,
    string $hostType,
    string $hostUuid,
    string $hostFieldName,
    ?string $afterUuid,
  ): void {
    $proxy = $context->getProxy($uuid);

    if (!$proxy) {
      return;
    }

    if ($context->isDeleted($uuid)) {
      return;
    }

    $proxy
      ->setHostUuid($hostUuid)
      ->setHostEntityType($hostType)
      ->setHostFieldName($hostFieldName);

    if (!$afterUuid) {
      $proxy = $context->removeProxy($uuid);
      $context->addProxy($proxy);
      return;
    }

    $context->moveProxyAfter($uuid, $afterUuid);
  }

}
