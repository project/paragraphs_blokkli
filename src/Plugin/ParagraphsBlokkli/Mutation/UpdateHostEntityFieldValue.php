<?php

namespace Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginBase;
use Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\UpdateFieldValueTrait;

/**
 * Update a field value on the host entity.
 *
 * @ParagraphMutation(
 *   id = "update_host_entity_field_value",
 *   label = @Translation("Update Host Entity Field Value"),
 *   description = @Translation("Update a single field value on the host entity."),
 *   arguments = {
 *     "langcode" = @ContextDefinition("string",
 *       label = @Translation("The langcode.")
 *     ),
 *     "fieldName" = @ContextDefinition("string",
 *       label = @Translation("The name of the field.")
 *     ),
 *     "value" = @ContextDefinition("string",
 *       label = @Translation("The updated field value.")
 *     ),
 *   }
 * )
 */
class UpdateHostEntityFieldValue extends ParagraphMutationPluginBase {

  use UpdateFieldValueTrait;

  /**
   * Execute the mutation.
   *
   * @param ParagraphMutationContextInterface $context
   *   The paragraph mutation context.
   * @param string $langcode
   *   The langcode.
   * @param string $fieldName
   *   The field name.
   * @param string $value
   *   The updated field value.
   */
  public function execute(ParagraphMutationContextInterface $context, string $langcode, string $fieldName, string $value): void {
    $hostEntity = $context->getHostEntity();
    $this->updateTextFieldValue($hostEntity, $langcode, $fieldName, $value);
  }

}
