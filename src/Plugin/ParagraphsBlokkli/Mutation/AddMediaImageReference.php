<?php

namespace Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\paragraphs_blokkli\Exception\MutationConfigurationException;
use Drupal\paragraphs_blokkli\Exception\MutationMissingEntityException;
use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginBase;
use Drupal\paragraphs_blokkli\ParagraphProxy;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliConfig;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Adds a new image paragraph from an ID.
 *
 * @ParagraphMutation(
 *   id = "add_media_image_reference",
 *   label = @Translation("Add Media Image Paragraph"),
 *   description = @Translation("Adds a new paragraph of type image for an ID"),
 *   arguments = {
 *     "id" = @ContextDefinition("string",
 *       label = @Translation("The ID of the media image."),
 *     ),
 *     "hostType" = @ContextDefinition("string",
 *       label = @Translation("The entity type of the target host.")
 *     ),
 *     "hostUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the target host.")
 *     ),
 *     "hostFieldName" = @ContextDefinition("string",
 *       label = @Translation("The field name of the target host.")
 *     ),
 *     "afterUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the paragraph after which to add this one."),
 *       required = FALSE,
 *     ),
 *   }
 * )
 */
class AddMediaImageReference extends ParagraphMutationPluginBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('uuid'),
      $container->get('paragraphs_blokkli.helper'),
      $container->get('paragraphs_blokkli.config'),
    );
  }

  /**
   * Constructs a new AddMediaImageReference mutation plugin.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Component\Uuid\UuidInterface $uuidHelper
   *   The UUID helper.
   * @param \Drupal\paragraphs_blokkli\ParagraphsBlokkliHelper $paragraphsBlokkliHelper
   *   The paragraphs blokkli helper.
   * @param \Drupal\paragraphs_blokkli\ParagraphsBlokkliConfig $blokkliConfig
   *   The blokkli config.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    EntityTypeManagerInterface $entityTypeManager,
    UuidInterface $uuidHelper,
    ParagraphsBlokkliHelper $paragraphsBlokkliHelper,
    protected ParagraphsBlokkliConfig $blokkliConfig,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition, $entityTypeManager, $uuidHelper, $paragraphsBlokkliHelper);
  }

  /**
   * Executes the paragraph mutation process.
   *
   * @param \Drupal\paragraphs_blokkli\ParagraphMutationContextInterface $context
   *   The context for the paragraph mutation.
   * @param string $id
   *   The ID of the media entity to be processed.
   * @param string $hostType
   *   The type of the host entity.
   * @param string $hostUuid
   *   The UUID of the host entity.
   * @param string $hostFieldName
   *   The name of the field on the host entity where the paragraph will be attached.
   * @param string|null $afterUuid
   *   (optional) The UUID of the paragraph that this new paragraph should be added after.
   */
  public function execute(
    ParagraphMutationContextInterface $context,
    string $id,
    string $hostType,
    string $hostUuid,
    string $hostFieldName,
    ?string $afterUuid,
  ): void {
    $media = $this->entityTypeManager->getStorage('media')->load($id);
    if (!$media) {
      throw new MutationMissingEntityException('media', $id);
    }
    $imageMediaBundle = $this->blokkliConfig->getMediaBundleImage();

    if (!$imageMediaBundle) {
      throw new MutationConfigurationException("Missing 'Media Bundle Image' configuration. In order to add media image references, please configure which media bundle to use.");
    }
    /** @var \Drupal\paragraphs_blokkli\PbEntityMappingStorage $mappingStorage */
    $mappingStorage = $this->entityTypeManager->getStorage('pb_entity_mapping');
    $mapping = $mappingStorage->findMappings('media', $imageMediaBundle)[0] ?? NULL;

    if (!$mapping) {
      throw new MutationConfigurationException("Missing entity mapping for entity type 'media' and bundle '$imageMediaBundle'");
    }

    $paragraphBundle = $mapping->getParagraphBundle();
    $paragraphField = $mapping->getParagraphField();

    $paragraph = $this->createNewParagraph([
      'type' => $paragraphBundle,
      'uuid' => $this->getUuidForNewEntity(),
      $paragraphField => [
        'target_id' => $id,
      ],
    ]);
    $proxy = new ParagraphProxy($paragraph, $hostType, $hostUuid, $hostFieldName);
    $context->addProxy($proxy, $afterUuid);
  }

}
