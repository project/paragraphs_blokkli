<?php

namespace Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\paragraphs_blokkli\Exception\MutationMissingEntityException;
use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginBase;
use Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\UpdateFieldValueTrait;

/**
 * Update a field value of a paragraph.
 *
 * @ParagraphMutation(
 *   id = "update_field_value",
 *   label = @Translation("Update Field Value"),
 *   description = @Translation("Update a single field value of a paragraph."),
 *   arguments = {
 *     "uuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the paragraph.")
 *     ),
 *     "langcode" = @ContextDefinition("string",
 *       label = @Translation("The langcode.")
 *     ),
 *     "fieldName" = @ContextDefinition("string",
 *       label = @Translation("The name of the field.")
 *     ),
 *     "value" = @ContextDefinition("string",
 *       label = @Translation("The updated field value.")
 *     ),
 *   }
 * )
 */
class UpdateFieldValue extends ParagraphMutationPluginBase {

  use UpdateFieldValueTrait;

  /**
   * Execute the mutation.
   *
   * @param ParagraphMutationContextInterface $context
   *   The paragraph mutation context.
   * @param string $uuid
   *   The UUID of the paragraph.
   * @param string $langcode
   *   The langcode.
   * @param string $fieldName
   *   The field name.
   * @param string $value
   *   The updated field value.
   */
  public function execute(ParagraphMutationContextInterface $context, string $uuid, string $langcode, string $fieldName, string $value): void {
    $proxy = $context->getProxy($uuid);
    if ($proxy === NULL) {
      throw new MutationMissingEntityException('paragraph', NULL, $uuid);
    }

    if ($context->isDeleted($uuid)) {
      return;
    }

    $paragraph = $proxy->getParagraph();

    if (!$paragraph) {
      return;
    }

    $this->updateTextFieldValue($paragraph, $langcode, $fieldName, $value);
    $paragraph->setNeedsSave(TRUE);
  }

}
