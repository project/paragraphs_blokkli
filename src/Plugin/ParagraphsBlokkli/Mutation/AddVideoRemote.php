<?php

namespace Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\media\MediaInterface;
use Drupal\paragraphs_blokkli\Exception\MutationConfigurationException;
use Drupal\paragraphs_blokkli\Exception\MutationStateChangedException;
use Drupal\paragraphs_blokkli\Exception\MutationViolationException;
use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginBase;
use Drupal\paragraphs_blokkli\ParagraphProxy;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliConfig;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Adds a new remote video paragraph from a video URL.
 *
 * @ParagraphMutation(
 *   id = "add_video_remote",
 *   label = @Translation("Add Video (Remote)"),
 *   description = @Translation("Adds a new paragraph of type Video (Remote)."),
 *   arguments = {
 *     "url" = @ContextDefinition("string",
 *       label = @Translation("The video URL."),
 *     ),
 *     "hostType" = @ContextDefinition("string",
 *       label = @Translation("The entity type of the target host.")
 *     ),
 *     "hostUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the target host.")
 *     ),
 *     "hostFieldName" = @ContextDefinition("string",
 *       label = @Translation("The field name of the target host.")
 *     ),
 *     "afterUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the paragraph after which to add this one."),
 *       required = FALSE,
 *     ),
 *   }
 * )
 */
class AddVideoRemote extends ParagraphMutationPluginBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('uuid'),
      $container->get('paragraphs_blokkli.helper'),
      $container->get('renderer'),
      $container->get('paragraphs_blokkli.config')
    );
  }

  /**
   * AddVideoRemote constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param UuidInterface $uuidHelper
   *   The UUID helper.
   * @param ParagraphsBlokkliHelper $paragraphsBlokkliHelper
   *   The paragraphs blokkli helper.
   * @param RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\paragraphs_blokkli\ParagraphsBlokkliConfig $blokkliConfig
   *   The blokkli config.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    EntityTypeManagerInterface $entityTypeManager,
    UuidInterface $uuidHelper,
    ParagraphsBlokkliHelper $paragraphsBlokkliHelper,
    protected RendererInterface $renderer,
    protected ParagraphsBlokkliConfig $blokkliConfig,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition, $entityTypeManager, $uuidHelper, $paragraphsBlokkliHelper);
  }

  /**
   * Executes the mutation operation.
   *
   * @param ParagraphMutationContextInterface $context
   *   The mutation context.
   * @param string $url
   *   The URL of the remote video.
   * @param string $hostType
   *   The type of the host entity.
   * @param string $hostUuid
   *   The UUID of the host entity.
   * @param string $hostFieldName
   *   The field name of the host entity.
   * @param string|null $afterUuid
   *   (optional) The UUID of the entity after which the new entity should be placed.
   */
  public function execute(
    ParagraphMutationContextInterface $context,
    string $url,
    string $hostType,
    string $hostUuid,
    string $hostFieldName,
    ?string $afterUuid,
  ): void {
    $configuredMediaBundle = $this->blokkliConfig->getMediaBundleRemoteVideo();
    if (!$configuredMediaBundle) {
      throw new MutationConfigurationException("Missing media bundle mapping for remote videos.");
    }

    /** @var \Drupal\paragraphs_blokkli\PbEntityMappingStorage $mappingStorage */
    $mappingStorage = $this->entityTypeManager->getStorage('pb_entity_mapping');
    $mapping = $mappingStorage->findMappings('media', $configuredMediaBundle)[0] ?? NULL;

    if (!$mapping) {
      throw new MutationConfigurationException("Missing mapping for entity type 'media' and bundle '$configuredMediaBundle'");
    }

    $renderContext = new RenderContext();
    /** @var \Drupal\media\Entity\Media $media */
    $media = $this->renderer->executeInRenderContext(
      $renderContext,
      fn () => $this->getOrCreateMedia($url, $configuredMediaBundle)
    );

    $paragraphBundle = $mapping->getParagraphBundle();
    $paragraphField = $mapping->getParagraphField();

    $paragraph = $this->createNewParagraph([
      'type' => $paragraphBundle,
      'uuid' => $this->getUuidForNewEntity('paragraph'),
      $paragraphField => [
        [
          'target_id' => $media->id(),
        ],
      ],
    ]);

    $proxy = new ParagraphProxy($paragraph, $hostType, $hostUuid, $hostFieldName);
    $context->addProxy($proxy, $afterUuid);
  }

  /**
   * Get or create the remote video media entity.
   *
   * @param string $url
   *   The remote video URL.
   * @param string $mediaBundle
   *   The media bundle.
   *
   * @return \Drupal\media\Entity\Media
   *   The media entity.
   */
  private function getOrCreateMedia(string $url, string $mediaBundle): MediaInterface {
    $mid = $this->configuration['media_id'] ?? NULL;
    $mediaStorage = $this->entityTypeManager->getStorage('media');

    if ($mid) {
      $media = $mediaStorage->load($mid);

      if ($media) {
        if ($media->bundle() !== $mediaBundle) {
          throw new MutationStateChangedException('The previously created remote video media entity does not match the configured bundle for remote video.');
        }

        return $media;
      }
    }

    /** @var \Drupal\media\Entity\MediaType|null $mediaType */
    $mediaType = $this->entityTypeManager->getStorage('media_type')->load($mediaBundle);

    if (!$mediaType) {
      throw new MutationConfigurationException("The configured media bundle '$mediaBundle' does not exist.");
    }

    $sourceField = $mediaType->getSource()->getSourceFieldDefinition($mediaType);

    if (!$sourceField) {
      throw new MutationConfigurationException("Failed to load source field definition for media bundle '$mediaBundle'");
    }

    $mediaSource = $mediaType->getSource();

    if ($mediaSource->getPluginId() !== 'oembed:video') {
      throw new MutationConfigurationException("The configured remote video media bundle '$mediaBundle' must use the 'oembed:video' source.");
    }

    $sourceFieldName = $sourceField->getName();

    // Try to find an existing entity with the same URL.
    /** @var \Drupal\media\Entity\Media|null $existing */
    $existing = array_values($mediaStorage->loadByProperties([
      'bundle' => $mediaBundle,
      $sourceFieldName => $url,
    ]))[0] ?? NULL;

    if ($existing) {
      return $existing;
    }

    // Create media entity for the remote video.
    /** @var \Drupal\media\Entity\Media $media */
    $media = $mediaStorage->create([
      'bundle' => $mediaBundle,
      'uuid' => $this->getUuidForNewEntity('media'),
      $sourceFieldName => $url,
    ]);

    // Must be called so that the source plugin properly sets the field values
    // such as name.
    $media->prepareSave();

    $violations = $media->validate();

    if ($violations->count()) {
      throw new MutationViolationException($violations);
    }

    // The media entity must be saved at this point, because the media
    // library widget throws an error when it tries to generate a URL.
    // But this is pretty much the same behavior when using the media
    // library on a node edit form: Creating a new media there will also
    // save it.
    $media->save();

    // Store the ID so that we don't create the media entity every time the
    // mutation runs.
    $this->configuration['media_id'] = $media->id();

    return $media;
  }

}
