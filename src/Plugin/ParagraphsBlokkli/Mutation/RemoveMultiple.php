<?php

namespace Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginBase;

/**
 * Remove multiple paragraphs.
 *
 * @ParagraphMutation(
 *   id = "remove_multiple",
 *   label = @Translation("Remove Multiple"),
 *   description = @Translation("Remove multiple paragraphs."),
 *   arguments = {
 *     "uuids" = @ContextDefinition("string",
 *       label = @Translation("The UUIDs of the paragraphs being removed."),
 *       multiple = TRUE,
 *     ),
 *   }
 * )
 */
class RemoveMultiple extends ParagraphMutationPluginBase {

  /**
   * Execute the mutation.
   *
   * @param ParagraphMutationContextInterface $context
   *   The paragraph mutation context.
   * @param string[] $uuids
   *   The UUIDs to remove.
   */
  public function execute(ParagraphMutationContextInterface $context, array $uuids): void {
    foreach ($uuids as $uuid) {
      $context->markAsDeleted($uuid);
    }
  }

}
