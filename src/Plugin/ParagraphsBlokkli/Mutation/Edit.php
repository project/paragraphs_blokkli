<?php

namespace Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\paragraphs_blokkli\Exception\MutationMissingEntityException;
use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginBase;

/**
 * Edit a paragraph.
 *
 * @ParagraphMutation(
 *   id = "edit",
 *   label = @Translation("Edit Paragraph"),
 *   description = @Translation("Edit a paragraph."),
 *   arguments = {
 *     "uuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the paragraph being edited.")
 *     ),
 *     "langcode" = @ContextDefinition("string",
 *       label = @Translation("The langcode.")
 *     ),
 *     "values" = @ContextDefinition("any",
 *       label = @Translation("The updated paragraph values.")
 *     ),
 *   }
 * )
 */
class Edit extends ParagraphMutationPluginBase {

  /**
   * Execute the mutation.
   *
   * @param ParagraphMutationContextInterface $context
   *   The paragraph mutation context.
   * @param string $uuid
   *   The UUID of the paragraph to edit.
   * @param string $langcode
   *   The langcode.
   * @param array $values
   *   The updated values.
   */
  public function execute(ParagraphMutationContextInterface $context, string $uuid, string $langcode, array $values): void {
    $proxy = $context->getProxy($uuid);
    if ($proxy === NULL) {
      throw new MutationMissingEntityException('paragraph', NULL, $uuid);
    }

    if ($context->isDeleted($uuid)) {
      return;
    }

    $paragraph = $proxy->getParagraph();

    if (!$paragraph) {
      return;
    }

    if ($paragraph->isTranslatable()) {
      if (!$paragraph->hasTranslation($langcode)) {
        $paragraph->addTranslation($langcode, $paragraph->toArray());
      }

      /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
      $paragraph = $paragraph->getTranslation($langcode);
    }

    foreach ($values as $key => $value) {
      $paragraph->set($key, $value);
    }

    $paragraph->setNeedsSave(TRUE);
  }

}
