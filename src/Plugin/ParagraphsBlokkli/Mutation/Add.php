<?php

namespace Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginBase;
use Drupal\paragraphs_blokkli\ParagraphProxy;

/**
 * Adds a new paragraph.
 *
 * @ParagraphMutation(
 *   id = "add",
 *   label = @Translation("Add Paragraph"),
 *   description = @Translation("Adds a new paragraph."),
 *   arguments = {
 *     "type" = @ContextDefinition("string",
 *       label = @Translation("The paragraph type."),
 *     ),
 *     "hostType" = @ContextDefinition("string",
 *       label = @Translation("The entity type of the target host.")
 *     ),
 *     "hostUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the target host.")
 *     ),
 *     "hostFieldName" = @ContextDefinition("string",
 *       label = @Translation("The field name of the target host.")
 *     ),
 *     "values" = @ContextDefinition("any",
 *       label = @Translation("The paragraph values."),
 *       required = FALSE,
 *     ),
 *     "afterUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the paragraph after which to add this one."),
 *       required = FALSE,
 *     ),
 *   }
 * )
 */
class Add extends ParagraphMutationPluginBase {

  /**
   * {@inheritdoc}
   */
  public function label() {
    $typeId = $this->configuration['type'] ?? NULL;
    if ($typeId) {
      $type = ParagraphsType::load($typeId)->label();
      return "Add " . $type;
    }
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * Execute the mutation.
   *
   * @param ParagraphMutationContextInterface $context
   *   The paragraph mutation context.
   * @param string $type
   *   The paragraph type.
   * @param string $hostType
   *   The host type.
   * @param string $hostUuid
   *   The host UUID.
   * @param string $hostFieldName
   *   The host field name.
   * @param array|null $values
   *   The paragraph values.
   * @param string|null $afterUuid
   *   After which paragraph the new one should be added.
   */
  public function execute(
    ParagraphMutationContextInterface $context,
    string $type,
    string $hostType,
    string $hostUuid,
    string $hostFieldName,
    ?array $values,
    ?string $afterUuid,
  ): void {
    $paragraph = $this->createNewParagraph(array_merge($values ?? [], [
      'type' => $type,
      'uuid' => $this->getUuidForNewEntity(),
    ]));

    $proxy = new ParagraphProxy($paragraph, $hostType, $hostUuid, $hostFieldName);
    $proxy->setParagraph($paragraph);
    $context->addProxy($proxy, $afterUuid);
  }

}
