<?php

namespace Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\paragraphs_blokkli\Exception\MutationConfigurationException;
use Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\FileUploadMutationBase;

/**
 * Handles pasted file data by creating a new file, media entity and a matching paragraph for the given file.
 *
 * @ParagraphMutation(
 *   id = "add_file",
 *   label = @Translation("Add File"),
 *   description = @Translation("Handles file upload, creating a media entity and adding a matching paragraph."),
 *   arguments = {
 *     "data" = @ContextDefinition("string",
 *       label = @Translation("The base64 encoded file data."),
 *     ),
 *     "fileName" = @ContextDefinition("string",
 *       label = @Translation("The name of the file."),
 *     ),
 *     "hostType" = @ContextDefinition("string",
 *       label = @Translation("The entity type of the target host.")
 *     ),
 *     "hostUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the target host.")
 *     ),
 *     "hostFieldName" = @ContextDefinition("string",
 *       label = @Translation("The field name of the target host.")
 *     ),
 *     "afterUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the paragraph after which to add this one."),
 *       required = FALSE,
 *     ),
 *   }
 * )
 */
class AddFile extends FileUploadMutationBase {

  /**
   * {@inheritdoc}
   */
  protected function getTargetMediaBundle(): string {
    $bundle = $this->blokkliConfig->getMediaBundleFile();
    if (!$bundle) {
      throw new MutationConfigurationException("Missing configuration for 'File Media Bundle'");
    }
    return $bundle;
  }

  /**
   * {@inheritdoc}
   */
  protected function getMediaSourcePluginId(): string {
    return 'file';
  }

}
