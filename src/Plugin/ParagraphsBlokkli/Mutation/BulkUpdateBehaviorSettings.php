<?php

namespace Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginBase;

/**
 * Update multiple behavior settings at once.
 *
 * @ParagraphMutation(
 *   id = "bulk_update_behavior_settings",
 *   label = @Translation("Bulk update behavior settings"),
 *   description = @Translation("Updates multiple behavior settings."),
 *   arguments = {
 *     "items" = @ContextDefinition("map",
 *       label = @Translation("A list of arrays with keys: uuid, pluginId, key, value."),
 *       multiple = TRUE
 *     ),
 *   }
 * )
 */
class BulkUpdateBehaviorSettings extends ParagraphMutationPluginBase {

  /**
   * Execute the mutation.
   *
   * @param ParagraphMutationContextInterface $context
   *   The paragraph mutation context.
   * @param array $items
   *   The behavior settings to update.
   */
  public function execute(ParagraphMutationContextInterface $context, array $items): void {
    foreach ($items as $item) {
      $uuid = $item['uuid'] ?? NULL;
      if (!$uuid) {
        // We can skip missing UUIDs.
        continue;
      }

      $proxy = $context->getProxy($uuid);
      if (!$proxy || $context->isDeleted($uuid)) {
        // We can skip missing proxies or if the paragraph has been deleted already.
        continue;
      }

      $pluginId = $item['pluginId'] ?? NULL;
      $key = $item['key'] ?? NULL;
      $value = $item['value'] ?? NULL;
      $proxy->setBehaviorSetting($pluginId, $key, $value);
    }
  }

}
