<?php

namespace Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginBase;

/**
 * Translate multiple paragraphs.
 *
 * @ParagraphMutation(
 *   id = "translate_multiple",
 *   label = @Translation("Translate multiple Paragraphs"),
 *   description = @Translation("Translate multiple paragraphs."),
 *   arguments = {
 *     "values" = @ContextDefinition("any",
 *       label = @Translation("The updated paragraph values.")
 *     ),
 *   }
 * )
 */
class TranslateMultiple extends ParagraphMutationPluginBase {

  /**
   * Execute the mutation.
   *
   * The provided values array should have this structure:
   *
   * [
   *   "UUID_OF_PARAGRAPH" => [
   *     "LANGCODE" => [
   *       "FIELD_NAME" => "VALUE",
   *       "FIELD_OTHER_NAME" => [],
   *     ],
   *   ],
   * ]
   *
   * @param ParagraphMutationContextInterface $context
   *   The paragraph mutation context.
   * @param array<string, array<string, array<string, array>>> $values
   *   The updated values.
   */
  public function execute(ParagraphMutationContextInterface $context, array $values): void {
    foreach ($values as $uuid => $langcodes) {
      $proxy = $context->getProxy($uuid);
      if ($proxy === NULL) {
        continue;
      }
      if ($context->isDeleted($uuid)) {
        continue;
      }
      $paragraph = $proxy->getParagraph();

      if (!$paragraph) {
        continue;
      }

      foreach ($langcodes as $langcode => $values) {
        if (!$paragraph->hasTranslation($langcode)) {
          $paragraph->addTranslation($langcode, $paragraph->toArray());
        }

        /** @var \Drupal\paragraphs\Entity\Paragraph $translation */
        $translation = $paragraph->getTranslation($langcode);
        foreach ($values as $key => $value) {
          $translation->set($key, $value);
        }
        $translation->setNeedsSave(TRUE);
      }
    }
  }

}
