<?php

namespace Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\paragraphs_blokkli\Exception\MutationConfigurationException;
use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginBase;
use Drupal\paragraphs_blokkli\ParagraphProxy;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliConfig;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Adds a new text paragraph from clipboard content..
 *
 * @ParagraphMutation(
 *   id = "add_clipboard_text",
 *   label = @Translation("Add Text Paragraph"),
 *   description = @Translation("Adds a new text paragraph."),
 *   arguments = {
 *     "text" = @ContextDefinition("string",
 *       label = @Translation("The text."),
 *     ),
 *     "hostType" = @ContextDefinition("string",
 *       label = @Translation("The entity type of the target host.")
 *     ),
 *     "hostUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the target host.")
 *     ),
 *     "hostFieldName" = @ContextDefinition("string",
 *       label = @Translation("The field name of the target host.")
 *     ),
 *     "afterUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the paragraph after which to add this one."),
 *       required = FALSE,
 *     ),
 *   }
 * )
 */
class AddClipboardText extends ParagraphMutationPluginBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('uuid'),
      $container->get('paragraphs_blokkli.helper'),
      $container->get('paragraphs_blokkli.config'),
      $container->get('renderer'),
    );
  }

  /**
   * Convert constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param UuidInterface $uuidHelper
   *   The UUID helper.
   * @param \Drupal\paragraphs_blokkli\ParagraphsBlokkliHelper $paragraphsBlokkliHelper
   *   The paragraph blokkli helper.
   * @param \Drupal\paragraphs_blokkli\ParagraphsBlokkliConfig $blokkliConfig
   *   The paragraphs blokkli config.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    EntityTypeManagerInterface $entityTypeManager,
    UuidInterface $uuidHelper,
    ParagraphsBlokkliHelper $paragraphsBlokkliHelper,
    protected ParagraphsBlokkliConfig $blokkliConfig,
    protected RendererInterface $renderer,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition, $entityTypeManager, $uuidHelper, $paragraphsBlokkliHelper);
  }

  /**
   * Execute the mutation.
   *
   * @param ParagraphMutationContextInterface $context
   *   The paragraph mutation context.
   * @param string $text
   *   The text.
   * @param string $hostType
   *   The host type.
   * @param string $hostUuid
   *   The host UUID.
   * @param string $hostFieldName
   *   The host field name.
   * @param string|null $afterUuid
   *   After which paragraph the new one should be added.
   */
  public function execute(
    ParagraphMutationContextInterface $context,
    string $text,
    string $hostType,
    string $hostUuid,
    string $hostFieldName,
    ?string $afterUuid,
  ): void {
    $setting = $this->blokkliConfig->getClipboardTextParagraph();
    if (!$setting) {
      throw new MutationConfigurationException("Missing 'Clipboard Text Paragraph' configuration.");
    }

    $paragraph = $this->createNewParagraph([
      'type' => $setting['bundle'],
      'uuid' => $this->getUuidForNewEntity(),
    ]);

    $fieldName = $setting['field'];
    $bundle = $setting['bundle'];

    $field = $paragraph->get($setting['field']);
    $allowedFormats = $field->getSetting('allowed_formats');
    if (empty($allowedFormats)) {
      throw new MutationConfigurationException("Field '$fieldName' on paragraph bundle '$bundle' does not define any allowed formats.");
    }
    $format = array_values($allowedFormats)[0];
    $filteredText = $this->getFilteredText($text, $format);
    $paragraph->set($setting['field'], [
      'value' => $filteredText,
      'format' => $format,
    ]);

    // Remove the original pasted text from the configuration.
    $this->configuration['text'] = '';
    $this->setContextValue('text', '');

    $proxy = new ParagraphProxy($paragraph, $hostType, $hostUuid, $hostFieldName);
    $context->addProxy($proxy, $afterUuid);
  }

  /**
   * Get the filtered markup to store on the paragraph.
   *
   * @param string $text
   *   The pasted text.
   * @param string $formatName
   *   The name of the format.
   *
   * @return string
   *   The filtered text that can be stored.
   */
  private function getFilteredText(string $text, string $formatName): string {
    // We store the text in the configuration, so it does not have to be
    // executed each time.
    if (!isset($this->configuration['processed_text'])) {
      $build = [
        '#type' => 'processed_text',
        '#text' => $text,
        '#format' => $formatName,
      ];

      // Render the markup using the allowed format. This makes sure we don't
      // store anything dangerous.
      $processed = $this->renderer->renderInIsolation($build);
      $markup = (string) $processed;

      $this->configuration['processed_text'] = $markup;
    }

    return $this->configuration['processed_text'];
  }

}
