<?php

namespace Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\paragraphs_blokkli\Exception\MutationMissingEntityException;
use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginBase;
use Drupal\paragraphs_blokkli\ParagraphProxy;

/**
 * Add a reusable paragraph.
 *
 * @ParagraphMutation(
 *   id = "add_reusable",
 *   label = @Translation("Add reusable"),
 *   description = @Translation("Add a reusable paragraph."),
 *   arguments = {
 *     "libraryItemUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the paragraphs library item entity."),
 *     ),
 *     "hostType" = @ContextDefinition("string",
 *       label = @Translation("The entity type of the target host.")
 *     ),
 *     "hostUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the target host.")
 *     ),
 *     "hostFieldName" = @ContextDefinition("string",
 *       label = @Translation("The field name of the target host.")
 *     ),
 *     "afterUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the paragraph after which to add this one."),
 *       required = FALSE,
 *     ),
 *   }
 * )
 */
class AddReusable extends ParagraphMutationPluginBase {

  /**
   * Execute the mutation.
   *
   * @param ParagraphMutationContextInterface $context
   *   The paragraph mutation context.
   * @param string $libraryItemUuid
   *   The UUID of the paragraphs_library_item.
   * @param string $hostType
   *   The host type.
   * @param string $hostUuid
   *   The host UUID.
   * @param string $hostFieldName
   *   The host field name.
   * @param string|null $afterUuid
   *   After which paragraph the new one should be added.
   */
  public function execute(
    ParagraphMutationContextInterface $context,
    string $libraryItemUuid,
    string $hostType,
    string $hostUuid,
    string $hostFieldName,
    ?string $afterUuid,
  ): void {
    $libraryItemStorage = $this->entityTypeManager->getStorage('paragraphs_library_item');
    $ids = $libraryItemStorage
      ->getQuery()
      ->condition('uuid', $libraryItemUuid)
      ->accessCheck(FALSE)
      ->execute();

    $id = array_values($ids)[0] ?? NULL;

    if (!$id) {
      throw new MutationMissingEntityException('paragraphs_library_item', NULL, $libraryItemUuid);
    }

    $paragraph = $this->createNewParagraph([
      'type' => 'from_library',
      'uuid' => $this->getUuidForNewEntity(),
    ]);

    $paragraph->set('field_reusable_paragraph', [
      'target_id' => $id,
    ]);

    $proxy = new ParagraphProxy($paragraph, $hostType, $hostUuid, $hostFieldName);
    $settings = $this->getInheritedSettings($id);
    $proxy->setBehaviorSettings($settings);
    $context->addProxy($proxy, $afterUuid);
  }

  /**
   * Get the options from the library paragraph.
   *
   * @param string $id
   *   The paragraphs_library_item ID.
   *
   * @return array
   *   The options.
   */
  private function getInheritedSettings(string $id) {
    $options = $this->configuration['inherited_options'] ?? NULL;
    if ($options === NULL) {
      /** @var \Drupal\paragraphs_library\Entity\LibraryItem $libraryItem */
      $libraryItem = $this->entityTypeManager->getStorage('paragraphs_library_item')->load($id);
      /** @var \Drupal\Core\Field\EntityReferenceFieldItemList $field */
      $field = $libraryItem->get('paragraphs');

      /** @var \Drupal\paragraphs\Entity\Paragraph|null $paragraph */
      $paragraph = $field->referencedEntities()[0] ?? NULL;

      if ($paragraph) {
        $options = $paragraph->getAllBehaviorSettings() ?? NULL;
      }
    }

    $this->configuration['inherited_options'] = $options ?? [];
    return $this->configuration['inherited_options'];
  }

}
