<?php

namespace Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\paragraphs_blokkli\Exception\MutationConfigurationException;
use Drupal\paragraphs_blokkli\Exception\MutationMissingEntityException;
use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginBase;
use Drupal\paragraphs_blokkli\ParagraphProxy;
use Drupal\paragraphs_blokkli\Traits\LinkOrReferenceFieldTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Adds a new entity reference paragraph.
 *
 * @ParagraphMutation(
 *   id = "add_entity_reference",
 *   label = @Translation("Add Entity Reference"),
 *   description = @Translation("Adds a new paragraph for the given entity reference."),
 *   arguments = {
 *     "targetType" = @ContextDefinition("string",
 *       label = @Translation("The entity type of the referenced entity."),
 *     ),
 *     "targetBundle" = @ContextDefinition("string",
 *       label = @Translation("The entity bundle of the referenced entity."),
 *     ),
 *     "targetId" = @ContextDefinition("string",
 *       label = @Translation("The ID of the referenced entity."),
 *     ),
 *     "paragraphBundle" = @ContextDefinition("string",
 *       label = @Translation("The paragraph bundle."),
 *     ),
 *     "hostType" = @ContextDefinition("string",
 *       label = @Translation("The entity type of the target host.")
 *     ),
 *     "hostUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the target host.")
 *     ),
 *     "hostFieldName" = @ContextDefinition("string",
 *       label = @Translation("The field name of the target host.")
 *     ),
 *     "afterUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the paragraph after which to add this one."),
 *       required = FALSE,
 *     ),
 *   }
 * )
 */
class AddEntityReference extends ParagraphMutationPluginBase {

  use LinkOrReferenceFieldTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('uuid'),
      $container->get('paragraphs_blokkli.helper'),
    );
  }

  /**
   * Executes the method.
   *
   * @param \Drupal\paragraphs_blokkli\ParagraphMutationContextInterface $context
   *   The context object for the mutation operation.
   * @param string $targetType
   *   The target entity type.
   * @param string $targetBundle
   *   The target entity bundle.
   * @param string $targetId
   *   The target entity ID.
   * @param string $paragraphBundle
   *   The paragraph bundle.
   * @param string $hostType
   *   The type of the host entity.
   * @param string $hostUuid
   *   The UUID of the host entity.
   * @param string $hostFieldName
   *   The field name of the host entity.
   * @param string|null $afterUuid
   *   The UUID of the paragraph after which the new paragraph should be added.
   */
  public function execute(
    ParagraphMutationContextInterface $context,
    string $targetType,
    string $targetBundle,
    string $targetId,
    string $paragraphBundle,
    string $hostType,
    string $hostUuid,
    string $hostFieldName,
    ?string $afterUuid,
  ): void {
    $entity = $this->entityTypeManager->getStorage($targetType)->load($targetId);
    if (!$entity) {
      throw new MutationMissingEntityException($targetType, $targetId);
    }

    $loadedBundle = $entity->bundle();

    if ($loadedBundle !== $targetBundle) {
      throw new MutationConfigurationException("The target entity bundle '$targetBundle' does not match the loaded bundle '$loadedBundle'");
    }

    /** @var \Drupal\paragraphs_blokkli\PbEntityMappingStorage $mappingStorage */
    $mappingStorage = $this->entityTypeManager->getStorage('pb_entity_mapping');
    $mapping = $mappingStorage->findMappings($targetType, $targetBundle)[0] ?? NULL;

    if (!$mapping) {
      throw new MutationConfigurationException("Missing mapping for entity type '$targetType' and bundle '$targetBundle'");
    }

    $bundle = $mapping->getParagraphBundle();
    if ($bundle !== $paragraphBundle) {
      throw new MutationConfigurationException("The requested bundle '$paragraphBundle' does not match the mapped paragraph bundle '$bundle'");
    }
    $fieldName = $mapping->getParagraphField();

    $paragraph = $this->createNewParagraph([
      'type' => $bundle,
      'uuid' => $this->getUuidForNewEntity(),
    ]);

    $this->setLinkOrEntityReferenceFieldValue($paragraph, $fieldName, $entity);

    $proxy = new ParagraphProxy($paragraph, $hostType, $hostUuid, $hostFieldName);
    $context->addProxy($proxy, $afterUuid);
  }

}
