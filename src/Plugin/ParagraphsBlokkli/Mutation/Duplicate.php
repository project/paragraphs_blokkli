<?php

namespace Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\DuplicateMutationBase;

/**
 * Duplicate an existing paragraph.
 *
 * @ParagraphMutation(
 *   id = "duplicate",
 *   label = @Translation("Duplicate"),
 *   description = @Translation("Duplicates an existing paragraph."),
 *   arguments = {
 *     "uuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the paragraph being duplicated.")
 *     ),
 *     "afterUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the paragraph after which to duplicate this one."),
 *       required = FALSE,
 *     ),
 *   }
 * )
 */
class Duplicate extends DuplicateMutationBase {

  /**
   * Execute the mutation.
   *
   * @param ParagraphMutationContextInterface $context
   *   The paragraph mutation context.
   * @param string $uuid
   *   The UUID of the paragraph to duplicate.
   * @param string|null $afterUuid
   *   The UUID after which the duplicated paragraphs should be added.
   */
  public function execute(ParagraphMutationContextInterface $context, string $uuid, string|null $afterUuid): void {
    $this->duplicateParagraph($context, $context, $uuid, $afterUuid);
  }

}
