<?php

namespace Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\paragraphs_blokkli\Exception\MutationConfigurationException;
use Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\FileUploadMutationBase;

/**
 * Handles pasted image data.
 *
 * Creates a new file, media entity and a matching paragraph for the given file.
 *
 * @ParagraphMutation(
 *   id = "add_image",
 *   label = @Translation("Add Image"),
 *   description = @Translation("Adds a new paragraph of type image."),
 *   arguments = {
 *     "data" = @ContextDefinition("string",
 *       label = @Translation("The base64 encoded image data."),
 *     ),
 *     "fileName" = @ContextDefinition("string",
 *       label = @Translation("The name of the file."),
 *     ),
 *     "hostType" = @ContextDefinition("string",
 *       label = @Translation("The entity type of the target host.")
 *     ),
 *     "hostUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the target host.")
 *     ),
 *     "hostFieldName" = @ContextDefinition("string",
 *       label = @Translation("The field name of the target host.")
 *     ),
 *     "afterUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the paragraph after which to add this one."),
 *       required = FALSE,
 *     ),
 *   }
 * )
 */
class AddImage extends FileUploadMutationBase {

  /**
   * {@inheritdoc}
   */
  protected function getTargetMediaBundle(): string {
    $bundle = $this->blokkliConfig->getMediaBundleImage();
    if (!$bundle) {
      throw new MutationConfigurationException("Missing configuration for 'Image Media Bundle'");
    }
    return $bundle;
  }

  /**
   * {@inheritdoc}
   */
  protected function getMediaSourcePluginId(): string {
    return 'image';
  }

}
