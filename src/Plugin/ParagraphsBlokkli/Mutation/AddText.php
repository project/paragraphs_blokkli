<?php

namespace Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginBase;
use Drupal\paragraphs_blokkli\ParagraphProxy;

/**
 * Adds a new text paragraph.
 *
 * @ParagraphMutation(
 *   id = "add_text",
 *   label = @Translation("Add Text Paragraph"),
 *   description = @Translation("Adds a new text paragraph."),
 *   arguments = {
 *     "text" = @ContextDefinition("string",
 *       label = @Translation("The text."),
 *     ),
 *     "bundle" = @ContextDefinition("string",
 *       label = @Translation("The paragrpah bundle."),
 *     ),
 *     "textFieldName" = @ContextDefinition("string",
 *       label = @Translation("The name of the text field."),
 *     ),
 *     "textFieldFormat" = @ContextDefinition("string",
 *       label = @Translation("The text format for the field."),
 *     ),
 *     "hostType" = @ContextDefinition("string",
 *       label = @Translation("The entity type of the target host.")
 *     ),
 *     "hostUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the target host.")
 *     ),
 *     "hostFieldName" = @ContextDefinition("string",
 *       label = @Translation("The field name of the target host.")
 *     ),
 *     "afterUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the paragraph after which to add this one."),
 *       required = FALSE,
 *     ),
 *   }
 * )
 */
class AddText extends ParagraphMutationPluginBase {

  /**
   * Execute the mutation.
   *
   * @param ParagraphMutationContextInterface $context
   *   The paragraph mutation context.
   * @param string $text
   *   The text.
   * @param string $bundle
   *   The paragraph bundle.
   * @param string $textFieldName
   *   The name of the text field.
   * @param string $textFieldFormat
   *   The text format (e.g. "basic_html").
   * @param string $hostType
   *   The host type.
   * @param string $hostUuid
   *   The host UUID.
   * @param string $hostFieldName
   *   The host field name.
   * @param string|null $afterUuid
   *   After which paragraph the new one should be added.
   */
  public function execute(
    ParagraphMutationContextInterface $context,
    string $text,
    string $bundle,
    string $textFieldName,
    string $textFieldFormat,
    string $hostType,
    string $hostUuid,
    string $hostFieldName,
    ?string $afterUuid,
  ): void {
    $paragraph = $this->createNewParagraph([
      'type' => $bundle,
      'uuid' => $this->getUuidForNewEntity(),
    ]);
    $paragraph->set($textFieldName, [
      'value' => $text,
      'format' => $textFieldFormat,
    ]);
    $proxy = new ParagraphProxy($paragraph, $hostType, $hostUuid, $hostFieldName);
    $context->addProxy($proxy, $afterUuid);
  }

}
