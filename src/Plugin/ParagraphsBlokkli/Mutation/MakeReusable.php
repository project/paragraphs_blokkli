<?php

namespace Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\paragraphs_blokkli\Exception\MutationMissingEntityException;
use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginBase;
use Drupal\paragraphs_blokkli\ParagraphProxy;

/**
 * Make a paragraph reusable.
 *
 * @ParagraphMutation(
 *   id = "make_reusable",
 *   label = @Translation("Make reusable"),
 *   description = @Translation("Makes a paragraph reusable."),
 *   arguments = {
 *     "uuid" = @ContextDefinition("string",
 *       label = @Translation("The paragraph to make reusable."),
 *     ),
 *     "label" = @ContextDefinition("string",
 *       label = @Translation("The label for the reusable paragraph."),
 *     ),
 *   }
 * )
 */
class MakeReusable extends ParagraphMutationPluginBase {

  /**
   * Execute the mutation.
   *
   * @param ParagraphMutationContextInterface $context
   *   The paragraph mutation context.
   * @param string $uuid
   *   The UUID of the paragraph to make reusable.
   * @param string $label
   *   The label for the library item.
   */
  public function execute(ParagraphMutationContextInterface $context, string $uuid, string $label): void {
    $proxy = $context->getProxy($uuid);
    if (!$proxy) {
      throw new MutationMissingEntityException('paragraph', NULL, $uuid);
    }

    if ($context->isDeleted($uuid)) {
      return;
    }
    $paragraph = $proxy->getParagraph();

    if (!$paragraph) {
      return;
    }
    $settings = $proxy->getBehaviorSettings();

    $libraryStorage = $this->entityTypeManager->getStorage('paragraphs_library_item');

    // First, create a library item.
    /** @var \Drupal\paragraphs_library\Entity\LibraryItem $libraryItem */
    $libraryItem = $libraryStorage->create([
      'label' => $label,
      'uuid' => $this->getUuidForNewEntity('library_item'),
    ]);

    // Set the paragraph's parent to the library item.
    $paragraph->setParentEntity($libraryItem, 'paragraphs');
    $libraryItem->set('paragraphs', [
      'entity' => $paragraph,
    ]);

    // Create a translation of the library item for every existing translation
    // of the paragraph.
    $paragraphLangcodes = array_keys($paragraph->getTranslationLanguages());
    foreach ($paragraphLangcodes as $langcode) {
      if (!$libraryItem->hasTranslation($langcode)) {
        $libraryItem->addTranslation($langcode, $libraryItem->toArray());
      }
    }

    // Create the library paragraph.
    $reusableUuid = $this->getUuidForNewEntity();
    $reusableParagraph = $this->createNewParagraph([
      'type' => 'from_library',
      'uuid' => $reusableUuid,
    ]);

    $reusableParagraph->set('field_reusable_paragraph', [
      'entity' => $libraryItem,
    ]);

    // Create proxy for reusable paragraph.
    $reusableProxy = new ParagraphProxy($reusableParagraph, $proxy->getHostEntityType(), $proxy->getHostUuid(), $proxy->getHostFieldName());

    // Assign the options to the new reusable proxy.
    $reusableProxy->setBehaviorSettings($settings);

    // Add the reusable proxy after the existing proxy.
    $context->addProxy($reusableProxy, $uuid);

    // Remove the existing proxy.
    $context->removeProxy($proxy->uuid());
  }

}
