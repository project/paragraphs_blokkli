<?php

namespace Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginBase;
use Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\UpdateFieldValueTrait;

/**
 * Replace the referenced media on a entity reference field of the host entity.
 *
 * @ParagraphMutation(
 *   id = "replace_host_entity_media",
 *   label = @Translation("Replace Media"),
 *   description = @Translation("Replace referenced media."),
 *   arguments = {
 *     "langcode" = @ContextDefinition("string",
 *       label = @Translation("The langcode.")
 *     ),
 *     "fieldName" = @ContextDefinition("string",
 *       label = @Translation("The name of the field.")
 *     ),
 *     "mediaId" = @ContextDefinition("string",
 *       label = @Translation("The new media ID.")
 *     ),
 *   }
 * )
 */
class ReplaceHostEntityMedia extends ParagraphMutationPluginBase {

  use UpdateFieldValueTrait;

  /**
   * Execute the mutation.
   *
   * @param ParagraphMutationContextInterface $context
   *   The paragraph mutation context.
   * @param string $langcode
   *   The langcode.
   * @param string $fieldName
   *   The field name.
   * @param string $mediaId
   *   The new media ID.
   */
  public function execute(ParagraphMutationContextInterface $context, string $langcode, string $fieldName, string $mediaId): void {
    $hostEntity = $context->getHostEntity();
    $this->updateEntityReferenceField($hostEntity, $langcode, $fieldName, 'media', $mediaId);
  }

}
