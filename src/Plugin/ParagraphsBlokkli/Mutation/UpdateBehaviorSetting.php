<?php

namespace Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginBase;

/**
 * Update a paragraph behavior setting.
 *
 * @ParagraphMutation(
 *   id = "update_behavior_setting",
 *   label = @Translation("Update behavior setting"),
 *   description = @Translation("Updates a single behavior setting."),
 *   arguments = {
 *     "uuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the paragraph to update the option of.")
 *     ),
 *     "pluginId" = @ContextDefinition("string",
 *       label = @Translation("The plugin ID of the setting.")
 *     ),
 *     "key" = @ContextDefinition("string",
 *       label = @Translation("The key of the setting.")
 *     ),
 *     "value" = @ContextDefinition("string",
 *       label = @Translation("The value of the setting.")
 *     ),
 *   }
 * )
 */
class UpdateBehaviorSetting extends ParagraphMutationPluginBase {

  /**
   * {@inheritdoc}
   */
  public function label() {
    $key = $this->configuration['key'] ?? NULL;
    $value = $this->configuration['value'] ?? NULL;
    if ($key) {
      return sprintf('Set "%s" to "%s"', $key, $value);
    }
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * Execute the mutation.
   *
   * @param ParagraphMutationContextInterface $context
   *   The paragraph mutation context.
   * @param string $pluginId
   *   The ID of the behavior plugin.
   * @param string $uuid
   *   The UUID of the paragraph.
   * @param string $key
   *   The behavior setting key.
   * @param string $value
   *   The behavior setting value.
   */
  public function execute(ParagraphMutationContextInterface $context, string $pluginId, string $uuid, string $key, string $value): void {
    $proxy = $context->getProxy($uuid);
    if ($proxy === NULL || $context->isDeleted($uuid)) {
      return;
    }
    $proxy->setBehaviorSetting($pluginId, $key, $value);
  }

}
