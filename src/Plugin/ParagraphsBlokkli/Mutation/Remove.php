<?php

namespace Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginBase;

/**
 * Remove a paragraph.
 *
 * @ParagraphMutation(
 *   id = "remove",
 *   label = @Translation("Remove Paragraph"),
 *   description = @Translation("Remove a paragraph."),
 *   arguments = {
 *     "uuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the paragraph being removed.")
 *     ),
 *   }
 * )
 */
class Remove extends ParagraphMutationPluginBase {

  /**
   * Execute the mutation.
   *
   * @param ParagraphMutationContextInterface $context
   *   The paragraph mutation context.
   * @param string $uuid
   *   The UUID of the paragraph to remove.
   */
  public function execute(ParagraphMutationContextInterface $context, string $uuid): void {
    $context->markAsDeleted($uuid);
  }

}
