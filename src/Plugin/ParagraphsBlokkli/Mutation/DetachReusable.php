<?php

namespace Drupal\paragraphs_blokkli\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginBase;
use Drupal\paragraphs_blokkli\ParagraphProxy;

/**
 * Detaches reusable paragraphs.
 *
 * @ParagraphMutation(
 *   id = "detach_reusable",
 *   label = @Translation("Detaches reusable paragraphs"),
 *   description = @Translation("Detaches reusable paragraphs."),
 *   arguments = {
 *     "uuids" = @ContextDefinition("string",
 *       label = @Translation("The paragraphs to detach."),
 *       multiple = TRUE,
 *     ),
 *   }
 * )
 */
class DetachReusable extends ParagraphMutationPluginBase {

  /**
   * Execute the mutation.
   *
   * @param ParagraphMutationContextInterface $context
   *   The paragraph mutation context.
   * @param string[] $uuids
   *   The UUIDs of the paragraph to detach.
   */
  public function execute(ParagraphMutationContextInterface $context, array $uuids): void {
    foreach ($uuids as $uuid) {
      $proxy = $context->getProxy($uuid);
      if (!$proxy) {
        return;
      }
      $fromLibrary = $proxy->getParagraph();

      if (!$fromLibrary->bundle() === 'from_library') {
        return;
      }

      $referenceField = $fromLibrary->get('field_reusable_paragraph');
      if (!$referenceField instanceof EntityReferenceFieldItemListInterface) {
        return;
      }

      /** @var \Drupal\paragraphs_library\Entity\LibraryItem|null $libraryItem */
      $libraryItem = $referenceField->referencedEntities()[0] ?? NULL;

      if (!$libraryItem) {
        return;
      }

      /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $paragraphReference */
      $paragraphReference = $libraryItem->get('paragraphs');
      /** @var \Drupal\paragraphs\Entity\Paragraph|null $reusableParagraph */
      $reusableParagraph = $paragraphReference->referencedEntities()[0] ?? NULL;

      if (!$reusableParagraph) {
        return;
      }

      $clone = $reusableParagraph->createDuplicate();
      $cloneUuid = $this->getUuidForNewEntity($reusableParagraph->uuid());
      $clone->set('uuid', $cloneUuid);

      $cloneProxy = new ParagraphProxy(
        $clone,
        $proxy->getHostEntityType(),
        $proxy->getHostUuid(),
        $proxy->getHostFieldName()
      );

      $cloneProxy->setBehaviorSettings($proxy->getBehaviorSettings());
      $context->addProxy($cloneProxy, $uuid);
      $context->removeProxy($uuid);
    }
  }

}
