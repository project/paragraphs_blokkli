<?php

namespace Drupal\paragraphs_blokkli\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\paragraphs_blokkli\ParagraphMutationInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationItemInterface;

/**
 * Plugin implementation of the 'paragraphs_blokkli_mutation' field type.
 *
 * @FieldType(
 *   id = "paragraphs_blokkli_mutation",
 *   label = @Translation("Paragraphs Blokkli Mutations"),
 *   module = "paragraphs_blokkli",
 *   description = @Translation("Stores configuration for a paragraphs_blokkli mutation."),
 *   no_ui = TRUE,
 *   cardinality = \Drupal\Core\Field\FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED
 * )
 */
class ParagraphsBlokkliMutationItem extends FieldItemBase implements ParagraphMutationItemInterface {

  /**
   * The mutation plugin instance.
   */
  protected ParagraphMutationInterface|null $mutationPlugin;

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'plugin_id' => [
          'type' => 'text',
          'size' => 'tiny',
          'not null' => TRUE,
        ],
        'timestamp' => [
          'type' => 'int',
          'not null' => TRUE,
        ],
        'enabled' => [
          'type' => 'int',
          'size' => 'tiny',
          'default' => 1,
          'not null' => FALSE,
        ],
        'configuration' => [
          'type' => 'blob',
          'size' => 'normal',
          'serialize' => TRUE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['plugin_id'] = DataDefinition::create('string')->setLabel(t('Plugin ID'))->setRequired(TRUE);
    $properties['timestamp'] = DataDefinition::create('timestamp')->setLabel(t('Timestamp'))->setRequired(TRUE);
    $properties['configuration'] = DataDefinition::create('any')->setLabel(t('Configuration'));
    $properties['enabled'] = DataDefinition::create('boolean')
      ->setLabel(t('Enabled'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('plugin_id')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(): bool {
    return (bool) $this->get('enabled')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function getMutationPlugin(): ParagraphMutationInterface {
    if (empty($this->mutationPlugin)) {
      $pluginId = $this->get('plugin_id')->getValue();
      $configuration = $this->get('configuration')->getValue();
      /** @var \Drupal\paragraphs_blokkli\ParagraphMutationPluginManager $pluginManager */
      $pluginManager = \Drupal::service('plugin.manager.paragraph_mutation');
      $this->mutationPlugin = $pluginManager->createInstance($pluginId, $configuration);
    }

    return $this->mutationPlugin;
  }

}
