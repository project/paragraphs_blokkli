<?php

namespace Drupal\paragraphs_blokkli\Plugin\paragraphs\Behavior;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;
use Drupal\paragraphs_blokkli\BlokkliOptionsSchemaHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a paragraphs behaviour plugin to store editor data.
 *
 * @ParagraphsBehavior(
 *   id = "paragraphs_blokkli_data",
 *   label = @Translation("Paragraph Blokkli Data"),
 *   description = @Translation("Stores the data from the blokkli editor."),
 * )
 */
class ParagraphsBlokkliData extends ParagraphsBehaviorBase {

  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityFieldManagerInterface $entity_field_manager,
    protected BlokkliOptionsSchemaHelper $blokkliOptionsSchema,
    protected LanguageManagerInterface $languageManager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_field_manager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_field.manager'),
      $container->get('paragraphs_blokkli.options_schema_helper'),
      $container->get('language_manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode) {
  }

  /**
   * Get the option definitions for a paragraph bundle.
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraph
   *   The paragraph.
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    $definitions = $this->blokkliOptionsSchema->getOptionsForBundle($paragraph->bundle());

    foreach ($definitions as $property => $definition) {
      $value = $paragraph->getBehaviorSetting($this->getPluginId(), $property, $definition['default']);
      $element = $this->getOptionFormElement($property, $definition, $value);
      if ($element) {
        $form[$property] = $element;
      }
    }
    return $form;
  }

  /**
   * Build the form element for an option.
   *
   * @param string $property
   *   The property.
   * @param array $definition
   *   The blökkli option definition.
   * @param mixed $value
   *   The current option value.
   *
   * @return array|null
   *   The form element.
   */
  protected function getOptionFormElement(string $property, array $definition, $value): array|null {
    $type = $definition['type'] ?? NULL;

    if (!$type) {
      return NULL;
    }

    $optionLabel = $definition['label'] ?? NULL;

    if (!$optionLabel) {
      return NULL;
    }

    $title = $this->translateWithContext($optionLabel);

    switch ($type) {
      case 'text':
        return [
          '#type' => 'textfield',
          '#title' => $title,
          '#default_value' => $value,
        ];

      case 'checkbox':
        return [
          '#type' => 'checkbox',
          '#title' => $title,
          '#default_value' => $value,
        ];

      case 'color':
        return [
          '#type' => 'color',
          '#title' => $title,
          '#default_value' => $value,
        ];

      case 'number':
        return [
          '#type' => 'number',
          '#title' => $title,
          '#min' => $definition['min'] ?? NULL,
          '#max' => $definition['max'] ?? NULL,
          '#default_value' => $value,
        ];

      case 'range':
        return [
          '#type' => 'range',
          '#title' => $title,
          '#min' => $definition['min'] ?? '0',
          '#max' => $definition['max'] ?? NULL,
          '#step' => $definition['step'] ?? '1',
          '#default_value' => $value,
        ];

      case 'checkboxes':
        $options = $property === 'bkVisibleLanguages'
          ? $this->getLanguageOptions()
          : $definition['options'];
        return [
          '#type' => 'checkboxes',
          '#title' => $title,
          '#default_value' => $this->blokkliOptionsSchema->getCheckboxesValue($value),
          '#options' => $options,
        ];

      case 'radios':
        $options = $definition['options'] ?? NULL;
        if (!$options) {
          return NULL;
        }
        return [
          '#type' => 'radios',
          '#title' => $title,
          '#default_value' => $value,
          '#options' => $this->mapRadioOptions($options),
        ];
    }

    return NULL;
  }

  /**
   * Get the language options for the 'bkVisibleLanguages' option.
   *
   * @return array<string, string>
   *   The options.
   */
  protected function getLanguageOptions(): array {
    $languages = $this->languageManager->getLanguages();
    return array_map(function (LanguageInterface $language) {
      return $language->getName();
    }, $languages);
  }

  /**
   * Build options.
   *
   * @param array $options
   *   The blökkli options.
   *
   * @return array<string, string>
   *   The options mapped to key => label.
   */
  protected function mapRadioOptions(array $options): array {
    $mapped = [];

    foreach ($options as $key => $option) {
      $label = $this->mapRadioOptionLabel($option) ?? $key;
      $mapped[$key] = $label;
    }

    return $mapped;
  }

  /**
   * Translate with blokkli context.
   *
   * @param string $text
   *   The text to translate.
   *
   * @return string
   *   The translated string.
   */
  protected function translateWithContext(string $text): string {
    // phpcs:ignore
    return $this->t($text, [], ['context' => 'blokkli option']);
  }

  /**
   * Map the option label.
   *
   * @param array|string $option
   *   The option.
   *
   * @return string|null
   *   The label.
   */
  protected function mapRadioOptionLabel($option): string|null {
    if (is_string($option)) {
      return $this->translateWithContext($option);
    }
    elseif (is_array($option)) {
      $label = $option['label'];
      if ($label) {
        return $this->translateWithContext($label);
      }
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(Paragraph $paragraph) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function submitBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    $filteredValues = $this->filterBehaviorFormSubmitValues($paragraph, $form, $form_state);

    $definitions = $this->blokkliOptionsSchema->getOptionsForBundle($paragraph->bundle());
    $mappedValues = [];

    foreach ($filteredValues as $property => $value) {
      $definition = $definitions[$property] ?? NULL;
      if (!$definition) {
        continue;
      }

      $type = $definition['type'];
      $persistableValue = $this->blokkliOptionsSchema->getPersistableValue($type, $value);
      $mappedValues[$property] = $persistableValue;
    }

    $paragraph->setBehaviorSettings($this->getPluginId(), $mappedValues);
  }

}
