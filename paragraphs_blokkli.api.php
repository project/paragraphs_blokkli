<?php

use Drupal\paragraphs_blokkli\BlokkliSession;
use Drupal\paragraphs_blokkli\EditStateInterface;

/**
 * Alter the blokkli session for previewing edit state and associated entities.
 *
 * @param \Drupal\paragraphs_blokkli\EditStateInterface $edit_state
 *   The edit state.
 * @param \Drupal\paragraphs_blokkli\BlokkliSession $blokkli_session
 *   The blokkli session.
 */
function hook_paragraphs_blokkli_preview_session_alter(EditStateInterface $edit_state, BlokkliSession $blokkli_session) {
  $host = $edit_state->getHostEntity();

  /** @var \Drupal\Core\Field\EntityReferenceFieldItemList $hero_field */
  $hero_field = $host->get('field_hero_image');

  $hero_media = $hero_field->referencedEntities()[0] ?? NULL;
  if ($hero_media) {
    // Add the hero media entity as a preview entity.
    $blokkli_session->addPreviewEntity($hero_media);
  }
}
