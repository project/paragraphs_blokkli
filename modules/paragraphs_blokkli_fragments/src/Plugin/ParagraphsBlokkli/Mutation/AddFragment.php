<?php

namespace Drupal\paragraphs_blokkli_fragments\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginBase;
use Drupal\paragraphs_blokkli\ParagraphProxy;

/**
 * Adds a new fragment paragraph.
 *
 * @ParagraphMutation(
 *   id = "add_fragment",
 *   label = @Translation("Add Fragment Paragraph"),
 *   description = @Translation("Adds a new fragment paragraph."),
 *   arguments = {
 *     "name" = @ContextDefinition("string",
 *       label = @Translation("The fragment name."),
 *     ),
 *     "hostType" = @ContextDefinition("string",
 *       label = @Translation("The entity type of the target host.")
 *     ),
 *     "hostUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the target host.")
 *     ),
 *     "hostFieldName" = @ContextDefinition("string",
 *       label = @Translation("The field name of the target host.")
 *     ),
 *     "afterUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the paragraph after which to add this one."),
 *       required = FALSE,
 *     ),
 *   }
 * )
 */
class AddFragment extends ParagraphMutationPluginBase {

  /**
   * {@inheritdoc}
   */
  public function label() {
    $typeId = $this->configuration['type'] ?? NULL;
    if ($typeId) {
      $type = ParagraphsType::load($typeId)->label();
      return "Add fragment " . $type;
    }
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * Execute the mutation.
   *
   * @param ParagraphMutationContextInterface $context
   *   The paragraph mutation context.
   * @param string $name
   *   The fragment name.
   * @param string $hostType
   *   The host type.
   * @param string $hostUuid
   *   The host UUID.
   * @param string $hostFieldName
   *   The host field name.
   * @param string|null $afterUuid
   *   After which paragraph the new one should be added.
   */
  public function execute(
    ParagraphMutationContextInterface $context,
    string $name,
    string $hostType,
    string $hostUuid,
    string $hostFieldName,
    ?string $afterUuid,
  ): void {
    $paragraph = $this->createNewParagraph([
      'type' => 'blokkli_fragment',
      'uuid' => $this->getUuidForNewEntity(),
      'field_blokkli_fragment_name' => $name,
    ]);

    $proxy = new ParagraphProxy($paragraph, $hostType, $hostUuid, $hostFieldName);
    $proxy->setParagraph($paragraph);
    $context->addProxy($proxy, $afterUuid);
  }

}
