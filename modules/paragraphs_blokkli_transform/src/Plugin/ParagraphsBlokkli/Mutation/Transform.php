<?php

namespace Drupal\paragraphs_blokkli_transform\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\paragraphs_blokkli\Exception\MutationBadDataException;
use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginBase;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliHelper;
use Drupal\paragraphs_blokkli_transform\ParagraphTransformPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Transform one or more paragraphs.
 *
 * @ParagraphMutation(
 *   id = "transform",
 *   label = @Translation("Apply the given transform plugin to the paragraphs."),
 *   description = @Translation("Transforms one or more paragraphs."),
 *   arguments = {
 *     "uuids" = @ContextDefinition("string",
 *       label = @Translation("The UUIDs of the paragraph being transformed."),
 *       multiple = TRUE,
 *     ),
 *     "pluginId" = @ContextDefinition("string",
 *       label = @Translation("The ID of the transform plugin.")
 *     ),
 *   }
 * )
 */
class Transform extends ParagraphMutationPluginBase {

  /**
   * The paragraph transform plugin manager.
   */
  protected ParagraphTransformPluginManager $pluginManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('uuid'),
      $container->get('paragraphs_blokkli.helper'),
      $container->get('plugin.manager.paragraph_transform')
    );
  }

  /**
   * Transform constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param UuidInterface $uuidHelper
   *   The UUID helper.
   * @param ParagraphsBlokkliHelper $paragraphsBlokkliHelper
   *   The paragraphs blokkli helper.
   * @param ParagraphTransformPluginManager $pluginManager
   *   The plugin manager.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    EntityTypeManagerInterface $entityTypeManager,
    UuidInterface $uuidHelper,
    ParagraphsBlokkliHelper $paragraphsBlokkliHelper,
    ParagraphTransformPluginManager $pluginManager,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition, $entityTypeManager, $uuidHelper, $paragraphsBlokkliHelper);
    $this->pluginManager = $pluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getAffectedParagraphUuid(): string|null {
    return $this->configuration['uuids'][0] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * Executes the transform plugin on a set of paragraphs.
   *
   * @param \Drupal\paragraphs_blokkli\ParagraphMutationContextInterface $context
   *   The paragraph mutation context.
   * @param array $uuids
   *   An array of paragraph UUIDs to transform.
   * @param string $pluginId
   *   The ID of the transform plugin to use.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   If the transform plugin cannot be created.
   */
  public function execute(ParagraphMutationContextInterface $context, array $uuids, string $pluginId): void {
    // We have to sort the UUIDs by their index, so that they are duplicated in the right order.
    $sorted = $uuids;
    usort($sorted, function ($a, $b) use ($context) {
      $indexA = $context->getIndex($a);
      $indexB = $context->getIndex($b);

      return $indexA <=> $indexB;
    });

    // If the transform plugin has been executed already, use this configuration.
    $executedConfiguration = $this->configuration['transform'] ?? [];

    if (!$this->pluginManager->hasDefinition($pluginId)) {
      throw new MutationBadDataException("The transform plugin '$pluginId' does not exist.");
    }

    /** @var \Drupal\paragraphs_blokkli_transform\ParagraphTransformPluginBase $instance */
    $instance = $this->pluginManager->createInstance($pluginId, $executedConfiguration);
    $instance->transform($context, $sorted);

    $this->configuration['transform'] = $instance->configuration;
  }

}
