<?php

namespace Drupal\paragraphs_blokkli_transform;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliApplicablePluginInterface;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliPluginTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for paragraph_transform plugins.
 */
abstract class ParagraphTransformPluginBase extends PluginBase implements ParagraphTransformInterface, ContainerFactoryPluginInterface, ParagraphsBlokkliApplicablePluginInterface {

  use DependencySerializationTrait;
  use ParagraphsBlokkliPluginTrait;

  /**
   * The UUID service.
   */
  protected UuidInterface $uuidHelper;

  /**
   * The plugin ID.
   *
   * @var string
   */
  protected $pluginId;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('uuid')
    );
  }

  /**
   * Convert constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param UuidInterface $uuidHelper
   *   The UUID helper.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    UuidInterface $uuidHelper,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->uuidHelper = $uuidHelper;
    $this->pluginId = $pluginId;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId(): string {
    return (string) $this->pluginId;
  }

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getBundles(): array {
    return $this->pluginDefinition['bundles'];
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetBundles(): array {
    return $this->pluginDefinition['targetBundles'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function supportsMultiple(): bool {
    return $this->pluginDefinition['multiple'] ?? FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getMin(): int {
    return $this->pluginDefinition['min'] ?? 1;
  }

  /**
   * {@inheritdoc}
   */
  public function getMax(): int {
    return $this->pluginDefinition['max'] ?? -1;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(FieldableEntityInterface $hostEntity, string $langcode): bool {
    // For translatable entities, transform plugins are only available for the
    // source language. Before this change, no transforms were possible in
    // blökkli during translation mode. However some transform plugins might
    // actually implement auto-translation without modifying the structure
    // of paragraphs, which is why transforms were enabled in blökkli during
    // translation mode.
    // To make existing transform plugins work the same as before, by default
    // a transform plugin won't be available during translation.
    if ($hostEntity instanceof TranslatableInterface && $hostEntity->isTranslatable()) {
      $sourceLanguage = $hostEntity->getUntranslated()->language()->getId();
      return $sourceLanguage === $langcode;
    }

    return TRUE;
  }

}
