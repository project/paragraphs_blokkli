<?php

namespace Drupal\paragraphs_blokkli_transform\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines paragraph_transform annotation object.
 *
 * @Annotation
 */
class ParagraphTransform extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The paragraph bundles this transform plugin applies to.
   *
   * @var string[]
   */
  public $bundles;

  /**
   * The resulting bundles after the transform is applied.
   *
   * @var string[]
   */
  public $targetBundles;

  /**
   * The minimum number of paragraphs.
   *
   * @var int|null
   */
  public $min;

  /**
   * The maximum number of paragraphs.
   *
   * @var int|null
   */
  public $max;

}
