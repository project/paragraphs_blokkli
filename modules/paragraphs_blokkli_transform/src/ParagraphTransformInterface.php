<?php

namespace Drupal\paragraphs_blokkli_transform;

use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;

/**
 * Interface for paragraph_transform plugins.
 */
interface ParagraphTransformInterface {

  /**
   * Get the plugin ID.
   *
   * @return string
   *   The plugin ID.
   */
  public function getPluginId(): string;

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label(): string;

  /**
   * Returns the bundles which this plugin applies to.
   *
   * @return string[]
   *   The bundles.
   */
  public function getBundles(): array;

  /**
   * Returns the bundles which this plugin might create.
   *
   * @return string[]
   *   The bundles.
   */
  public function getTargetBundles(): array;

  /**
   * The minimum number of paragraphs.
   *
   * @return int
   *   The minimum number of paragraphs.
   */
  public function getMin(): int;

  /**
   * The maximum number of paragraphs.
   *
   * @return int
   *   The maximum number of paragraphs.
   */
  public function getMax(): int;

  /**
   * Transform the given paragraph.
   *
   * @param ParagraphMutationContextInterface $context
   *   The paragraph mutation context.
   * @param string[] $uuids
   *   The UUIDs to transform.
   */
  public function transform(ParagraphMutationContextInterface $context, array $uuids);

}
