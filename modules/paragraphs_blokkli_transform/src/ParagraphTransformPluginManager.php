<?php

namespace Drupal\paragraphs_blokkli_transform;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * ParagraphTransform plugin manager.
 */
class ParagraphTransformPluginManager extends DefaultPluginManager {

  /**
   * Constructs ParagraphTransformPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/ParagraphsBlokkli/Transform',
      $namespaces,
      $module_handler,
      'Drupal\paragraphs_blokkli_transform\ParagraphTransformInterface',
      'Drupal\paragraphs_blokkli_transform\Annotation\ParagraphTransform'
    );
    $this->alterInfo('paragraph_transform_info');
    $this->setCacheBackend($cache_backend, 'paragraph_transform_plugins');
  }

  /**
   * Find plugins for the given bundle.
   *
   * @param string $bundle
   *   The bundles.
   *
   * @return ParagraphTransformInterface[]
   *   The transform plugins.
   */
  public function getPluginForBundleCombination(string $bundle): array {
    $definitions = $this->getDefinitions();
    $instances = [];
    foreach ($definitions as $pluginId => $definition) {
      if (in_array($bundle, $definition['bundles'])) {
        $instances[] = $this->createInstance($pluginId);
      }
    }

    return $instances;
  }

}
