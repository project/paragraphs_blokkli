<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli_search;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\paragraphs_blokkli_search\Model\LibraryItemsSearchResults;

/**
 * Interface for search plugins providing library items search.
 */
interface ParagraphsBlokkliSearchLibraryItemsInterface {

  /**
   * The search type.
   */
  public const SEARCH_TYPE = 'library_items';

  /**
   * Get the results for the library items search.
   *
   * @param string[] $bundles
   *   The bundles to filter for.
   * @param string|null $text
   *   The search term.
   * @param int|null $page
   *   The current page.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface|null $metadata
   *   The cache metadata.
   *
   * @return \Drupal\paragraphs_blokkli_search\Model\LibraryItemsSearchResults
   *   The library items results.
   */
  public function getLibraryItemsSearchResults(array $bundles, ?string $text = NULL, ?int $page = NULL, ?RefinableCacheableDependencyInterface $metadata = NULL): LibraryItemsSearchResults;

}
