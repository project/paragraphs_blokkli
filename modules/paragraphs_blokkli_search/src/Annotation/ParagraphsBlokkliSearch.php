<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli_search\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines paragraphs_blokkli_search annotation object.
 *
 * @Annotation
 */
final class ParagraphsBlokkliSearch extends Plugin {

  /**
   * The plugin ID.
   */
  public string $id;

  /**
   * The human-readable name of the plugin.
   *
   * @ingroup plugin_translatable
   */
  public string $label;

  /**
   * The description of the plugin.
   *
   * @ingroup plugin_translatable
   */
  public string $description;

}
