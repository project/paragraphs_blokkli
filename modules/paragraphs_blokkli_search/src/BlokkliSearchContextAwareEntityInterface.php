<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli_search;

/**
 * Provides an interface for entities to define their search context.
 */
interface BlokkliSearchContextAwareEntityInterface {

  /**
   * Return the search context value for an entity.
   *
   * The search context should give blökkli users additional context about a
   * search result or media library item. For example, when there are entities
   * with the same title, a helpful context would be an ID or a category to help
   * differentiate the search results.
   *
   * If no context is returned, blökkli will default to a context consisting of:
   * ID - Entity Type - Entity Bundle.
   *
   * @return string|null
   *   The context value.
   */
  public function getBlokkliSearchContextValue(): string|null;

}
