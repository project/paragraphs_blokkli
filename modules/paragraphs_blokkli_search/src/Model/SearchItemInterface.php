<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli_search\Model;

/**
 * Interface for search items.
 */
interface SearchItemInterface {

  /**
   * Get the entity type.
   *
   * @return string
   *   The entity type.
   */
  public function getEntityTypeId(): string;

  /**
   * Get the entity bundle.
   *
   * @return string
   *   The entity bundle.
   */
  public function bundle(): string;

  /**
   * Get the entity ID.
   *
   * @return string
   *   The entity ID.
   */
  public function getEntityId(): string;

}
