<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli_search\Model;

/**
 * A class for library items search results.
 */
class LibraryItemsSearchResults {

  /**
   * Constructor.
   *
   * @param int $perPage
   *   The items per page.
   * @param int $total
   *   The total number of items.
   * @param string[] $ids
   *   The IDs of the paragraphs_library_item entities.
   */
  public function __construct(
    public readonly int $perPage,
    public readonly int $total,
    public readonly array $ids,
  ) {}

}
