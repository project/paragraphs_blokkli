<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli_search\Model;

/**
 * A model class for a media library search item.
 */
class MediaLibrarySearchItem implements SearchItemInterface {

  /**
   * Constructor.
   *
   * @param string $mediaId
   *   The ID of the media entity.
   * @param string $mediaBundle
   *   The bundle of the media entity.
   * @param string $label
   *   The label.
   * @param string $context
   *   The additional context.
   * @param string|null $thumbnail
   *   The URL for a thumbnail image.
   * @param string|null $icon
   *   The blökkli icon to display.
   */
  public function __construct(
    public readonly string $mediaId,
    public readonly string $mediaBundle,
    public readonly string $label,
    public readonly string $context,
    public readonly string|null $thumbnail = NULL,
    public readonly string|null $icon = NULL,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeId(): string {
    return 'media';
  }

  /**
   * {@inheritdoc}
   */
  public function bundle(): string {
    return $this->mediaBundle;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityId(): string {
    return $this->mediaId;
  }

}
