<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli_search\Model;

/**
 * Provides an interface defining a blokkli search tab entity type.
 */
class MediaLibrarySearchResults {

  /**
   * Constructor.
   *
   * @param int $perPage
   *   The number of results per page.
   * @param int $total
   *   The total number of items.
   * @param \Drupal\paragraphs_blokkli_search\Model\MediaLibrarySearchItem[] $items
   *   The search result items.
   * @param array $filters
   *   The available filters.
   */
  public function __construct(
    public readonly int $perPage,
    public readonly int $total,
    public readonly array $items,
    public readonly array $filters,
  ) {}

}
