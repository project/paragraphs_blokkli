<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli_search\Model;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Class for a blökkli search result item.
 */
class ContentSearchItem implements SearchItemInterface {

  /**
   * The related content entity.
   */
  protected ContentEntityInterface|null $entity = NULL;

  /**
   * Constructs the object.
   *
   * @param string $entityType
   *   The entity type.
   * @param string $entityBundle
   *   The entity bundle.
   * @param string $entityId
   *   The entity ID.
   * @param string $title
   *   The title.
   * @param string|null $text
   *   The text.
   * @param string|null $imageUrl
   *   The full URL for an image to show for the result.
   * @param string|null $context
   *   Optional additional context displayed after the title.
   */
  public function __construct(
    public readonly string $entityType,
    public readonly string $entityBundle,
    public readonly string $entityId,
    public readonly string $title,
    public readonly string|null $text = NULL,
    public readonly string|null $imageUrl = NULL,
    public readonly string|null $context = NULL,
  ) {
  }

  /**
   * Set the entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   */
  public function setEntity(ContentEntityInterface $entity) {
    $this->entity = $entity;
  }

  /**
   * Get the entity.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *   The entity.
   */
  public function getEntity(): ContentEntityInterface|null {
    return $this->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeId(): string {
    return $this->entityType;
  }

  /**
   * {@inheritdoc}
   */
  public function bundle(): string {
    return $this->entityBundle;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityId(): string {
    return $this->entityId;
  }

}
