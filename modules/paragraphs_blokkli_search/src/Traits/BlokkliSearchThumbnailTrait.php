<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli_search\Traits;

use Drupal\Core\Entity\EntityInterface;
use Drupal\image\Entity\ImageStyle;
use Drupal\media\MediaInterface;
use Drupal\paragraphs_blokkli_search\BlokkliSearchThumbnailAwareEntityInterface;

/**
 * Reusable methods for getting a thumbnail for an entity.
 */
trait BlokkliSearchThumbnailTrait {

  /**
   * Get the thumbnail URL for an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param string $imageStyleName
   *   The name of the image style to use.
   *
   * @return string|null
   *   The thumbnail URL.
   */
  protected function getEntityThumbnail(EntityInterface $entity, string $imageStyleName): string|null {
    $possibleMedia = $entity;

    // If the entity implements this interface it may define its own thumbnail
    // media entity.
    if ($entity instanceof BlokkliSearchThumbnailAwareEntityInterface) {
      $media = $entity->getBlokkliThumbnailMedia();
      if ($media) {
        $possibleMedia = $media;
      }
    }

    if (!$possibleMedia instanceof MediaInterface) {
      return NULL;
    }

    /** @var \Drupal\file\Entity\File|null $file */
    $file = $possibleMedia->get('thumbnail')->entity;

    if (!$file) {
      return NULL;
    }

    $uri = $file->getFileUri();
    $imageStyle = ImageStyle::load($imageStyleName);

    if (!$imageStyle) {
      return NULL;
    }

    return $imageStyle->buildUrl($uri);
  }

}
