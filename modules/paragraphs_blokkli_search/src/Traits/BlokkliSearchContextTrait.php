<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli_search\Traits;

use Drupal\Core\Entity\EntityInterface;
use Drupal\paragraphs_blokkli_search\BlokkliSearchContextAwareEntityInterface;

/**
 * Reusable method for getting the 'search context' of an entity.
 */
trait BlokkliSearchContextTrait {

  /**
   * Get the search context for an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return string
   *   The search context.
   */
  protected function getEntitySearchContext(EntityInterface $entity): string {
    if ($entity instanceof BlokkliSearchContextAwareEntityInterface) {
      $providedContext = $entity->getBlokkliSearchContextValue();
      if ($providedContext) {
        return $providedContext;
      }
    }

    $contexts = [
      $entity->id(),
      $entity->getEntityType()->getLabel(),
    ];

    /** @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo */
    $entityTypeBundleInfo = \Drupal::service('entity_type.bundle.info');
    $bundleInfo = $entityTypeBundleInfo->getBundleInfo($entity->getEntityTypeId());
    $bundleLabel = $bundleInfo[$entity->bundle()]['label'] ?? NULL;
    if ($bundleLabel) {
      $contexts[] = $bundleLabel;
    }

    return implode(' - ', $contexts);
  }

}
