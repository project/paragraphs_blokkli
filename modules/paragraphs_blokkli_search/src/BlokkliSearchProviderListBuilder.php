<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli_search;

use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a listing of blokkli search providers.
 */
final class BlokkliSearchProviderListBuilder extends DraggableListBuilder {

  /**
   * {@inheritdoc}
   */
  protected $limit = FALSE;

  /**
   * Constructs a new PbEntityMappingListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entityType
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    EntityTypeInterface $entityType,
    EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($entityType, $entityTypeManager->getStorage($entityType->id()));
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entityType): self {
    return new static(
      $entityType,
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'paragraphs_blokkli_search_providers';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['label'] = $this->t('Label');
    $header['id'] = $this->t('ID');
    $header['search_type'] = $this->t('Search type');
    $header['search_plugin'] = $this->t('Search Plugin');
    $header['status'] = $this->t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    $row = [];

    /** @var \Drupal\paragraphs_blokkli_search\BlokkliSearchProviderInterface $entity */
    $row['label'] = $entity->label();

    $row['id'] = [
      '#markup' => $entity->id(),
    ];

    $row['search_type'] = [
      '#markup' => $entity->getSearchType(),
    ];

    $row['search_plugin'] = [
      '#markup' => $entity->getSearchPluginId(),
    ];

    $row['status'] = [
      '#markup' => $entity->status() ? $this->t('Enabled') : $this->t('Disabled'),
    ];
    return $row + parent::buildRow($entity);
  }

}
