<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli_search;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\paragraphs_blokkli_search\Model\MediaLibrarySearchResults;

/**
 * Interface for search plugins providing media library search.
 */
interface ParagraphsBlokkliSearchMediaLibraryInterface {

  /**
   * The search type.
   */
  public const SEARCH_TYPE = 'media_library';

  /**
   * Get the results for the media library search.
   *
   * @param string|null $text
   *   The search term.
   * @param string|null $bundle
   *   The bundle to filter for.
   * @param int|null $page
   *   The current page.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface|null $metadata
   *   The cache metadata.
   *
   * @return \Drupal\paragraphs_blokkli_search\Model\MediaLibrarySearchResults
   *   The media library results.
   */
  public function getMediaLibraryResults(?string $text = NULL, ?string $bundle = NULL, ?int $page = NULL, ?RefinableCacheableDependencyInterface $metadata = NULL): MediaLibrarySearchResults;

}
