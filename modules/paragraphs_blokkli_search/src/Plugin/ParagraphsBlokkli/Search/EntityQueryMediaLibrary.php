<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli_search\Plugin\ParagraphsBlokkli\Search;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\media\MediaInterface;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliConfig;
use Drupal\paragraphs_blokkli_search\Model\MediaLibrarySearchItem;
use Drupal\paragraphs_blokkli_search\Model\MediaLibrarySearchResults;
use Drupal\paragraphs_blokkli_search\ParagraphsBlokkliSearchMediaLibraryInterface;
use Drupal\paragraphs_blokkli_search\ParagraphsBlokkliSearchPluginBase;
use Drupal\paragraphs_blokkli_search\Traits\BlokkliSearchContextTrait;
use Drupal\paragraphs_blokkli_search\Traits\BlokkliSearchThumbnailTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Search plugin that performns an entity query for a single entity type.
 *
 * @ParagraphsBlokkliSearch(
 *   id = "entity_query_media_library",
 *   label = @Translation("Entity Query Media Library"),
 *   description = @Translation("Performs an entity query on the database to search for media entities."),
 * )
 */
class EntityQueryMediaLibrary extends ParagraphsBlokkliSearchPluginBase implements ContainerFactoryPluginInterface, ParagraphsBlokkliSearchMediaLibraryInterface {

  use StringTranslationTrait;
  use DependencySerializationTrait;
  use BlokkliSearchThumbnailTrait;
  use BlokkliSearchContextTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('paragraphs_blokkli.config'),
    );
  }

  /**
   * EntityQueryMediaLibrary constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityBundleInfo
   *   Entity bundle info.
   * @param \Drupal\paragraphs_blokkli\ParagraphsBlokkliConfig $blokkliConfig
   *   The blokkli config.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    EntityTypeManagerInterface $entityTypeManager,
    protected EntityTypeBundleInfoInterface $entityBundleInfo,
    protected ParagraphsBlokkliConfig $blokkliConfig,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition, $entityTypeManager);
  }

  /**
   * {@inheritdoc}
   */
  public function getMediaLibraryResults(?string $text = NULL, ?string $bundle = NULL, ?int $page = NULL, ?RefinableCacheableDependencyInterface $metadata = NULL): MediaLibrarySearchResults {
    $perPage = (int) $this->getConfiguration()['per_page'];
    $filterBundles = $this->getFilterBundles($bundle);

    if (empty($filterBundles)) {
      return [];
    }

    $storage = $this->entityTypeManager->getStorage('media');
    $entityType = $storage->getEntityType();

    $offset = ($page ?? 0) * $perPage;

    $query = $storage
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('bundle', $filterBundles, 'IN')
      ->range($offset, $perPage);

    if ($text) {
      $query->condition('name', "%$text%", 'LIKE');
    }

    $countQuery = clone $query;
    $countQuery->range(NULL, NULL)->count();
    $count = $countQuery->execute();

    $ids = $query->execute();

    $entities = $storage->loadMultiple($ids);

    $results = [];

    foreach ($entities as $entity) {
      $results[] = $this->mapEntity($entity);
      $metadata?->addCacheableDependency($entity);
    }

    $metadata?->addCacheTags($entityType->getListCacheTags());

    return new MediaLibrarySearchResults(
      $perPage,
      $count,
      array_filter($results),
      $this->getFilters(),
    );
  }

  /**
   * Map the entity.
   *
   * @param \Drupal\media\Entity\Media $media
   *   The media entity.
   *
   * @return \Drupal\paragraphs_blokkli_search\Model\MediaLibrarySearchItem|null
   *   The media result item.
   */
  protected function mapEntity(MediaInterface $media): MediaLibrarySearchItem|null {
    $id = $media->id();
    $label = $media->label();
    $bundle = $media->bundle();
    $context = $this->getEntitySearchContext($media);

    if (!$id || !$label || !$bundle) {
      return NULL;
    }

    $imageStyleName = $this->blokkliConfig->getImageStyle();
    $thumbnail = $imageStyleName ? $this->getEntityThumbnail($media, $imageStyleName) : NULL;
    return new MediaLibrarySearchItem($id, $bundle, $label, $context, $thumbnail);
  }

  /**
   * Get the media bundles to filter for.
   *
   * @param string|null $filteredBundle
   *
   * @return string[]
   *   The media bundles to filter for.
   */
  protected function getFilterBundles(string|null $filteredBundle): array {
    // Get the media bundles for which a mapping exists.
    /** @var \Drupal\paragraphs_blokkli\PbEntityMappingStorage $mappingStorage */
    $mappingStorage = $this->entityTypeManager->getStorage('pb_entity_mapping');
    $possibleBundles = $mappingStorage->getMappedBundlesForEntityType('media');

    // If no bundle is specified, filter by all supported bundles.
    if (!$filteredBundle || $filteredBundle === 'all') {
      return $possibleBundles;
    }

    // Bundle is not valid.
    if (!in_array($filteredBundle, $possibleBundles)) {
      return $possibleBundles;
    }

    // Filter by specified bundle.
    return [$filteredBundle];
  }

  /**
   * Build the available filters.
   *
   * @return array
   *   The available filters.
   */
  protected function getFilters(): array {
    /** @var \Drupal\paragraphs_blokkli\PbEntityMappingStorage $mappingStorage */
    $mappingStorage = $this->entityTypeManager->getStorage('pb_entity_mapping');
    $possibleBundles = $mappingStorage->getMappedBundlesForEntityType('media');
    $allBundles = $this->entityBundleInfo->getBundleInfo('media');

    $bundleOptions = [
      'all' => $this->t('All'),
    ];

    foreach ($allBundles as $bundle => $info) {
      if (in_array($bundle, $possibleBundles)) {
        $bundleOptions[$bundle] = $info['label'];
      }
    }

    return [
      [
        'id' => 'text',
        'type' => 'text',
        'label' => $this->t('Text'),
        'placeholder' => $this->t('Enter search term'),
      ],
      [
        'id' => 'bundle',
        'type' => 'select',
        'label' => $this->t('Media type'),
        'default' => 'all',
        'options' => $bundleOptions,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function canBeUsed(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'per_page' => 32,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();

    $perPage = $form_state->getValue(['search_plugin', 'configuration', $this->getPluginId(), 'per_page']) ?? $configuration['per_page'] ?? NULL;

    $form['per_page'] = [
      '#type' => 'number',
      '#title' => $this->t('Items per page'),
      '#default_value' => $perPage,
      '#min' => 16,
      '#max' => 64,
      '#step' => 1,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getPersistableConfiguration(FormStateInterface $formState): array {
    $perPage = $formState->getValue(['search_plugin', 'configuration', $this->getPluginId(), 'per_page']);

    return [
      'per_page' => $perPage,
    ];
  }

}
