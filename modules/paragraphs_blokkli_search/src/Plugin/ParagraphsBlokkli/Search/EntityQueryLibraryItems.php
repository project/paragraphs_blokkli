<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli_search\Plugin\ParagraphsBlokkli\Search;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\paragraphs_blokkli_search\Model\LibraryItemsSearchResults;
use Drupal\paragraphs_blokkli_search\ParagraphsBlokkliSearchLibraryItemsInterface;
use Drupal\paragraphs_blokkli_search\ParagraphsBlokkliSearchPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Search plugin that performns an entity query for a single entity type.
 *
 * @ParagraphsBlokkliSearch(
 *   id = "entity_query_library_items",
 *   label = @Translation("Entity Query Library Items"),
 *   description = @Translation("Performs an entity query on the database to search for paragraphs library items."),
 * )
 */
class EntityQueryLibraryItems extends ParagraphsBlokkliSearchPluginBase implements ContainerFactoryPluginInterface, ParagraphsBlokkliSearchLibraryItemsInterface {

  use StringTranslationTrait;
  use DependencySerializationTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
    );
  }

  /**
   * EntityQueryMediaLibrary constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityBundleInfo
   *   Entity bundle info.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    EntityTypeManagerInterface $entityTypeManager,
    protected EntityTypeBundleInfoInterface $entityBundleInfo,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition, $entityTypeManager);
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraryItemsSearchResults(array $bundles, ?string $text = NULL, ?int $page = NULL, ?RefinableCacheableDependencyInterface $metadata = NULL): LibraryItemsSearchResults {
    $configuration = $this->getConfiguration();
    $storage = $this->entityTypeManager->getStorage('paragraphs_library_item');
    $entityType = $storage->getEntityType();

    $perPage = (int) $configuration['per_page'];

    $offset = ($page ?? 0) * $perPage;

    $query = $storage
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('paragraphs.target_id.entity:paragraph.type', $bundles, 'IN')
      ->range($offset, $perPage);

    if ($text) {
      $query->condition('label', "%$text%", 'LIKE');
    }

    $countQuery = clone $query;
    $countQuery->range(NULL, NULL)->count();
    $count = $countQuery->execute();

    $ids = $query->execute();

    $metadata?->addCacheTags($entityType->getListCacheTags());

    return new LibraryItemsSearchResults($perPage, $count, $ids);
  }

  /**
   * {@inheritdoc}
   */
  public function canBeUsed(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'per_page' => 20,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();
    $perPage = $form_state->getValue(['search_plugin', 'configuration', $this->getPluginId(), 'per_page']) ?? $configuration['per_page'] ?? NULL;

    $form['per_page'] = [
      '#type' => 'number',
      '#title' => $this->t('Items per page'),
      '#default_value' => $perPage,
      '#min' => 8,
      '#max' => 50,
      '#step' => 1,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getPersistableConfiguration(FormStateInterface $formState): array {
    $perPage = $formState->getValue(['search_plugin', 'configuration', $this->getPluginId(), 'per_page']);

    return [
      'per_page' => $perPage,
    ];
  }

}
