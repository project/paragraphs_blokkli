<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli_search\Plugin\ParagraphsBlokkli\Search;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\StringItemBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliConfig;
use Drupal\paragraphs_blokkli_search\Model\ContentSearchItem;
use Drupal\paragraphs_blokkli_search\ParagraphsBlokkliSearchContentInterface;
use Drupal\paragraphs_blokkli_search\ParagraphsBlokkliSearchPluginBase;
use Drupal\paragraphs_blokkli_search\Traits\BlokkliSearchContextTrait;
use Drupal\paragraphs_blokkli_search\Traits\BlokkliSearchThumbnailTrait;
use Drupal\text\Plugin\Field\FieldType\TextItemBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Search plugin that performns an entity query for a single entity type.
 *
 * @ParagraphsBlokkliSearch(
 *   id = "entity_query",
 *   label = @Translation("Entity Query"),
 *   description = @Translation("Performs an entity query on the database. Only supports a single entity type per search."),
 * )
 */
class EntityQuery extends ParagraphsBlokkliSearchPluginBase implements ContainerFactoryPluginInterface, ParagraphsBlokkliSearchContentInterface {

  use StringTranslationTrait;
  use DependencySerializationTrait;
  use BlokkliSearchContextTrait;
  use BlokkliSearchThumbnailTrait;

  /**
   * Field types that can be used for text.
   */
  const TEXT_FIELD_TYPES = ['text', 'text_long', 'text_with_summary', 'string'];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('paragraphs_blokkli.config'),
      $container->get('entity_field.manager'),
    );
  }

  /**
   * EntityQuery constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\paragraphs_blokkli\ParagraphsBlokkliConfig $blokkliConfig
   *   The blokkli config.
   * @param \Drupal\Core\Entity\EntityFieldManager $entityFieldManager
   *   The entity field manager.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    EntityTypeManagerInterface $entityTypeManager,
    protected ParagraphsBlokkliConfig $blokkliConfig,
    protected EntityFieldManagerInterface $entityFieldManager,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition, $entityTypeManager);
  }

  /**
   * {@inheritdoc}
   */
  public function getResults(string $text, ?RefinableCacheableDependencyInterface $metadata = NULL): array {
    $configuration = $this->getConfiguration();
    $entityTypeId = $configuration['entity_type'] ?? NULL;
    $selectedBundles = $configuration['bundles'] ?? [];

    if (!$entityTypeId || empty($selectedBundles)) {
      return [];
    }

    /** @var \Drupal\paragraphs_blokkli\PbEntityMappingStorage $mappingStorage */
    $mappingStorage = $this->entityTypeManager->getStorage('pb_entity_mapping');

    // Get the bundles for the entity type for which a mapping exists.
    $mappedBundles = $mappingStorage->getMappedBundlesForEntityType($entityTypeId);

    // Get the intersection of the enabled bundles and the bundles for which a
    // mapping exists.
    $bundles = array_intersect($mappedBundles, $selectedBundles);

    if (empty($bundles)) {
      return [];
    }

    $storage = $this->entityTypeManager->getStorage($entityTypeId);
    $entityType = $storage->getEntityType();

    $ids = $storage
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition($entityType->getKey('label'), "%$text%", 'LIKE')
      ->condition($entityType->getKey('bundle'), $bundles, 'IN')
      ->range(0, 50)
      ->execute();

    $entities = $storage->loadMultiple($ids);

    $results = [];

    foreach ($entities as $entity) {
      $results[] = $this->mapEntity($entity);
      $metadata?->addCacheableDependency($entity);
    }

    $metadata?->addCacheTags($entityType->getListCacheTags());

    return $results;
  }

  /**
   * Map the entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   *
   * @return \Drupal\paragraphs_blokkli_search\Model\ContentSearchItem
   *   The search result.
   */
  protected function mapEntity(ContentEntityInterface $entity): ContentSearchItem {
    $imageStyle = $this->blokkliConfig->getImageStyle();
    $thumbnail = $imageStyle ? $this->getEntityThumbnail($entity, $imageStyle) : NULL;
    $result = new ContentSearchItem(
      $entity->getEntityTypeId(),
      $entity->bundle(),
      $entity->id(),
      $entity->label(),
      $this->getEntityText($entity),
      $thumbnail,
      $this->getEntitySearchContext($entity),
    );

    $result->setEntity($entity);
    return $result;
  }

  /**
   * Get the text for an entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   *
   * @return string|null
   *   The text.
   */
  protected function getEntityText(ContentEntityInterface $entity): string|null {
    $possibleFields = $this->getConfiguration()['text_fields'] ?? [];

    foreach ($possibleFields as $fieldName) {
      if ($entity->hasField($fieldName)) {
        $field = $entity->get($fieldName);
        if (!in_array($field->getFieldDefinition()->getType(), self::TEXT_FIELD_TYPES)) {
          continue;
        }

        $item = $field->first();
        if (!$item instanceof TextItemBase && !$item instanceof StringItemBase) {
          continue;
        }

        $text = $item->get('value')->getValue();
        if (is_string($text)) {
          // Return a safe, truncated summary of the text.
          return Unicode::truncate(
            strip_tags(html_entity_decode($text)),
            100,
            FALSE,
            TRUE);
        }
      }
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function canBeUsed(): bool {
    $entityTypeId = $this->getDerivativeId();

    /** @var \Drupal\paragraphs_blokkli\PbEntityMappingStorage $mappingStorage */
    $mappingStorage = $this->entityTypeManager->getStorage('pb_entity_mapping');
    $mappedBundles = $mappingStorage->getMappedBundlesForEntityType($entityTypeId);

    // This search plugin may only be used if at least one mapping for the
    // given entity type exists.
    return !empty($mappedBundles);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();
    $entityTypeId = $form_state->getValue(['search_plugin', 'configuration', $this->getPluginId(), 'entity_type']) ?? $configuration['entity_type'] ?? NULL;

    $entityTypes = $this->entityTypeManager->getDefinitions();

    $entityTypeOptions = [
      '' => $this->t('Select an entity type'),
    ];

    foreach ($entityTypes as $id => $definition) {
      if ($definition instanceof ContentEntityTypeInterface) {
        $entityTypeOptions[$id] = $definition->getLabel();
      }
    }

    $form['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity Type'),
      '#options' => $entityTypeOptions,
      '#default_value' => $entityTypeId,
      '#ajax' => [
        'callback' => [$this, 'reloadFields'],
        'wrapper' => 'edit-search-plugin-configuration-wrapper',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Loading bundles'),
        ],
      ],
    ];

    if ($entityTypeId) {
      /** @var \Drupal\paragraphs_blokkli\PbEntityMappingStorage $mappingStorage */
      $mappingStorage = $this->entityTypeManager->getStorage('pb_entity_mapping');
      $mappedBundles = $mappingStorage->getMappedBundlesForEntityType($entityTypeId);

      /** @var \Drupal\Core\Entity\EntityTypeBundleInfo $entityBundleInfo */
      $entityBundleInfo = \Drupal::service('entity_type.bundle.info');

      $bundleInfo = $entityBundleInfo->getBundleInfo($entityTypeId);

      $form['bundles'] = [
        '#type' => 'container',
      ];

      $fieldOptions = [];
      $fieldDescriptions = [];

      foreach ($bundleInfo as $bundle => $info) {
        $selected = $configuration['bundles'] ?? [];
        $label = $info['label'] ?? $bundle;
        $disabled = !in_array($bundle, $mappedBundles);
        $form['bundles'][$bundle] = [
          '#type' => 'checkbox',
          '#title' => $label,
          '#disabled' => $disabled,
          '#default_value' => !$disabled && in_array($bundle, $selected),
        ];

        if ($disabled) {
          $form['bundles'][$bundle]['#description'] = $this->t('This bundle is disabled because no entity mapping exists.');
        }
        else {
          $fields = $this->entityFieldManager->getFieldDefinitions($entityTypeId, $bundle);

          foreach ($fields as $fieldName => $field) {
            if (empty($fieldOptions[$fieldName])) {
              $fieldType = $field->getType();
              if (in_array($fieldType, self::TEXT_FIELD_TYPES)) {
                $fieldLabel = $field->getLabel();
                $fieldOptions[$fieldName] = "$fieldLabel ($fieldName) [$fieldType]";
                $fieldDescriptions[$fieldName] = [
                  '#description' => $field->getDescription(),
                ];
              }
            }
          }
        }
      }

      $form['text_fields'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Text fields'),
        '#options' => $fieldOptions,
        '#default_value' => $configuration['text_fields'] ?? [],
        ...$fieldDescriptions,
      ];

    }

    return $form;
  }

  /**
   * Ajax Callback for form reload.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function reloadFields(array &$form, FormStateInterface $form_state) {
    return $form['search_plugin']['configuration'][$this->getPluginId()];
  }

  /**
   * {@inheritdoc}
   */
  public function getPersistableConfiguration(FormStateInterface $formState): array {
    $entityTypeId = $formState->getValue(['search_plugin', 'configuration', $this->getPluginId(), 'entity_type']);
    $bundles = $formState->getValue(['search_plugin', 'configuration', $this->getPluginId(), 'bundles']);
    $textFields = $formState->getValue(['search_plugin', 'configuration', $this->getPluginId(), 'text_fields']);

    $selected = array_keys(array_filter($bundles));

    return [
      'entity_type' => $entityTypeId,
      'bundles' => $selected,
      'text_fields' => $textFields,
    ];
  }

}
