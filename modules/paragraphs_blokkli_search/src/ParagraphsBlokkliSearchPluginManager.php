<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli_search;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\paragraphs_blokkli_search\Annotation\ParagraphsBlokkliSearch;

/**
 * ParagraphsBlokkliSearch plugin manager.
 */
final class ParagraphsBlokkliSearchPluginManager extends DefaultPluginManager {

  /**
   * Constructs the object.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/ParagraphsBlokkli/Search', $namespaces, $module_handler, ParagraphsBlokkliSearchInterface::class, ParagraphsBlokkliSearch::class);
    $this->alterInfo('paragraphs_blokkli_search_info');
    $this->setCacheBackend($cache_backend, 'paragraphs_blokkli_search_plugins');
  }

}
