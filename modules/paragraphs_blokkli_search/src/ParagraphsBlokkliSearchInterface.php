<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli_search;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Interface for paragraphs_blokkli_search plugins.
 */
interface ParagraphsBlokkliSearchInterface extends ConfigurableInterface, PluginFormInterface {

  /**
   * Returns the translated plugin label.
   */
  public function label(): string;

  /**
   * Returns the translated plugin description.
   */
  public function description(): string;

  /**
   * Whether the search plugin can be used.
   */
  public function canBeUsed(): bool;

  /**
   * Get the plugin configuration to persist.
   *
   * @param \Drupal\Core\Form\FormState $formState
   *   The form state.
   *
   * @return array
   *   The configuration to persist.
   */
  public function getPersistableConfiguration(FormStateInterface $formState): array;

  /**
   * Whether the search plugin supports the given type.
   */
  public function supportsType(string $type): bool;

}
