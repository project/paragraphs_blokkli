<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli_search\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\paragraphs_blokkli_search\BlokkliSearchProviderInterface;
use Drupal\paragraphs_blokkli_search\ParagraphsBlokkliSearchInterface;

/**
 * Defines the blokkli search provider entity type.
 *
 * @ConfigEntityType(
 *   id = "blokkli_search_provider",
 *   label = @Translation("Blokkli Search Provider"),
 *   label_collection = @Translation("Blokkli Search Providers"),
 *   label_singular = @Translation("blokkli search provider"),
 *   label_plural = @Translation("blokkli search providers"),
 *   label_count = @PluralTranslation(
 *     singular = "@count blokkli search provider",
 *     plural = "@count blokkli search providers",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\paragraphs_blokkli_search\BlokkliSearchProviderListBuilder",
 *     "form" = {
 *       "add" = "Drupal\paragraphs_blokkli_search\Form\BlokkliSearchProviderForm",
 *       "edit" = "Drupal\paragraphs_blokkli_search\Form\BlokkliSearchProviderForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   config_prefix = "blokkli_search_provider",
 *   admin_permission = "administer blokkli settings",
 *   links = {
 *     "collection" = "/admin/config/content/blokkli/search",
 *     "add-form" = "/admin/config/content/blokkli/search/add",
 *     "edit-form" = "/admin/config/content/blokkli/search/{blokkli_search_provider}",
 *     "delete-form" = "/admin/config/content/blokkli/search/{blokkli_search_provider}/delete",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "weight" = "weight",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "weight",
 *     "description",
 *     "search_type",
 *     "search_plugin_id",
 *     "search_plugin_configuration",
 *   },
 * )
 */
final class BlokkliSearchProvider extends ConfigEntityBase implements BlokkliSearchProviderInterface {

  /**
   * The ID.
   *
   * @var string
   */
  protected string $id;

  /**
   * The label.
   *
   * @var string
   */
  protected string $label;

  /**
   * The search type.
   *
   * @var string
   */
  protected string $search_type = 'content';

  /**
   * The description.
   *
   * @var string
   */
  protected string $description = '';

  /**
   * The search plugin.
   *
   * @var string
   */
  protected $search_plugin_id = '';

  /**
   * {@inheritdoc}
   */
  protected array $search_plugin_configuration = [];

  /**
   * {@inheritdoc}
   */
  protected int $weight = 0;

  /**
   * {@inheritdoc}
   */
  public function getSearchPluginId(): string {
    return $this->search_plugin_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getSearchPluginConfiguration(): array|null {
    return $this->search_plugin_configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function getSearchType(): string {
    return $this->search_type;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight(): int {
    return $this->weight;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight(int $weight): static {
    $this->weight = $weight;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSearchPlugin(): ParagraphsBlokkliSearchInterface {
    /** @var \Drupal\paragraphs_blokkli_search\ParagraphsBlokkliSearchPluginManager $pluginManager */
    $pluginManager = \Drupal::service('plugin.manager.paragraphs_blokkli_search');

    return $pluginManager->createInstance($this->getSearchPluginId(), $this->getSearchPluginConfiguration());
  }

}
