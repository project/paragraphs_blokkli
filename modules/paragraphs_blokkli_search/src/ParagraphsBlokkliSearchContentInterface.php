<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli_search;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;

/**
 * Interface for search plugins providing content search.
 */
interface ParagraphsBlokkliSearchContentInterface {

  /**
   * The search type.
   */
  public const SEARCH_TYPE = 'content';

  /**
   * Get the results for the given search term.
   *
   * @param string $text
   *   The search term.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface|null $metadata
   *   The cache metadata.
   *
   * @return \Drupal\paragraphs_blokkli_search\Model\ContentSearchItem[]
   *   The search results.
   */
  public function getResults(string $text, ?RefinableCacheableDependencyInterface $metadata = NULL): array;

}
