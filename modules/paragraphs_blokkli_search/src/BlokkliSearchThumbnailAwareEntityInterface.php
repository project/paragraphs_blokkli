<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli_search;

use Drupal\media\MediaInterface;

/**
 * Provides an interface for entities to define their search thumbnail.
 */
interface BlokkliSearchThumbnailAwareEntityInterface {

  /**
   * Return the media entity to use as a thumbnail.
   *
   * @return \Drupal\media\MediaInterface|null
   *   The media entity.
   */
  public function getBlokkliThumbnailMedia(): MediaInterface|null;

}
