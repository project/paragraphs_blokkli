<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli_search;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a blokkli search provider entity type.
 */
interface BlokkliSearchProviderInterface extends ConfigEntityInterface {

  /**
   * Get the search plugin ID.
   *
   * @return string
   *   The search plugin ID.
   */
  public function getSearchPluginId(): string;

  /**
   * Get the search plugin configuration.
   *
   * @return array|null
   *   The search plugin configuration.
   */
  public function getSearchPluginConfiguration(): array|null;

  /**
   * Get the search type.
   *
   * @return string
   *   The search type.
   */
  public function getSearchType(): string;

  /**
   * Get the search plugin instance.
   *
   * @return \Drupal\paragraphs_blokkli_search\ParagraphsBlokkliSearchInterface
   *   The search plugin.
   */
  public function getSearchPlugin(): ParagraphsBlokkliSearchInterface;

  /**
   * Gets the weight of the provider.
   *
   * @return int
   *   The weight.
   */
  public function getWeight(): int|null;

  /**
   * Sets the weight of the provider.
   *
   * @param int $weight
   *   The weight.
   *
   * @return $this
   *   The provider.
   */
  public function setWeight(int $weight): static;

}
