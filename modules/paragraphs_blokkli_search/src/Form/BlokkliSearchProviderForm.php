<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli_search\Form;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs_blokkli_search\Entity\BlokkliSearchProvider;
use Drupal\paragraphs_blokkli_search\ParagraphsBlokkliSearchContentInterface;
use Drupal\paragraphs_blokkli_search\ParagraphsBlokkliSearchLibraryItemsInterface;
use Drupal\paragraphs_blokkli_search\ParagraphsBlokkliSearchMediaLibraryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Blokkli Search Provider form.
 */
final class BlokkliSearchProviderForm extends EntityForm {

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\paragraphs_blokkli_search\ParagraphsBlokkliSearchPluginManager $searchPluginManager
   *   The search plugin manager.
   */
  public function __construct(
    protected PluginManagerInterface $searchPluginManager,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.paragraphs_blokkli_search'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\paragraphs_blokkli_search\Entity\BlokkliSearchProvider $entity */
    $entity = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $entity->label(),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => [BlokkliSearchProvider::class, 'load'],
      ],
      '#disabled' => !$entity->isNew(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $entity->status(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $entity->get('description'),
    ];

    $input = $form_state->getUserInput();

    $searchType = $input['search_type'] ?? $entity->getSearchType();

    $form['search_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Search Type'),
      '#options' => [
        'content' => $this->t('Content'),
        'media_library' => $this->t('Media Library'),
        'library_items' => $this->t('Paragraphs library items'),
      ],
      '#default_value' => $searchType,
      'content' => [
        '#description' => $this->t('Provides search results for the content search in blökkli.'),
      ],
      'media_library' => [
        '#description' => $this->t('Provides search results for the media library in blökkli.'),
      ],
      'library_items' => [
        '#description' => $this->t('Provide search results for finding paragraphs library items.'),
      ],
      '#ajax' => [
        'callback' => '::searchTypeCallback',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Loading search plugins'),
        ],
      ],
    ];

    $form['search_plugin'] = [
      '#type' => 'container',
      '#prefix' => '<div id="search-plugin">',
      '#suffix' => '</div>',
      '#tree' => TRUE,
    ];

    if (!$searchType) {
      return $form;
    }

    $searchPluginId = $form_state->getValue(['search_plugin', 'id']) ?? $entity->get('search_plugin_id');

    $pluginDefinitions = $this->searchPluginManager->getDefinitions();
    $options = [];

    foreach ($pluginDefinitions as $id => $definition) {
      $instance = $this->searchPluginManager->createInstance($id);
      if ($searchType === ParagraphsBlokkliSearchContentInterface::SEARCH_TYPE && $instance instanceof ParagraphsBlokkliSearchContentInterface) {
        $options[$id] = $definition['label'];
      }
      elseif ($searchType === ParagraphsBlokkliSearchMediaLibraryInterface::SEARCH_TYPE && $instance instanceof ParagraphsBlokkliSearchMediaLibraryInterface) {
        $options[$id] = $definition['label'];
      }
      elseif ($searchType === ParagraphsBlokkliSearchLibraryItemsInterface::SEARCH_TYPE && $instance instanceof ParagraphsBlokkliSearchLibraryItemsInterface) {
        $options[$id] = $definition['label'];
      }
    }

    if (empty($options)) {
      $form['search_plugin']['no_plugins'] = [
        '#markup' => $this->t('There are no search plugins available for the selected search type.'),
      ];

      return $form;
    }

    $form['search_plugin']['id'] = [
      '#type' => 'radios',
      '#title' => $this->t('Search Plugin'),
      '#required' => TRUE,
      '#options' => $options,
      '#default_value' => $searchPluginId,
      '#ajax' => [
        'callback' => '::searchPluginCallback',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Loading search plugin form'),
        ],
      ],
    ];

    foreach ($pluginDefinitions as $id => $definition) {
      $form['search_plugin']['id'][$id] = [
        '#description' => $definition['description'],
      ];
    }

    $form['search_plugin']['configuration'] = [
      '#type' => 'container',
      '#prefix' => '<div id="edit-search-plugin-configuration-wrapper">',
      '#suffix' => '</div>',
      '#tree' => TRUE,
    ];

    /** @var \Drupal\paragraphs_blokkli_search\ParagraphsBlokkliSearchInterface|null $instance */
    $instance = $searchPluginId ? $this->searchPluginManager->createInstance($searchPluginId) : NULL;

    if ($instance) {
      $configuration = $entity->get('search_plugin_configuration') ?? [];
      $instance->setConfiguration($configuration);

      $form['search_plugin']['configuration'][$searchPluginId] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Plugin Configuration'),
        '#tree' => TRUE,
      ];

      $form['search_plugin']['configuration'][$searchPluginId] += $instance->buildConfigurationForm([], $form_state);
    }

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Check if it is an ajax call, if yes delete the search plugin configuration errors
    // to unblock the change between search plugins.
    if ($form_state->getTriggeringElement()['#type'] === 'radio') {
      $form_errors = $form_state->getErrors();
      $form_state->clearErrors();
      unset($form_errors['search_config']);
      foreach ($form_errors as $name => $error_message) {
        $form_state->setErrorByName($name, $error_message);
      }
    }
  }

  /**
   * Ajax callback triggered when selecting a search type.
   *
   * @param array $form
   *   The form array.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response.
   */
  public function searchTypeCallback(array $form) {
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#search-plugin', $form['search_plugin']));
    return $response;
  }

  /**
   * Ajax callback triggered when selecting a search plugin.
   *
   * @param array $form
   *   The form array.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response.
   */
  public function searchPluginCallback(array $form) {
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#edit-search-plugin-configuration-wrapper', $form['search_plugin']['configuration']));
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $searchType = $form_state->getValue('search_type');
    $searchPluginId = $form_state->getValue(['search_plugin', 'id']);
    /** @var \Drupal\paragraphs_blokkli_search\Entity\BlokkliSearchProvider $entity */
    $entity = $this->entity;
    /** @var \Drupal\paragraphs_blokkli_search\ParagraphsBlokkliSearchInterface $pluginInstance */
    $pluginInstance = $this->searchPluginManager->createInstance($searchPluginId);
    $searchPluginConfiguration = $pluginInstance->getPersistableConfiguration($form_state);

    $entity->set('search_type', $searchType);
    $entity->set('search_plugin_id', $searchPluginId);
    $entity->set('search_plugin_configuration', $searchPluginConfiguration);
    $result = parent::save($form, $form_state);

    $message_args = ['%label' => $this->entity->label()];
    $this->messenger()->addStatus(
      match($result) {
        \SAVED_NEW => $this->t('Created new example %label.', $message_args),
        \SAVED_UPDATED => $this->t('Updated example %label.', $message_args),
      }
    );
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

}
