<?php

namespace Drupal\paragraphs_blokkli_graphql\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controller for the dummy path.
 */
class PrefixController extends ControllerBase {

  /**
   * Returns an empty 200 response.
   */
  public function response() {
    return new JsonResponse([], 200);
  }

}
