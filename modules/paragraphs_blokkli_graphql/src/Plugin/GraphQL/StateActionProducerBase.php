<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Perform the redo action on the edit state.
 */
class StateActionProducerBase extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('paragraphs_blokkli.manager'),
    );
  }

  /**
   * StateActionProducerBase constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\paragraphs_blokkli\ParagraphsBlokkliManager $blokkliManager
   *   The blokkli manager.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    protected ParagraphsBlokkliManager $blokkliManager,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
  }

}
