<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\SchemaExtension;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Entity\Plugin\DataType\EntityAdapter;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;
use Drupal\graphql_core_schema\CoreSchemaExtensionInterface;
use Drupal\graphql_core_schema\EntitySchemaHelper;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginBase;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginManager;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliEditStateInterface;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliManager;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliMutatedField;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliMutatedState;
use Drupal\paragraphs_blokkli\Plugin\Field\FieldType\ParagraphsBlokkliMutationItem;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\InterfaceType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Schema;
use GraphQL\Utils\SchemaPrinter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * A schema extension to add paragraph editing functionality.
 *
 * @SchemaExtension(
 *   id = "paragraphs_blokkli_edit",
 *   name = "Paragraphs Blokkli Edit",
 *   description = "Add support for paragraph editing.",
 *   schema = "core_composable"
 * )
 */
class EditExtension extends SdlSchemaExtensionPluginBase implements CoreSchemaExtensionInterface, ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('paragraphs_blokkli.manager'),
      $container->get('plugin.manager.paragraph_mutation'),
      $container->get('entity_type.bundle.info'),
      $container->get('renderer'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * The constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param \Drupal\paragraphs_blokkli\ParagraphsBlokkliManager $paragraphsBlokkliManager
   * @param \Drupal\paragraphs_blokkli\ParagraphMutationPluginManager $mutationPluginManager
   *   The paragrah_mutation plugin manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundleInfo
   *   The entity type bundle info service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    ModuleHandlerInterface $moduleHandler,
    protected ParagraphsBlokkliManager $paragraphsBlokkliManager,
    protected ParagraphMutationPluginManager $mutationPluginManager,
    protected EntityTypeBundleInfoInterface $bundleInfo,
    protected RendererInterface $renderer,
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition, $moduleHandler);
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeDependencies() {
    return ['paragraph', 'paragraphs_blokkli_edit_state', 'paragraphs_type'];
  }

  /**
   * {@inheritdoc}
   */
  public function getExtensionDependencies() {
    return ['entity_query'];
  }

  /**
   * Get the GraphQL type for a mutation argument context type.
   *
   * @param string $mutationName
   *   The name of the mutation.
   * @param string $dataType
   *   The data type from the ContextDefinition.
   *
   * @return \GraphQL\Type\Definition\Type|null
   *   The GraphQL type.
   */
  private function getGraphqlType(string $mutationName, string $dataType): ?Type {
    switch ($dataType) {
      case 'string':
        return Type::string();
    }

    if ($mutationName === 'bulk_update_behavior_settings' && $dataType === 'map') {
      return new InputObjectType([
        'name' => 'ParagraphsBlokkliBulkUpdateBehaviorSettingsInput',
        'fields' => [
          'uuid' => Type::nonNull(Type::string()),
          'pluginId' => Type::nonNull(Type::string()),
          'key' => Type::nonNull(Type::string()),
          'value' => Type::nonNull(Type::string()),
        ],
      ]);
    }
    elseif ($mutationName === 'add_entity_reference_multiple' && $dataType === 'map') {
      return new InputObjectType([
        'name' => 'ParagraphsBlokkliAddEntityReferenceMultipleInput',
        'fields' => [
          'targetType' => Type::nonNull(Type::string()),
          'targetBundle' => Type::nonNull(Type::string()),
          'targetId' => Type::nonNull(Type::string()),
          'paragraphBundle' => Type::nonNull(Type::string()),
        ],
      ]);
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseDefinition() {
    $base = parent::getBaseDefinition();

    $definitions = $this->mutationPluginManager->getDefinitions();

    $mutationResultType = new ObjectType([
      'name' => 'ParagraphsBlokkliMutationResult',
      'fields' => [
        'success' => Type::boolean(),
      ],
    ]);

    $mutationFields = [];
    foreach ($definitions as $mutationName => $definition) {
      $inputArgs = [];

      /** @var \Drupal\Core\Plugin\Context\ContextDefinition $contextDefinition */
      foreach ($definition['arguments'] as $argName => $contextDefinition) {
        $dataType = $contextDefinition->getDataType();
        $isMultiple = $contextDefinition->isMultiple();
        $graphqlDataType = $this->getGraphqlType($mutationName, $dataType);
        if ($graphqlDataType) {
          $graphqlType = $isMultiple ? Type::listOf($graphqlDataType) : $graphqlDataType;
          $inputArgs[$argName] = [
            'name' => $argName,
            'description' => (string) $contextDefinition->getLabel(),
            'type' => $contextDefinition->isRequired() ? Type::nonNull($graphqlType) : $graphqlType,
          ];
        }
      }

      $description = $definition['description'] ?? $definition['label'] ?? NULL;
      $mutationFields[$mutationName] = [
        'name' => $mutationName,
        'args' => $inputArgs,
        'description' => (string) $description,
        'type' => $mutationResultType,
      ];
    }

    $mutationStateType = new ObjectType([
      'name' => 'ParagraphsEditMutationState',
      'fields' => $mutationFields,
    ]);

    $blokkliPropsType = new ObjectType([
      'name' => 'ParagraphsBlokkliProps',
      'fields' => [
        'canEdit' => Type::nonNull(Type::boolean()),
        'entityType' => Type::nonNull(Type::string()),
        'entityBundle' => Type::nonNull(Type::string()),
        'entityUuid' => Type::nonNull(Type::string()),
        'editLabel' => Type::string(),
        'language' => Type::nonNull(Type::string()),
      ],
    ]);

    $entityTypeExtensions = [];

    foreach ($this->entityTypeManager->getDefinitions() as $entityTypeId => $definition) {
      if ($definition instanceof ContentEntityTypeInterface) {
        $bundles = $this->bundleInfo->getBundleInfo($entityTypeId);

        // Ignore entity types without bundles, except for the
        // paragraphs_library_item type, which is the only entity type without
        // bundles that can be used with blökkli.
        if (
          count($bundles) === 1 &&
          array_keys($bundles)[0] === $entityTypeId &&
          $entityTypeId !== 'paragraphs_library_item'
        ) {
          continue;
        }

        $entityTypeExtensions[] = new InterfaceType([
          'name' => EntitySchemaHelper::toPascalCase($entityTypeId),
          'fields' => [
            'blokkliProps' => [
              'name' => 'blokkliProps',
              'description' => 'The props for the blokkli provider component.',
              'type' => Type::nonNull($blokkliPropsType),
            ],
          ],
        ]);
      }
    }

    $schema = new Schema([
      'types' => [
        $mutationStateType,
        ...$entityTypeExtensions,
      ],
    ]);

    $content = [
      SchemaPrinter::doPrint($schema),
      $base,
    ];

    return implode("\n", $content);
  }

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry): void {
    $builder = new ResolverBuilder();

    $canUseBlokkli = $builder->produce('paragraphs_blokkli_can_use_blokkli');

    $resolveWithAccessCheck = function ($producer) use ($builder, $canUseBlokkli) {
      return $builder->cond([[$canUseBlokkli, $producer]]);
    };

    // Paragraph conversion plugins.
    $registry->addFieldResolver(
      'ParagraphsType',
      'allowReusable',
      $builder->callback(function (ParagraphsType $type) {
        return $type->getThirdPartySetting('paragraphs_library', 'allow_library_conversion', FALSE);
      })
    );

    $registry->addFieldResolver(
      'Entity',
      'blokkliProps',
      $builder->produce('paragraphs_blokkli_props')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver(
      'ParagraphsType',
      'isTranslatable',
      $builder->callback(function (ParagraphsType $type) {
        $info = $this->bundleInfo->getBundleInfo('paragraph');
        $bundleInfo = $info[$type->id()] ?? NULL;
        return $bundleInfo['translatable'] ?? FALSE;
      })
    );

    // Paragraph conversion plugins.
    $registry->addFieldResolver(
      'Query',
      'paragraphsBlokkliConversions',
      $resolveWithAccessCheck($builder->produce('paragraphs_blokkli_conversions'))
    );

    $registry->addFieldResolver(
      'Query',
      'pbGetUrlPrefixes',
      $builder->produce('paragraphs_blokkli_url_prefixes')
    );

    $registry->addFieldResolver(
      'Query',
      'pbGetSupportedClipboards',
      $resolveWithAccessCheck($builder->produce('paragraphs_blokkli_supported_clipboards'))
    );

    $registry->addFieldResolver(
      'Query',
      'pbGetFieldConfig',
      $resolveWithAccessCheck($builder->produce('paragraphs_blokkli_field_config'))
    );

    $registry->addFieldResolver(
      'Query',
      'pbGetEditableFieldConfig',
      $resolveWithAccessCheck(
        $builder->produce('paragraphs_blokkli_editable_field_config')
          ->map('entityType', $builder->fromArgument('entityType'))
          ->map('entityBundle', $builder->fromArgument('entityBundle'))
      )
    );

    $registry->addFieldResolver(
      'Query',
      'pbGetDroppableFieldConfig',
      $resolveWithAccessCheck(
        $builder->produce('paragraphs_blokkli_droppable_field_config')
          ->map('entityType', $builder->fromArgument('entityType'))
          ->map('entityBundle', $builder->fromArgument('entityBundle'))
      )
    );

    // Available features.
    $registry->addFieldResolver(
      'Query',
      'paragraphsBlokkliAvailableFeatures',
      $resolveWithAccessCheck($builder->produce('paragraphs_blokkli_available_features'))
    );

    $registry->addFieldResolver(
      'Query',
      'pbGetImportSourceEntities',
      $resolveWithAccessCheck(
      $builder->produce('paragraphs_blokkli_import_source_entities')
        ->map('entityType', $builder->fromArgument('entityType'))
        ->map('entityUuid', $builder->fromArgument('entityUuid'))
        ->map('searchText', $builder->fromArgument('searchText'))
      )
    );

    $registry->addFieldResolver(
      'ParagraphsBlokkliConversion',
      'sourceBundle',
      $builder->callback(function (array $definition) {
        return $definition['source_bundle'];
      })
    );

    $registry->addFieldResolver(
      'ParagraphsBlokkliConversion',
      'targetBundle',
      $builder->callback(function (array $definition) {
        return $definition['target_bundle'];
      })
    );

    $registry->addFieldResolver(
      'Query',
      'paragraphsBlokkliAllowedTypes',
      $resolveWithAccessCheck($builder->produce('paragraphs_blokkli_allowed_types'))
    );

    // Load the paragraphs edit state.
    $registry->addFieldResolver('Query', 'getParagraphsEditState',
      $builder->cond([
        [
          $builder->produce('paragraphs_blokkli_can_use_blokkli'),
          $builder->compose(
            $builder->produce('paragraphs_blokkli_load_host_entity')
              ->map('entityType', $builder->fromArgument('entityType'))
              ->map('entityUuid', $builder->fromArgument('entityUuid'))
              ->map('langcode', $builder->fromArgument('langcode')),
            $builder->produce('paragraphs_blokkli_load_edit_state')
              ->map('entity', $builder->fromParent())
              ->map('historyIndex', $builder->fromArgument('historyIndex'))
          ),
        ],
      ])
    );

    $resolveEntity = $builder->produce('paragraphs_blokkli_edit_state_entity')
      ->map('state', $builder->fromParent())
      ->map('langcode', $builder->produce('current_language'));

    $registry->addFieldResolver(
      'ParagraphsBlokkliEditState',
      'entity',
      $resolveEntity
    );

    $registry->addFieldResolver(
      'ParagraphsBlokkliEditState',
      'mutatedEntity',
      $resolveEntity
    );

    $registry->addFieldResolver(
      'ParagraphsBlokkliEditState',
      'translationState',
      $builder->produce('paragraphs_blokkli_translation_state')->map('state', $builder->fromParent())
    );

    $registry->addFieldResolver(
      'ParagraphsBlokkliEditState',
      'currentUserIsOwner',
      $builder->callback(function (ParagraphsBlokkliEditStateInterface $value, $args, ResolveContext $resolveContext) {
        $currentUser = \Drupal::currentUser();
        $resolveContext->addCacheableDependency($currentUser);
        return (string) $value->getOwnerId() === (string) $currentUser->id();
      })
    );

    $registry->addFieldResolver(
      'ParagraphsBlokkliEditState',
      'ownerName',
      $builder->callback(function (ParagraphsBlokkliEditStateInterface $value, $args, ResolveContext $resolveContext) {
        $owner = $value->getOwner();
        $resolveContext->addCacheableDependency($owner);
        return $owner->getDisplayName();
      })
    );

    $registry->addFieldResolver(
      'ParagraphsBlokkliEditState',
      'previewUrl',
      $builder->callback(function (ParagraphsBlokkliEditStateInterface $state) {
        $context = new RenderContext();
        return $this->renderer->executeInRenderContext($context, function () use ($state) {
          $previewHash = $state->get('preview_hash')->getString();
          return Url::fromRoute('paragraphs_blokkli.preview_grant', [
            'id' => $state->id(),
            'hash' => $previewHash,
          ])->toString();
        });
      })
    );

    $registry->addFieldResolver(
      'ParagraphsEditViolation',
      'message',
      $builder->callback(function (ConstraintViolationInterface $violation) {
        return $violation->getMessage();
      })
    );

    $registry->addFieldResolver(
      'ParagraphsEditViolation',
      'entityType',
      $builder->callback(function (ConstraintViolationInterface $violation) {
        $root = $violation->getRoot();

        if ($root instanceof EntityAdapter) {
          return $root->getEntity()->getEntityTypeId();
        }
      })
    );

    $registry->addFieldResolver(
      'ParagraphsEditViolation',
      'entityUuid',
      $builder->callback(function (ConstraintViolationInterface $violation) {
        $root = $violation->getRoot();

        if ($root instanceof EntityAdapter) {
          return $root->getEntity()->uuid();
        }
      })
    );

    $registry->addFieldResolver(
      'ParagraphsEditViolation',
      'code',
      $builder->callback(function (ConstraintViolationInterface $violation) {
        return $violation->getCode();
      })
    );

    $registry->addFieldResolver(
      'ParagraphsEditViolation',
      'propertyPath',
      $builder->callback(function (ConstraintViolationInterface $violation) {
        return $violation->getPropertyPath();
      })
    );

    $registry->addFieldResolver(
      'FieldItemTypeParagraphsBlokkliMutation',
      'plugin',
      $builder->callback(function (ParagraphsBlokkliMutationItem $item) {
        return $item->getMutationPlugin();
      })
    );

    $registry->addFieldResolver(
      'ParagraphsBlokkliEditState',
      'mutatedState',
      $builder
        ->produce('paragraphs_blokkli_edit_state_mutated_state')
        ->map('state', $builder->fromParent())
        ->map('historyIndex', $builder->fromContext('paragraphs_blokkli_history_index'))
    );

    $registry->addFieldResolver(
      'ParagraphsBlokkliMutatedState',
      'fields',
      $builder->callback(function (ParagraphsBlokkliMutatedState $state, $args, ResolveContext $resolveContext, ResolveInfo $info, FieldContext $field) {
        if (!empty($args['langcode'])) {
          $field->setContextValue('language', $args['langcode']);
        }
        return $state->getFields();
      })
    );

    $registry->addFieldResolver(
      'ParagraphsBlokkliMutatedState',
      'behaviorSettings',
      $builder->callback(function (ParagraphsBlokkliMutatedState $state) {
        return $state->getBehaviorSettings();
      })
    );

    $registry->addFieldResolver(
      'ParagraphsBlokkliMutatedState',
      'violations',
      $builder->callback(function (ParagraphsBlokkliMutatedState $state) {
        return $state->getViolations();
      })
    );

    $registry->addFieldResolver(
      'ParagraphsBlokkliMutatedField',
      'name',
      $builder->callback(function (ParagraphsBlokkliMutatedField $field) {
        return $field->getFieldName();
      })
    );

    $registry->addFieldResolver(
      'ParagraphsBlokkliMutatedField',
      'entityType',
      $builder->callback(function (ParagraphsBlokkliMutatedField $field) {
        return $field->getEntityType();
      })
    );

    $registry->addFieldResolver(
      'ParagraphsBlokkliMutatedField',
      'entityUuid',
      $builder->callback(function (ParagraphsBlokkliMutatedField $field) {
        return $field->getEntityUuid();
      })
    );

    $registry->addFieldResolver(
      'ParagraphsBlokkliMutatedField',
      'list',
      $builder->compose(
        $builder->callback(function (ParagraphsBlokkliMutatedField $field) {
          return $field->getParagraphs();
        }),
        $builder->map(
          $builder->produce('entity_translation_fallback')
            ->map('entity', $builder->fromParent())
            ->map('langcode', $builder->produce('current_language'))
            ->map('fallback', $builder->fromValue(TRUE))
        )
      )
    );

    // Resolve the target bundles.
    $registry->addFieldResolver(
      'FieldItemListEntityReferenceRevisions',
      'targetBundles',
      $builder->callback(function (EntityReferenceRevisionsFieldItemList $value) {
        return $this->paragraphsBlokkliManager->getAllowedParagraphTypes($value);
      })
    );

    $registry->addFieldResolver(
      'FieldItemListEntityReferenceRevisions',
      'name',
      $builder->callback(function (EntityReferenceRevisionsFieldItemList $value) {
        return $value->getFieldDefinition()->getName();
      })
    );

    $registry->addFieldResolver(
      'Paragraph',
      'props',
      $builder->callback(function (ParagraphInterface $paragraph) {
        return $paragraph;
      })
    );

    // Resolve the target bundles.
    $registry->addFieldResolver(
      'FieldItemListEntityReferenceRevisions',
      'canEdit',
      $builder->callback(function (EntityReferenceRevisionsFieldItemList $field, $args, ResolveContext $context) {
        $allowedEntity = $field->getParent()->getValue();
        if ($allowedEntity && $allowedEntity instanceof EntityInterface) {
          $result = $allowedEntity
            ->access('update', NULL, TRUE)
            ->andIf($field->access('update', NULL, TRUE));
          $context->addCacheableDependency($result);
          return $result->isAllowed();
        }
        return FALSE;
      })
    );

    $registry->addFieldResolver(
      'ParagraphsBlokkliMutationPlugin',
      'label',
      $builder->callback(function (ParagraphMutationPluginBase $plugin) {
        return $plugin->label();
      })
    );

    $registry->addFieldResolver(
      'ParagraphsBlokkliMutationPlugin',
      'affectedParagraphUuid',
      $builder->callback(function (ParagraphMutationPluginBase $plugin) {
        return $plugin->getAffectedParagraphUuid();
      })
    );

    $this->registerMutations($registry, $builder);

    $registry->addFieldResolver(
      'Query',
      'pbMediaLibraryGetResults',
      $resolveWithAccessCheck(
        $builder->produce('paragraphs_blokkli_media_library_results')
          ->map('text', $builder->fromArgument('text'))
          ->map('bundle', $builder->fromArgument('bundle'))
          ->map('page', $builder->fromArgument('page'))
      )
    );

    $registry->addFieldResolver(
      'ParagraphsBlokkliMediaItem',
      'targetBundles',
      $builder
        ->produce('paragraphs_blokkli_matching_bundle')
        ->map('item', $builder->fromParent())
    );

    $registry->addTypeResolver('ParagraphsBlokkliMediaLibraryFilter', function (array $filter) {
      if ($filter['type'] === 'text') {
        return 'ParagraphsBlokkliMediaLibraryFilterText';
      }
      elseif ($filter['type'] === 'select') {
        return 'ParagraphsBlokkliMediaLibraryFilterSelect';
      }
    });
  }

  /**
   * Registers the resolvers for mutations.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   The resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   The resolver builder.
   */
  private function registerMutations($registry, $builder) {
    // Load the paragraphs edit state.
    $registry->addFieldResolver('Mutation', 'paragraphsEditMutationState',
      $builder->cond([[
        $builder->produce('paragraphs_blokkli_can_use_blokkli'),
        $builder->compose(
          $builder->produce('entity_load_by_uuid')
            ->map('type', $builder->callback(function ($value, $args) {
              return strtolower($args['entityType']);
            }))
            ->map('uuid', $builder->fromArgument('entityUuid'))
            ->map('access', $builder->fromValue(TRUE))
            ->map('access_operation', $builder->fromValue('update')),
          $builder->callback(function (EntityInterface $value) {
            if ($value instanceof FieldableEntityInterface) {
              return $this->paragraphsBlokkliManager->getParagraphsEditState($value);
            }
          })
        ),
      ],
      ])
    );
    $resolveArguments = $builder->callback(function ($value, $args) {
      return $args;
    });

    $mutationDefinitions = array_keys($this->mutationPluginManager->getDefinitions());
    foreach ($mutationDefinitions as $key) {
      $registry->addFieldResolver(
        'ParagraphsEditMutationState',
        $key,
        $builder->produce('paragraphs_blokkli_mutation')
          ->map('state', $builder->fromParent())
          ->map('pluginId', $builder->fromValue($key))
          ->map('values', $resolveArguments)
      );
    }

    $registry->addFieldResolver(
      'ParagraphsEditMutationState',
      'revertAllChanges',
      $builder->produce('paragraphs_blokkli_revert_all_changes')
        ->map('state', $builder->fromParent())
    );

    $registry->addFieldResolver(
      'ParagraphsEditMutationState',
      'undo',
      $builder->produce('paragraphs_blokkli_undo')
        ->map('state', $builder->fromParent())
    );

    $registry->addFieldResolver(
      'ParagraphsEditMutationState',
      'redo',
      $builder->produce('paragraphs_blokkli_redo')
        ->map('state', $builder->fromParent())
    );

    $registry->addFieldResolver(
      'ParagraphsEditMutationState',
      'setHistoryIndex',
      $builder->produce('paragraphs_blokkli_set_history_index')
        ->map('state', $builder->fromParent())
        ->map('index', $builder->fromArgument('index'))
    );

    $registry->addFieldResolver(
      'ParagraphsEditMutationState',
      'setMutationStatus',
      $builder->produce('paragraphs_blokkli_set_mutation_status')
        ->map('state', $builder->fromParent())
        ->map('index', $builder->fromArgument('index'))
        ->map('status', $builder->fromArgument('status'))
    );

    $registry->addFieldResolver(
      'ParagraphsEditMutationState',
      'takeOwnership',
      $builder->produce('paragraphs_blokkli_take_ownership')
        ->map('state', $builder->fromParent())
    );

    $registry->addFieldResolver(
      'ParagraphsEditMutationState',
      'publish',
      $builder->produce('paragraphs_blokkli_publish')
        ->map('state', $builder->fromParent())
        ->map('createNewState', $builder->fromArgument('createNewState'))
    );

    $registry->addFieldResolver(
      'ParagraphsBlokkliEntity',
      'status',
      $builder->callback(function (EntityInterface $entity) {
        if ($entity instanceof EntityPublishedInterface) {
          return $entity->isPublished();
        }

        // Assume to be published, so blökkli doesn't show a red dot.
        return TRUE;
      })
    );

    $registry->addFieldResolver(
      'ParagraphsBlokkliEntity',
      'label',
      $builder->callback(function (EntityInterface $entity) {
        return $entity->label();
      })
    );

    $registry->addFieldResolver(
      'ParagraphsBlokkliEntity',
      'bundleLabel',
      $builder->callback(function (EntityInterface $entity) {
        $entityTypeLabel = (string) $entity->getEntityType()->getLabel();
        $bundle = $entity->bundle();
        $info = $this->bundleInfo->getBundleInfo($entity->getEntityTypeId());
        $bundleInfo = $info[$bundle] ?? NULL;
        $bundleLabel = (string) ($bundleInfo['label'] ?? $bundle);

        // Only return the entity type label if the bundle label is identical.
        // This is the case for entity types without bundles.
        if ($entityTypeLabel === $bundleLabel) {
          return $entityTypeLabel;
        }

        return implode(' » ', [$entityTypeLabel, $bundleLabel]);
      })
    );

    $registry->addFieldResolver(
      'Paragraph',
      'paragraphsBlokkliOptions',
      $builder->callback(function (ParagraphInterface $paragraph) {
        $allSettings = $paragraph->getAllBehaviorSettings();
        $settings = $allSettings['paragraphs_blokkli_data'] ?? NULL;
        if (!empty($settings) && is_array($settings)) {
          return $settings;
        }

        return new \stdClass();
      })
    );
  }

}
