<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\SchemaExtension;

use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;

/**
 * A schema extension to add support for the paragraphs_blokkli_search module.
 *
 * @SchemaExtension(
 *   id = "paragraphs_blokkli_search",
 *   name = "Paragraphs Blokkli Search",
 *   description = "Add support for search plugins.",
 *   schema = "core_composable"
 * )
 */
class SearchExtension extends SdlSchemaExtensionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry): void {
    $builder = new ResolverBuilder();

    $registry->addFieldResolver(
      'Query',
      'paragraphsBlokkliSearch',
      $builder->cond([
        [
          $builder->produce('paragraphs_blokkli_can_use_blokkli'),
          $builder->produce('paragraphs_blokkli_search')
            ->map('id', $builder->fromArgument('id'))
            ->map('text', $builder->fromArgument('text')),
        ],
      ])
    );

    $registry->addFieldResolver(
      'Query',
      'paragraphsBlokkliSearchTabs',
      $builder->cond([
        [
          $builder->produce('paragraphs_blokkli_can_use_blokkli'),
          $builder->produce('paragraphs_blokkli_content_search_tabs'),
        ],
      ])
    );

    $registry->addFieldResolver(
      'ParagraphsBlokkliSearchItem',
      'targetBundles',
      $builder
        ->produce('paragraphs_blokkli_matching_bundle')
        ->map('item', $builder->fromParent())
    );
  }

}
