<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\SchemaExtension;

use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;
use Drupal\paragraphs_blokkli_transform\ParagraphTransformInterface;

/**
 * A schema extension to add support for transform plugins.
 *
 * @SchemaExtension(
 *   id = "paragraphs_blokkli_transform",
 *   name = "Paragraphs Blokkli Transform",
 *   description = "Add support for transform plugins.",
 *   schema = "core_composable"
 * )
 */
class TransformExtension extends SdlSchemaExtensionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeDependencies() {
    return ['paragraph', 'paragraphs_type'];
  }

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry): void {
    $builder = new ResolverBuilder();

    $registry->addFieldResolver(
      'Query',
      'paragraphsBlokkliGetTransformPlugins',
      $builder->cond([
        [
          $builder->produce('paragraphs_blokkli_can_use_blokkli'),
          $builder->compose(
            $builder->produce('paragraphs_blokkli_load_host_entity')
              ->map('entityType', $builder->fromArgument('entityType'))
              ->map('entityUuid', $builder->fromArgument('entityUuid'))
              ->map('langcode', $builder->fromArgument('langcode')),
            $builder->produce('paragraphs_blokkli_transform_plugins')
              ->map('entity', $builder->fromParent())
              ->map('langcode', $builder->fromArgument('langcode')),
          ),
        ],
      ])
    );

    $registry->addFieldResolver(
      'ParagraphsBlokkliTransformPlugin',
      'id',
      $builder->callback(function (ParagraphTransformInterface $plugin) {
        return $plugin->getPluginId();
      })
    );

    $registry->addFieldResolver(
      'ParagraphsBlokkliTransformPlugin',
      'label',
      $builder->callback(function (ParagraphTransformInterface $plugin) {
        return $plugin->label();
      })
    );

    $registry->addFieldResolver(
      'ParagraphsBlokkliTransformPlugin',
      'bundles',
      $builder->callback(function (ParagraphTransformInterface $plugin) {
        return $plugin->getBundles();
      })
    );

    $registry->addFieldResolver(
      'ParagraphsBlokkliTransformPlugin',
      'targetBundles',
      $builder->callback(function (ParagraphTransformInterface $plugin) {
        return $plugin->getTargetBundles();
      })
    );

    $registry->addFieldResolver(
      'ParagraphsBlokkliTransformPlugin',
      'min',
      $builder->callback(function (ParagraphTransformInterface $plugin) {
        return $plugin->getMin();
      })
    );

    $registry->addFieldResolver(
      'ParagraphsBlokkliTransformPlugin',
      'max',
      $builder->callback(function (ParagraphTransformInterface $plugin) {
        return $plugin->getMax();
      })
    );
  }

}
