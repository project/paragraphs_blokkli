<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\SchemaExtension;

use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;

/**
 * A schema extension to add support for the paragraphs_blokkli_search module.
 *
 * @SchemaExtension(
 *   id = "paragraphs_blokkli_library",
 *   name = "Paragraphs Blokkli Library",
 *   description = "Add support for paragraphs_library.",
 *   schema = "core_composable"
 * )
 */
class LibraryExtension extends SdlSchemaExtensionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry): void {
    $builder = new ResolverBuilder();

    $registry->addFieldResolver(
      'Query',
      'paragraphsBlokkliSearchLibraryItems',
      $builder->cond([[
        $builder->produce('paragraphs_blokkli_can_use_blokkli'),
        $builder->produce('paragraphs_blokkli_library_items_search')
          ->map('bundles', $builder->fromArgument('bundles'))
          ->map('text', $builder->fromArgument('text'))
          ->map('page', $builder->fromArgument('page')),
      ],
      ])

    );

    $registry->addFieldResolver(
      'ParagraphsBlokkliLibraryItemsSearchResults',
      'items',
      $builder->produce('entity_load_multiple')
        ->map('type', $builder->fromValue('paragraphs_library_item'))
        ->map('ids', $builder->callback(function ($results) {
          /** @var \Drupal\paragraphs_blokkli_search\Model\LibraryItemsSearchResults $results */
          return $results->ids;
        }))
    );
  }

}
