<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Produces a list of droppable field configs.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_droppable_field_config",
 *   name = @Translation("Paragraphs Blokkli Droppable Field Config"),
 *   description = @Translation("Procudes a list of all droppable fields."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The list of fields.")
 *   ),
 *   consumes = {
 *     "entityType" = @ContextDefinition("string",
 *       label = @Translation("The entity type.")
 *     ),
 *     "entityBundle" = @ContextDefinition("string",
 *       label = @Translation("The entity bundle.")
 *     ),
 *   }
 * )
 */
class ParagraphsBlokkliDroppableFieldConfig extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_field.manager'),
      $container->get('entity_type.bundle.info'),
    );
  }

  /**
   * EntityLoad constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   * @param EntityTypeBundleInfoInterface $bundleInfo
   *   The bundle info service.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    protected EntityFieldManagerInterface $entityFieldManager,
    protected EntityTypeBundleInfoInterface $bundleInfo,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
  }

  /**
   * Resolver.
   */
  public function resolve(string $entityType, string $entityBundle) {
    $fieldConfigs = $this->getDroppableFields($entityType, $entityBundle);
    $bundles = array_keys($this->bundleInfo->getBundleInfo('paragraph'));
    foreach ($bundles as $bundle) {
      $fieldConfigs = array_merge($fieldConfigs, $this->getDroppableFields('paragraph', $bundle));
    }

    return $fieldConfigs;
  }

  /**
   * Get the fields for the given entity type and bundle.
   *
   * @param string $entityType
   *   The entity type.
   * @param string $entityBundle
   *   The entity bundle.
   *
   * @return array
   *   The field configs.
   */
  private function getDroppableFields(string $entityType, string $entityBundle) {
    $configs = [];
    $fields = $this->entityFieldManager->getFieldDefinitions($entityType, $entityBundle);
    foreach ($fields as $fieldName => $fieldDefinition) {
      // Skip fields that can't be edited.
      if (
        $fieldDefinition->isReadOnly() ||
        $fieldDefinition->isInternal() ||
        $fieldDefinition->isComputed() ||
        !$fieldDefinition->isDisplayConfigurable('form')
      ) {
        continue;
      }
      $fieldType = $fieldDefinition->getType();

      if ($fieldType !== 'entity_reference' && $fieldType !== 'entity_reference_revisions') {
        continue;
      }
      $settings = $fieldDefinition->getSettings();
      $storageDefinition = $fieldDefinition->getFieldStorageDefinition();
      $targetType = $settings['target_type'] ?? NULL;
      if ($targetType !== 'media') {
        continue;
      }
      $targetBundles = $settings['handler_settings']['target_bundles'] ?? [];

      $configs[] = [
        'name' => $fieldName,
        'label' => $fieldDefinition->getLabel(),
        'entityType' => $entityType,
        'entityBundle' => $entityBundle,
        'allowedEntityType' => $targetType,
        'allowedBundles' => $targetBundles,
        // @todo .
        'canEdit' => TRUE,
        'cardinality' => $storageDefinition->getCardinality(),
        'required' => (bool) $fieldDefinition->isRequired(),
      ];
    }

    return $configs;
  }

}
