<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Produces the matching block/paragraph bundle for an entity.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_matching_bundle",
 *   name = @Translation("Paragraphs Blokkli Matching Bundle"),
 *   produces = @ContextDefinition("string",
 *     label = @Translation("The paragraph bundle.")
 *   ),
 *   consumes = {
 *     "item" = @ContextDefinition("any",
 *       label = @Translation("The entity or search result item."),
 *     ),
 *   }
 * )
 */
class ParagraphsBlokkliMatchingBundle extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * ParagraphsBlokkliMediaLibraryResults constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
  }

  /**
   * Resolver.
   *
   * @param \Drupal\Core\Entity\EntityInterface|\Drupal\paragraphs_blokkli_search\Model\SearchItemInterface $item
   *   The item.
   * @param \Drupal\graphql\GraphQL\Execution\FieldContext $fieldContext
   *   The field context.
   *
   * @return string[]
   *   The paragraph bundles.
   */
  public function resolve($item, FieldContext $fieldContext): array {
    $entityType = $item->getEntityTypeId();
    $entityBundle = $item->bundle();

    /** @var \Drupal\paragraphs_blokkli\PbEntityMappingStorage $mappingStorage */
    $mappingStorage = $this->entityTypeManager->getStorage('pb_entity_mapping');
    $fieldContext->addCacheTags($mappingStorage->getEntityType()->getListCacheTags());

    $mappings = $mappingStorage->findMappings($entityType, $entityBundle);

    return array_map(function ($mapping) {
      return $mapping->getParagraphBundle();
    }, $mappings);
  }

}
