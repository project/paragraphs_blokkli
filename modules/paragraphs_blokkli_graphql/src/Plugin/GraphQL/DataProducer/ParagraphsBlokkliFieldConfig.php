<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Procudes the list of possible paragraph conversion.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_field_config",
 *   name = @Translation("Paragraphs Blokkli Field Config"),
 *   description = @Translation("Procudes a list of all existing paragraph fields."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The list of paragraph fields.")
 *   ),
 * )
 */
class ParagraphsBlokkliFieldConfig extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * EntityLoad constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\Core\Entity\EntityFieldManager $entityFieldManager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    protected EntityFieldManagerInterface $entityFieldManager,
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
  }

  /**
   * Resolver.
   */
  public function resolve() {
    $fieldConfigs = [];
    $fieldMap = $this->entityFieldManager->getFieldMapByFieldType('entity_reference_revisions');
    foreach ($fieldMap as $entityType => $fields) {
      foreach ($fields as $fieldName => $fieldOptions) {
        foreach ($fieldOptions['bundles'] as $bundle) {
          $fields = $this->entityFieldManager->getFieldDefinitions($entityType, $bundle);
          /** @var \Drupal\Core\Field\BaseFieldDefinition|null $field */
          $field = $fields[$fieldName] ?? NULL;
          if ($field && $field->getType() === 'entity_reference_revisions') {
            $settings = $field->getSettings();
            $targetType = $settings['target_type'] ?? NULL;
            $storageDefinition = $field->getFieldStorageDefinition();

            if ($targetType === 'paragraph') {
              // Special handling for the library item: Its field does not set
              // which bundles are allowed; instead this information is stored
              // on the paragraph bundle (whether the bundle can be "promoted to
              // library ").
              $targetBundles = $entityType === 'paragraphs_library_item'
                ? $this->getLibraryParagraphBundles()
                : $settings['handler_settings']['target_bundles'] ?? [];

              $fieldConfigs[] = [
                'name' => $fieldName,
                'entityType' => $entityType,
                'entityBundle' => $bundle,
                'label' => $field instanceof FieldConfig ? $field->label() : $storageDefinition->getLabel(),
                'cardinality' => $storageDefinition->getCardinality(),
                'allowedBundles' => $targetBundles,
                'canEdit' => TRUE,
              ];
            }
          }
        }
      }
    }

    return $fieldConfigs;
  }

  /**
   * Get the paragraph bundles that can be made reusable.
   *
   * @return string[]
   *   The bundles that can be made reusable.
   */
  protected function getLibraryParagraphBundles(): array {
    /** @var \Drupal\paragraphs\Entity\ParagraphsType[] $types */
    $types = $this->entityTypeManager->getStorage('paragraphs_type')->loadMultiple();

    $allowed = [];

    foreach ($types as $type) {
      if ($type->getThirdPartySetting('paragraphs_library', 'allow_library_conversion', FALSE)) {
        $allowed[] = $type->id();
      }
    }

    return $allowed;
  }

}
