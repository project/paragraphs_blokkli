<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Procudes the available paragraphs_blokkli features.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_available_features",
 *   name = @Translation("Paragraphs Blokkli Available Features"),
 *   description = @Translation("Procudes the available paragraphs_blokkli features."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The available features.")
 *   ),
 * )
 */
class ParagraphsBlokkliAvailableFeatures extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The module handler.
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The paragraph mutation plugin manager.
   */
  protected ParagraphMutationPluginManager $mutationPluginManager;

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('plugin.manager.paragraph_mutation'),
    );
  }

  /**
   * EntityLoad constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param ParagraphMutationPluginManager $mutationPluginManager
   *   The paragraph mutation plugin manager.   *.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    ModuleHandlerInterface $moduleHandler,
    ParagraphMutationPluginManager $mutationPluginManager,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->moduleHandler = $moduleHandler;
    $this->mutationPluginManager = $mutationPluginManager;
  }

  /**
   * Resolver.
   */
  public function resolve() {
    $mutations = array_keys($this->mutationPluginManager->getDefinitions());
    return [
      'conversion' => $this->moduleHandler->moduleExists('paragraphs_blokkli_conversion'),
      'comment' => $this->moduleHandler->moduleExists('paragraphs_blokkli_comment'),
      'library' => $this->moduleHandler->moduleExists('paragraphs_library'),
      'mutations' => $mutations,
    ];
  }

}
