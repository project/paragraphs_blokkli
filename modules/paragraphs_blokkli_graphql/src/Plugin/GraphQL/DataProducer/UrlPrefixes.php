<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Procudes the URL prefixes.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_url_prefixes",
 *   name = @Translation("Paragraphs Blokkli URL Prefixes"),
 *   description = @Translation("The URL prefixes."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The URL prefixes.")
 *   ),
 * )
 */
class UrlPrefixes extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The path used to generate the language prefix.
   */
  const PREFIX_PATH = '/paragraphs_blokkli_graphql/prefix';

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('language_manager'),
    );
  }

  /**
   * EntityLoad constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    protected LanguageManagerInterface $languageManager,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
  }

  /**
   * Resolver.
   *
   * @return array{ 'langcode': string, 'prefix': string }
   *   The URL prefixes.
   */
  public function resolve() {
    $languages = $this->languageManager->getLanguages();
    $prefixes = [];

    // There is no API in Drupal core to determine the final language prefixes because
    // they can be changes with outbound path processors.
    //
    // Create a URL using our dummy path for the given language.
    // This is a safe way to get the correct "language prefix",
    // as some sites may have different approaches to these prefixes, e.g.
    // a country+langcode or having a language without any prefix at all.
    foreach ($languages as $language) {

      $url = Url::fromUserInput(self::PREFIX_PATH, [
        'language' => $language,
      ]);

      // Generate the processed path.
      $path = $url->toString(TRUE)->getGeneratedUrl();

      // For example, this might be:
      // /fr/paragraphs_blokkli_graphql/prefix
      // By removing our dummy path, we end up with the prefix:
      // /fr
      // In case of a language with no prefix, we end up with an empty string.
      $prefix = str_replace(self::PREFIX_PATH, '', $path);

      $prefixes[] = [
        'langcode' => $language->getId(),
        'prefix' => $prefix,
      ];
    }
    return $prefixes;
  }

}
