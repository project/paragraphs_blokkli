<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliApplicablePluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Produces the available transform plugins.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_transform_plugins",
 *   name = @Translation("Paragraphs Blokkli Transform Plugins"),
 *   description = @Translation("Resolves the available transform plugins."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The transform plugins.")
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("The host entity.")
 *     ),
 *     "langcode" = @ContextDefinition("string",
 *       label = @Translation("The langcode.")
 *     ),
 *   }
 * )
 */
class GetTransformPlugins extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.paragraph_transform', ContainerInterface::NULL_ON_INVALID_REFERENCE),
    );
  }

  /**
   * EntityLoad constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\paragraphs_blokkli_transform\ParagraphTransformPluginManager|null $pluginManager
   *   The transform plugin manager.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    protected $pluginManager,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
  }

  /**
   * Resolver.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The host entity.
   * @param string $langcode
   *   The langcode.
   */
  public function resolve(FieldableEntityInterface $entity, string $langcode) {
    if (!$this->pluginManager) {
      return [];
    }

    $definitions = $this->pluginManager->getDefinitions();
    $plugins = [];

    foreach (array_keys($definitions) as $pluginId) {
      $plugin = $this->pluginManager->createInstance($pluginId);
      if ($plugin instanceof ParagraphsBlokkliApplicablePluginInterface) {
        // Skip if the plugin does not apply for the given context.
        if (!$plugin->applies($entity, $langcode)) {
          continue;
        }
      }

      $plugins[] = $plugin;
    }

    return $plugins;
  }

}
