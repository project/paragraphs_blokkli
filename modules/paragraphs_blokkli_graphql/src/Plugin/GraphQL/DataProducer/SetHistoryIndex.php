<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\DataProducer;

use Drupal\paragraphs_blokkli\EditStateInterface;
use Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\StateActionProducerBase;

/**
 * Set history index of an edit state.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_set_history_index",
 *   name = @Translation("Paragraphs Blokkli Set History Index"),
 *   description = @Translation("Set history index of an edit state."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The mutation.")
 *   ),
 *   consumes = {
 *     "state" = @ContextDefinition("any",
 *       label = @Translation("The paragraphs edit state.")
 *     ),
 *     "index" = @ContextDefinition("integer",
 *       label = @Translation("The index")
 *     ),
 *   }
 * )
 */
class SetHistoryIndex extends StateActionProducerBase {

  /**
   * Resolver.
   *
   * @param \Drupal\paragraphs_blokkli\EditStateInterface $state
   *   The edit state.
   * @param int $index
   *   The index.
   */
  public function resolve(EditStateInterface $state, int $index) {
    $state->setHistoryIndex($index);
    $this->blokkliManager->saveState($state);
    return [
      'success' => TRUE,
      'state' => $state,
    ];
  }

}
