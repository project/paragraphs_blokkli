<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Procudes the translation state.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_import_source_entities",
 *   name = @Translation("Paragraphs Blokkli Import Source Entities"),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The import source entities result.")
 *   ),
 *   consumes = {
 *     "entityType" = @ContextDefinition("string",
 *       label = @Translation("The entity type.")
 *     ),
 *     "entityUuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the entity for which to get the import sources.")
 *     ),
 *     "searchText" = @ContextDefinition("string",
 *       label = @Translation("The text for searching."),
 *       required = FALSE,
 *     ),
 *   }
 * )
 */
class ParagraphsBlokkliImportSourceEntities extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('paragraphs_blokkli.helper')
    );
  }

  /**
   * ParagraphsBlokkliImportSourceEntities constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param ParagraphsBlokkliHelper $paragraphsBlokkliHelper
   *   The paragraphs blokkli helper.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected paragraphsBlokkliHelper $paragraphsBlokkliHelper,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
  }

  /**
   * Resolver.
   */
  public function resolve(string $entityType, string $entityUuid, string|null $searchText) {
    $storage = $this->entityTypeManager->getStorage($entityType);
    $entities = $storage->loadByProperties(['uuid' => $entityUuid]);
    /** @var \Drupal\Core\Entity\FieldableEntityInterface|null $entity */
    $entity = array_values($entities)[0] ?? NULL;
    if ($entity && $entity->access('view')) {
      $entityType = $storage->getEntityType();
      $fieldNames = $this->paragraphsBlokkliHelper->getParagraphReferenceFieldNames($entity);

      $query = $storage->getQuery()->accessCheck(TRUE);
      $bundleKey = $entityType->getKey('bundle');
      if ($bundleKey) {
        $query->condition($bundleKey, $entity->bundle());
      }

      if ($searchText) {
        $labelKey = $entityType->getKey('label');
        if ($labelKey) {
          $searchValue = strtolower($searchText);
          $query->condition($labelKey, $searchValue, 'CONTAINS');
        }
      }
      $queryGroup = $query->orConditionGroup();
      foreach ($fieldNames as $fieldName) {
        $queryGroup->condition($fieldName, NULL, 'IS NOT NULL');
      }
      $query->condition($queryGroup);
      $query->condition('uuid', $entityUuid, '<>');
      $query->range(0, 100);

      $result = $query->execute();
      $items = [];
      foreach ($result as $id) {
        $importEntity = $storage->load($id);
        $items[] = [
          'uuid' => $importEntity->uuid(),
          'label' => $importEntity->label(),
        ];
      }

      return [
        'total' => $query->count()->execute(),
        'items' => $items,
      ];
    }
    return [
      'total' => 0,
      'items' => [],
    ];
  }

}
