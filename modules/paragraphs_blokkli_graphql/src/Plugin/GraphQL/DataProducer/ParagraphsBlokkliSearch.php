<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\paragraphs_blokkli_search\ParagraphsBlokkliSearchContentInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Produces the search results.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_search",
 *   name = @Translation("Paragraphs Blokkli Search"),
 *   description = @Translation("Produces the search results."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The search results.")
 *   ),
 *   consumes = {
 *     "id" = @ContextDefinition("string",
 *       label = @Translation("The search ID."),
 *     ),
 *     "text" = @ContextDefinition("string",
 *       label = @Translation("The search text."),
 *     ),
 *   }
 * )
 */
class ParagraphsBlokkliSearch extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.paragraphs_blokkli_search', ContainerInterface::NULL_ON_INVALID_REFERENCE),
    );
  }

  /**
   * EntityLoad constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\paragraphs_blokkli_search\ParagraphsBlokkliSearchPluginManager|null $searchPluginManager
   *   The search plugin manager.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected PluginManagerInterface|null $searchPluginManager,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
  }

  /**
   * Resolver.
   *
   * @param string $id
   *   The search ID.
   * @param string $text
   *   The search text.
   * @param \Drupal\graphql\GraphQL\Execution\FieldContext $fieldContext
   *   The field context.
   *
   * @return \Drupal\paragraphs_blokkli_search\ParagraphsBlokkliSearchContentInterface[]
   *   The search results.
   */
  public function resolve(string $id, string $text, FieldContext $fieldContext) {
    if (!$this->entityTypeManager->hasDefinition('blokkli_search_provider')) {
      return [];
    }

    if (!$this->searchPluginManager) {
      return [];
    }

    $storage = $this->entityTypeManager->getStorage('blokkli_search_provider');

    /** @var \Drupal\paragraphs_blokkli_search\Entity\BlokkliSearchProvider|null $searchEntity */
    $searchEntity = $storage->load($id);
    $fieldContext->addCacheTags($storage->getEntityType()->getListCacheTags());

    if (!$searchEntity) {
      return [];
    }

    $fieldContext->addCacheableDependency($searchEntity);

    $pluginId = $searchEntity->getSearchPluginId();
    $configuration = $searchEntity->getSearchPluginConfiguration();

    /** @var \Drupal\paragraphs_blokkli_search\ParagraphsBlokkliSearchInterface $plugin */
    $plugin = $this->searchPluginManager->createInstance($pluginId, $configuration);

    if (!$plugin instanceof ParagraphsBlokkliSearchContentInterface) {
      return [];
    }

    return $plugin->getResults($text, $fieldContext);
  }

}
