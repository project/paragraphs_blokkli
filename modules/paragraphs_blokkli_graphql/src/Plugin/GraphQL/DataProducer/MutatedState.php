<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\paragraphs_blokkli\EditStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Resolves the mutated state.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_edit_state_mutated_state",
 *   name = @Translation("Paragraphs Blokkli Edit State Mutated State"),
 *   description = @Translation("Resolves the mutated state."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The mutated state.")
 *   ),
 *   consumes = {
 *     "state" = @ContextDefinition("any",
 *       label = @Translation("The paragraphs edit state.")
 *     ),
 *     "historyIndex" = @ContextDefinition("integer",
 *       label = @Translation("The history index."),
 *       required = FALSE,
 *     ),
 *   }
 * )
 */
class MutatedState extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('renderer'),
    );
  }

  /**
   * ParagraphsBlokkliMediaLibraryResults constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    protected RendererInterface $renderer,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
  }

  /**
   * Resolver.
   *
   * @param \Drupal\paragraphs_blokkli\EditStateInterface $state
   *   The edit state.
   * @param int|null $historyIndex
   *   The history index.
   */
  public function resolve(EditStateInterface $state, int|null $historyIndex = NULL) {
    $context = new RenderContext();
    return $this->renderer->executeInRenderContext($context, function () use ($state, $historyIndex) {
      return $state->getMutatedState(FALSE, $historyIndex);
    });
  }

}
