<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\StringItemBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\text\Plugin\Field\FieldType\TextItemBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Procudes the list of possible paragraph conversion.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_editable_field_config",
 *   name = @Translation("Paragraphs Blokkli Editable Field Config"),
 *   description = @Translation("Procudes a list of all editable paragraph fields."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The list of paragraph fields.")
 *   ),
 *   consumes = {
 *     "entityType" = @ContextDefinition("string",
 *       label = @Translation("The entity type.")
 *     ),
 *     "entityBundle" = @ContextDefinition("string",
 *       label = @Translation("The entity bundle.")
 *     ),
 *   }
 * )
 */
class ParagraphsBlokkliEditableFieldConfig extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_field.manager'),
      $container->get('entity_type.bundle.info'),
    );
  }

  /**
   * EntityLoad constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   * @param EntityTypeBundleInfoInterface $bundleInfo
   *   The bundle info service.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    protected EntityFieldManagerInterface $entityFieldManager,
    protected EntityTypeBundleInfoInterface $bundleInfo,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
  }

  /**
   * Resolver.
   */
  public function resolve(string $entityType, string $entityBundle) {
    $fieldConfigs = $this->getEditableFields($entityType, $entityBundle);
    $bundles = array_keys($this->bundleInfo->getBundleInfo('paragraph'));
    foreach ($bundles as $bundle) {
      $fieldConfigs = array_merge($fieldConfigs, $this->getEditableFields('paragraph', $bundle));
    }

    return $fieldConfigs;
  }

  /**
   * Get the fields for the given entity type and bundle.
   *
   * @param string $entityType
   *   The entity type.
   * @param string $entityBundle
   *   The entity bundle.
   *
   * @return array
   *   The field configs.
   */
  private function getEditableFields(string $entityType, string $entityBundle) {
    $configs = [];
    $fields = $this->entityFieldManager->getFieldDefinitions($entityType, $entityBundle);
    foreach ($fields as $fieldName => $fieldDefinition) {
      // Skip fields that can't be edited.
      if (
        $fieldDefinition->isReadOnly() ||
        $fieldDefinition->isInternal() ||
        $fieldDefinition->isComputed() ||
        !$fieldDefinition->isDisplayConfigurable('form')
      ) {
        continue;
      }

      $fieldClass = $fieldDefinition->getItemDefinition()->getClass();
      $isString = is_subclass_of($fieldClass, StringItemBase::class) || $fieldClass === StringItemBase::class;
      $isText = is_subclass_of($fieldClass, TextItemBase::class) || $fieldClass === TextItemBase::class;

      // We are only interested in string or text fields.
      if (!$isString && !$isText) {
        continue;
      }

      $storageDefinition = $fieldDefinition->getFieldStorageDefinition();
      $maxLength = -1;
      $fieldSettings = $fieldDefinition->getSettings();
      if (isset($fieldSettings['max_length'])) {
        $maxLength = $fieldSettings['max_length'];
      }

      $configs[] = [
        'name' => $fieldName,
        'entityType' => $entityType,
        'entityBundle' => $entityBundle,
        'label' => $fieldDefinition->getLabel(),
        'type' => $isText ? 'frame' : 'plain',
        'required' => $fieldDefinition->isRequired(),
        'cardinality' => $storageDefinition->getCardinality(),
        'maxLength' => empty($maxLength) ? -1 : $maxLength,
      ];
    }

    return $configs;
  }

}
