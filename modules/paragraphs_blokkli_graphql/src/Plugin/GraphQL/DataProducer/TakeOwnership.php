<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\DataProducer;

use Drupal\paragraphs_blokkli\EditStateInterface;
use Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\StateActionProducerBase;

/**
 * Take ownership of an edit state.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_take_ownership",
 *   name = @Translation("Paragraphs Blokkli Take Ownership"),
 *   description = @Translation("Takes ownership of an edit state."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The mutation.")
 *   ),
 *   consumes = {
 *     "state" = @ContextDefinition("any",
 *       label = @Translation("The paragraphs edit state.")
 *     ),
 *   }
 * )
 */
class TakeOwnership extends StateActionProducerBase {

  /**
   * Resolver.
   *
   * @param \Drupal\paragraphs_blokkli\EditStateInterface $state
   *   The edit state.
   */
  public function resolve(EditStateInterface $state) {
    $this->blokkliManager->takeOwnership($state);
    $this->blokkliManager->saveState($state);
    return [
      'success' => TRUE,
      'state' => $state,
    ];
  }

}
