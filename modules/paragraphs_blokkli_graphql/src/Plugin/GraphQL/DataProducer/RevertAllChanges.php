<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\DataProducer;

use Drupal\paragraphs_blokkli\EditStateInterface;
use Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\StateActionProducerBase;

/**
 * Reverts all changes on an edit state.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_revert_all_changes",
 *   name = @Translation("Paragraphs Blokkli Revert All Changes"),
 *   description = @Translation("Reverts all changes."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The mutation.")
 *   ),
 *   consumes = {
 *     "state" = @ContextDefinition("any",
 *       label = @Translation("The paragraphs edit state.")
 *     ),
 *   }
 * )
 */
class RevertAllChanges extends StateActionProducerBase {

  /**
   * Resolver.
   *
   * @param \Drupal\paragraphs_blokkli\EditStateInterface $state
   *   The edit state.
   */
  public function resolve(EditStateInterface $state) {
    // Deletes the state from temp store.
    $this->blokkliManager->revertAllChanges($state);

    // Get the entity and create a new state.
    $entity = $state->getHostEntity();
    $newState = $this->blokkliManager->getParagraphsEditState($entity);
    $this->blokkliManager->saveState($newState);
    return [
      'success' => TRUE,
      'state' => $newState,
    ];
  }

}
