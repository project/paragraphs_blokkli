<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\DataProducer;

use Drupal\paragraphs_blokkli\EditStateInterface;
use Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\StateActionProducerBase;

/**
 * Perform the undo action on the edit state.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_undo",
 *   name = @Translation("Paragraphs Blokkli Undo"),
 *   description = @Translation("Perform the undo action on the edit state."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The mutation.")
 *   ),
 *   consumes = {
 *     "state" = @ContextDefinition("any",
 *       label = @Translation("The paragraphs edit state.")
 *     ),
 *   }
 * )
 */
class Undo extends StateActionProducerBase {

  /**
   * Resolver.
   *
   * @param \Drupal\paragraphs_blokkli\EditStateInterface $state
   *   The edit state.
   */
  public function resolve(EditStateInterface $state) {
    $state->undo();
    $this->blokkliManager->saveState($state);
    return [
      'success' => TRUE,
      'state' => $state,
    ];
  }

}
