<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliConfig;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Produces a valid host entity.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_load_host_entity",
 *   name = @Translation("Paragraphs Blokkli Load Host Entity"),
 *   description = @Translation("Resolves a valid host entity."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The host entity.")
 *   ),
 *   consumes = {
 *     "entityType" = @ContextDefinition("string",
 *       label = @Translation("The entity type.")
 *     ),
 *     "entityUuid" = @ContextDefinition("string",
 *       label = @Translation("The entity UUID.")
 *     ),
 *     "langcode" = @ContextDefinition("string",
 *       label = @Translation("The langcode.")
 *     ),
 *   }
 * )
 */
class LoadHostEntity extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('paragraphs_blokkli.manager'),
      $container->get('paragraphs_blokkli.config'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * EntityLoad constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\paragraphs_blokkli\ParagraphsBlokkliManager $blokkliManager
   *   The blokkli manager.
   * @param \Drupal\paragraphs_blokkli\ParagraphsBlokkliConfig $blokkliConfig
   *   The blokkli config.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    protected ParagraphsBlokkliManager $blokkliManager,
    protected ParagraphsBlokkliConfig $blokkliConfig,
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
  }

  /**
   * Resolver.
   *
   * @param string $entityType
   *   The entity type.
   * @param string $entityUuid
   *   The entity UUID.
   * @param string $langcode
   *   The langcode.
   * @param \Drupal\graphql\GraphQL\Execution\FieldContext $fieldContext
   *   The field context.
   */
  public function resolve(string $entityType, string $entityUuid, string $langcode, FieldContext $fieldContext) {
    $entityTypeId = strtolower($entityType);
    if (!$this->entityTypeManager->hasDefinition($entityTypeId)) {
      return NULL;
    }

    $entities = $this->entityTypeManager->getStorage($entityTypeId)->loadByProperties(['uuid' => $entityUuid]);
    $entity = array_values($entities)[0] ?? NULL;

    if (!$entity) {
      return NULL;
    }

    $fieldContext->addCacheableDependency($entity);

    if (!$this->blokkliConfig->isEnabledForEntity($entity)) {
      return NULL;
    }

    if ($entity instanceof TranslatableInterface && $entity->isTranslatable()) {
      if ($entity->hasTranslation($langcode)) {
        $entity = $entity->getTranslation($langcode);
      }
    }

    $access = $entity->access('view', NULL, TRUE);
    $fieldContext->addCacheableDependency($access);

    return $entity;
  }

}
