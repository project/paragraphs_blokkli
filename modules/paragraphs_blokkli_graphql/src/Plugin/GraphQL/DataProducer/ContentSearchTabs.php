<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Component\Utility\SortArray;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Produces the search results.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_content_search_tabs",
 *   name = @Translation("Paragraphs Blokkli Content Search Tabs"),
 *   description = @Translation("Produces the content search tabs."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The content search tabs.")
 *   ),
 * )
 */
class ContentSearchTabs extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
    );
  }

  /**
   * EntityLoad constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
  }

  /**
   * Resolver.
   *
   * @return array
   *   The search tabs.
   */
  public function resolve(FieldContext $fieldContext) {
    if (!$this->entityTypeManager->hasDefinition('blokkli_search_provider')) {
      return [];
    }

    $storage = $this->entityTypeManager->getStorage('blokkli_search_provider');
    $fieldContext->addCacheTags($storage->getEntityType()->getListCacheTags());
    /** @var \Drupal\paragraphs_blokkli_search\Entity\BlokkliSearchProvider[] $entities */
    $entities = $storage->loadMultiple();

    $tabs = [];

    foreach ($entities as $entity) {
      if (!$entity->status()) {
        continue;
      }

      $plugin = $entity->getSearchPlugin();

      if ($plugin->supportsType('content')) {
        $tabs[] = [
          'id' => $entity->id(),
          'label' => $entity->label(),
          'weight' => $entity->getWeight(),
        ];
      }
    }

    uasort($tabs, [SortArray::class, 'sortByWeightElement']);

    return $tabs;
  }

}
