<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Entity\TranslatableInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\paragraphs_blokkli\EditStateInterface;

/**
 * Resolves the host entity of the edit state.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_edit_state_entity",
 *   name = @Translation("Paragraphs Blokkli Edit State Entity"),
 *   description = @Translation("Resolves the edit state entity."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The mutation.")
 *   ),
 *   consumes = {
 *     "state" = @ContextDefinition("any",
 *       label = @Translation("The paragraphs edit state.")
 *     ),
 *     "langcode" = @ContextDefinition("string",
 *       label = @Translation("The current langcode.")
 *     ),
 *   }
 * )
 */
class EditStateEntity extends DataProducerPluginBase {

  /**
   * Resolver.
   *
   * @param \Drupal\paragraphs_blokkli\EditStateInterface $state
   *   The edit state.
   * @param string $langcode
   *   The current langcode.
   */
  public function resolve(EditStateInterface $state, string $langcode) {
    // We can skip access checks here because the edit state already "inherits"
    // permissions from its host entity.
    $entity = $state->getHostEntity();

    // Get the current translation if available.
    if (
      $entity instanceof TranslatableInterface &&
      $entity->isTranslatable() &&
      $entity->hasTranslation($langcode)
    ) {
      return $entity->getTranslation($langcode);
    }

    return $entity;
  }

}
