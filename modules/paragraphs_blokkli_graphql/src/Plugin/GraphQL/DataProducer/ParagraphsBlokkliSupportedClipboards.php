<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Component\Utility\Bytes;
use Drupal\Component\Utility\Environment;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\graphql_core_schema\EntitySchemaHelper;
use Drupal\media\Plugin\media\Source\OEmbedInterface;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliConfig;
use Drupal\paragraphs_blokkli\PbEntityMappingStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Produces the supported clipboard types.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_supported_clipboards",
 *   name = @Translation("Paragraphs Blokkli Supported Clipboards"),
 *   description = @Translation("Produces the supported clipboard types."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The list of clipboard types.")
 *   ),
 * )
 */
class ParagraphsBlokkliSupportedClipboards extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The pb_entity_mapping storage.
   */
  protected PbEntityMappingStorage $mappingStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('paragraphs_blokkli.config'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * EntityLoad constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\paragraphs_blokkli\ParagraphsBlokkliConfig $blokkliConfig
   *   The blokkli config.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    protected ParagraphsBlokkliConfig $blokkliConfig,
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->mappingStorage = $entityTypeManager->getStorage('pb_entity_mapping');
  }

  /**
   * Resolver.
   */
  public function resolve(FieldContext $fieldContext) {
    $fieldContext->addCacheableDependency($this->blokkliConfig);
    $fieldContext->addCacheTags($this->mappingStorage->getEntityType()->getListCacheTags());

    return array_filter([
      $this->getRichText(),
      $this->getMediaClipboard('image', $this->blokkliConfig->getMediaBundleImage()),
      $this->getMediaClipboard('file', $this->blokkliConfig->getMediaBundleFile()),
      $this->getRemoteVideo(),
    ]);
  }

  /**
   * Get the clipboard setting for file/image.
   *
   * @param string $sourcePluginId
   *   The supported media source plugin ID.
   * @param string|null $mediaBundle
   *   The media bundle to get the mappings for.
   *
   * @return array|null
   *   The clipboard setting.
   */
  protected function getMediaClipboard(string $sourcePluginId, string|null $mediaBundle = NULL) {
    if (!$mediaBundle) {
      return NULL;
    }

    /** @var \Drupal\media\Entity\MediaType|null $mediaType */
    $mediaType = $this->entityTypeManager->getStorage('media_type')->load($mediaBundle);

    if (!$mediaType) {
      return NULL;
    }

    $mappings = $this->mappingStorage->findMappings('media', $mediaBundle);
    if (empty($mappings)) {
      return NULL;
    }

    $possibleBundles = array_map(function ($mapping) {
      return $mapping->getParagraphBundle();
    }, $mappings);

    $source = $mediaType->getSource();

    if ($source->getPluginId() !== $sourcePluginId) {
      return NULL;
    }

    $sourceField = $source->getSourceFieldDefinition($mediaType);

    if (!$sourceField) {
      return NULL;
    }

    $fileExtensions = $sourceField->getSetting('file_extensions') ?? '';

    $maxFilesize = Bytes::toNumber(Environment::getUploadMaxSize());
    $configuredMaxFilesize = $sourceField->getSetting('max_filesize');
    if (!empty($configuredMaxFilesize)) {
      $maxFilesize = min($maxFilesize, Bytes::toNumber($configuredMaxFilesize));
    }

    $typename = EntitySchemaHelper::toPascalCase(['ParagraphsBlokkliSupportedClipboard', $sourcePluginId]);

    return [
      '__typename' => $typename,
      'possibleParagraphBundles' => $possibleBundles,
      'fileExtensions' => explode(' ', $fileExtensions),
      'maxFileSize' => $maxFilesize,
    ];
  }

  /**
   * Get the clipboard setting for remote videos.
   *
   * @return array|null
   *   The clipboard video setting.
   */
  protected function getRemoteVideo() {
    $mediaBundle = $this->blokkliConfig->getMediaBundleRemoteVideo();

    if (!$mediaBundle) {
      return NULL;
    }

    /** @var \Drupal\media\Entity\MediaType|null $mediaType */
    $mediaType = $this->entityTypeManager->getStorage('media_type')->load($mediaBundle);

    if (!$mediaType) {
      return NULL;
    }

    $mappings = $this->mappingStorage->findMappings('media', $mediaBundle);
    if (empty($mappings)) {
      return NULL;
    }

    $possibleBundles = array_map(function ($mapping) {
      return $mapping->getParagraphBundle();
    }, $mappings);

    $source = $mediaType->getSource();

    if (!$source instanceof OEmbedInterface) {
      return NULL;
    }

    $providers = $source->getProviders();

    $videoProviders = [];

    foreach ($providers as $provider) {
      $key = strtolower($provider);
      if ($key === 'vimeo') {
        $videoProviders[] = 'VIMEO';
      }
      elseif ($key === 'youtube') {
        $videoProviders[] = 'YOUTUBE';
      }
    }

    if (empty($videoProviders)) {
      return NULL;
    }

    return [
      '__typename' => 'ParagraphsBlokkliSupportedClipboardRemoteVideo',
      'possibleParagraphBundles' => $possibleBundles,
      'videoProviders' => $videoProviders,
    ];
  }

  /**
   * Get the clipboard setting for rich text.
   *
   * @return array|null
   *   The clipboard rich text setting.
   */
  protected function getRichText() {
    $settings = $this->blokkliConfig->getClipboardTextParagraph();

    if (!$settings) {
      return NULL;
    }

    /** @var \Drupal\paragraphs\Entity\ParagraphsType|null $paragraphType */
    $paragraphType = $this->entityTypeManager->getStorage('paragraphs_type')->load($settings['bundle']);

    if (!$paragraphType) {
      return NULL;
    }

    return [
      '__typename' => 'ParagraphsBlokkliSupportedClipboardRichText',
      'possibleParagraphBundles' => [$settings['bundle']],
    ];
  }

}
