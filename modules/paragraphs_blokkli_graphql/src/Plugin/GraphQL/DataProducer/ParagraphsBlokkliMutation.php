<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\paragraphs_blokkli\EditStateInterface;
use Drupal\paragraphs_blokkli\Exception\MutationException;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginManager;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Produce a new paragraphs blokkli mutation.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_mutation",
 *   name = @Translation("Paragraphs Blokkli Mutation"),
 *   description = @Translation("Creates a new paragraphs blokkli mutation."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The mutation.")
 *   ),
 *   consumes = {
 *     "state" = @ContextDefinition("any",
 *       label = @Translation("The paragraphs edit state.")
 *     ),
 *     "pluginId" = @ContextDefinition("string",
 *       label = @Translation("The mutation plugin id.")
 *     ),
 *     "values" = @ContextDefinition("any",
 *       label = @Translation("The values for the mutation."),
 *     ),
 *   }
 * )
 */
class ParagraphsBlokkliMutation extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.paragraph_mutation'),
      $container->get('paragraphs_blokkli.manager'),
      $container->get('renderer'),
    );
  }

  /**
   * EntityLoad constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\paragraphs_blokkli\ParagraphMutationPluginManager $mutationPluginManager
   *   The paragraph mutation plugin manager.
   * @param \Drupal\paragraphs_blokkli\ParagraphsBlokkliManager $blokkliManager
   *   The blokkli manager.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    protected ParagraphMutationPluginManager $mutationPluginManager,
    protected ParagraphsBlokkliManager $blokkliManager,
    protected RendererInterface $renderer,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
  }

  /**
   * Resolver.
   */
  public function resolve(EditStateInterface $state, string $pluginId, array $values) {
    $context = new RenderContext();

    return $this->renderer->executeInRenderContext($context, function () use ($state, $pluginId, $values) {
      try {
        $mutation = $this->mutationPluginManager->createInstance($pluginId, $values);
        $state->addMutation($mutation);
        $state->getMutatedState();
        $this->blokkliManager->saveState($state);
      }
      catch (MutationException $e) {
        return [
          'state' => NULL,
          'success' => FALSE,
          'errors' => [
            $e->getMessage(),
          ],
        ];
      }

      return [
        'state' => $state,
        'success' => TRUE,
        'errors' => [],
      ];
    });
  }

}
