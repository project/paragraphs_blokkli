<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\paragraphs_blokkli\BlokkliSession;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Performs the access check wether the current user can generally use blökkli.
 *
 * This will return TRUE if:
 * - The user has the required permission or
 * - The paragraphs_blokkli session exists.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_can_use_blokkli",
 *   name = @Translation("Can use blökkli"),
 *   description = @Translation("Returns TRUE if the current user can use blökkli."),
 *   produces = @ContextDefinition("boolean",
 *     label = @Translation("TRUE if the current user can use blökkli.")
 *   ),
 * )
 */
class CanUseBlokkli extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('request_stack'),
    );
  }

  /**
   * CanUseBlokkli constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    array $plugin_definition,
    protected AccountInterface $currentUser,
    protected RequestStack $requestStack,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * Resolver.
   *
   * @param \Drupal\graphql\GraphQL\Execution\FieldContext $fieldContext
   *   The field context.
   *
   * @return bool
   *   TRUE if the current user can use blökkli.
   */
  public function resolve(FieldContext $fieldContext): bool {
    // In order to use blökkli, the user must have at least the permission to
    // edit an edit state.
    $hasPermission = $this->currentUser->hasPermission('edit paragraphs blokkli edit state');
    $fieldContext->addCacheContexts(['user.permissions', 'session.exists']);

    if ($hasPermission) {
      return TRUE;
    }

    $request = $this->requestStack->getCurrentRequest();
    if (!$request->hasSession(TRUE)) {
      return FALSE;
    }

    $session = $request->getSession();
    $blokkliSession = BlokkliSession::fromSession($session);
    return $blokkliSession->hasAnyPreviewGrant();
  }

}
