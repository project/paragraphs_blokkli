<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Performs the search for library items.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_library_items_search",
 *   name = @Translation("Paragraphs Blokkli Library Items Search"),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The search result.")
 *   ),
 *   consumes = {
 *     "bundles" = @ContextDefinition("any",
 *       label = @Translation("The bundle."),
 *       required = TRUE,
 *     ),
 *     "text" = @ContextDefinition("string",
 *       label = @Translation("The search text."),
 *       required = FALSE,
 *     ),
 *     "page" = @ContextDefinition("integer",
 *       label = @Translation("The page."),
 *       required = FALSE,
 *     ),
 *   }
 * )
 */
class LibraryItemsSearch extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
    );
  }

  /**
   * ParagraphsBlokkliMediaLibraryResults constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
  }

  /**
   * Resolver.
   *
   * @param string[] $bundles
   * @param string|null $text
   * @param int|null $page
   * @param FieldContext|null $fieldContext
   */
  public function resolve(array $bundles, string|null $text, int|null $page, FieldContext|null $fieldContext) {
    if (!$this->entityTypeManager->hasDefinition('blokkli_search_provider')) {
      return [];
    }

    $storage = $this->entityTypeManager->getStorage('blokkli_search_provider');
    $fieldContext?->addCacheTags($storage->getEntityType()->getListCacheTags());

    /** @var \Drupal\paragraphs_blokkli_search\Entity\BlokkliSearchProvider|null $searchEntity */
    $searchEntity = array_values($storage->loadByProperties([
      'search_type' => 'library_items',
    ]))[0] ?? NULL;

    if (!$searchEntity) {
      return NULL;
    }

    $plugin = $searchEntity->getSearchPlugin();

    if (!$plugin->supportsType('library_items')) {
      return NULL;
    }

    /** @var \Drupal\paragraphs_blokkli_search\ParagraphsBlokkliSearchLibraryItemsInterface $plugin */
    return $plugin->getLibraryItemsSearchResults($bundles, $text, $page, $fieldContext);
  }

}
