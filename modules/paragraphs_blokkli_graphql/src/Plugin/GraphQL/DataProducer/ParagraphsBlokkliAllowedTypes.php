<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Procudes the list of allowed paragraphs by bundle and field.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_allowed_types",
 *   name = @Translation("Paragraphs Blokkli Allowed Types"),
 *   description = @Translation("Procudes the list of allowed paragraphs by bundle and field."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The list of allowed paragraphs.")
 *   ),
 * )
 */
class ParagraphsBlokkliAllowedTypes extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity field manager.
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_field.manager'),
    );
  }

  /**
   * EntityLoad constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    EntityFieldManagerInterface $entityFieldManager,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityFieldManager = $entityFieldManager;
  }

  /**
   * Resolver.
   */
  public function resolve(FieldContext $fieldContext) {
    $fieldMap = $this->entityFieldManager->getFieldMapByFieldType('entity_reference_revisions');

    $items = [];

    foreach ($fieldMap as $entityType => $fields) {
      foreach ($fields as $fieldName => $fieldOptions) {
        foreach ($fieldOptions['bundles'] as $bundle) {
          $fields = $this->entityFieldManager->getFieldDefinitions($entityType, $bundle);
          $field = $fields[$fieldName] ?? NULL;
          if ($field && $field->getType() === 'entity_reference_revisions') {
            $settings = $field->getSettings();
            $targetType = $settings['target_type'] ?? NULL;
            $fieldContext->addCacheableDependency($field);

            if ($targetType === 'paragraph') {
              $targetBundles = $settings['handler_settings']['target_bundles'] ?? [];
              $items[] = [
                'entity_type' => $entityType,
                'bundle' => $bundle,
                'field_name' => $fieldName,
                'allowed_types' => $targetBundles,
              ];
            }
          }
        }
      }
    }
    return $items;
  }

}
