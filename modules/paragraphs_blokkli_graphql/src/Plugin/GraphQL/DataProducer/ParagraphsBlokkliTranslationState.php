<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\paragraphs_blokkli\EditStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Procudes the translation state.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_translation_state",
 *   name = @Translation("Paragraphs Blokkli Translation State"),
 *   description = @Translation("The translation state."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The available features.")
 *   ),
 *   consumes = {
 *     "state" = @ContextDefinition("entity",
 *       label = @Translation("The edit state.")
 *     ),
 *   }
 * )
 */
class ParagraphsBlokkliTranslationState extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('language_manager'),
    );
  }

  /**
   * EntityLoad constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    protected LanguageManagerInterface $languageManager,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
  }

  /**
   * Resolver.
   */
  public function resolve(EditStateInterface $state) {
    $entity = $state->getHostEntity();
    if (!$entity) {
      return NULL;
    }

    $isTranslatable = FALSE;
    $sourceLanguage = $entity->language();
    $availableLanguages = $this->languageManager->getLanguages();
    $translations = [];

    if ($entity instanceof TranslatableInterface) {
      $isTranslatable = $entity->isTranslatable();
      $translations = $this->mapTranslations($availableLanguages, $entity);
    }

    return [
      'isTranslatable' => $isTranslatable,
      'sourceLanguage' => $sourceLanguage->getId(),
      'availableLanguages' => $availableLanguages,
      'translations' => $translations,
    ];
  }

  /**
   * Get the langcodes from an array of languages.
   *
   * @param \Drupal\Core\Language\LanguageInterface[] $availableLanguages
   *   The available languages.
   * @param \Drupal\Core\Entity\TranslatableInterface $entity
   *   The entity.
   *
   * @return array
   *   The array of ParagraphsBlokkliTranslationStateEntityTranslation types.
   */
  private function mapTranslations(array $availableLanguages, TranslatableInterface $entity): array {
    $items = [];
    foreach ($availableLanguages as $language) {
      $items[] = $this->mapTranslationsItem($language, $entity);
    }
    return $items;
  }

  /**
   * Map a translation.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language to map.
   * @param \Drupal\Core\Entity\TranslatableInterface $entity
   *   The entity.
   *
   * @return array
   *   The ParagraphsBlokkliTranslationStateEntityTranslation GraphQL type.
   */
  private function mapTranslationsItem(LanguageInterface $language, TranslatableInterface $entity): array {
    $translation = $entity;
    $langcode = $language->getId();
    $hasTranslation = $entity->hasTranslation($langcode);

    $url = NULL;
    $editUrl = NULL;
    $redirect = Url::fromRoute('paragraphs_blokkli.redirect')->toString(TRUE)->getGeneratedUrl();

    if ($translation->hasLinkTemplate('edit-form')) {
      // The default edit form for untranslatable entities or for the source language.
      $editUrl = $translation->toUrl('edit-form');
    }

    if ($hasTranslation) {
      $translation = $entity->getTranslation($langcode);
      if (
        !$translation->isDefaultTranslation() &&
        $translation->hasLinkTemplate('drupal:content-translation-edit')
      ) {
        $editUrl = $translation
          ->toUrl('drupal:content-translation-edit', [
            'language' => $language,
          ])
          ->setRouteParameter('language', $langcode);
      }
    }
    else {
      if ($translation->hasLinkTemplate('drupal:content-translation-add')) {
        $editUrl = $translation
          ->toUrl('drupal:content-translation-add', [
            'language' => $language,
          ])
          ->setRouteParameter('source', $entity->language()->getId())
          ->setRouteParameter('target', $langcode);
      }
    }

    $status = $translation instanceof EntityPublishedInterface ? $translation->isPublished() : TRUE;

    if ($editUrl) {
      $editUrl->setOption('query', ['destination' => $redirect]);
    }

    // Must support canonical links. However, the commerce_product_variation
    // entity type does not support it, but hacks it in via the toUrl method
    // on the entity.
    if (
        $translation->hasLinkTemplate('canonical')
        || $translation->getEntityTypeId() === 'commerce_product_variation'
    ) {
      $url = $translation->toUrl();
    }

    return [
      'id' => $langcode,
      'status' => $status,
      'exists' => $hasTranslation,
      'url' => $url ? $url->toString(TRUE)->getGeneratedUrl() : NULL,
      'editUrl' => $editUrl ? $editUrl->toString(TRUE)->getGeneratedUrl() : NULL,
    ];
  }

}
