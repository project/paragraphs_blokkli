<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\paragraphs_blokkli\Entity\ParagraphsBlokkliEditState;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Loads the edit state.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_load_edit_state",
 *   name = @Translation("Paragraphs Blokkli Load Edit State"),
 *   description = @Translation("Resolves the edit state."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The edit state.")
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("The host entity.")
 *     ),
 *     "historyIndex" = @ContextDefinition("integer",
 *       label = @Translation("The desired history index."),
 *       required = FALSE,
 *     ),
 *   }
 * )
 */
class LoadEditState extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('paragraphs_blokkli.manager'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * EntityLoad constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\paragraphs_blokkli\ParagraphsBlokkliManager $blokkliManager
   *   The blokkli manager.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    protected ParagraphsBlokkliManager $blokkliManager,
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
  }

  /**
   * Resolver.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity.
   * @param int|null $historyIndex
   *   The desired history index.
   * @param \Drupal\graphql\GraphQL\Execution\FieldContext $fieldContext
   *   The GraphQL field context.
   */
  public function resolve(FieldableEntityInterface $entity, $historyIndex, FieldContext $fieldContext) {
    // Add the custom cache tag for the host entity.
    $fieldContext->addCacheTags(
      ParagraphsBlokkliEditState::getBlokkliHostCacheTags($entity->uuid())
    );

    $fieldContext->setContextValue(
      'paragraphs_blokkli_history_index',
      $historyIndex ?? NULL
    );

    // First, try to load the edit state without creating one.
    $state = $this->blokkliManager->getParagraphsEditState($entity, TRUE);

    // No state exists yet. See if the user can create one.
    if (!$state) {
      $accessHandler = $this->entityTypeManager->getAccessControlHandler('paragraphs_blokkli_edit_state');
      $accessResult = $accessHandler->createAccess(NULL, NULL, [], TRUE);
      $fieldContext->addCacheableDependency($accessResult);

      // If creating an edit state is allowed, try to create one.
      if ($accessResult->isAllowed()) {
        $state = $this->blokkliManager->getParagraphsEditState($entity);
      }
    }

    if (!$state) {
      return NULL;
    }

    $fieldContext->addCacheableDependency($state);

    // Now check if viewing the edit state is possible.
    $access = $state->access('view', NULL, TRUE);
    $fieldContext->addCacheableDependency($access);
    if (!$access->isAllowed()) {
      return NULL;
    }

    return $state;
  }

}
