<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Build the props for the blokkli provider component.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_props",
 *   name = @Translation("Paragraphs Blokkli Props"),
 *   description = @Translation("Returns the blokkli provider props."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The props.")
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("The entity.")
 *     ),
 *   }
 * )
 */
class BlokkliProps extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('paragraphs_blokkli.config'),
      $container->get('entity_type.bundle.info'),
    );
  }

  /**
   * CurrentUser constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   * @param \Drupal\paragraphs_blokkli\ParagraphsBlokkliConfig $blokkliConfig
   *   The blokkli config.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo $bundleInfo
   *   The bundle info service.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    array $plugin_definition,
    protected AccountInterface $currentUser,
    protected ParagraphsBlokkliConfig $blokkliConfig,
    protected EntityTypeBundleInfoInterface $bundleInfo,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * Resolver.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param \Drupal\graphql\GraphQL\Execution\FieldContext $fieldContext
   *   The field context.
   *
   * @return array
   *   The blokkli props.
   */
  public function resolve(EntityInterface $entity, FieldContext $fieldContext): array {
    $canEdit = $this->canEdit($entity, $fieldContext);

    return [
      'canEdit' => $canEdit,
      'entityType' => $entity->getEntityTypeId(),
      'entityBundle' => $entity->bundle(),
      'entityUuid' => $entity->uuid(),
      'language' => $entity->language()->getId(),
      'editLabel' => $canEdit ? $this->getEditLabel($entity) : NULL,
    ];
  }

  /**
   * Determine if the current user can use blokkli on the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param \Drupal\graphql\GraphQL\Execution\FieldContext $fieldContext
   *   The field context.
   *
   * @return bool
   *   Whether the user can use blokkli on the entity.
   */
  protected function canEdit(EntityInterface $entity, FieldContext $fieldContext): bool {
    $fieldContext->addCacheContexts(['user.permissions']);

    // General permission required for using blökkli.
    if (!$this->currentUser->hasPermission('edit paragraphs blokkli edit state')) {
      return FALSE;
    }

    // Now add the config as a cache dependency.
    $fieldContext->addCacheableDependency($this->blokkliConfig);

    // Entity is not enabled for use with blökkli.
    if (!$this->blokkliConfig->isEnabledForEntity($entity)) {
      return FALSE;
    }

    // Use the entity's access result to determine whether blökkli can be used.
    $accessResult = $entity->access('update', $this->currentUser, TRUE);
    $fieldContext->addCacheableDependency($accessResult);

    return $accessResult->isAllowed();
  }

  /**
   * Get the edit label.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return string|null
   *   The edit label.
   */
  protected function getEditLabel(EntityInterface $entity): string|null {
    $entityType = $entity->getEntityTypeId();

    $info = $this->bundleInfo->getBundleInfo($entityType);
    $bundleLabel = $info[$entity->bundle()]['label'] ?? NULL;

    if (!$bundleLabel) {
      return NULL;
    }

    return $this->t('Edit @entity', ['@entity' => $bundleLabel]);
  }

}
