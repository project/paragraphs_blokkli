<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Procudes the translation state.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_media_library_results",
 *   name = @Translation("Paragraphs Blokkli Media Library Results"),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The bundles.")
 *   ),
 *   consumes = {
 *     "text" = @ContextDefinition("string",
 *       label = @Translation("The search text."),
 *       required = FALSE,
 *     ),
 *     "bundle" = @ContextDefinition("string",
 *       label = @Translation("The bundle."),
 *       required = FALSE,
 *     ),
 *     "page" = @ContextDefinition("integer",
 *       label = @Translation("The page."),
 *       required = FALSE,
 *     ),
 *   }
 * )
 */
class ParagraphsBlokkliMediaLibraryResults extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
    );
  }

  /**
   * ParagraphsBlokkliMediaLibraryResults constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
  }

  /**
   * Resolver.
   *
   * @param string|null $text
   * @param string|null $bundle
   * @param int|null $page
   * @param FieldContext|null $fieldContext
   */
  public function resolve(string|null $text, string|null $bundle, int|null $page, FieldContext|null $fieldContext) {
    if (!$this->entityTypeManager->hasDefinition('blokkli_search_provider')) {
      return [];
    }

    $storage = $this->entityTypeManager->getStorage('blokkli_search_provider');
    $fieldContext?->addCacheTags($storage->getEntityType()->getListCacheTags());

    /** @var \Drupal\paragraphs_blokkli_search\Entity\BlokkliSearchProvider|null $searchEntity */
    $searchEntity = array_values($storage->loadByProperties([
      'search_type' => 'media_library',
      'status' => TRUE,
    ]))[0] ?? NULL;

    if (!$searchEntity) {
      return NULL;
    }

    $plugin = $searchEntity->getSearchPlugin();

    if (!$plugin->supportsType('media_library')) {
      return NULL;
    }

    /** @var \Drupal\paragraphs_blokkli_search\ParagraphsBlokkliSearchMediaLibraryInterface $plugin */
    return $plugin->getMediaLibraryResults($text, $bundle, $page, $fieldContext);
  }

}
