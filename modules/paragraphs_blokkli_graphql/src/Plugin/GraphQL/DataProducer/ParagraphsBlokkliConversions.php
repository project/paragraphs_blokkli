<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\paragraphs_blokkli_conversion\ParagraphConversionPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Procudes the list of possible paragraph conversion.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_conversions",
 *   name = @Translation("Paragraphs Blokkli Conversions"),
 *   description = @Translation("Procudes the list of possible paragraph conversions."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The list of paragraph conversions.")
 *   ),
 * )
 */
class ParagraphsBlokkliConversions extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity field manager.
   */
  protected ParagraphConversionPluginManager $pluginManager;

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.paragraph_conversion'),
    );
  }

  /**
   * EntityLoad constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param ParagraphConversionPluginManager $pluginManager
   *   The paragraph_conversion plugin manager.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    ParagraphConversionPluginManager $pluginManager,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->pluginManager = $pluginManager;
  }

  /**
   * Resolver.
   */
  public function resolve() {
    return $this->pluginManager->getDefinitions();
  }

}
