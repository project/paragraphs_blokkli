<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\DataProducer;

use Drupal\paragraphs_blokkli\EditStateInterface;
use Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\StateActionProducerBase;

/**
 * Perform the redo action on the edit state.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_redo",
 *   name = @Translation("Paragraphs Blokkli Redo"),
 *   description = @Translation("Perform the redo action on the edit state."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The mutation.")
 *   ),
 *   consumes = {
 *     "state" = @ContextDefinition("any",
 *       label = @Translation("The paragraphs edit state.")
 *     ),
 *   }
 * )
 */
class Redo extends StateActionProducerBase {

  /**
   * Resolver.
   *
   * @param \Drupal\paragraphs_blokkli\EditStateInterface $state
   *   The edit state.
   */
  public function resolve(EditStateInterface $state) {
    $state->redo();
    $this->blokkliManager->saveState($state);
    return [
      'success' => TRUE,
      'state' => $state,
    ];
  }

}
