<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\DataProducer;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\paragraphs_blokkli\EditStateInterface;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Publish an edit state.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_publish",
 *   name = @Translation("Paragraphs Blokkli Publish"),
 *   description = @Translation("Publishes an edit state."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The mutation result.")
 *   ),
 *   consumes = {
 *     "state" = @ContextDefinition("any",
 *       label = @Translation("The paragraphs edit state.")
 *     ),
 *     "createNewState" = @ContextDefinition("boolean",
 *       label = @Translation("Whether to create a new edit state after publishing."),
 *       required = FALSE,
 *       default = FALSE,
 *     ),
 *   }
 * )
 */
class Publish extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('paragraphs_blokkli.manager'),
      $container->get('renderer'),
    );
  }

  /**
   * EntityLoad constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\paragraphs_blokkli\ParagraphsBlokkliManager $blokkliManager
   *   The blokkli manager.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    protected ParagraphsBlokkliManager $blokkliManager,
    protected RendererInterface $renderer,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
  }

  /**
   * Resolver.
   *
   * @param \Drupal\paragraphs_blokkli\EditStateInterface $state
   *   The edit state.
   * @param bool|null $createNewState
   *   Whether to create a new state after publishing.
   */
  public function resolve(EditStateInterface $state, bool|null $createNewState) {
    return $this->renderer->executeInRenderContext(new RenderContext(), function () use ($state, $createNewState) {
      $entity = $state->getHostEntity();
      $success = $this->blokkliManager->publish($state, FALSE, TRUE);

      if ($success) {
        $state = $createNewState
          ? $this->blokkliManager->getParagraphsEditState($entity)
          : NULL;
        return [
          'success' => TRUE,
          'state' => $state,
        ];
      }

      return [
        'success' => FALSE,
        'state' => $state,
      ];
    });
  }

}
