<?php

namespace Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\DataProducer;

use Drupal\paragraphs_blokkli\EditStateInterface;
use Drupal\paragraphs_blokkli_graphql\Plugin\GraphQL\StateActionProducerBase;

/**
 * Produce a new paragraphs blokkli mutation.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_set_mutation_status",
 *   name = @Translation("Paragraphs Blokkli Set Mutation Status"),
 *   description = @Translation("Changes the status of a mutation item."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("The mutation.")
 *   ),
 *   consumes = {
 *     "state" = @ContextDefinition("any",
 *       label = @Translation("The paragraphs edit state.")
 *     ),
 *     "index" = @ContextDefinition("integer",
 *       label = @Translation("The index of the mutation item.")
 *     ),
 *     "status" = @ContextDefinition("boolean",
 *       label = @Translation("The status.")
 *     ),
 *   }
 * )
 */
class SetMutationStatus extends StateActionProducerBase {

  /**
   * Resolver.
   */
  public function resolve(EditStateInterface $state, int $index, bool $status) {
    $state->setMutationStatus($index, $status);
    $this->blokkliManager->saveState($state);
    return [
      'success' => TRUE,
      'state' => $state,
    ];
  }

}
