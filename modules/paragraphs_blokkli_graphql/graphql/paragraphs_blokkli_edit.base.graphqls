"""
An object of options managed by the paragraph component itself.
"""
scalar ParagraphsBlokkliOptions

interface Paragraph {
  """
  The untyped paragraph options.
  """
  paragraphsBlokkliOptions: ParagraphsBlokkliOptions

  props: Paragraph
}

type ParagraphsBlokkliAllowedType {
  """
  The entity type.
  """
  entityType: String!

  """
  The bundle. For entity types without bundles, this is the same value as entityType.
  """
  bundle: String!

  """
  The name of the field.
  """
  fieldName: String!

  """
  The allowed paragraph types.
  """
  allowedTypes: [String]
}

type ParagraphsBlokkliMutationPlugin {
  label: String
  affectedParagraphUuid: String
}

type ParagraphsEditViolation {
  message: String!
  code: String
  propertyPath: String
  entityType: String
  entityUuid: String
}

type ParagraphsBlokkliConversion {
  sourceBundle: String!
  targetBundle: String!
}

type ParagraphsBlokkliMutatedField {
  name: String!
  entityType: String!
  entityUuid: String!
  list: [Paragraph]!
}

type ParagraphsBlokkliMutatedState {
  """
  Array of available paragraph fields that can be edited, containing the mutated paragraphs.
  """
  fields(langcode: String): [ParagraphsBlokkliMutatedField!]

  """
  Object containing UUIDs of paragraphs as the key and their mutated options as the value.
  """
  behaviorSettings: MapData

  """
  A list of validation violations for the entity and all its fields.
  """
  violations: [ParagraphsEditViolation]
}

type ParagraphsBlokkliAvailableFeatures {
  """
  Whether the conversions feature is available.
  """
  conversion: Boolean!

  """
  Whether the commenting feature is available.
  """
  comment: Boolean!

  """
  Whether the library feature is available.
  """
  library: Boolean!

  """
  List of all enabled mutation plugin IDs.
  """
  mutations: [String]
}

type ParagraphsBlokkliTranslationStateEntityTranslation {
  id: String
  url: String
  editUrl: String
  exists: Boolean
  status: Boolean
}

type ParagraphsBlokkliTranslationState {
  """
  Whether the entity is translatable.
  """
  isTranslatable: Boolean

  """
  The source language.
  """
  sourceLanguage: String

  """
  The generally available languages.
  """
  availableLanguages: [LanguageInterface]

  """
  The translations (existing or non existing) available for the entity.
  """
  translations: [ParagraphsBlokkliTranslationStateEntityTranslation]
}

type ParagraphsBlokkliImportSourceEntities {
  total: Int
  items: [ParagraphsBlokkliImportSourceEntity]
}

type ParagraphsBlokkliImportSourceEntity {
  label: String!
  uuid: String!
}

type ParagraphsBlokkliEntity {
  status: Boolean
  label: String
  bundleLabel: String
}

type ParagraphsBlokkliDroppableFieldConfig {
  name: String!
  label: String!
  entityType: String!
  entityBundle: String!
  cardinality: Int!
  canEdit: Boolean!
  allowedEntityType: String!
  allowedBundles: [String]!
  required: Boolean!
}

type ParagraphsBlokkliFieldConfig {
  name: String!
  entityType: String!
  entityBundle: String!
  label: String!
  cardinality: Int!
  canEdit: Boolean!
  allowedBundles: [String]!
}

enum ParagraphsBlokkliEditableFieldConfigType {
  plain
  frame
}

enum ParagraphsBlokkliRemoteVideoProvider {
  YOUTUBE
  VIMEO
}

interface ParagraphsBlokkliSupportedClipboard {
  possibleParagraphBundles: [String]
}

type ParagraphsBlokkliSupportedClipboardRemoteVideo implements ParagraphsBlokkliSupportedClipboard {
  possibleParagraphBundles: [String]!
  videoProviders: [ParagraphsBlokkliRemoteVideoProvider]!
}

type ParagraphsBlokkliSupportedClipboardImage implements ParagraphsBlokkliSupportedClipboard {
  possibleParagraphBundles: [String]!
  fileExtensions: [String]!
  maxFileSize: Int!
}

type ParagraphsBlokkliSupportedClipboardFile implements ParagraphsBlokkliSupportedClipboard {
  possibleParagraphBundles: [String]!
  fileExtensions: [String]!
  maxFileSize: Int!
}

type ParagraphsBlokkliSupportedClipboardRichText implements ParagraphsBlokkliSupportedClipboard {
  possibleParagraphBundles: [String]!
}

type ParagraphsBlokkliEditableFieldConfig {
  name: String!
  label: String!
  entityType: String!
  entityBundle: String!
  type: ParagraphsBlokkliEditableFieldConfigType!
  required: Boolean!
  maxLength: Int!
}

interface ParagraphsBlokkliMediaLibraryFilter {
  id: String!
  type: String!
  label: String!
}

type ParagraphsBlokkliMediaLibraryFilterText implements ParagraphsBlokkliMediaLibraryFilter {
  id: String!
  type: String!
  label: String!
  placeholder: String!
}

type ParagraphsBlokkliMediaLibraryFilterSelect implements ParagraphsBlokkliMediaLibraryFilter {
  id: String!
  type: String!
  label: String!
  default: String!
  options: MapData
}

type ParagraphsBlokkliMediaLibraryResults {
  items: [ParagraphsBlokkliMediaItem]
  perPage: Int!
  total: Int!
  filters: [ParagraphsBlokkliMediaLibraryFilter]
}

type ParagraphsBlokkliMediaItem {
  mediaId: String!
  label: String!
  context: String!
  targetBundles: [String]!
  thumbnail: String
  icon: String
  mediaBundle: String
}

type ParagraphsBuilderUrlPrefix {
  langcode: String!
  prefix: String!
}
