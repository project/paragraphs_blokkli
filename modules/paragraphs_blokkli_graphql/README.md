Paragraphs Blokkli GraphQL
==========================

This module provides schema extensions to integrate Paragraphs Blokkli using
GraphQL.

Configuration
-------------

1. Enable the module.
2. In your GraphQL server, enable the Paragraphs Blokkli Edit extension.
3. Enable at least one paragraph field (classic example: `field_paragraphs` on
   a node type).
4. Enable the Paragraphs Blokkli Edit State entity type.
5. Expose the `mutations` field in the Paragraphs Blokkli Edit State entity
   type.
