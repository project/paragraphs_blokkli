<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli_search_api\Plugin\search_api\processor;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\paragraphs_blokkli_search\Traits\BlokkliSearchContextTrait;
use Drupal\paragraphs_blokkli_search_api\SearchProcessorPluginBase;

/**
 * Indexes the thumbnail for an entity.
 *
 * @SearchApiProcessor(
 *   id = "paragraphs_blokkli_search_context",
 *   label = @Translation("Blokkli: Search Context"),
 *   description = @Translation("Indexes the context of the entity."),
 *   stages = {
 *     "add_properties" = 0,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class BlokkliSearchContext extends SearchProcessorPluginBase {

  use BlokkliSearchContextTrait;

  /**
   * {@inheritdoc}
   */
  protected function getValuesToIndex(FieldableEntityInterface $entity): array|null {
    $context = $this->getEntitySearchContext($entity);
    return [$context];
  }

}
