<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli_search_api\Plugin\search_api\processor;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliConfig;
use Drupal\paragraphs_blokkli_search\Traits\BlokkliSearchThumbnailTrait;
use Drupal\paragraphs_blokkli_search_api\SearchProcessorPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Indexes the thumbnail for an entity.
 *
 * @SearchApiProcessor(
 *   id = "paragraphs_blokkli_search_thumbnail",
 *   label = @Translation("Blokkli: Search Thumbnail"),
 *   description = @Translation("Indexes the thumbnail URL of the entity."),
 *   stages = {
 *     "add_properties" = 0,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class BlokkliSearchThumbnail extends SearchProcessorPluginBase {

  use BlokkliSearchThumbnailTrait;

  /**
   * The blokkli config.
   */
  protected ParagraphsBlokkliConfig $blokkliConfig;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var static $plugin */
    $plugin = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $plugin->setBlokkliConfig($container->get('paragraphs_blokkli.config'));
    return $plugin;
  }

  /**
   * Retrieves the blokkli config.
   *
   * @return \Drupal\paragraphs_blokkli\ParagraphsBlokkliConfig
   *   The blokkli config.
   */
  public function getBlokkliConfig() {
    return $this->blokkliConfig ?: \Drupal::service('paragraphs_blokkli.config');
  }

  /**
   * Sets the entity type manager.
   *
   * @param \Drupal\paragraphs_blokkli\ParagraphsBlokkliConfig $blokkliConfig
   *   The blokkli config.
   *
   * @return $this
   */
  public function setBlokkliConfig(ParagraphsBlokkliConfig $blokkliConfig) {
    $this->blokkliConfig = $blokkliConfig;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  protected function getValuesToIndex(FieldableEntityInterface $entity): array|null {
    $imageStyleName = $this->getBlokkliConfig()->getImageStyle();
    if (!$imageStyleName) {
      return NULL;
    }

    $url = $this->getEntityThumbnail($entity, $imageStyleName);

    if (!$url) {
      return NULL;
    }

    return [$url];
  }

}
