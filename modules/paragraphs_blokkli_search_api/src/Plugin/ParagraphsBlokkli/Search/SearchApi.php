<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli_search_api\Plugin\ParagraphsBlokkli\Search;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Element\Checkboxes;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\paragraphs_blokkli_search\Model\ContentSearchItem;
use Drupal\paragraphs_blokkli_search\ParagraphsBlokkliSearchContentInterface;
use Drupal\paragraphs_blokkli_search\ParagraphsBlokkliSearchPluginBase;

/**
 * Search plugin that performns a search using search_api.
 *
 * @ParagraphsBlokkliSearch(
 *   id = "search_api",
 *   label = @Translation("Search API"),
 *   description = @Translation("Perform a search on a search_api index."),
 * )
 */
class SearchApi extends ParagraphsBlokkliSearchPluginBase implements ContainerFactoryPluginInterface, ParagraphsBlokkliSearchContentInterface {

  use StringTranslationTrait;
  use DependencySerializationTrait;

  /**
   * {@inheritdoc}
   */
  public function getResults(string $text, ?RefinableCacheableDependencyInterface $metadata = NULL): array {
    $indexId = $this->getIndexId();
    $fieldMapping = $this->getFieldMapping();

    if (!$indexId || !$fieldMapping) {
      return [];
    }

    /** @var \Drupal\search_api\Entity\Index|null $index */
    $index = $this->entityTypeManager->getStorage('search_api_index')->load($indexId);

    if (!$index) {
      return [];
    }

    $metadata?->addCacheableDependency($index);
    $metadata?->addCacheTags(['search_api_list:' . $index->id()]);

    $query = $index->query();
    $query->keys($text);
    $query->range(0, 50);

    $orCondition = $query->createConditionGroup('OR');

    foreach ($this->getGroupedBundleFilters() as $entityType => $bundles) {
      $entityTypeField = $fieldMapping['entity_type'];
      $bundleField = $fieldMapping['entity_bundle'];
      $condition = $query->createConditionGroup('AND');
      $condition->addCondition($entityTypeField, $entityType, '=');
      $condition->addCondition($bundleField, $bundles, 'IN');
      $orCondition->addConditionGroup($condition);
    }

    $query->addConditionGroup($orCondition);

    $items = $query->execute()->getResultItems();

    $resultItems = [];

    foreach ($items as $item) {
      $resultItems[] = $this->mapSearchItem($item, $fieldMapping);
    }

    return array_filter($resultItems);
  }

  /**
   * Get the field mapping.
   *
   * @return array{
   *   'entity_type': string|null,
   *   'entity_bundle': string|null,
   *   'entity_id': string|null,
   *   'title': string|null,
   *   'text': string|null,
   *   'image_url': string|null,
   *   'context': string|null,
   *   }
   */
  protected function getFieldMapping(): array {
    return $this->configuration['field_mapping'] ?? [];
  }

  /**
   * Get the ID of the search_api index.
   *
   * @return string|null
   *   The index ID.
   */
  protected function getIndexId(): string|null {
    return $this->configuration['index_id'] ?? NULL;
  }

  /**
   * Get the grouped and valid bundle filters.
   *
   * @return array<string, string[]>
   *   The bundle filters, keyed by entity type.
   */
  protected function getGroupedBundleFilters(): array {
    $bundleFilters = $this->configuration['bundle_filters'] ?? [];

    $groupedBundleFilters = [];

    foreach ($bundleFilters as $key) {
      [$entityType, $bundle] = explode(':', $key);
      $groupedBundleFilters[$entityType][] = $bundle;
    }

    /** @var \Drupal\paragraphs_blokkli\PbEntityMappingStorage $mappingStorage */
    $mappingStorage = $this->entityTypeManager->getStorage('pb_entity_mapping');

    // Only keep the bundle filters for which a mapping exists.
    foreach ($groupedBundleFilters as $entityType => $bundles) {
      $enabledMappings = $mappingStorage->getMappedBundlesForEntityType($entityType);
      $possibleBundles = array_intersect($bundles, $enabledMappings);

      // There are no bundles available for this entity type, remove the filter.
      if (empty($possibleBundles)) {
        unset($groupedBundleFilters[$entityType]);
      }
      else {
        $groupedBundleFilters[$entityType] = $possibleBundles;
      }
    }

    return $groupedBundleFilters;
  }

  /**
   * Map the search item.
   *
   * @param \Drupal\search_api\Item\Item $item
   *   The item.
   * @param array $fieldMapping
   *   The field mapping.
   *
   * @return \Drupal\paragraphs_blokkli_search\Model\ContentSearchItem|null
   *   The search result.
   */
  protected function mapSearchItem($item, array $fieldMapping): ContentSearchItem|null {
    $fields = [
      'entity_type',
      'entity_bundle',
      'entity_id',
      'title',
      'text',
      'image_url',
      'context',
    ];

    $fieldValues = [];

    foreach ($fields as $fieldName) {
      $itemFieldName = $fieldMapping[$fieldName] ?? NULL;
      if (!$itemFieldName) {
        continue;
      }

      if ($item->getField($itemFieldName)) {
        $itemFieldValue = $item->getField($itemFieldName)->getValues()[0] ?? NULL;
        if (is_scalar($itemFieldValue)) {
          $fieldValues[$fieldName] = (string) $itemFieldValue;
        }
      }
    }

    $entityType = $fieldValues['entity_type'] ?? NULL;
    $entityBundle = $fieldValues['entity_bundle'] ?? NULL;
    $entityId = $fieldValues['entity_id'] ?? NULL;
    $title = !empty($fieldValues['title']) ? $fieldValues['title'] : '';
    $text = !empty($fieldValues['text']) ? $fieldValues['text'] : '';
    $imageUrl = $fieldValues['image_url'] ?? NULL;
    $context = !empty($fieldValues['context']) ? $fieldValues['context'] : '';

    // Return if one of the required values are not present.
    if (!$entityType || !$entityBundle || !$entityId || !$title) {
      return NULL;
    }

    return new ContentSearchItem(
      $entityType,
      $entityBundle,
      $entityId,
      strip_tags($title),
      strip_tags($text),
      $imageUrl,
      strip_tags($context)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function canBeUsed(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    if (!$this->entityTypeManager->hasDefinition('search_api_index')) {
      return [];
    }

    $configuration = $this->getConfiguration();
    $indexId = $form_state->getValue(['search_plugin', 'configuration', $this->getPluginId(), 'index_id']) ?? $configuration['index_id'] ?? NULL;

    $indexStorage = $this->entityTypeManager->getStorage('search_api_index');
    /** @var \Drupal\search_api\Entity\Index[] $indices */
    $indices = $indexStorage->loadMultiple();

    $indexOptions = [
      '' => $this->t('Select an index'),
    ];

    foreach ($indices as $id => $index) {
      $indexOptions[$id] = $index->label();
    }

    $form['index_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Index'),
      '#options' => $indexOptions,
      '#default_value' => $indexId,
      '#required' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'reloadFields'],
        'wrapper' => 'edit-search-plugin-configuration-wrapper',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Loading field mapping'),
        ],
      ],
    ];

    if (!$indexId) {
      return $form;
    }

    /** @var \Drupal\search_api\Entity\Index|null $selectedIndex */
    $selectedIndex = $indexStorage->load($indexId);

    if (!$selectedIndex) {
      return $form;
    }

    $form['field_mapping'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Field Mapping'),
      '#description' => $this->t('Select which fields of the index should be used to populate the blökkli search result item.'),
      '#description_display' => 'before',
    ];

    $fieldMapping = $form_state->getValue(['search_plugin', 'configuration', $this->getPluginId(), 'field_mapping']) ?? $configuration['field_mapping'] ?? NULL;

    $fields = [
      'entity_type' => [
        $this->t('Entity type'),
        $this->t('The machine name of the entity type (e.g. "node").'),
      ],
      'entity_bundle' => [
        $this->t('Entity bundle'),
        $this->t('The machine name of the entity bundle (e.g. "page").'),
      ],
      'entity_id' => [
        $this->t('Entity ID'),
        $this->t('The ID of the entity.'),
      ],
      'title' => [
        $this->t('Title'),
        $this->t('The title of the entity. Will be displayed as the title of the search result item.'),
      ],
      'text' => [
        $this->t('Text'),
        $this->t('The main descriptive text, such as a lead text or summary. Will be displayed in the search result item.'),
      ],
      'image_url' => [
        $this->t('Image URL'),
        $this->t('The URL to an image for this entity. Should have an aspect ratio of 1/1 and have a size of 300x300.'),
      ],
      'context' => [
        $this->t('Context'),
        $this->t('Additional short text displayed below the title. Can be used to help editors differentiate between entities with the same title.'),
      ],
    ];

    $requiredFields = ['entity_type', 'entity_bundle', 'entity_id', 'title'];

    $indexFields = $selectedIndex->getFields(TRUE);
    $indexFieldOptions = [
      '' => $this->t('Select a field'),
    ];

    foreach ($indexFields as $id => $indexField) {
      $indexFieldLabel = $indexField->getLabel();
      $indexFieldOptions[$id] = "$indexFieldLabel ($id)";
    }

    foreach ($fields as $key => [$label, $description]) {
      $required = in_array($key, $requiredFields);

      $form['field_mapping'][$key] = [
        '#type' => 'select',
        '#title' => $label,
        '#description' => $description,
        '#options' => $indexFieldOptions,
        '#required' => $required,
        '#default_value' => $fieldMapping[$key] ?? NULL,
      ];
    }

    $datasources = $selectedIndex->getDatasourceIds();

    $bundleFilters = $form_state->getValue(['search_plugin', 'configuration', $this->getPluginId(), 'bundle_filters']) ?? $configuration['bundle_filters'] ?? [];

    /** @var \Drupal\paragraphs_blokkli\PbEntityMappingStorage $mappingStorage */
    $mappingStorage = $this->entityTypeManager->getStorage('pb_entity_mapping');

    $form['bundle_filters'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled bundles'),
      '#description' => $this->t('Select which entity types and bundles to include in the search results.'),
    ];

    foreach ($datasources as $id) {
      /** @var \Drupal\search_api\Plugin\search_api\datasource\ContentEntity $plugin */
      $plugin = $selectedIndex->getDatasource($id);
      if (!method_exists($plugin, 'getBundles') || !method_exists($plugin, 'getEntityTypeId')) {
        continue;
      }

      $entityTypeId = $plugin->getEntityTypeId();
      if (!$entityTypeId) {
        continue;
      }
      /** @var \Drupal\Core\Entity\ContentEntityType|null $entityType */
      $entityType = $this->entityTypeManager->getDefinition($entityTypeId, FALSE);
      if (!$entityType) {
        continue;
      }

      $bundles = $plugin->getBundles();
      $entityTypeLabel = $entityType->getLabel();

      $mappedBundles = $mappingStorage->getMappedBundlesForEntityType($entityTypeId);

      foreach ($bundles as $bundleId => $bundleLabel) {
        $key = "$entityTypeId:$bundleId";
        $form['bundle_filters']['#options'][$key] = "$entityTypeLabel - $bundleLabel ($bundleId)";

        $disabled = !in_array($bundleId, $mappedBundles);

        // If the bundle is not mapped, disable the checkbox.
        if ($disabled) {
          $form['bundle_filters'][$key] = [
            '#disabled' => TRUE,
            '#description' => $this->t('This bundle is disabled because no entity mapping exists.'),
          ];
          // Remove the key from the selected bundle filters.
          $index = array_search($key, $bundleFilters);
          if ($index !== FALSE) {
            unset($bundleFilters[$index]);
          }
        }
      }
    }

    $form['bundle_filters']['#default_value'] = $bundleFilters;

    return $form;
  }

  /**
   * Ajax Callback for form reload.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function reloadFields(array &$form, FormStateInterface $form_state) {
    return $form['search_plugin']['configuration'][$this->getPluginId()];
  }

  /**
   * {@inheritdoc}
   */
  public function getPersistableConfiguration(FormStateInterface $formState): array {
    $indexId = $formState->getValue(['search_plugin', 'configuration', $this->getPluginId(), 'index_id']);
    $fieldMapping = $formState->getValue(['search_plugin', 'configuration', $this->getPluginId(), 'field_mapping']);
    $bundleFilters = $formState->getValue(['search_plugin', 'configuration', $this->getPluginId(), 'bundle_filters']);
    $bundleFilters = Checkboxes::getCheckedCheckboxes($bundleFilters);

    return [
      'index_id' => $indexId,
      'field_mapping' => $fieldMapping,
      'bundle_filters' => $bundleFilters,
    ];
  }

}
