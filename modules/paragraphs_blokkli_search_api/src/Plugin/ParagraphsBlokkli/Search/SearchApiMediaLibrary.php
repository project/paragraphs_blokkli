<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli_search_api\Plugin\ParagraphsBlokkli\Search;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\paragraphs_blokkli\Exception\BlokkliConfigurationException;
use Drupal\paragraphs_blokkli_search\Model\MediaLibrarySearchItem;
use Drupal\paragraphs_blokkli_search\Model\MediaLibrarySearchResults;
use Drupal\paragraphs_blokkli_search\ParagraphsBlokkliSearchMediaLibraryInterface;
use Drupal\paragraphs_blokkli_search\ParagraphsBlokkliSearchPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Search plugin that loads media library results via search_api.
 *
 * @ParagraphsBlokkliSearch(
 *   id = "search_api_media_library",
 *   label = @Translation("Search API Media Library"),
 *   description = @Translation("Perform a search for media library items on a search_api index."),
 * )
 */
class SearchApiMediaLibrary extends ParagraphsBlokkliSearchPluginBase implements ContainerFactoryPluginInterface, ParagraphsBlokkliSearchMediaLibraryInterface {

  use StringTranslationTrait;
  use DependencySerializationTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
    );
  }

  /**
   * EntityQueryMediaLibrary constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityBundleInfo
   *   Entity bundle info.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    EntityTypeManagerInterface $entityTypeManager,
    protected EntityTypeBundleInfoInterface $entityBundleInfo,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition, $entityTypeManager);
  }

  /**
   * {@inheritdoc}
   */
  public function getMediaLibraryResults(?string $text = NULL, ?string $bundle = NULL, ?int $page = NULL, ?RefinableCacheableDependencyInterface $metadata = NULL): MediaLibrarySearchResults {
    $indexId = $this->getIndexId();
    $fieldMapping = $this->getFieldMapping();

    if (!$indexId) {
      throw new BlokkliConfigurationException('Missing index_id configuration.');
    }

    if (!$fieldMapping) {
      throw new BlokkliConfigurationException('Missing field_mapping configuration.');
    }

    /** @var \Drupal\search_api\Entity\Index|null $index */
    $index = $this->entityTypeManager->getStorage('search_api_index')->load($indexId);

    if (!$index) {
      throw new BlokkliConfigurationException("The configured index '$indexId' does not exist.");
    }

    $metadata?->addCacheableDependency($index);
    $metadata?->addCacheTags(['search_api_list:' . $index->id()]);

    /** @var \Drupal\paragraphs_blokkli\PbEntityMappingStorage $mappingStorage */
    $mappingStorage = $this->entityTypeManager->getStorage('pb_entity_mapping');
    $mappedBundles = $mappingStorage->getMappedBundlesForEntityType('media');

    $items = [];
    $perPage = (int) $this->getConfiguration()['per_page'];
    $total = 0;
    $facetBundles = [];

    if (!empty($mappedBundles)) {
      $query = $index->query();
      if ($text) {
        $query->keys($text);
      }

      $bundleFieldName = $fieldMapping['entity_bundle'];

      $offset = ($page ?? 0) * $perPage;

      $query->range($offset, $perPage);
      $andCondition = $query->createConditionGroup('AND');
      $andCondition->addCondition($fieldMapping['entity_type'], 'media');

      if ($bundle && in_array($bundle, $mappedBundles)) {
        $andCondition->addCondition($bundleFieldName, $bundle);
      }
      else {
        $andCondition->addCondition($fieldMapping['entity_bundle'], $mappedBundles, 'IN');
      }
      $query->addConditionGroup($andCondition);
      $query->setOption('search_api_facets', [
        $bundleFieldName => [
          'field' => $bundleFieldName,
          'limit' => 100,
          'missing' => FALSE,
          'operator' => 'or',
          'min_count' => 0,
        ],
      ]);

      $queryResult = $query->execute();

      $total = $queryResult->getResultCount() ?? 0;

      foreach ($queryResult->getResultItems() as $item) {
        $items[] = $this->mapSearchItem($item, $fieldMapping);
      }

      $facetBundles = array_filter(array_map(function ($item) {
        $filter = $item['filter'] ?? NULL;
        if (!$filter) {
          return NULL;
        }
        return trim($filter, '"');
      }, $queryResult->getExtraData('search_api_facets')[$bundleFieldName] ?? []));
    }

    return new MediaLibrarySearchResults(
      $perPage,
      $total,
      array_filter($items),
      $this->getFilters($facetBundles),
    );
  }

  /**
   * Build the available filters.
   *
   * @return array
   *   The filters.
   */
  protected function getFilters(array $facetBundles): array {
    /** @var \Drupal\paragraphs_blokkli\PbEntityMappingStorage $mappingStorage */
    $mappingStorage = $this->entityTypeManager->getStorage('pb_entity_mapping');
    $possibleBundles = $mappingStorage->getMappedBundlesForEntityType('media');
    $allBundles = $this->entityBundleInfo->getBundleInfo('media');

    $bundleOptions = [
      'all' => $this->t('All'),
    ];

    foreach ($allBundles as $bundle => $info) {
      if (in_array($bundle, $possibleBundles) && in_array($bundle, $facetBundles)) {
        $bundleOptions[$bundle] = $info['label'];
      }
    }

    return [
      [
        'id' => 'text',
        'type' => 'text',
        'label' => $this->t('Text'),
        'placeholder' => $this->t('Enter search term'),
      ],
      [
        'id' => 'bundle',
        'type' => 'select',
        'label' => $this->t('Media type'),
        'default' => 'all',
        'options' => $bundleOptions,
      ],
    ];
  }

  /**
   * Get the field mapping.
   *
   * @return array{
   *   'entity_type': string|null,
   *   'entity_bundle': string|null,
   *   'entity_id': string|null,
   *   'title': string|null,
   *   'image_url': string|null,
   *   'context': string|null,
   *   }
   */
  protected function getFieldMapping(): array {
    return $this->configuration['field_mapping'] ?? [];
  }

  /**
   * Get the ID of the search_api index.
   *
   * @return string|null
   *   The index ID.
   */
  protected function getIndexId(): string|null {
    return $this->configuration['index_id'] ?? NULL;
  }

  /**
   * Map the search item.
   *
   * @param \Drupal\search_api\Item\Item $item
   *   The item.
   * @param array $fieldMapping
   *   The field mapping.
   *
   * @return \Drupal\paragraphs_blokkli_search\Model\MediaLibrarySearchItem|null
   *   The search result.
   */
  protected function mapSearchItem($item, array $fieldMapping): MediaLibrarySearchItem|null {
    $fields = [
      'entity_bundle',
      'entity_id',
      'title',
      'image_url',
      'context',
    ];

    $fieldValues = [];

    foreach ($fields as $fieldName) {
      $itemFieldName = $fieldMapping[$fieldName] ?? NULL;
      if (!$itemFieldName) {
        continue;
      }

      if ($item->getField($itemFieldName)) {
        $itemFieldValue = $item->getField($itemFieldName)->getValues()[0] ?? NULL;
        if (is_scalar($itemFieldValue)) {
          $fieldValues[$fieldName] = (string) $itemFieldValue;
        }
      }
    }

    $entityBundle = $fieldValues['entity_bundle'] ?? NULL;
    $entityId = $fieldValues['entity_id'] ?? NULL;
    $title = $fieldValues['title'] ?? NULL;
    $imageUrl = $fieldValues['image_url'] ?? NULL;
    $context = $fieldValues['context'] ?? NULL;

    // Return if one of the required values are not present.
    if (!$entityBundle || !$entityId || !$title || !$context) {
      return NULL;
    }

    return new MediaLibrarySearchItem(
      $entityId,
      $entityBundle,
      $title,
      $context,
      $imageUrl,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function canBeUsed(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();
    $indexId = $form_state->getValue(['search_plugin', 'configuration', $this->getPluginId(), 'index_id']) ?? $configuration['index_id'] ?? NULL;

    $indexStorage = $this->entityTypeManager->getStorage('search_api_index');
    /** @var \Drupal\search_api\Entity\Index[] $indices */
    $indices = $indexStorage->loadMultiple();

    $indexOptions = [
      '' => $this->t('Select an index'),
    ];

    foreach ($indices as $id => $index) {
      $indexOptions[$id] = $index->label();
    }

    $form['index_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Index'),
      '#options' => $indexOptions,
      '#default_value' => $indexId,
      '#required' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'reloadFields'],
        'wrapper' => 'edit-search-plugin-configuration-wrapper',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Loading field mapping'),
        ],
      ],
    ];

    if (!$indexId) {
      return $form;
    }

    /** @var \Drupal\search_api\Entity\Index|null $selectedIndex */
    $selectedIndex = $indexStorage->load($indexId);

    if (!$selectedIndex) {
      return $form;
    }

    $form['field_mapping'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Field Mapping'),
      '#description' => $this->t('Select which fields of the index should be used to populate the blökkli search result item.'),
      '#description_display' => 'before',
    ];

    $fieldMapping = $form_state->getValue(['search_plugin', 'configuration', $this->getPluginId(), 'field_mapping']) ?? $configuration['field_mapping'] ?? NULL;

    $fields = [
      'entity_type' => [
        $this->t('Entity type'),
        $this->t('The machine name of the entity type (e.g. "media").'),
      ],
      'entity_bundle' => [
        $this->t('Entity bundle'),
        $this->t('The machine name of the entity bundle (e.g. "image").'),
      ],
      'entity_id' => [
        $this->t('Entity ID'),
        $this->t('The ID of the entity.'),
      ],
      'title' => [
        $this->t('Title'),
        $this->t('The title of the entity. Will be displayed as the title of the search result item.'),
      ],
      'image_url' => [
        $this->t('Image URL'),
        $this->t('The URL to an image for this entity. Should have an aspect ratio of 1/1 and have a size of 300x300.'),
      ],
      'context' => [
        $this->t('Context'),
        $this->t('Additional short text displayed below the title. Can be used to help editors differentiate between entities with the same title.'),
      ],
    ];

    $requiredFields = ['entity_type', 'entity_bundle', 'entity_id', 'title'];

    $indexFields = $selectedIndex->getFields(TRUE);
    $indexFieldOptions = [
      '' => $this->t('Select a field'),
    ];

    foreach ($indexFields as $id => $indexField) {
      $indexFieldLabel = $indexField->getLabel();
      $indexFieldOptions[$id] = "$indexFieldLabel ($id)";
    }

    foreach ($fields as $key => [$label, $description]) {
      $required = in_array($key, $requiredFields);

      $form['field_mapping'][$key] = [
        '#type' => 'select',
        '#title' => $label,
        '#description' => $description,
        '#options' => $indexFieldOptions,
        '#required' => $required,
        '#default_value' => $fieldMapping[$key] ?? NULL,
      ];
    }

    return $form;
  }

  /**
   * Ajax Callback for form reload.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function reloadFields(array &$form, FormStateInterface $form_state) {
    return $form['search_plugin']['configuration'][$this->getPluginId()];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'index_id' => NULL,
      'per_page' => 32,
      'field_mapping' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getPersistableConfiguration(FormStateInterface $formState): array {
    $indexId = $formState->getValue(['search_plugin', 'configuration', $this->getPluginId(), 'index_id']);
    $fieldMapping = $formState->getValue(['search_plugin', 'configuration', $this->getPluginId(), 'field_mapping']);

    return [
      'index_id' => $indexId,
      'field_mapping' => $fieldMapping,
    ];
  }

}
