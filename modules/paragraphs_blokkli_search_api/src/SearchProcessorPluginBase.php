<?php

declare(strict_types=1);

namespace Drupal\paragraphs_blokkli_search_api;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Entity\Plugin\DataType\EntityAdapter;
use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Processor\ProcessorProperty;

/**
 * Base class for custom search api processors.
 */
abstract class SearchProcessorPluginBase extends ProcessorPluginBase {

  /**
   * Get the property data type.
   *
   * @return string
   *   The property data type.
   */
  protected function getPropertyDataType(): string {
    return 'string';
  }

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(?DatasourceInterface $datasource = NULL): array {
    $properties = [];

    if (!$datasource) {
      $pluginDefinition = $this->getPluginDefinition();
      $definition = [
        'label' => $pluginDefinition['label'],
        'description' => $pluginDefinition['description'],
        'type' => $this->getPropertyDataType(),
        'is_list' => FALSE,
        'processor_id' => $this->getPluginId(),
      ];
      $properties[$this->getPluginId()] = new ProcessorProperty($definition);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldValues(ItemInterface $item): void {
    $fields = array_merge(
      $this->getFieldsHelper()->filterForPropertyPath($item->getFields(FALSE), $item->getDatasourceId(), $this->getPluginId()),
      $this->getFieldsHelper()->filterForPropertyPath($item->getFields(FALSE), NULL, $this->getPluginId()),
    );

    /** @var \Drupal\search_api\Item\FieldInterface|null $field */
    $field = reset($fields);

    if (!$field) {
      return;
    }

    $object = $item->getOriginalObject();
    if (!$object instanceof EntityAdapter) {
      return;
    }

    $entity = $object->getEntity();
    if (!$entity instanceof FieldableEntityInterface) {
      return;
    }

    $values = $this->getValuesToIndex($entity) ?? [];

    foreach ($values as $value) {
      $field->addValue($value);
    }
  }

  /**
   * Build the values to index.
   *
   * @param FieldableEntityInterface $entity
   *   The entity.
   *
   * @return array|null
   *   The values.
   */
  abstract protected function getValuesToIndex(FieldableEntityInterface $entity): array|null;

}
