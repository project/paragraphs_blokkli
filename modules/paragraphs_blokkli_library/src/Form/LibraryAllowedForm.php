<?php

namespace Drupal\paragraphs_blokkli_library\Form;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Renders a form to define which paragraph bundles can be promoted to the library.
 */
class LibraryAllowedForm extends FormBase {

  /**
   * Constructs a new LibraryAllowedForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager service.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected EntityFieldManagerInterface $entityFieldManager,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'paragraphs_blokkli_library_allowed';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\paragraphs\Entity\ParagraphsType[] $bundles */
    $bundles = $this->entityTypeManager->getStorage('paragraphs_type')->loadMultiple();

    $form['allowed_bundles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Allow promoting to library'),
      '#description' => $this->t('Select which paragraph bundles can be promoted to a library item.'),
      '#default_value' => [],
      '#description_display' => 'before',
      '#options' => [],
    ];

    // Render checkboxes for every paragraph bundle.
    foreach ($bundles as $id => $bundle) {
      // These two can never be promoted to the library.
      if ($id === 'from_library' || $id === 'blokkli_fragment') {
        continue;
      }

      $allowed = $bundle->getThirdPartySetting(
        'paragraphs_library',
        'allow_library_conversion',
        FALSE
      );

      $label = $bundle->label();
      $form['allowed_bundles']['#options'][$id] = "$label ($id)";
      $form['allowed_bundles'][$id] = [
        '#description' => $bundle->getDescription(),
      ];
      if ($allowed) {
        $form['allowed_bundles']['#default_value'][] = $id;
      }
    }

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Save'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $bundles = $form_state->getValue('allowed_bundles');

    foreach ($bundles as $id => $allowed) {
      /** @var \Drupal\paragraphs\Entity\ParagraphsType $bundle */
      $bundle = $this->entityTypeManager->getStorage('paragraphs_type')->load($id);
      $oldValue = $bundle->getThirdPartySetting('paragraphs_library', 'allow_library_conversion', FALSE);
      $newValue = (bool) $allowed;
      if ($oldValue !== $newValue) {
        $bundle->setThirdPartySetting('paragraphs_library', 'allow_library_conversion', $newValue);
        $bundle->save();
      }
    }
  }

}
