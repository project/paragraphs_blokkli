<?php

namespace Drupal\paragraphs_blokkli_library\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Route subscriber.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('entity.paragraphs_library_item.canonical')) {
      // The default path is prefixed with /admin, but in order for the library
      // item to be editable with blökkli it needs a non-admin prefix.
      $route->setPath('/blokkli/library-item/{paragraphs_library_item}');
    }
  }

}
