<?php

namespace Drupal\paragraphs_blokkli_conversion;

use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;

/**
 * Interface for paragraph_conversion plugins.
 */
interface ParagraphConversionInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

  /**
   * Returns the source bundle ID.
   *
   * @return string
   *   The source bundle ID.
   */
  public function getSourceBundle();

  /**
   * Returns the target bundle ID.
   *
   * @return string
   *   The target bundle ID.
   */
  public function getTargetBundle();

  /**
   * Convert the given paragraph to entity values for the target bundle.
   *
   * The method is called for each translation of the paragraph.
   * The method must not produce any side effects, as it can be called several
   * times for the same entity. Saving entities here would create them each
   * time.
   *
   * In addition, if the paragraph contains nested paragraphs, the
   * getTargetFieldForParagraph() method should return the target field name.
   * Values for entity reference fields containing paragraphs should not be
   * returned here.
   *
   * @param ParagraphInterface $paragraph
   *   The paragraph that should be converted.
   * @param ParagraphMutationContextInterface $context
   *   The paragraph mutation context.
   *
   * @return array|null
   *   The values for the target bundle paragraph or NULL if the conversion is not possible.
   */
  public function convert(ParagraphInterface $paragraph, ParagraphMutationContextInterface $context): ?array;

  /**
   * Get the target field name for a nested paragraph.
   *
   * If the paragraph that is being converted has nested paragraphs, this
   * method should return the name of the field in which to place this
   * nested paragraph.
   *
   * @param ParagraphInterface $nestedParagraph
   *   The nested paragraph that needs to be moved.
   *
   * @return string|null
   *   The name of the field where the paragraph should be placed.
   */
  public function getTargetFieldForParagraph(ParagraphInterface $nestedParagraph): string|null;

}
