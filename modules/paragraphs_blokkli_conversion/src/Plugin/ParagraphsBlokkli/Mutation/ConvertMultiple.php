<?php

namespace Drupal\paragraphs_blokkli_conversion\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli_conversion\Plugin\ConvertMutationBase;

/**
 * Convert multiple existing paragraphs to another type.
 *
 * @ParagraphMutation(
 *   id = "convert_multiple",
 *   label = @Translation("Convert multiple paragraphs to another type"),
 *   description = @Translation("Converts existing paragraphs to another type."),
 *   arguments = {
 *     "uuids" = @ContextDefinition("string",
 *       label = @Translation("The UUIDs of the paragraphs being converted."),
 *       multiple = TRUE
 *     ),
 *     "targetBundle" = @ContextDefinition("string",
 *       label = @Translation("The bundle to which the paragraph should be converted.")
 *     ),
 *   }
 * )
 */
class ConvertMultiple extends ConvertMutationBase {

  /**
   * {@inheritdoc}
   */
  public function getAffectedParagraphUuid(): string|null {
    $uuid = $this->configuration['uuids'][0] ?? NULL;
    if ($uuid) {
      return $this->getUuidForNewEntity($uuid);
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * Executes the paragraph conversion.
   *
   * @param ParagraphMutationContextInterface $context
   *   The paragraph mutation context.
   * @param string[] $uuids
   *   The UUIDs to convert.
   * @param string $targetBundle
   *   The target bundle.
   */
  public function execute(ParagraphMutationContextInterface $context, array $uuids, string $targetBundle): void {
    foreach ($uuids as $uuid) {
      $this->convertParagraph($context, $uuid, $targetBundle);
    }
  }

}
