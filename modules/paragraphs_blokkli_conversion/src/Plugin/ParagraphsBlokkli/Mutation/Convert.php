<?php

namespace Drupal\paragraphs_blokkli_conversion\Plugin\ParagraphsBlokkli\Mutation;

use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli_conversion\Plugin\ConvertMutationBase;

/**
 * Convert an existing paragraph to another type.
 *
 * @ParagraphMutation(
 *   id = "convert",
 *   label = @Translation("Convert to another type"),
 *   description = @Translation("Converts an existing paragraph to another type."),
 *   arguments = {
 *     "uuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the paragraph being converted.")
 *     ),
 *     "targetBundle" = @ContextDefinition("string",
 *       label = @Translation("The bundle to which the paragraph should be converted.")
 *     ),
 *   }
 * )
 */
class Convert extends ConvertMutationBase {

  /**
   * {@inheritdoc}
   */
  public function getAffectedParagraphUuid(): string|null {
    $uuid = $this->configuration['uuid'] ?? NULL;
    if ($uuid) {
      return $this->getUuidForNewEntity($uuid);
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * Executes the paragraph conversion.
   *
   * @param ParagraphMutationContextInterface $context
   *   The paragraph mutation context.
   * @param string $uuid
   *   The UUID of the paragraph to be converted.
   * @param string $targetBundle
   *   The target bundle to convert the paragraph to.
   */
  public function execute(ParagraphMutationContextInterface $context, string $uuid, string $targetBundle): void {
    $this->convertParagraph($context, $uuid, $targetBundle);
  }

}
