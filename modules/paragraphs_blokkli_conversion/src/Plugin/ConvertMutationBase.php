<?php

namespace Drupal\paragraphs_blokkli_conversion\Plugin;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList;
use Drupal\paragraphs_blokkli\Exception\MutationBadDataException;
use Drupal\paragraphs_blokkli\Exception\MutationException;
use Drupal\paragraphs_blokkli\Exception\MutationMissingEntityException;
use Drupal\paragraphs_blokkli\ParagraphMutationContextInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationPluginBase;
use Drupal\paragraphs_blokkli\ParagraphProxy;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliHelper;
use Drupal\paragraphs_blokkli_conversion\ParagraphConversionPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for conversion plugins.
 */
class ConvertMutationBase extends ParagraphMutationPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The paragraph conversion plugin manager.
   */
  protected ParagraphConversionPluginManager $pluginManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('uuid'),
      $container->get('paragraphs_blokkli.helper'),
      $container->get('plugin.manager.paragraph_conversion')
    );
  }

  /**
   * Convert constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param UuidInterface $uuidHelper
   *   The UUID helper.
   * @param ParagraphsBlokkliHelper $paragraphsBlokkliHelper
   *   The paragraphs blokkli helper.
   * @param ParagraphConversionPluginManager $pluginManager
   *   The paragraph_conversion plugin manager.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    EntityTypeManagerInterface $entityTypeManager,
    UuidInterface $uuidHelper,
    ParagraphsBlokkliHelper $paragraphsBlokkliHelper,
    ParagraphConversionPluginManager $pluginManager,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition, $entityTypeManager, $uuidHelper, $paragraphsBlokkliHelper);
    $this->pluginManager = $pluginManager;
  }

  /**
   * Convert the paragraph.
   *
   * @param ParagraphMutationContextInterface $context
   *   The paragraph mutation context.
   * @param string $uuid
   *   The UUID to convert.
   * @param string $targetBundle
   *   The target bundle.
   */
  protected function convertParagraph(ParagraphMutationContextInterface $context, string $uuid, string $targetBundle) {
    $proxy = $context->getProxy($uuid);
    if (!$proxy) {
      throw new MutationMissingEntityException('paragraph', NULL, $uuid);
    }

    if ($context->isDeleted($uuid)) {
      return;
    }

    $paragraph = $proxy->getParagraph();

    if (!$paragraph) {
      return;
    }
    $sourceBundle = $paragraph->bundle();
    $plugin = $this->pluginManager->getPluginForBundleCombination($sourceBundle, $targetBundle);

    if (!$plugin) {
      throw new MutationBadDataException("No conversion plugin exists to convert paragraph from '$sourceBundle' to '$targetBundle'.");
    }

    $values = $plugin->convert($paragraph, $context);

    if ($values === NULL) {
      throw new MutationException('Failed to convert paragraph.');
    }

    $newUuid = $this->getUuidForNewEntity($uuid);
    /** @var \Drupal\paragraphs\Entity\Paragraph $newParagraph */
    $newParagraph = $this->entityTypeManager->getStorage('paragraph')->create(array_merge([
      'type' => $targetBundle,
      'uuid' => $newUuid,
    ], $values));

    // Loop over proxies that have the paragraph we convert as their host.
    $nestedParagraphProxies = $context->getProxiesForHost('paragraph', $paragraph->uuid());
    foreach ($nestedParagraphProxies as $nestedProxy) {
      $nestedParagraph = $nestedProxy->getParagraph();
      if ($nestedParagraph) {
        // Ask the plugin in which field the paragraph should be moved
        // on the converted paragraph.
        $targetField = $plugin->getTargetFieldForParagraph($nestedParagraph);
        if ($targetField) {
          if ($newParagraph->hasField($targetField)) {
            $field = $newParagraph->get($targetField);
            if ($field instanceof EntityReferenceRevisionsFieldItemList) {
              $definition = $field->getFieldDefinition();
              $settings = $definition->getSettings();
              $targetBundles = array_values($settings['handler_settings']['target_bundles'] ?? []);
              if (in_array($nestedParagraph->bundle(), $targetBundles)) {
                $nestedProxy->setHostUuid($newUuid);
                $nestedProxy->setHostFieldName($targetField);
                continue;
              }
            }
          }
        }
      }
      $context->markAsDeleted($nestedProxy->uuid());
    }

    // Handle translation values.
    $languages = $paragraph->getTranslationLanguages(FALSE);
    foreach ($languages as $language) {
      $langcode = $language->getId();

      if ($paragraph->hasTranslation($langcode)) {
        $translatedParagraph = $paragraph->getTranslation($langcode);
        $translatedValues = $plugin->convert($translatedParagraph, $context);
        if (!empty($translatedValues)) {
          $newParagraph->addTranslation($langcode, $translatedValues);
        }
      }
    }

    // Create a new proxy for our converted paragraph.
    $newProxy = new ParagraphProxy(
      $newParagraph,
      $proxy->getHostEntityType(),
      $proxy->getHostUuid(),
      $proxy->getHostFieldName()
    );

    // Add the proxy after the old paragraph.
    $context->addProxy($newProxy, $proxy->uuid());

    // Finally, set the existing paragraph to be deleted.
    $context->markAsDeleted($proxy->uuid());
  }

}
