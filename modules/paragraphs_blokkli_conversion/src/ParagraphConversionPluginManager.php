<?php

namespace Drupal\paragraphs_blokkli_conversion;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * ParagraphConversion plugin manager.
 */
class ParagraphConversionPluginManager extends DefaultPluginManager {

  /**
   * Constructs ParagraphConversionPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/ParagraphsBlokkli/Conversion',
      $namespaces,
      $module_handler,
      'Drupal\paragraphs_blokkli_conversion\ParagraphConversionInterface',
      'Drupal\paragraphs_blokkli_conversion\Annotation\ParagraphConversion'
    );
    $this->alterInfo('paragraph_conversion_info');
    $this->setCacheBackend($cache_backend, 'paragraph_conversion_plugins');
  }

  /**
   * Find a plugin for the given source and target bundles.
   *
   * @param string $sourceBundle
   *   The source bundle ID.
   * @param string $targetBundle
   *   The target bundle ID.
   *
   * @return ParagraphConversionInterface|null
   *   The conversion plugin.
   */
  public function getPluginForBundleCombination(string $sourceBundle, string $targetBundle): ParagraphConversionInterface|null {
    $definitions = $this->getDefinitions();
    foreach ($definitions as $pluginId => $definition) {
      if (
        $definition['source_bundle'] === $sourceBundle &&
        $definition['target_bundle'] === $targetBundle
      ) {
        return $this->createInstance($pluginId);
      }
    }
    return NULL;
  }

}
