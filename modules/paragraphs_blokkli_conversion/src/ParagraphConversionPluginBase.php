<?php

namespace Drupal\paragraphs_blokkli_conversion;

use Drupal\Component\Plugin\PluginBase;
use Drupal\paragraphs\ParagraphInterface;

/**
 * Base class for paragraph_conversion plugins.
 */
abstract class ParagraphConversionPluginBase extends PluginBase implements ParagraphConversionInterface {

  /**
   * {@inheritdoc}
   */
  public function label() {
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceBundle(): string {
    return $this->pluginDefinition['source_bundle'];
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetBundle(): string {
    return $this->pluginDefinition['target_bundle'];
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetFieldForParagraph(ParagraphInterface $nestedParagraph): ?string {
    return NULL;
  }

}
