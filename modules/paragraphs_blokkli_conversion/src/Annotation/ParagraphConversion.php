<?php

namespace Drupal\paragraphs_blokkli_conversion\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines paragraph_conversion annotation object.
 *
 * @Annotation
 */
class ParagraphConversion extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The source paragraph bundle.
   *
   * @var string
   */
  public $source_bundle;

  /**
   * The target paragraph bundle.
   *
   * @var string
   */
  public $target_bundle;

}
