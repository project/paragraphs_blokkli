<?php

namespace Drupal\paragraphs_blokkli_comment\Plugin\GraphQL\DataProducer;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\comment\CommentStorageInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliEditStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Add a comment.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_add_comment",
 *   name = @Translation("Paragraphs Blokkli: Add Comment"),
 *   description = @Translation("Adds a new comment."),
 *   produces = @ContextDefinition("entity:paragraphs_blokkli_edit_state",
 *     label = @Translation("The edit state.")
 *   ),
 *   consumes = {
 *     "state" = @ContextDefinition("entity:paragraphs_blokkli_edit_state",
 *       label = @Translation("The edit state."),
 *     ),
 *     "uuids" = @ContextDefinition("string",
 *       label = @Translation("The UUIDs of the paragraphs."),
 *       multiple = TRUE,
 *     ),
 *     "body" = @ContextDefinition("string",
 *       label = @Translation("The comment body."),
 *     ),
 *   }
 * )
 */
class AddComment extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The comment storage.
   */
  protected CommentStorageInterface $commentStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition,
  ) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('entity_type.manager'),
    );
  }

  /**
   * The constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->commentStorage = $entityTypeManager->getStorage('comment');
  }

  /**
   * The resolver.
   *
   * @param ParagraphsBlokkliEditStateInterface $state
   *   The edit state.
   * @param string[] $uuids
   *   The UUIDs the comment references.
   * @param string $body
   *   The comment body.
   */
  public function resolve(ParagraphsBlokkliEditStateInterface $state, array $uuids, string $body) {
    $entity = $state->getHostEntity();
    if (!$entity) {
      return;
    }
    $this->commentStorage->create([
      'type' => 'blokkli_node',
      'entity_id' => $entity->id(),
      'field_target_uuids' => $uuids,
      'field_name' => 'field_blokkli_comments',
      'entity_type' => 'node',
      'comment_body' => $body,
    ])->save();
    return $state;
  }

}
