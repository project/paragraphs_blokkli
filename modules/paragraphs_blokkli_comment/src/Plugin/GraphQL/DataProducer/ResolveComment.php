<?php

namespace Drupal\paragraphs_blokkli_comment\Plugin\GraphQL\DataProducer;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\comment\CommentStorageInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliEditStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Add a comment.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_resolve_comment",
 *   name = @Translation("Paragraphs Blokkli: Resolve Comment"),
 *   description = @Translation("Resolves a comment."),
 *   produces = @ContextDefinition("entity:paragraphs_blokkli_edit_state",
 *     label = @Translation("The edit state.")
 *   ),
 *   consumes = {
 *     "state" = @ContextDefinition("entity:paragraphs_blokkli_edit_state",
 *       label = @Translation("The edit state."),
 *     ),
 *     "uuid" = @ContextDefinition("string",
 *       label = @Translation("The UUID of the comment."),
 *     ),
 *   }
 * )
 */
class ResolveComment extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The comment storage.
   */
  protected CommentStorageInterface $commentStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition,
  ) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('entity_type.manager'),
    );
  }

  /**
   * The constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->commentStorage = $entityTypeManager->getStorage('comment');
  }

  /**
   * Resolves a comment by setting the "field_resolved" flag to TRUE.
   *
   * @param \Drupal\paragraphs_blokkli\ParagraphsBlokkliEditStateInterface $state
   *   The edit state.
   * @param string $uuid
   *   The UUID of the comment to resolve.
   *
   * @return \Drupal\paragraphs_blokkli\ParagraphsBlokkliEditStateInterface
   *   The updated edit state after resolving the comment.
   */
  public function resolve(ParagraphsBlokkliEditStateInterface $state, string $uuid) {
    /** @var \Drupal\comment\Entity\Comment[] */
    $comments = $this->commentStorage->loadByProperties(['uuid' => $uuid]);
    $comment = array_values($comments)[0] ?? NULL;
    if ($comment) {
      $comment->set('field_resolved', TRUE);
      $comment->save();
    }
    return $state;
  }

}
