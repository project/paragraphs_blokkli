<?php

namespace Drupal\paragraphs_blokkli_comment\Plugin\GraphQL\DataProducer;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\comment\CommentStorageInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\paragraphs_blokkli\ParagraphsBlokkliEditStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Get all comments.
 *
 * @DataProducer(
 *   id = "paragraphs_blokkli_get_comments",
 *   name = @Translation("Paragraphs Blokkli: Get Comments"),
 *   description = @Translation("Get all comments."),
 *   produces = @ContextDefinition("entity:comment",
 *     label = @Translation("The comment entity."),
 *     multiple = TRUE,
 *   ),
 *   consumes = {
 *     "state" = @ContextDefinition("entity:paragraphs_blokkli_edit_state",
 *       label = @Translation("The edit state."),
 *     ),
 *   }
 * )
 */
class GetComments extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The comment storage.
   */
  protected CommentStorageInterface $commentStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition,
  ) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('entity_type.manager'),
    );
  }

  /**
   * The constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->commentStorage = $entityTypeManager->getStorage('comment');
  }

  /**
   * Resolves the comments associated with the host entity.
   *
   * @param ParagraphsBlokkliEditStateInterface $state
   *   The edit state representing the host entity.
   *
   * @return \Drupal\comment\CommentInterface[]|null
   *   An array of comment entities associated with the host entity, or NULL if no host entity is found.
   */
  public function resolve(ParagraphsBlokkliEditStateInterface $state) {
    $entity = $state->getHostEntity();
    if (!$entity) {
      return NULL;
    }
    return $this->commentStorage->loadByProperties([
      'entity_id' => $entity->id(),
      'entity_type' => $entity->getEntityTypeId(),
    ]);
  }

}
