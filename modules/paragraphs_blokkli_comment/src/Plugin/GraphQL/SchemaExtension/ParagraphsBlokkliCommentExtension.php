<?php

namespace Drupal\paragraphs_blokkli_comment\Plugin\GraphQL\SchemaExtension;

use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;
use Drupal\graphql_core_schema\CoreSchemaExtensionInterface;

/**
 * A schema extension for the topic taxonomy.
 *
 * @SchemaExtension(
 *   id = "paragraphs_blokkli_comment",
 *   name = "Paragraphs Blokkli Comment",
 *   description = "Adds support for comments on Paragraphs Blokkli.",
 *   schema = "core_composable"
 * )
 */
class ParagraphsBlokkliCommentExtension extends SdlSchemaExtensionPluginBase implements CoreSchemaExtensionInterface {

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeDependencies() {
    return ['comment'];
  }

  /**
   * {@inheritdoc}
   */
  public function getExtensionDependencies() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry): void {
    $builder = new ResolverBuilder();

    $registry->addFieldResolver(
      'ParagraphsEditMutationState',
      'addComment',
      $builder->compose(
        $builder->produce('paragraphs_blokkli_add_comment')
          ->map('state', $builder->fromParent())
          ->map('uuids', $builder->fromArgument('paragraphUuids'))
          ->map('body', $builder->fromArgument('body')),
        $builder->produce('paragraphs_blokkli_get_comments')
          ->map('state', $builder->fromParent())
      )
    );

    $registry->addFieldResolver(
      'ParagraphsEditMutationState',
      'resolveComment',
      $builder->compose(
        $builder->produce('paragraphs_blokkli_resolve_comment')
          ->map('state', $builder->fromParent())
          ->map('uuid', $builder->fromArgument('uuid')),
        $builder->produce('paragraphs_blokkli_get_comments')
          ->map('state', $builder->fromParent())
      )
    );

    $registry->addFieldResolver(
      'ParagraphsBlokkliEditState',
      'comments',
      $builder->produce('paragraphs_blokkli_get_comments')
        ->map('state', $builder->fromParent())
    );
  }

}
