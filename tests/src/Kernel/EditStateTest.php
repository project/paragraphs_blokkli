<?php

namespace Drupal\Tests\paragraphs_blokkli\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\paragraphs\FunctionalJavascript\ParagraphsTestBaseTrait;
use Drupal\node\Entity\Node;

/**
 * Test the edit state.
 *
 * @group paragraphs_blokkli
 */
class EditStateTest extends KernelTestBase {

  use ParagraphsTestBaseTrait;
  use ParagraphsBlokkliTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'paragraphs_blokkli',
    'paragraphs',
    'node',
    'user',
    'system',
    'language',
    'field',
    'entity_reference_revisions',
    'file',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('paragraph');
    $this->installEntitySchema('paragraphs_blokkli_edit_state');
    $this->installSchema('system', ['sequences']);
    $this->installSchema('node', ['node_access']);
    \Drupal::moduleHandler()->loadInclude('paragraphs', 'install');

    $config = $this->config('paragraphs_blokkli.settings');
    $data['node'] = [
      'paragraphed_test',
    ];
    $config->set('enabled_types', $data);
    $config->save();
  }

  /**
   * Test the undo feature.
   */
  public function testUndo(): void {
    $this->addParagraphedContentType('paragraphed_test');
    $paragraph_type = 'text_paragraph';
    $this->addParagraphsType($paragraph_type);
    $this->addFieldtoParagraphType($paragraph_type, 'field_text', 'string');

    $node = Node::create([
      'title' => 'Test Node',
      'type' => 'paragraphed_test',
      'field_paragraphs' => [],
    ]);
    $node->save();

    $state = $this->getEditState($node);
    $helper = $this->getParagraphsBlokkliTestHelper($state);
    // Add four paragraphs.
    $helper->addTextParagraph();
    $helper->addTextParagraph();
    $helper->addTextParagraph();
    $helper->addTextParagraph();

    // Undo twice.
    $state->undo();
    $state->undo();

    // Publish.
    $this->paragraphsBlokkliPublish($state, TRUE);

    $node = Node::load($node->id());
    $paragraphs = $node->get('field_paragraphs')->referencedEntities();
    // Should only have created 2 paragraphs.
    $this->assertCount(2, $paragraphs);
  }

  /**
   * Test the redo feature.
   */
  public function testRedo(): void {
    $this->addParagraphedContentType('paragraphed_test');
    $paragraph_type = 'text_paragraph';
    $this->addParagraphsType($paragraph_type);
    $this->addFieldtoParagraphType($paragraph_type, 'field_text', 'string');

    $node = Node::create([
      'title' => 'Test Node',
      'type' => 'paragraphed_test',
      'field_paragraphs' => [],
    ]);
    $node->save();

    $state = $this->getEditState($node);
    $helper = $this->getParagraphsBlokkliTestHelper($state);
    // Add four paragraphs.
    $helper->addTextParagraph();
    $helper->addTextParagraph();
    $helper->addTextParagraph();
    $helper->addTextParagraph();

    // Undo twice.
    $state->undo();
    $state->undo();

    // Redo once.
    $state->redo();

    // Publish.
    $this->paragraphsBlokkliPublish($state, TRUE);

    $node = Node::load($node->id());
    $paragraphs = $node->get('field_paragraphs')->referencedEntities();
    // Should only have created 3 paragraphs.
    $this->assertCount(3, $paragraphs);
  }

  /**
   * Test that an undo rewrites the history.
   */
  public function testRewriteHistory(): void {
    $this->addParagraphedContentType('paragraphed_test');
    $paragraph_type = 'text_paragraph';
    $this->addParagraphsType($paragraph_type);
    $this->addFieldtoParagraphType($paragraph_type, 'field_text', 'string');

    $node = Node::create([
      'title' => 'Test Node',
      'type' => 'paragraphed_test',
      'field_paragraphs' => [],
    ]);
    $node->save();

    $state = $this->getEditState($node);
    $helper = $this->getParagraphsBlokkliTestHelper($state);
    // Add four paragraphs.
    $helper->addTextParagraph('1');
    $helper->addTextParagraph('2');
    $helper->addTextParagraph('3');
    $helper->addTextParagraph('4');

    // Undo three times.
    $state->undo();
    $state->undo();
    $state->undo();

    $helper->addTextParagraph('5');

    // Publish.
    $this->paragraphsBlokkliPublish($state, TRUE);

    $node = Node::load($node->id());
    $paragraphs = $node->get('field_paragraphs')->referencedEntities();
    // Should only have created 3 paragraphs.
    $this->assertCount(2, $paragraphs);
    $this->assertEquals('5', $paragraphs[0]->get('field_text')->value);
    $this->assertEquals('1', $paragraphs[1]->get('field_text')->value);
  }

  /**
   * Test the setHistoryIndex method.
   */
  public function testSetHistoryIndex(): void {
    $this->addParagraphedContentType('paragraphed_test');
    $paragraph_type = 'text_paragraph';
    $this->addParagraphsType($paragraph_type);
    $this->addFieldtoParagraphType($paragraph_type, 'field_text', 'string');

    $node = Node::create([
      'title' => 'Test Node',
      'type' => 'paragraphed_test',
      'field_paragraphs' => [],
    ]);
    $node->save();

    $state = $this->getEditState($node);
    $helper = $this->getParagraphsBlokkliTestHelper($state);
    $helper->addTextParagraph();
    $helper->addTextParagraph();
    $helper->addTextParagraph();
    $helper->addTextParagraph();
    $state->setHistoryIndex(0);

    // Publish.
    $this->paragraphsBlokkliPublish($state, TRUE);

    $node = Node::load($node->id());
    $paragraphs = $node->get('field_paragraphs')->referencedEntities();
    // Should only have created 1 paragraph.
    $this->assertCount(1, $paragraphs);

    $state = $this->getEditState($node);
    $helper = $this->getParagraphsBlokkliTestHelper($state);
    $helper->addTextParagraph();
    $helper->addTextParagraph();
    $helper->addTextParagraph();
    $helper->addTextParagraph();

    $state->setHistoryIndex(-2);
    $this->assertEquals($state->getCurrentIndex(), 3);

    $state->setHistoryIndex(100);
    $this->assertEquals($state->getCurrentIndex(), 3);

    $state->setHistoryIndex(1);
    $this->assertEquals($state->getCurrentIndex(), 1);
    $state->setHistoryIndex(0);
    $this->assertEquals($state->getCurrentIndex(), 0);
    $state->setHistoryIndex(-1);
    $this->assertEquals($state->getCurrentIndex(), -1);

    $state->setHistoryIndex(3);
    $this->assertEquals($state->getCurrentIndex(), 3);
    $state->setHistoryIndex(4);
    $this->assertEquals($state->getCurrentIndex(), 3);
  }

}
