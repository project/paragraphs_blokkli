<?php

namespace Drupal\Tests\paragraphs_blokkli\Kernel;

use Drupal\paragraphs_blokkli\EditStateInterface;
use Drupal\paragraphs_blokkli\ParagraphMutationItemInterface;
use Drupal\paragraphs_blokkli\Plugin\Field\FieldType\ParagraphsBlokkliMutationItem;

/**
 * Test the custom user tokens.
 */
class ParagraphsBlokkliTestHelper {

  public function __construct(
    protected EditStateInterface $editState,
  ) {}

  /**
   * Create a new mutation and add it to the edit state.
   *
   * @param string $mutationName
   * @param array $values
   *
   * @return \Drupal\paragraphs_blokkli\ParagraphMutationItemInterface
   *   The created mutation.
   */
  protected function createAndAddMutation(string $mutationName, array $values = []): ParagraphMutationItemInterface {
    /** @var \Drupal\paragraphs_blokkli\ParagraphMutationPluginManager $manager */
    $manager = \Drupal::service('plugin.manager.paragraph_mutation');
    $mutation = $manager->createInstance($mutationName, $values);
    $this->editState->addMutation($mutation);
    $this->editState->save();
    $mutations = $this->editState->getMutations();
    return $mutations[count($mutations) - 1];
  }

  /**
   * Add a text paragraph to the edit state.
   *
   * @param string|null $text
   *   The text value for the paragraph. Default is an empty string.
   * @param string|null $afterUuid
   *   The UUID of the paragraph after which the new paragraph should be added.
   * @param string|null $hostType
   *   The entity type of the host entity. Default is NULL.
   * @param string|null $hostUuid
   *   The UUID of the host entity. Default is NULL.
   * @param string|null $hostFieldName
   *   The field name of the host entity. Default is NULL.
   *
   * @return string
   *   The UUID of the affected paragraph.
   */
  public function addTextParagraph(string|null $text = '', string|null $afterUuid = NULL, string|null $hostType = NULL, string|null $hostUuid = NULL, string|null $hostFieldName = NULL): string {
    $host = $this->editState->getHostEntity();
    $mutation = $this->createAndAddMutation('add', [
      'type' => 'text_paragraph',
      'hostType' => $hostType ?? $host->getEntityTypeId(),
      'hostUuid' => $hostUuid ?? $host->uuid(),
      'hostFieldName' => $hostFieldName ?? 'field_paragraphs',
      'values' => [
        'field_text' => $text ?? 'Initial text value.',
      ],
      'afterUuid' => $afterUuid,
    ]);
    return $mutation->getMutationPlugin()->getAffectedParagraphUuid();
  }

  /**
   * Duplicate a paragraph and return the UUID of the duplicated paragraph.
   *
   * @param string $uuid
   *   The UUID of the paragraph to be duplicated.
   *
   * @return string
   *   The UUID of the duplicated paragraph.
   */
  public function duplicateParagraph(string $uuid): string {
    $mutation = $this->createAndAddMutation('duplicate', [
      'uuid' => $uuid,
    ]);
    if ($mutation instanceof ParagraphsBlokkliMutationItem) {
      return $mutation->getMutationPlugin()->getUuidForNewEntity($uuid);
    }
    return $mutation->getMutationPlugin()->getAffectedParagraphUuid();
  }

  public function remove(string $uuid): static {
    $this->createAndAddMutation('remove', [
      'uuid' => $uuid,
    ]);
    return $this;
  }

  public function removeMultiple(array $uuids): static {
    $this->createAndAddMutation('remove_multiple', [
      'uuids' => $uuids,
    ]);
    return $this;
  }

  public function editParagraph(string $uuid, array $values): static {
    $this->createAndAddMutation('edit', [
      'uuid' => $uuid,
      'langcode' => 'en',
      'values' => $values,
    ]);
    return $this;
  }

  public function updateBehaviorSetting(string $uuid, string $pluginId, string $key, string $value): static {
    $this->createAndAddMutation('update_behavior_setting', [
      'uuid' => $uuid,
      'pluginId' => $pluginId,
      'key' => $key,
      'value' => $value,
    ]);
    return $this;
  }

  public function bulkUpdateBehaviorSettings(array $items): static {
    $this->createAndAddMutation('bulk_update_behavior_settings', [
      'items' => $items,
    ]);
    return $this;
  }

  public function moveParagraph(string $uuid, string|null $afterUuid = NULL, string|null $hostType = NULL, string|null $hostUuid = NULL, string|null $hostFieldName = NULL): static {
    $host = $this->editState->getHostEntity();
    $this->createAndAddMutation('move', [
      'uuid' => $uuid,
      'hostType' => $hostType ?? $host->getEntityTypeId(),
      'hostUuid' => $hostUuid ?? $host->uuid(),
      'hostFieldName' => $hostFieldName ?? 'field_paragraphs',
      'afterUuid' => $afterUuid,
    ]);
    return $this;
  }

  public function moveMultiple(array $uuids, string|null $afterUuid = NULL, string|null $hostType = NULL, string|null $hostUuid = NULL, string|null $hostFieldName = NULL): static {
    $host = $this->editState->getHostEntity();
    $this->createAndAddMutation('move_multiple', [
      'uuids' => $uuids,
      'hostType' => $hostType ?? $host->getEntityTypeId(),
      'hostUuid' => $hostUuid ?? $host->uuid(),
      'hostFieldName' => $hostFieldName ?? 'field_paragraphs',
      'afterUuid' => $afterUuid,
    ]);
    return $this;
  }

}
