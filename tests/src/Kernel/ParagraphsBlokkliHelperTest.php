<?php

namespace Drupal\Tests\paragraphs_blokkli\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\paragraphs\FunctionalJavascript\ParagraphsTestBaseTrait;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs_blokkli\ParagraphProxyInterface;

/**
 * Test the helper.
 *
 * @group paragraphs_blokkli
 */
class ParagraphsBlokkliHelperTest extends KernelTestBase {

  use ParagraphsTestBaseTrait;
  use ParagraphsBlokkliTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'paragraphs_blokkli',
    'paragraphs',
    'node',
    'user',
    'system',
    'language',
    'field',
    'entity_reference_revisions',
    'file',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('paragraph');
    $this->installEntitySchema('paragraphs_blokkli_edit_state');
    $this->installSchema('system', ['sequences']);
    $this->installSchema('node', ['node_access']);
    \Drupal::moduleHandler()->loadInclude('paragraphs', 'install');

    $config = $this->config('paragraphs_blokkli.settings');
    $data['node'] = [
      'paragraphed_test',
    ];
    $config->set('enabled_types', $data);
    $config->save();
  }

  /**
   * Test the buildProxies method.
   */
  public function testBuildProxies(): void {
    $this->addParagraphedContentType('paragraphed_test');
    $paragraph_type = 'text_paragraph';
    $nested_type = 'container';
    $this->addParagraphsType($paragraph_type);
    $this->addParagraphsType($nested_type);
    $this->addFieldtoParagraphType($paragraph_type, 'field_text', 'string');
    $this->addParagraphsField($nested_type, 'field_container_paragraphs', 'paragraph');

    $containers = [];

    for ($i = 0; $i < 5; $i++) {
      $paragraphs = [];
      for ($j = 0; $j < 5; $j++) {
        $paragraphs[] = Paragraph::create([
          'type' => $paragraph_type,
          'field_text' => $i . ' ' . $j,
        ]);
      }
      $containers[] = Paragraph::create([
        'type' => $nested_type,
        'field_container_paragraphs' => $paragraphs,
      ]);
    }

    $node = Node::create([
      'title' => 'Test Node',
      'type' => 'paragraphed_test',
      'field_paragraphs' => $containers,
    ]);
    $node->save();
    /** @var \Drupal\paragraphs_blokkli\ParagraphsBlokkliHelper $helper */
    $helper = $this->container->get('paragraphs_blokkli.helper');

    $proxies = $helper->buildProxies($node);
    // 5 containers + 5 containers * 5 text = 30.
    $this->assertCount(30, $proxies);
  }

  /**
   * Test the buildProxies method with multiple fields.
   *
   * @group current
   */
  public function testBuildProxiesWithMultipleFields(): void {
    $this->addParagraphedContentType('paragraphed_test');
    $paragraph_type = 'text_paragraph';
    $nested_type = 'container';
    $this->addParagraphsType($paragraph_type);
    $this->addParagraphsType($nested_type);
    $this->addFieldtoParagraphType($paragraph_type, 'field_text', 'string');
    $this->addParagraphsField($nested_type, 'field_container_paragraphs', 'paragraph');
    $this->addParagraphsField('paragraphed_test', 'field_footer_paragraphs', 'node');

    $containerChild = Paragraph::create([
      'type' => 'text_paragraph',
      'field_text' => 'A',
    ]);
    $containerChildUuid = $containerChild->uuid();

    $container = Paragraph::create([
      'type' => 'container',
      'field_container_paragraphs' => [
        $containerChild,
      ],
    ]);
    $containerUuid = $container->uuid();

    $footerText = Paragraph::create([
      'type' => 'text_paragraph',
      'field_text' => 'B',
    ]);
    $footerTextUuid = $footerText->uuid();

    $node = Node::create([
      'title' => 'Test Node',
      'type' => 'paragraphed_test',
      'field_paragraphs' => [$container],
      'field_footer_paragraphs' => [$footerText],
    ]);
    $node->save();

    $node = Node::load($node->id());

    /** @var \Drupal\paragraphs_blokkli\ParagraphsBlokkliHelper $helper */
    $helper = $this->container->get('paragraphs_blokkli.helper');

    $proxies = $helper->buildProxies($node);
    $this->assertCount(3, $proxies);

    $findProxy = function ($uuid, $proxies): ParagraphProxyInterface {
      foreach ($proxies as $proxy) {
        if ($proxy->uuid() === $uuid) {
          return $proxy;
        }
      }
      throw new \RuntimeException('Proxy not found');
    };

    $containerChildProxy = $findProxy($containerChildUuid, $proxies);
    $containerProxy = $findProxy($containerUuid, $proxies);
    $footerTextProxy = $findProxy($footerTextUuid, $proxies);

    $this->assertNotNull($containerChildProxy);
    $this->assertNotNull($containerProxy);
    $this->assertNotNull($footerTextProxy);

    $this->assertEquals('field_paragraphs', $containerProxy->getHostFieldName());
    $this->assertEquals('node', $containerProxy->getHostEntityType());
    $this->assertEquals($node->uuid(), $containerProxy->getHostUuid());

    $this->assertEquals('field_footer_paragraphs', $footerTextProxy->getHostFieldName());
    $this->assertEquals('node', $footerTextProxy->getHostEntityType());
    $this->assertEquals($node->uuid(), $footerTextProxy->getHostUuid());

    $this->assertEquals('field_container_paragraphs', $containerChildProxy->getHostFieldName());
    $this->assertEquals('paragraph', $containerChildProxy->getHostEntityType());
    $this->assertEquals($containerUuid, $containerChildProxy->getHostUuid());
  }

}
