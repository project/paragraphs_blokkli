<?php

namespace Drupal\Tests\paragraphs_blokkli\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\paragraphs\FunctionalJavascript\ParagraphsTestBaseTrait;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Test the paragraph mutations.
 *
 * @group paragraphs_blokkli
 */
class EditMutationsTest extends KernelTestBase {

  use ParagraphsTestBaseTrait;
  use ParagraphsBlokkliTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'paragraphs_blokkli',
    'paragraphs',
    'node',
    'user',
    'system',
    'language',
    'field',
    'entity_reference_revisions',
    'file',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('paragraph');
    $this->installEntitySchema('paragraphs_blokkli_edit_state');
    $this->installSchema('system', ['sequences']);
    $this->installSchema('node', ['node_access']);
    \Drupal::moduleHandler()->loadInclude('paragraphs', 'install');

    // Mock the config.
    $config = $this->config('paragraphs_blokkli.settings');
    $data['node'] = [
      'paragraphed_test',
    ];
    $config->set('enabled_types', $data);
    $config->save();

  }

  /**
   * Test the add mutation.
   */
  public function testAddMutation(): void {
    $this->addParagraphedContentType('paragraphed_test');
    $paragraph_type = 'text_paragraph';
    $this->addParagraphsType($paragraph_type);
    $this->addFieldtoParagraphType($paragraph_type, 'field_text', 'string');

    $node = Node::create([
      'title' => 'Test Node',
      'type' => 'paragraphed_test',
      'field_paragraphs' => [],
    ]);
    $node->save();

    $state = $this->getEditState($node);
    $helper = $this->getParagraphsBlokkliTestHelper($state);
    $helper->addTextParagraph();
    $helper->addTextParagraph();
    $helper->addTextParagraph();
    $helper->addTextParagraph();
    $mutatedState = $state->getMutatedState();

    /** @var \Drupal\paragraphs_blokkli\ParagraphsBlokkliMutatedField $fields */
    $fields = $mutatedState->getFields();

    $mutated_fields = reset($fields);
    if (!$mutated_fields) {
      $this->fail('No mutated fields found.');
    }
    $paragraphs = $mutated_fields->getParagraphs();

    $this->assertCount(1, $fields, 'Altered one field.');
    $this->assertCount(4, $paragraphs, 'Added four paragraphs.');
    $this->assertEquals(0, $node->get('field_paragraphs')->count(), 'Did not alter the actual fields.');
  }

  /**
   * Test the duplicate mutation.
   */
  public function testDuplicateMutation(): void {
    $this->addParagraphedContentType('paragraphed_test');
    $paragraph_type = 'text_paragraph';
    $this->addParagraphsType($paragraph_type);
    $this->addFieldtoParagraphType($paragraph_type, 'field_text', 'string');

    $paragraph = Paragraph::create([
      'type' => 'text_paragraph',
      'field_text' => 'Example text that is very long and needs to be shortened.',
    ]);
    $paragraph->setBehaviorSettings('foobar', ['my_setting' => '100']);
    $paragraphUuid = $paragraph->uuid();

    $node = Node::create([
      'title' => 'Test Node',
      'type' => 'paragraphed_test',
      'field_paragraphs' => [
        $paragraph,
      ],
    ]);
    $node->save();

    $state = $this->getEditState($node);
    $helper = $this->getParagraphsBlokkliTestHelper($state);
    $helper->duplicateParagraph($paragraphUuid);
    $mutatedState = $state->getMutatedState();

    /** @var \Drupal\paragraphs_blokkli\ParagraphsBlokkliMutatedField $fields */
    $fields = $mutatedState->getFields();
    $mutated_fields = reset($fields);
    if (!$mutated_fields) {
      $this->fail('No mutated fields found.');
    }
    $paragraphs = $mutated_fields->getParagraphs();

    $this->assertCount(2, $paragraphs, 'Duplicated one paragraph.');
    $this->assertEquals(
      $paragraphs[0]->get('field_text')->value,
      $paragraphs[1]->get('field_text')->value,
      'Duplicated the field values.'
    );
    $this->assertFalse($paragraphs[0]->isNew());
    $this->assertTrue($paragraphs[1]->isNew());
    $this->assertNotEquals($paragraphs[0]->uuid(), $paragraphs[1]->uuid(), 'Created a new UUID for the duplicate.');
    $this->assertEquals(
      $paragraphs[0]->getBehaviorSetting('foobar', 'my_setting'),
      $paragraphs[1]->getBehaviorSetting('foobar', 'my_setting'),
      'Has duplicated the behavior settings.'
    );
  }

  /**
   * Test the duplicate with nested mutation.
   */
  public function testDuplicateNestedMutation(): void {
    $this->addParagraphedContentType('paragraphed_test');
    $paragraph_type = 'text_paragraph';
    $nested_type = 'container';
    $this->addParagraphsType($paragraph_type);
    $this->addParagraphsType($nested_type);
    $this->addFieldtoParagraphType($paragraph_type, 'field_text', 'string');
    $this->addParagraphsField($nested_type, 'field_container_paragraphs', 'paragraph');

    $paragraphText1 = Paragraph::create([
      'type' => 'text_paragraph',
      'field_text' => 'Text 1',
    ]);

    $paragraphText2 = Paragraph::create([
      'type' => 'text_paragraph',
      'field_text' => 'Text 2',
    ]);

    $paragraphContainer = Paragraph::create([
      'type' => 'container',
      'field_container_paragraphs' => [
        $paragraphText1,
        $paragraphText2,
      ],
    ]);

    $node = Node::create([
      'title' => 'Test Node',
      'type' => 'paragraphed_test',
      'field_paragraphs' => [
        $paragraphContainer,
      ],
    ]);
    $node->save();

    $state = $this->getEditState($node);
    $helper = $this->getParagraphsBlokkliTestHelper($state);
    $helper->duplicateParagraph($paragraphContainer->uuid());
    $this->paragraphsBlokkliPublish($state, TRUE);

    $node = Node::load($node->id());
    /** @var Paragraph[] $paragraphs */
    $paragraphs = $node->get('field_paragraphs')->referencedEntities();

    $this->assertCount(2, $paragraphs);
    foreach ($paragraphs as $paragraph) {
      /** @var Paragraph[] $nested */
      $nested = $paragraph->get('field_container_paragraphs')->referencedEntities();
      $this->assertCount(2, $nested);
      $this->assertEquals('Text 1', $nested[0]->get('field_text')->value);
      $this->assertEquals('Text 2', $nested[1]->get('field_text')->value);
    }

    $this->assertNotEquals(
      $paragraphs[0]->get('field_container_paragraphs')->referencedEntities()[0]->uuid(),
      $paragraphs[1]->get('field_container_paragraphs')->referencedEntities()[0]->uuid(),
    );
    $this->assertNotEquals(
      $paragraphs[0]->get('field_container_paragraphs')->referencedEntities()[1]->uuid(),
      $paragraphs[1]->get('field_container_paragraphs')->referencedEntities()[1]->uuid(),
    );
  }

  /**
   * Test the duplicate with translations.
   */
  public function testDuplicateWithTranslationsMutation(): void {
    // Add German.
    ConfigurableLanguage::create(['id' => 'de'])->save();

    $this->addParagraphedContentType('paragraphed_test');
    $paragraph_type = 'text_paragraph';
    $this->addParagraphsType($paragraph_type);
    $this->addFieldtoParagraphType($paragraph_type, 'field_text', 'string', [
      'translatable' => TRUE,
    ]);

    $paragraph = Paragraph::create([
      'type' => 'text_paragraph',
      'field_text' => 'English text',
    ]);
    $paragraph->addTranslation('de', [
      'field_text' => 'German text',
    ]);
    $paragraphUuid = $paragraph->uuid();

    $node = Node::create([
      'title' => 'Test Node',
      'type' => 'paragraphed_test',
      'field_paragraphs' => [
        $paragraph,
      ],
    ]);
    $node->save();
    $translation = $node->addTranslation('de', [
      'title' => 'Test Node German',
    ]);
    $translation->save();

    $state = $this->getEditState($node);
    $helper = $this->getParagraphsBlokkliTestHelper($state);
    $helper->duplicateParagraph($paragraphUuid);
    $mutatedState = $state->getMutatedState();

    /** @var \Drupal\paragraphs_blokkli\ParagraphsBlokkliMutatedField $fields */
    $fields = $mutatedState->getFields();
    $mutated_fields = reset($fields);
    if (!$mutated_fields) {
      $this->fail('No mutated fields found.');
    }
    $paragraphs = $mutated_fields->getParagraphs();

    $original = $paragraphs[0];
    $duplicate = $paragraphs[1];
    $this->assertNotEquals($original->uuid(), $duplicate->uuid());
    $this->assertTrue($original->hasTranslation('de'));
    $this->assertTrue($duplicate->hasTranslation('de'));
    $this->assertEquals('English text', $original->get('field_text')->value);
    $this->assertEquals('English text', $duplicate->get('field_text')->value);
    $this->assertEquals('German text', $original->getTranslation('de')->get('field_text')->value);
    $this->assertEquals('German text', $duplicate->getTranslation('de')->get('field_text')->value);
  }

  /**
   * Test the edit mutation.
   */
  public function testEditMutation(): void {
    $this->addParagraphedContentType('paragraphed_test');
    $paragraph_type = 'text_paragraph';
    $this->addParagraphsType($paragraph_type);
    $this->addFieldtoParagraphType($paragraph_type, 'field_text', 'string');

    $paragraph = Paragraph::create([
      'type' => 'text_paragraph',
      'field_text' => 'Original Text',
    ]);

    $node = Node::create([
      'title' => 'Test Node',
      'type' => 'paragraphed_test',
      'field_paragraphs' => [$paragraph],
    ]);
    $node->save();

    $state = $this->getEditState($node);
    $helper = $this->getParagraphsBlokkliTestHelper($state);
    $helper->editParagraph($paragraph->uuid(), [
      'field_text' => 'Edited Text',
    ]);
    $this->paragraphsBlokkliPublish($state, TRUE);

    $node = Node::load($node->id());
    $updatedParagraph = $node->get('field_paragraphs')->referencedEntities()[0];
    $this->assertEquals('Edited Text', $updatedParagraph->get('field_text')->value);
  }

  /**
   * Test the move mutation.
   */
  public function testMoveMutation(): void {
    $this->addParagraphedContentType('paragraphed_test');
    $paragraph_type = 'text_paragraph';
    $this->addParagraphsType($paragraph_type);
    $this->addFieldtoParagraphType($paragraph_type, 'field_text', 'string');

    $paragraphs = [];
    for ($i = 0; $i < 5; $i++) {
      $paragraphs[] = Paragraph::create([
        'type' => 'text_paragraph',
        'field_text' => (string) ($i + 1),
      ]);
    }

    $getTexts = function ($entity) {
      return array_map(function (Paragraph $paragraph) {
        return $paragraph->get('field_text')->value;
      }, $entity->get('field_paragraphs')->referencedEntities());
    };

    $node = Node::create([
      'title' => 'Test Node',
      'type' => 'paragraphed_test',
      'field_paragraphs' => $paragraphs,
    ]);
    $node->save();
    $this->assertEquals(['1', '2', '3', '4', '5'], $getTexts($node));

    $firstUuid = $paragraphs[0]->uuid();
    $thirdUuid = $paragraphs[2]->uuid();
    $lastUuid = $paragraphs[count($paragraphs) - 1]->uuid();

    $state = $this->getEditState($node);
    $helper = $this->getParagraphsBlokkliTestHelper($state);
    // Move the first paragraph at the end of the list.
    $helper->moveParagraph($firstUuid, $lastUuid);
    // Move the third paragraph to the top of the list.
    $helper->moveParagraph($thirdUuid);
    $this->paragraphsBlokkliPublish($state, TRUE);

    $node = Node::load($node->id());
    $this->assertEquals(['3', '2', '4', '5', '1'], $getTexts($node));
  }

  /**
   * Test the move mutation.
   */
  public function testMoveMultiple(): void {
    $this->addParagraphedContentType('paragraphed_test');
    $paragraph_type = 'text_paragraph';
    $this->addParagraphsType($paragraph_type);
    $this->addFieldtoParagraphType($paragraph_type, 'field_text', 'string');

    $paragraphs = [];
    for ($i = 0; $i < 5; $i++) {
      $paragraphs[] = Paragraph::create([
        'type' => 'text_paragraph',
        'field_text' => (string) ($i + 1),
      ]);
    }

    $getTexts = function ($entity) {
      return array_map(function (Paragraph $paragraph) {
        return $paragraph->get('field_text')->value;
      }, $entity->get('field_paragraphs')->referencedEntities());
    };

    $node = Node::create([
      'title' => 'Test Node',
      'type' => 'paragraphed_test',
      'field_paragraphs' => $paragraphs,
    ]);
    $node->save();
    $this->assertEquals(['1', '2', '3', '4', '5'], $getTexts($node));

    $firstUuid = $paragraphs[0]->uuid();
    $secondUuid = $paragraphs[1]->uuid();
    $lastUuid = $paragraphs[count($paragraphs) - 1]->uuid();

    $state = $this->getEditState($node);
    $helper = $this->getParagraphsBlokkliTestHelper($state);
    // Move the first and second to the end of the list.
    $helper->moveMultiple([$firstUuid, $secondUuid], $lastUuid);
    $this->paragraphsBlokkliPublish($state, TRUE);

    $node = Node::load($node->id());
    $this->assertEquals(['3', '4', '5', '1', '2'], $getTexts($node));
  }

  /**
   * Test the remove mutation.
   */
  public function testRemoveMutation(): void {
    $this->addParagraphedContentType('paragraphed_test');
    $paragraph_type = 'text_paragraph';
    $this->addParagraphsType($paragraph_type);
    $this->addFieldtoParagraphType($paragraph_type, 'field_text', 'string');

    $paragraphs = [];
    for ($i = 0; $i < 5; $i++) {
      $paragraphs[] = Paragraph::create([
        'type' => 'text_paragraph',
        'field_text' => (string) ($i + 1),
      ]);
    }

    $getTexts = function ($entity) {
      return array_map(function (Paragraph $paragraph) {
        return $paragraph->get('field_text')->value;
      }, $entity->get('field_paragraphs')->referencedEntities());
    };

    $node = Node::create([
      'title' => 'Test Node',
      'type' => 'paragraphed_test',
      'field_paragraphs' => $paragraphs,
    ]);
    $node->save();
    $this->assertEquals(['1', '2', '3', '4', '5'], $getTexts($node));

    $secondUuid = $paragraphs[1]->uuid();
    $lastUuid = $paragraphs[count($paragraphs) - 1]->uuid();

    $state = $this->getEditState($node);
    $helper = $this->getParagraphsBlokkliTestHelper($state);
    $helper->remove($secondUuid);
    $helper->remove($lastUuid);
    $this->paragraphsBlokkliPublish($state, TRUE);

    $node = Node::load($node->id());
    $this->assertEquals(['1', '3', '4'], $getTexts($node));
  }

  /**
   * Test the remove multiple mutation.
   */
  public function testRemoveMultipleMutation(): void {
    $this->addParagraphedContentType('paragraphed_test');
    $paragraph_type = 'text_paragraph';
    $this->addParagraphsType($paragraph_type);
    $this->addFieldtoParagraphType($paragraph_type, 'field_text', 'string');

    $paragraphs = [];
    for ($i = 0; $i < 5; $i++) {
      $paragraphs[] = Paragraph::create([
        'type' => 'text_paragraph',
        'field_text' => (string) ($i + 1),
      ]);
    }

    $getTexts = function ($entity) {
      return array_map(function (Paragraph $paragraph) {
        return $paragraph->get('field_text')->value;
      }, $entity->get('field_paragraphs')->referencedEntities());
    };

    $node = Node::create([
      'title' => 'Test Node',
      'type' => 'paragraphed_test',
      'field_paragraphs' => $paragraphs,
    ]);
    $node->save();
    $this->assertEquals(['1', '2', '3', '4', '5'], $getTexts($node));

    $firstUuid = $paragraphs[0]->uuid();
    $lastUuid = $paragraphs[count($paragraphs) - 1]->uuid();

    $state = $this->getEditState($node);
    $helper = $this->getParagraphsBlokkliTestHelper($state);
    $helper->removeMultiple([$firstUuid, $lastUuid]);
    $this->paragraphsBlokkliPublish($state, TRUE);

    $node = Node::load($node->id());
    $this->assertEquals(['2', '3', '4'], $getTexts($node));
  }

  /**
   * Test the update_behavior_setting mutation.
   */
  public function testUpdateBehaviorSettingMutation(): void {
    $this->addParagraphedContentType('paragraphed_test');
    $paragraph_type = 'text_paragraph';
    $this->addParagraphsType($paragraph_type);
    $this->addFieldtoParagraphType($paragraph_type, 'field_text', 'string');

    $paragraph = Paragraph::create([
      'type' => 'text_paragraph',
      'field_text' => 'Foobar 1',
    ]);
    $paragraph->setBehaviorSettings('my_plugin', ['my_key' => 'original']);
    $paragraph->setBehaviorSettings('my_plugin', ['my_other_key' => 'original']);

    $secondParagraph = Paragraph::create([
      'type' => 'text_paragraph',
      'field_text' => 'Foobar 2',
    ]);
    $secondParagraph->setBehaviorSettings('my_plugin', ['my_key' => 'original']);

    $node = Node::create([
      'title' => 'Test Node',
      'type' => 'paragraphed_test',
      'field_paragraphs' => [$paragraph, $secondParagraph],
    ]);
    $node->save();

    $state = $this->getEditState($node);
    $helper = $this->getParagraphsBlokkliTestHelper($state);
    $helper->bulkUpdateBehaviorSettings([
      [
        'uuid' => $paragraph->uuid(),
        'pluginId' => 'my_plugin',
        'key' => 'my_key',
        'value' => '1',
      ],
      [
        'uuid' => $secondParagraph->uuid(),
        'pluginId' => 'my_plugin',
        'key' => 'my_key',
        'value' => '2',
      ],
    ]);
    $this->paragraphsBlokkliPublish($state, TRUE);

    $node = Node::load($node->id());
    /** @var Paragraph[] $paragraphs */
    $paragraphs = $node->get('field_paragraphs')->referencedEntities();
    $this->assertEquals([
      'my_plugin' => [
        'my_key' => '1',
        'my_other_key' => 'original',
      ],
    ], $paragraphs[0]->getAllBehaviorSettings());

    $this->assertEquals([
      'my_plugin' => [
        'my_key' => '2',
      ],
    ], $paragraphs[1]->getAllBehaviorSettings());
  }

  /**
   * Test the update_behavior_setting mutation.
   */
  public function testAllMutationsTogether(): void {
    $this->addParagraphedContentType('paragraphed_test');
    $paragraph_type = 'text_paragraph';
    $this->addParagraphsType($paragraph_type);
    $this->addFieldtoParagraphType($paragraph_type, 'field_text', 'string');

    $node = Node::create([
      'title' => 'Test Node',
      'type' => 'paragraphed_test',
      'field_paragraphs' => [],
    ]);
    $node->save();

    $state = $this->getEditState($node);
    $helper = $this->getParagraphsBlokkliTestHelper($state);
    $uuid7 = $helper->addTextParagraph('7');
    $helper->addTextParagraph('8');
    $helper->addTextParagraph('9');
    $uuid10 = $helper->addTextParagraph('10');
    $uuid11 = $helper->addTextParagraph('11');
    $helper->moveMultiple([$uuid10, $uuid11], $uuid7);
    $helper->updateBehaviorSetting($uuid11, 'my_plugin', 'my_key', 'foobar');
    $helper->addTextParagraph('12', $uuid10);
    $uuid13 = $helper->addTextParagraph('13');
    $uuid14 = $helper->addTextParagraph('14');
    $helper->removeMultiple([$uuid13, $uuid14]);
    $cloneUuid = $helper->duplicateParagraph($uuid7);
    $helper->editParagraph($cloneUuid, ['field_text' => '15']);
    $helper->bulkUpdateBehaviorSettings([
      [
        'uuid' => $cloneUuid,
        'pluginId' => 'my_plugin',
        'key' => 'my_key',
        'value' => 'A',
      ],
      [
        'uuid' => $uuid7,
        'pluginId' => 'my_plugin',
        'key' => 'my_key',
        'value' => 'B',
      ],
    ]);

    $this->paragraphsBlokkliPublish($state, TRUE);

    $node = Node::load($node->id());
    /** @var Paragraph[] $paragraphs */
    $paragraphs = $node->get('field_paragraphs')->referencedEntities();
    $actual = array_map(function (Paragraph $paragraph) {
      $text = $paragraph->get('field_text')->value;
      $behaviorSetting = $paragraph->getBehaviorSetting('my_plugin', 'my_key');
      return [
        'text' => $text,
        'behavior_setting' => $behaviorSetting,
      ];
    }, $paragraphs);

    $expected = [
      [
        'text' => '9',
        'behavior_setting' => NULL,
      ],
      [
        'text' => '8',
        'behavior_setting' => NULL,
      ],
      [
        'text' => '7',
        'behavior_setting' => 'B',
      ],
      [
        'text' => '15',
        'behavior_setting' => 'A',
      ],
      [
        'text' => '10',
        'behavior_setting' => NULL,
      ],
      [
        'text' => '12',
        'behavior_setting' => NULL,
      ],
      [
        'text' => '11',
        'behavior_setting' => 'foobar',
      ],
    ];
    $this->assertEquals($expected, $actual);
  }

}
