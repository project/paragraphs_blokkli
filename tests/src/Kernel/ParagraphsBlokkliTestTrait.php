<?php

namespace Drupal\Tests\paragraphs_blokkli\Kernel;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\paragraphs_blokkli\EditStateInterface;

/**
 * Test the custom user tokens.
 */
trait ParagraphsBlokkliTestTrait {

  /**
   * Get edit state.
   *
   * @param FieldableEntityInterface $entity
   */
  protected function getEditState(FieldableEntityInterface $entity): EditStateInterface|null {
    /** @var \Drupal\paragraphs_blokkli\ParagraphsBlokkliManager $manager */
    $manager = \Drupal::service('paragraphs_blokkli.manager');
    return $manager->getParagraphsEditState($entity);
  }

  /**
   * Persist the changes.
   *
   * @param EditStateInterface $editState
   * @param bool $force_reload
   */
  protected function paragraphsBlokkliPublish(EditStateInterface $editState, $force_reload = FALSE) {
    /** @var \Drupal\paragraphs_blokkli\ParagraphsBlokkliManager $manager */
    $manager = \Drupal::service('paragraphs_blokkli.manager');
    return $manager->publish($editState, $force_reload);
  }

  /**
   * Get test helper.
   *
   * @param EditStateInterface $editState
   *
   * @return ParagraphsBlokkliTestHelper
   *   The test helper.
   */
  protected function getParagraphsBlokkliTestHelper(EditStateInterface $editState): ParagraphsBlokkliTestHelper {
    return new ParagraphsBlokkliTestHelper($editState);
  }

}
